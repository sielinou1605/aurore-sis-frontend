import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './layout/admin/admin.component';
import {AuthComponent} from './layout/auth/auth.component';
import {HelpComponent} from './pages/user/help/help.component';
import {UserGuardService} from './shared/_helpers/user-guard.service';
import {PaiementAbonnementComponent} from './pages/user/paiement-abonnement/paiement-abonnement.component';

// @ts-ignore
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'dashboard', canActivate: [UserGuardService],
        loadChildren: () =>
          import('./pages/dashboard/dashboard-default/dashboard-default.module')
            .then(m => m.DashboardDefaultModule)
      },
      {
        path: 'welcome', canActivate: [UserGuardService],
        loadChildren: () => import('./pages/user/welcome/welcome.module').then(m => m.WelcomeModule)
      },
      {
        path: 'administration', canActivate: [UserGuardService],
        data: {action: 'bouton-administration'},
        loadChildren: () =>
          import('./pages/user/administration/administration.module')
            .then(m => m.AdministrationModule)
      },
      {
        path: 'finances', canActivate: [UserGuardService],
        data: {action: 'bouton-finance'},
        loadChildren: () =>
          import('./pages/user/finances/finances.module').then(m => m.FinancesModule)
      },
      {
        path: 'eleves', canActivate: [UserGuardService],
        data: {action: 'bouton-eleve'},
        loadChildren: () =>
          import('./pages/user/eleves/eleves.module').then(m => m.ElevesModule)
      },
      {
        path: 'gestion-notes', canActivate: [UserGuardService],
        data: {action: 'bouton-gestion-note'},
        loadChildren: () =>
          import('./pages/user/espace-travail/espace-travail.module')
            .then(m => m.EspaceTravailModule)
      },
      // {
      //   path: 'notes', canActivate: [UserGuardService],
      //   data: {action: 'notes'},
      //   loadChildren: () =>
      //     import('./pages/user/espace-travail/notes/notes.module')
      //       .then(m => m.NotesModule)
      // },
      {
        path: 'discipline', canActivate: [UserGuardService],
        data: {action: 'bouton-discipline'},
        loadChildren: () =>
          import('./pages/user/censorat/censorat.module').then(m => m.CensoratModule)
      },
      {
        path: 'etats', canActivate: [UserGuardService],
        data: {action: 'bouton-etat'},
        loadChildren: () =>
          import('./pages/user/etats/etats.module').then(m => m.EtatsModule)
      },
      {
        path: 'messages', canActivate: [UserGuardService],
        data: {action: 'bouton-message'},
        loadChildren: () =>
          import('./pages/user/messages/messages.module').then(m => m.MessagesModule)
      },
      {
        path: 'user', canActivate: [UserGuardService],
        loadChildren: () =>
          import('./pages/user/profile/profile.module')
            .then(m => m.ProfileModule)
      },
      {
        path: 'help',
        component: HelpComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Aide',
          titre: 'userEleveAjaout.Aide_Aurore',
          icon: 'icofont-question-circle bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Lorem_Ipsum_Dolor_Sit_Amet_Consectetur_Adipisicing_Elit_Censorat',
          status: true,
          current_role: 'welcome'
        }
      }
    ]
  },
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'authentication',
        loadChildren: () =>
          import('./pages/auth/auth.module').then(m => m.AuthModule)
      }
    ]
  },
  {
    path: 'souscriptions',
    component: PaiementAbonnementComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Aide',
      titre: 'userEleveAjaout.Aide_Aurore',
      icon: 'icofont-question-circle bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Prolongement_de_la_licence_d_utilisation_Aurore_Licence',
      status: true,
      current_role: 'welcome'
    }
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
