import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {TokenStorageService} from './shared/_services/token-storage.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = localStorage.getItem('activelang') === 'en' ? 'Welcome ! Aurore App' : 'Bienvenue ! Aurore App';

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private router: Router,
    private translate: TranslateService,
    private tokenStorage: TokenStorageService
  ) {
    this.siteLanguage = tokenStorage.getActiveLang();
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

  }

  ngOnInit() {
    // tslint:disable-next-line:comment-format
    // this.langue.changeLang(this.lang);
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
