import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-title',
  template: '<span></span>'
})
export class TitleComponent implements OnInit {

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private titleService: Title,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(() => {
        let currentRoute = this.route.root;
        let title = '';
        do {
          const childrenRoutes = currentRoute.children;
          currentRoute = null;
          childrenRoutes.forEach(routes => {
            if (routes.outlet === 'primary') {
              title = routes.snapshot.data.titre;
              currentRoute = routes;
            }
          });
        } while (currentRoute);
        this.titleService.setTitle( title );
      });
  }

  ngOnInit() {
    // this.translate.setDefaultLang(this.translate.getBrowserLang());
  }
}
