import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private translate: TranslateService
  ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
  }
}
