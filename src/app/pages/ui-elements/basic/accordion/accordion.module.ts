import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AccordionRoutingModule} from './accordion-routing.module';
import {AccordionComponent} from './accordion.component';
import {SharedModule} from '../../../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        AccordionRoutingModule,
        SharedModule,
        TranslateModule,
    ],
  declarations: [AccordionComponent]
})
export class AccordionModule { }
