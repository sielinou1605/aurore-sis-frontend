/* tslint:disable:triple-equals */
import {Component, OnInit} from '@angular/core';
import {LicenceModel} from '../../../shared/_models/response-dto/common/licence-model';
import {GenericsService} from '../../../shared/_services/generics.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-paiement-abonnement',
  templateUrl: './paiement-abonnement.component.html',
  styleUrls: ['./paiement-abonnement.component.scss']
})
export class PaiementAbonnementComponent implements OnInit {
  public licenses: LicenceModel[];
  totalElements;
  page = 0;
  pageSize = 20;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(private genericsService: GenericsService,
              private translate: TranslateService
              ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
    this.onloadListLicences();
  }

  setPageChange(event: number) {
    this.page = event - 1;
    this.onloadListLicences();
  }

  private onloadListLicences() {
    this.genericsService.getResource(`serial/cas/liste?page=${this.page}&size=${this.pageSize}`)
      .then((response: any) => {
        if (response.success == true) {
          this.licenses = response.data.content;
          console.log(this.licenses);
        } else {
          console.log(response);
        }
      }).catch(err => {
      console.log(err);
    });
  }

}
