/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {
  CommunicationModelResponse,
  MessageModelRequest,
  ModelCampagneResponse, ModelEnvoieMessageRequest
} from '../../../../shared/_models/request-dto/common/management-sms-model';
import {ParamEtat} from '../../../../shared/_models/request-dto/common/etats-model';
import {TranslateService} from '@ngx-translate/core';
import {AppConstants} from '../../../../shared/element/app.constants';
import {Router} from '@angular/router';
import {StorageService} from '../../../../shared/_services/storage.service';
import {AuthService} from '../../../../shared/_services/auth.service';

@Component({
  selector: 'app-m-welcome',
  templateUrl: './m-welcome.component.html',
  styleUrls: ['./m-welcome.component.scss']
})
export class MWelcomeComponent implements OnInit, OnDestroy {
  communications: CommunicationModelResponse[];
  communicationForm: FormGroup;
  submitted: boolean;
  modelesCompagne: ModelCampagneResponse[];
  public currentCommunication: CommunicationModelResponse;
  public pageNumber = 0;
  public pageSize = 20;
  public totalElements: number;
  public filtreCampagne: any;
  private dateDebut: string;
  private dateFin: string;
  filtreComForm: FormGroup;
  searchForm: FormGroup;

  public settings: any;
  public availableFiltre: any;
  validated: boolean;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  public listCommunications: any;
  constructor(
    private fb: FormBuilder,
    private genericsService: GenericsService,
    private storageService: StorageService,
    private modalService: NgbModal,
    config: NgbModalConfig,
    private router: Router,
    public authService: AuthService,
    private translate: TranslateService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    console.log(' On init m-Welcome ----------');
    const date = new Date(Date.now());
    this.dateDebut = '2000-01-01T00:00';
    this.dateFin = date.toISOString().substr(0, 16);
    this.onReloadListMessage('');
    this.onloadCommunications(this.dateDebut, this.dateFin);
    this.onloadModeleCampagne();
    this.initFormSubmitMessage();
    this.initFormFiltreCom();
    localStorage.getItem('activelang') === 'en' ?
      this.settings = {
        singleSelection: true,
        idField: 'value',
        textField: 'text',
        enableCheckAll: true,
        selectAllText: 'Check all',
        unSelectAllText: 'Uncheck all',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 5,
        searchPlaceholderText: 'Search',
        noDataAvailablePlaceholderText: 'No data available',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      }
      : this.settings = {
        singleSelection: true,
        idField: 'valeur',
        textField: 'texte',
        enableCheckAll: true,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 5,
        searchPlaceholderText: 'Rechercher',
        noDataAvailablePlaceholderText: 'Aucune donnée disponible',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };
  }
  private initFormSubmitMessage() {
    this.communicationForm = this.fb.group({
      modele: [''],
      titre: ['', Validators.required],
      destinataire: [''],
      message: ['', Validators.required],
      params: [[]]
    });
  }
  onChangeOptions() {
    if (this.f.destinataire.value.length >= 9) {
      this.communicationForm.controls['modele'].clearValidators();
      this.communicationForm.controls['titre'].setValidators([Validators.required, Validators.min(1)]);
    } else {
      this.communicationForm.controls['titre'].clearValidators();
      this.communicationForm.controls['modele'].setValidators([Validators.required, Validators.min(1)]);
    }
    this.communicationForm.get('titre').updateValueAndValidity();
    this.communicationForm.get('modele').updateValueAndValidity();
  }
  get search() { return this.searchForm.controls; }
  public onReloadListMessage(event, reInitPage?: boolean) {}
  get f() { return this.communicationForm.controls; }
  onEditCommunication(communication, content: TemplateRef<any>) {
    this.currentCommunication = communication;
    this.modalService.open(content,
      {ariaLabelledBy: 'send-message', size: 'lg'}).result.then((result) => {
    }, (reason) => {
    });
  }
  onResetCommunication() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormSubmitMessage();
    this.initFormFiltreCom();
    this.tp.clear();
  }
  onSubmitCommunication() {
    this.submitted = true;
    this.validated = true;
    if (this.communicationForm.invalid || this.filtreComForm.invalid) {
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Error in the form', 'error', 7000)
        : this.genericsService.confirmResponseAPI('Erreur dans le formulaire', 'error', 7000);
      this.validated = false;
      return;
    }
    const params = [];
    this.tp.controls.forEach(item => {
      if (params.find( param => param.nom == item.value.nomParamEtat) == undefined ) {
        params.push(new ParamEtat(
          item.value.nomParamEtat,
          (item.value.type == 0 || item.value.type == 1 || item.value.type == 4)
            ?  (item.value.valeur.length > 0 ? item.value.valeur : 0)
            : (item.value.valeur.length > 0 ? item.value.valeur[0].valeur : 0)
        ));
      }
    });
    const dto = new MessageModelRequest(this.f.modele.value,
      this.f.destinataire.value.length > 0 ? this.f.destinataire.value : '',
      this.f.message.value, this.f.titre.value, params);
    console.log('dto : ', dto);



    // this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    if (dto.idSysMessageDataSource != 1) {
      this.genericsService.postResource(`systeme/communication/generer`, dto).then((response: any) => {
        this.validated = false;
        if (response.data.length == 0) {
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmResponseAPI('Aucun numero trouvé', 'success', 7000)
            : this.genericsService.confirmResponseAPI('Aucun numero trouvé', 'success', 7000);
          this.validated = false;
        } else {
          this.genericsService.confirmResponseAPI(response.message, 'success');
          if (this.modalService.hasOpenModals()) {
            this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
          }
          sessionStorage.setItem('dtoCom', JSON.stringify(response));
          this.router.navigate(['/messages/detail-communication']).then(() => {
          });
        }
        // this.onloadCommunications(this.dateDebut, this.dateFin);
      }).catch(reason => {
        this.validated = false;
        console.log(reason);
        if (reason.status == 400) {
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmModalResponseAPI(reason.error.message, AppConstants.FAIL_OPERATIONEN,  'error')
            : this.genericsService.confirmModalResponseAPI(reason.error.message, AppConstants.FAIL_OPERATIONFR,  'error');
        } else {
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmResponseAPI('Error sending message !!!', 'error', 6000)
            :  this.genericsService.confirmResponseAPI('Erreur d\'envoie de message !!!', 'error', 6000);
        }
      });
    } else {
      const dto = new ModelEnvoieMessageRequest([], 0,
        this.f.destinataire.value.length > 0 ? this.f.destinataire.value : '', this.f.message.value);
      this.genericsService.postResource(`systeme/communication/envoyer`, dto)
        .then((response: any) => {
          console.log(response.data);
          this.validated = false;
          this.genericsService.confirmResponseAPI(response.message, 'success');
          if (this.modalService.hasOpenModals()) {
            this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
          }
        }).catch(reason => {
        console.log(reason);
        this.validated = false;
        if (reason.status == 400) {
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmModalResponseAPI(reason.error.message, AppConstants.FAIL_OPERATIONEN,  'error')
            : this.genericsService.confirmModalResponseAPI(reason.error.message, AppConstants.FAIL_OPERATIONFR,  'error');
        } else {
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmResponseAPI('Error sending message !!!', 'error', 6000)
            :  this.genericsService.confirmResponseAPI('Erreur d\'envoie de message !!!', 'error', 6000);
        }
      });
    }

  }
  private onloadCommunications(dateDebut, dateFin) {
    const params = `size=${this.pageSize}&page=${this.pageNumber}&dateDebut=${dateDebut}&dateFin=${dateFin}`;
    this.genericsService.stopLoadingPage();
    this.genericsService
      .getResource(`systeme/communication/liste?${params}`)
      .then((response: any) => {
        if (response.success == true) {
          this.communications = response.data.content;
          this.listCommunications = response.data.content;
          this.totalElements = response.data.totalElements;
          console.log(this.communications);
        }
        this.genericsService.stopLoadingPage();
      }).catch(reason => {
      console.log('messages', reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Error loading messages', 'error', 7000)
        : this.genericsService.confirmResponseAPI('Erreur de chargements des messages', 'error', 7000);
    });
  }
  setPageChange(event) {
    this.pageNumber = event - 1;
    this.onloadCommunications(this.dateDebut, this.dateFin);
  }
  private onloadModeleCampagne() {
    this.genericsService.getResource(`systeme/communication/source/liste`).then((response: any) => {
      if (response.success == true) {
        this.modelesCompagne = response.data;
      }
    }).catch(reason => {
      console.log('modele messages', reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Error in loading message templates', 'error', 7000)
        : this.genericsService.confirmResponseAPI('Erreur de chargements modeles de messages', 'error', 7000);
    });
  }

  private onloadFiltreFormSourceCommunication(idSource) {
    this.genericsService.getResource(`systeme/communication/source/${idSource}/traiter/1/filtre/liste`).then((response: any) => {
      if (response.success == true) {
        this.filtreCampagne = response.data;
        console.log(this.filtreCampagne);
        this.communicationForm.patchValue({
          message: this.filtreCampagne.messageDataSource.modeleMessage
        });
        this.tp.clear();
        this.availableFiltre = [];
        this.filtreCampagne.filtres.forEach(elt => {
          this.availableFiltre[elt.filtre.id] = elt.sourceDonnees;
          this.tp.push(
            this.fb.group({
              libelle: [elt.filtre.libelle],
              nomParamEtat: [elt.filtre.nomParametreEtat],
              nom: [elt.filtre.id],
              valeur: [[]],
              type: [elt.filtre.typeSelection]
            })
          );
        });
      }
    }).catch(reason => {
      console.log('modele messages', reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Error in loading message templates', 'error', 7000)
        : this.genericsService.confirmResponseAPI('Erreur de chargements modeles de messages', 'error', 7000);
    });
  }
  ngOnDestroy(): void {
    this.onResetCommunication();
  }
  onResetDetails() {
    this.currentCommunication = null;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onDisplayCommunication(communication: any, detailCom: TemplateRef<any>) {
    this.currentCommunication = communication;
    console.log(communication);
    // const params = `size=40`;
    /*this.modalService.open(detailCom,
      {ariaLabelledBy: 'detail-communication', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });*/
    this.genericsService.getResource(`systeme/sms-log/communications/${communication.id}/liste`)
      .then((response: any) => {
        if (response.success == true) {
          console.log(response.data);
          if (response.data.messageSms.totalElements == 0) {
            localStorage.getItem('activelang') === 'en'
              ? this.genericsService.confirmResponseAPI('Aucun numero n\as ete trouvé dans cette communication', 'success', 7000)
              : this.genericsService.confirmResponseAPI('Aucun numero n\as ete trouvé dans cette communication', 'success', 7000);
          } else {
            sessionStorage.setItem('dtoCom', JSON.stringify(response));
            this.router.navigate(['/messages/detail-communication/' + communication.id]).then(() => {});
          }
        }
      }).catch(reason => {
      console.log('modele messages', reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Error in loading message templates', 'error', 7000)
        : this.genericsService.confirmResponseAPI('Erreur de chargements modeles de messages', 'error', 7000);
    });
  }
  onUpdateMessage() {
    if (this.f.modele.value.length > 0) {
      this.onloadFiltreFormSourceCommunication(this.f.modele.value);
    } else  {
      this.tp.clear();
      this.initFormSubmitMessage();
    }
  }

  get fp() { return this.filtreComForm.controls; }
  get tp() { return this.fp.filtreData as FormArray; }
  private initFormFiltreCom() {
    this.filtreComForm = this.fb.group({
      filtreData: this.fb.array([])
    });
  }
}
