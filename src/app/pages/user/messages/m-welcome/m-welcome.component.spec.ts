import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MWelcomeComponent} from './m-welcome.component';

describe('MWelcomeComponent', () => {
  let component: MWelcomeComponent;
  let fixture: ComponentFixture<MWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
