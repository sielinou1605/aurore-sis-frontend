/* tslint:disable:triple-equals */
import {Component, OnInit} from '@angular/core';
import {StorageService} from '../../../../shared/_services/storage.service';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {AppConstants} from '../../../../shared/element/app.constants';
import { ModelEnvoieMessageRequest } from '../../../../shared/_models/request-dto/common/management-sms-model';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../shared/_services/auth.service';

@Component({
  selector: 'app-detail-communication',
  templateUrl: './detail-communication.component.html',
  styleUrls: ['./detail-communication.component.scss']
})

export class DetailCommunicationComponent implements OnInit {
  public messagesList = [];
  public auteur: String;
  public titreModel: String;
  public totalMessage: number;
  public totalMessageNumber: number;
  public listMessage = [];
  public allSelectMessage: boolean;
  private decission = 2;
  public pageNumber = 0;
  public pageSize = 20;
  public predicat = '';
  public totalElements: number;
  private idCom: any;
  sizeForm: FormGroup;
  searchForm: FormGroup;

  constructor(
    private storageService: StorageService,
    private genericsService: GenericsService,
    private route: ActivatedRoute,
    public authService: AuthService,
    private fb: FormBuilder) {
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: [''],
    });
    this.idCom = this.route.snapshot.params.idCom;
    const response = JSON.parse(window.sessionStorage.getItem('dtoCom'));
    console.log('this.idCom : ', this.idCom);
    console.log(JSON.parse(window.sessionStorage.getItem('dtoCom')));
    if (this.idCom != undefined) {
      console.log('Detail communication');
      this.auteur = response.data.auteur;
      this.titreModel = response.data.titre;
      this.totalMessage = response.data.messageSms.totalElements;
      this.totalMessageNumber = response.data.messageSms.totalElements;
      this.messagesList = response.data.messageSms.content;
      this.totalElements = response.data.messageSms.totalElements;
      console.log('totalMessage', this.totalMessage);
      console.log(this.messagesList);
    } else {
      this.idCom = response.data[0].communication.id;
      this.auteur = response.data[0].communication.auteur;
      this.titreModel = response.data[0].communication.titre;
      this.totalMessage = response.data.length;
      this.messagesList = response.data;
      this.totalElements = response.data.length;
    }
    this.initSizeForm();
  }

  onSendMessage() {
    console.log('' +
      '', this.listMessage, 'valeur select:', this.decission);

    if (this.listMessage.length > 0) {
      const dto = new ModelEnvoieMessageRequest(this.listMessage);
      this.genericsService.postResource(`systeme/communication/envoyer?decission=` + this.decission, dto)
        .then((response: any) => {
          console.log(response.data);
          this.genericsService.confirmResponseAPI(response.message, 'success');

        }).catch(reason => {
        console.log(reason);
        if (reason.status == 400) {
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmModalResponseAPI(reason.error.message, AppConstants.FAIL_OPERATIONEN,  'error')
            : this.genericsService.confirmModalResponseAPI(reason.error.message, AppConstants.FAIL_OPERATIONFR,  'error');
        } else {
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmResponseAPI('Error sending message !!!', 'error', 6000)
            :  this.genericsService.confirmResponseAPI('Erreur d\'envoie de message !!!', 'error', 6000);
        }
      });
    }

  }

  // filterMessage() {
  //   this.genericsService.getResource('').then({
  //
  //   });
  //   this.messagesList.forEach((message, i) => {
  //     this.listMessage.push(message.id);
  //   });
  // }

  onAllMessageCheckboxChange(event: any) {
    if (event.target.checked) {
      this.listMessage = [];
      this.allSelectMessage = true;
      this.messagesList.forEach((message, i) => {
        this.listMessage.push(message.id);
      });
    } else {
      this.listMessage = [];
      this.allSelectMessage = false;
    }
  }

  onMessageCheckboxChange(event: any) {
    const i = this.listMessage.findIndex((mat) => mat == event.target.value);
    if (event.target.checked) {
      if (i == -1) {
        this.listMessage.push(event.target.value);
      }
    } else {
      if (i > -1) {
        this.listMessage.splice(i, 1);
      }
    }
  }

  formatNbrePage(number: any) {
    return Math.ceil(number.length / 160);
  }

  onChangeOptions(event: any) {
    this.decission = event.target.id;
  }

  setPageChange(event) {
    this.pageNumber = event - 1;
    this.onloadMessages(this.predicat, this.pageSize, this.pageNumber);
  }
  get searchElt() {
    return this.searchForm.controls;
  }
  onloadMessages(predicat: string, size: number, page: number) {
    predicat = this.searchElt.search.value;
    this.genericsService.startLoadingPage();
    console.log('predicat : ', predicat, 'this.pageSize : ', size, 'this.pageNumber : ', page);
    const params = `predicat=${predicat}&size=${size}&page=${page}`;

    this.genericsService
      .getResource(`systeme/sms-log/communications/${this.idCom}/liste?${params}`)
      .then((response: any) => {
        if (response.success == true) {
          this.messagesList = response.data.messageSms.content;
          this.totalElements = response.data.messageSms.totalElements;
         // this.totalMessage = response.data.messageSms.totalElements;
         this.totalMessageNumber = response.data.messageSms.totalElements;
          this.allSelectMessage = false;
        }
        this.genericsService.stopLoadingPage();
      }).catch(reason => {
      console.log('messages', reason);
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Error loading messages', 'error', 7000)
        : this.genericsService.confirmResponseAPI('Erreur de chargements des messages', 'error', 7000);
    });
  }



  /*onDisplayCommunication(communication: any) {
    console.log(communication);
    /!*this.modalService.open(detailCom,
      {ariaLabelledBy: 'detail-communication', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });*!/
    ///sms-log/communications/{idCom}/liste
    this.genericsService.getResource(`systeme/sms-log/communications/${communication.id}/liste`).then((response: any) => {

    }).catch(reason => {
      console.log('modele messages', reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Error in loading message templates', 'error', 7000)
        :this.genericsService.confirmResponseAPI('Erreur de chargements modeles de messages', 'error', 7000);
    });
  }
  */


  private initSizeForm() {
    this.sizeForm = this.fb.group({
      'sizeSms': ['', Validators.required]
    });
  }

  onRefreshList() {
    if (this.sizeForm.invalid) {
      return;
    }
    this.pageNumber = 0;
    let size = this.sizeForm.value.sizeSms;
    if (size > this.totalMessage) {
      size = this.totalMessage;
    }
    this.pageSize = size;
    this.onloadMessages(this.predicat, this.pageSize, this.pageNumber);
  }
}
