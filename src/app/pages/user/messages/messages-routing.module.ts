import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MWelcomeComponent} from './m-welcome/m-welcome.component';
import {MessagesComponent} from "./messages.component";
import {DetailCommunicationComponent} from "./detail-communication/detail-communication.component";


const routes: Routes = [
  {
    path: '',
    component: MessagesComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Communication',
      titre: localStorage.getItem('activelang') === 'en' ? 'Communication | Aurore' : 'Communication | Aurore',
      icon: 'icofont-ui-messaging bg-googleplus',
      breadcrumb_caption: 'userEleveAjaout.Messages_SMS_Communication',
      status: true,
      current_role: 'messages',
      action: 'bouton-message'
    },
    children: [
      {
        path: '',
        redirectTo: 'list-communication',
        pathMatch: 'full'
      },
      {
        path: 'detail-communication',
        component: DetailCommunicationComponent,
        data: {
          breadcrumb: 'Previsualisation messages',
          titre: localStorage.getItem('activelang') === 'en' ? 'Communication | Aurore' : 'Communication | Aurore',
          icon: 'icofont-ui-messaging bg-googleplus',
          breadcrumb_caption: 'Configuration des messages a envoyer',
          status: true,
          current_role: 'messages',
          action: 'bouton-message'
        }
      },
      {
        path: 'detail-communication/:idCom',
        component: DetailCommunicationComponent,
        data: {
          breadcrumb: 'Previsualisation messages',
          titre: localStorage.getItem('activelang') === 'en' ? 'Communication | Aurore' : 'Communication | Aurore',
          icon: 'icofont-ui-messaging bg-googleplus',
          breadcrumb_caption: 'Configuration des messages a envoyer',
          status: true,
          current_role: 'messages',
          action: 'bouton-message'
        }
      },
      {
        path: 'list-communication',
        component: MWelcomeComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Communication',
          titre: localStorage.getItem('activelang') === 'en' ? 'Communication | Aurore' : 'Communication | Aurore',
          icon: 'icofont-ui-messaging bg-googleplus',
          breadcrumb_caption: 'userEleveAjaout.Messages_SMS_Communication',
          status: true,
          current_role: 'messages',
          action: 'bouton-message'
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule { }
