/* tslint:disable:triple-equals */
import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-proces-verbaux',
  templateUrl: './proces-verbaux.component.html',
  styleUrls: ['./proces-verbaux.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ProcesVerbauxComponent implements OnInit, OnDestroy {
  @ViewChild('modalSelectSequence', { static: true }) modalSequence: ElementRef;
  eleves: any;
  selectSequenceClasseForm: FormGroup;
  submitted: boolean;
  public formSelect: any;
  public trimestres: any;
  public currentTrimestre: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor( private genericsService: GenericsService,  private fb: FormBuilder,
               private modalService: NgbModal, config: NgbModalConfig,
               private router: Router, public tokenStorage: TokenStorageService,
               public authService: AuthService,
               private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(81);
  }

  ngOnInit() {
    this.onloadDataFilter();
    this.initFormSelectSequence();
    this.modalService.open(this.modalSequence,
      {ariaLabelledBy: 'select-sequence', scrollable: true}).result.then((result) => {
    }, (reason) => {});
  }

  private initFormSelectSequence() {
    this.selectSequenceClasseForm = this.fb.group({
      trimestre: ['', Validators.required],
    });
  }
  get f() { return this.selectSequenceClasseForm.controls; }

  onDisplayListeEleve() {
    this.submitted = true;
    if (this.selectSequenceClasseForm.invalid) {
      return;
    }
    this.currentTrimestre = this.trimestres.find(item => item.id_trimestre == this.f.trimestre.value);
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  private onloadDataFilter() {
    this.genericsService.getResource(`admin/bulletin/filtre/trimestre`).then((result: any) => {
      console.log(result);
      if (result.success === true) {
        this.trimestres = result.data.trimestres;
      }
    }).catch((reason: any) => {
      console.log(reason);
      this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des classes', 'error');
    });
  }

  onResetSaisieAbsences() {
    this.router.navigate(['/discipline']).then(() => {
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
    });
  }

  ngOnDestroy(): void {
    this.onResetSaisieAbsences();
  }
}
