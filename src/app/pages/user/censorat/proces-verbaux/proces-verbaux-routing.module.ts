import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProcesVerbauxComponent} from './proces-verbaux.component';


const routes: Routes = [
  {
    path: '', component: ProcesVerbauxComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Procès_verbaux',
      // titre: 'userEleveAjaout.Procès_verbaux_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Minutes of meetings | Aurore' : 'Procès verbaux | Aurore',
      icon: 'icofont-law-document bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_procès_verbaux_Procès_verbaux',
      status: true,
      current_role: 'censeur'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcesVerbauxRoutingModule { }
