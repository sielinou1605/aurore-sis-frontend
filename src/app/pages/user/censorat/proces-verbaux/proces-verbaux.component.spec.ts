import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProcesVerbauxComponent} from './proces-verbaux.component';

describe('ProcesVerbauxComponent', () => {
  let component: ProcesVerbauxComponent;
  let fixture: ComponentFixture<ProcesVerbauxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcesVerbauxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcesVerbauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
