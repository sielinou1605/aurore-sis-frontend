import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProcesVerbauxRoutingModule} from './proces-verbaux-routing.module';
import {ProcesVerbauxComponent} from './proces-verbaux.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ProcesVerbauxComponent],
    imports: [
        CommonModule,
        ProcesVerbauxRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class ProcesVerbauxModule { }
