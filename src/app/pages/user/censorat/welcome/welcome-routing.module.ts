import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome.component';


const routes: Routes = [
  {
    path: '', component: WelcomeComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Discipline',
      // titre: 'userEleveAjaout.Discipline_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Discipline | Aurore' : 'Discipline | Aurore',
      icon: 'icofont-education bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_inscriptions_absences_sanctions_notes_et_pvs_Discipline',
      status: true,
      current_role: 'censeur'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
