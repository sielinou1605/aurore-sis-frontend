import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {WelcomeRoutingModule} from './welcome-routing.module';
import {WelcomeComponent} from './welcome.component';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [WelcomeComponent],
    imports: [
        CommonModule,
        WelcomeRoutingModule,
        TranslateModule
    ]
})
export class WelcomeModule { }
