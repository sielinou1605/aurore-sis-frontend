import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EnregistrerSanctionsRoutingModule} from './enregistrer-sanctions-routing.module';
import {EnregistrerSanctionsComponent} from './enregistrer-sanctions.component';
import {SharedModule} from '../../../../shared/shared.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [EnregistrerSanctionsComponent],
    imports: [
        CommonModule,
        EnregistrerSanctionsRoutingModule,
        SharedModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class EnregistrerSanctionsModule { }
