import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EnregistrerSanctionsComponent} from './enregistrer-sanctions.component';


const routes: Routes = [
  {
    path: '', component: EnregistrerSanctionsComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Sanctions',
      // titre: 'userEleveAjaout.Sanctions_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Sanctions | Aurore' : 'Sanctions | Aurore',
      icon: 'icofont-teacher bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Liste_des_sactions_de_la_plateforme_Sanctions',
      status: true,
      current_role: 'censeur'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnregistrerSanctionsRoutingModule { }
