/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../shared/_services/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {GenericsObjectSanctionFilter} from '../../../../shared/_models/response-dto/graduation/sanction-response-model';
import {AjouterSanctionModel} from '../../../../shared/_models/request-dto/graduation/gestion-eleve-absence-model';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-enregistrer-sanctions',
  templateUrl: './enregistrer-sanctions.component.html',
  styleUrls: ['./enregistrer-sanctions.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class EnregistrerSanctionsComponent implements OnInit, OnDestroy {
  public sanctions: any;
  totalElements;
  page = 0;
  pageSize = 20;
  public currentSanction: any;
  currentSwitch =  localStorage.getItem('activelang') === 'en' ? 'All':'Tous';
  selectObjectForm: FormGroup;
  submitted: boolean;
  objectSelect: GenericsObjectSanctionFilter[];
  currentSelect: string;
  currentUrl = `admin/sanction/liste/paged?page=${this.page}&size=${this.pageSize}`;
  currentAnnee: number;
  sanctionForm: FormGroup;
  sequences: any;
  public penalites: any;

  public switchTab = [];
  public titleValue1: string;
  public titleValue2: string;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private genericsService: GenericsService, private fb: FormBuilder,
    private route: ActivatedRoute, private router: Router,
    private modalService: NgbModal, config: NgbModalConfig,
    public authService: AuthService, public tokenStorage: TokenStorageService,
    private translate: TranslateService) {

    this.titleValue2 = localStorage.getItem('activelang') === 'en' ? 'Click to set to perform' : 'Clicquer pour mettre à réaliser';
   this.titleValue1 = localStorage.getItem('activelang') === 'en'
    ? 'Click to set to unrealized' : 'Clicquer pour mettre à non réaliser';
    localStorage.getItem('activelang') === 'en'
      ? this.switchTab = ['All', 'Matricule', 'Class', 'Level', 'Cycle', 'Establishment']
      : this.switchTab = ['Tous', 'Matricule', 'Classe', 'Niveau', 'Cycle', 'Etablissement'];

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.onloadListSanctions();
    this.initFormFilterFormSanction();
    this.initFormEditSanction();
    this.onloadListSequence();
    this.onloadListTypeSanction();
    this.currentAnnee = this.tokenStorage.getUser().codeAnnee;
    this.currentSelect = localStorage.getItem('activelang') === 'en' ? 'All':'Tous';
  }
  private onloadListTypeSanction() {
    this.genericsService.getResource(`parametre/type-penalite/liste`).then((response: any) => {
      this.penalites = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadListSequence() {
    this.genericsService.getResource(`admin/bulletin/filtre/sequence`).then((response: any) => {
      this.sequences = response.data.sequences;
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadListSanctions() {
    console.log(this.currentSwitch);
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(this.currentUrl).then((response: any) => {
      console.log(response);
      this.sanctions = response.data.content;
      this.totalElements = response.data.totalElements;
      this.genericsService.stopLoadingPage();
      if (this.modalService.hasOpenModals()) {
        this.onResetSelectObjet();
      }
    }).catch(reason => {
      this.genericsService.stopLoadingPage();
      console.log(reason);
    });
  }
  setPageChange(event) {
    this.page = event - 1;
    this.currentUrl = `admin/sanction/liste/paged?page=${this.page}&size=${this.pageSize}`;
    this.onloadListSanctions();
  }
  onResetSanction() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
    this.initFormEditSanction();
  }
  onMakeRealiser(sanction: any) {
    this.genericsService.getResource(`admin/sanction/${sanction.id_sanction}/modifier`).then((response: any) => {
      this.onloadListSanctions();
    }).catch(reason => {
      console.log(reason);
    });
  }
  onDeleteSanction(sanction: any) {
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: 'Are you sure?',
        text: 'This operation is irreversible !',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`admin/sanction/${sanction.id_sanction}/supprimer`).then((resultDelete: any) => {
            this.onloadListSanctions();
            this.genericsService.confirmModalResponseAPI( 'Deleted!', 'Your file has been deleted.', 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI('No Deleted!', 'cet element n\'a pas été supprimé.', 'error');
          });
        }
      })
      : Swal.fire({
        title: 'Vous êtes sûr ?',
        text: 'Cette opération est irreversible !',
        icon: 'avertissement',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, supprimez-le !'
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`admin/sanction/${sanction.id_sanction}/supprimer`).then((resultDelete: any) => {
            this.onloadListSanctions();
            this.genericsService.confirmModalResponseAPI( 'Deleted!', 'Votre fichier a été supprimé.', 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI('No Deleted!', 'cet element n\'a pas été supprimé.', 'error');
          });
        }
      });
  }
  ngOnDestroy(): void {
    this.onResetSanction();
  }
  onDetailsSanction(sanction: any, sanctionEleve: TemplateRef<any>) {
    this.currentSanction = sanction;
    this.modalService.open(sanctionEleve,
      {ariaLabelledBy: 'sanction-eleve', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  swicthTo(type: string, modalSelectObject: TemplateRef<any>) {
    this.modalService.open(modalSelectObject,
      {ariaLabelledBy: 'select-objet', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
    this.currentSwitch = type;
    console.log(this.currentSwitch);
    this.objectSelect = [];

    if (type == 'Classe' || type == 'Class') {
      this.genericsService.getResource('admin/classes').then((response: any) => {
        response.data.forEach(elt => {
          this.objectSelect.push(new GenericsObjectSanctionFilter(elt.id, elt.nomClasse));
        });
      }).catch(reason => console.log(reason));
    } else if (type == 'Niveau' || type == 'Level') {
      this.genericsService.getResource('admin/niveaus').then((response: any) => {
        response.data.forEach(elt => {
          this.objectSelect.push(new GenericsObjectSanctionFilter(elt.id, elt.nomNiveau));
        });
      }).catch(reason => console.log(reason));
    } else if (type == 'Cycle') {
      this.genericsService.getResource('admin/cycles').then((response: any) => {
        response.data.forEach(elt => {
          this.objectSelect.push(new GenericsObjectSanctionFilter(elt.id_cycle, elt.nom_cycle + ' - ' + elt.section.nom_section));
        });
      }).catch(reason => console.log(reason));
    } else if (type == 'Etablissement' || type == 'Establishment') {
      this.genericsService.getResource('admin/etablissements').then((response: any) => {
        response.data.forEach(elt => {
          // if (elt.institution) {
            this.objectSelect.push(new GenericsObjectSanctionFilter(elt.id_etablissement, elt.nom_etablissement));
          // }
        });
      }).catch(reason => console.log(reason));
    } else if (type == 'Matricule') {
      this.currentSelect = this.f.cle.value;
      console.log(type, 'On ne charge pas la liste des élèves pour optimiser l\'affichage');
    } else {
      this.currentUrl = `admin/sanction/liste/paged?page=${this.page}&size=${this.pageSize}`;

      console.log("type last ",this.currentUrl)
      this.onloadListSanctions();
    }
    console.log("type ",this.currentSwitch);
  }

  private initFormFilterFormSanction() {
    this.selectObjectForm = this.fb.group({
      cle: ['', Validators.required]
    });
  }

  onResetSelectObjet() {
    this.submitted = false;
    this.initFormFilterFormSanction();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }

  onDisplayListeSanction() {
    this.submitted = true;
    if (this.selectObjectForm.invalid) {
      return;
    }
    if (this.currentSwitch != 'Matricule') {
      this.currentSelect = this.objectSelect.find(elt => elt.cle == this.f.cle.value).valeur;
    } else {
      this.currentSelect = this.f.cle.value;
    }
    const  suffix = `/${this.f.cle.value}/annee/${this.currentAnnee}/liste?page=${this.page}&size=${this.pageSize}`;
    if (this.currentSwitch == 'Classe' || this.currentSwitch == 'Class') {
      this.currentUrl = `admin/sanction/classe${suffix}`;
    } else if (this.currentSwitch == 'Level' || this.currentSwitch == 'Niveau') {
      this.currentUrl = `admin/sanction/niveau${suffix}`;
    } else if (this.currentSwitch == 'Cycle' || this.currentSwitch == 'Cycle') {
      this.currentUrl = `admin/sanction/cycle${suffix}`;
    } else if (this.currentSwitch == 'Etablishment' || this.currentSwitch == 'Etablissement') {
      this.currentUrl = `admin/sanction/etablissement${suffix}`;
    } else if (this.currentSwitch == 'Personnel number' || this.currentSwitch == 'Matricule') {
      this.currentUrl = `admin/sanction/eleve${suffix}`;
    } else {
      this.currentUrl = `admin/sanction/liste/paged?page=${this.page}&size=${this.pageSize}`;
    }
    this.onloadListSanctions();
  }

  get f() { return this.selectObjectForm.controls; }

  private initFormEditSanction() {
    this.sanctionForm = this.fb.group({
      sequence: ['', Validators.required],
      datesanction: ['', Validators.required],
      motif: ['', Validators.required],
      penalite: ['', Validators.required],
      debut: ['', Validators.required],
      fin: [''],
      realisation: ['false'],
      temoin: [''],
      matricule: [''],
      idClasse: ['']
    });
  }

  get g() { return this.sanctionForm.controls; }

  onSaveSanction() {
    this.submitted = true;
    if (this.sanctionForm.invalid) {
      return;
    }
    const penalite = this.penalites.find(elt => elt.id == this.g.penalite.value);
    const dto = new AjouterSanctionModel(this.g.debut.value, this.g.fin.value,
      this.g.datesanction.value, this.g.sequence.value, this.g.matricule.value,
      this.g.motif.value, this.g.realisation.value, this.g.temoin.value, this.g.motif.value,
      this.g.penalite.value, this.g.idClasse.value, penalite.libelle, this.currentSanction.id_sanction);
    this.genericsService.putResource(`admin/sanction/modifier`, dto).then((response: any) => {
      console.log(response);
      this.onResetSanction();
      this.onloadListSanctions();
    }).catch(reason => {
      console.log(reason);
    });
  }

  onModifierSanction(sanction: any, modifSanctionEleve: TemplateRef<any>) {
    this.currentSanction = sanction;
    this.modalService.open(modifSanctionEleve,
      {ariaLabelledBy: 'modif-sanction-eleve', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
    if (sanction) {
      this.sanctionForm.patchValue({
        sequence: sanction.sequence.id_sequence,
        datesanction: sanction.date_penalite,
        motif: sanction.motif_sanction,
        penalite: sanction.typePenalite ? sanction.typePenalite.id : '',
        debut: sanction.date_debut_penalite,
        fin: sanction.date_fin_penalite,
        realisation: sanction.realisation_sanction,
        temoin: sanction.temoin_faute,
        matricule: sanction.eleve ? sanction.eleve.matricule : '',
        idClasse: sanction.classe ? sanction.classe.id : ''
      });
    }
  }
}
