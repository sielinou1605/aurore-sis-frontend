import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EnregistrerSanctionsComponent} from './enregistrer-sanctions.component';

describe('EnregistrerSanctionsComponent', () => {
  let component: EnregistrerSanctionsComponent;
  let fixture: ComponentFixture<EnregistrerSanctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnregistrerSanctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnregistrerSanctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
