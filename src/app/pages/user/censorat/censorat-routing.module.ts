import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CensoratComponent} from './censorat.component';
import {HelpComponent} from './help/help.component';


const routes: Routes = [
  {
    path: '', component: CensoratComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Discipline',
      // titre: 'userEleveAjaout.Discipline_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Discipline | Aurore' : 'Discipline | Aurore',
      icon: 'icofont-education bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_absences_sanctions_et_proces_verbaux_Discipline',
      status: true,
      current_role: 'censeur',
      action: 'bouton-discipline'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome',
        loadChildren: () => import('./welcome/welcome.module')
          .then(m => m.WelcomeModule)
      },
      {
        path: 'remplir-absences',
        loadChildren: () =>
          import('./remplir-absences/remplir-absences.module').then(m => m.RemplirAbsencesModule)
      },
      {
        path: 'enregistrer-sanctions',
        loadChildren: () =>
          import('./enregistrer-sanctions/enregistrer-sanctions.module').then(m => m.EnregistrerSanctionsModule)
      },
      {
        path: 'proces-verbaux',
        loadChildren: () =>
          import('./proces-verbaux/proces-verbaux.module').then(m => m.ProcesVerbauxModule)
      },
      {
        path: 'help', component: HelpComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Aide',
          titre: localStorage.getItem('activelang') === 'en' ? 'Help | Aurore' : 'Aide | Aurore',
         // titre: 'userEleveAjaout.Aide_Aurore',
          icon: 'icofont-question-circle bg-c-green',
          breadcrumb_caption: 'userEleveAjaout.Aide_concernant_le_module_discipline_Animateur_pédagogique',
          status: true,
          current_role: 'censeur'
        }
      },
      {
        path: '**',
        loadChildren: () =>
          import('./remplir-absences/remplir-absences.module').then(m => m.RemplirAbsencesModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CensoratRoutingModule { }
