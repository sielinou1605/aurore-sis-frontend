import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CensoratComponent} from './censorat.component';

describe('CensoratComponent', () => {
  let component: CensoratComponent;
  let fixture: ComponentFixture<CensoratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CensoratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CensoratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
