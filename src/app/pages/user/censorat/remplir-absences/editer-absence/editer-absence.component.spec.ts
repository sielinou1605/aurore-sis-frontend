import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditerAbsenceComponent } from './editer-absence.component';

describe('EditerAbsenceComponent', () => {
  let component: EditerAbsenceComponent;
  let fixture: ComponentFixture<EditerAbsenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditerAbsenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditerAbsenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
