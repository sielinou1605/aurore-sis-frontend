/* tslint:disable:triple-equals */
import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {AbsencesARemplir, JustifierAbsenceModel} from '../../../../../shared/_models/request-dto/graduation/gestion-eleve-absence-model';

@Component({
  selector: 'app-editer-absence',
  templateUrl: './editer-absence.component.html',
  styleUrls: ['./editer-absence.component.scss']
})
export class EditerAbsenceComponent implements OnInit, OnDestroy {

  @ViewChild('modalSelectSequence', { static: true }) modalSequence: ElementRef;
  absences: any;
  selectSequenceClasseForm: FormGroup;
  submitted: boolean;
  public formSelect: any;
  private absencesForm: FormGroup;
  public currentClasse;
  public currentSequence;
  public classe: any;
  public eleve: any;
  public sequence: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  private routeMatricule: string;
  private routeClasse: number;
  private routeSequence: number;
  constructor(
    private genericsService: GenericsService,  private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    private router: Router, public tokenStorage: TokenStorageService,
    public authService: AuthService,
    private translate: TranslateService,
    private route: ActivatedRoute
  ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(49);
  }

  ngOnInit() {
    this.initFormSelectSequence();
    this.initAbsencesForm();
    this.routeMatricule = this.route.snapshot.params.matricule;
    this.routeClasse = this.route.snapshot.params.classe;
    this.routeSequence =  this.route.snapshot.params.sequence;
    this.onloadDataFilter();
    console.log(this.routeClasse, this.routeMatricule);
    if (this.routeMatricule == '0' || this.routeClasse == 0 || this.currentSequence == 0) {
      this.modalService.open(this.modalSequence, {ariaLabelledBy: 'select-sequence', scrollable: true})
        .result.then((result) => {}, (reason) => {
        console.log(reason);
      });
    } else {
      this.onloadAbsences(this.routeMatricule, this.routeSequence);
    }
  }

  private initFormSelectSequence() {
    this.selectSequenceClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      matricule: ['', Validators.required]
    });
  }
  private initAbsencesForm() {
    this.absencesForm = this.fb.group({
      absencesData: new FormArray([])
    });
  }
  get f() { return this.selectSequenceClasseForm.controls; }
  get fAbsences() { return this.absencesForm.controls; }
  get tAbsences() { return this.fAbsences.absencesData as FormArray; }

  onDisplayListeAbsences() {
    this.submitted = true;
    if (this.selectSequenceClasseForm.invalid) {
      return;
    }
    this.currentSequence = this.formSelect.sequences.find(item => item.id == this.f.sequence.value);
    console.log(this.f.matricule.value, this.f.sequence.value);
    this.onloadAbsences(this.f.matricule.value, this.f.sequence.value);
  }

  private onloadAbsences(matricule: any, sequence: any) {
    this.genericsService.getResource(`admin/absence/eleve/${matricule}/sequence/${sequence}/liste`).then((response: any) => {
      if (response.success === true) {
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
        this.absences = response.data.absences;
        this.classe = response.data.classe;
        this.eleve = response.data.eleve;
        this.sequence = response.data.sequence;
        response.data.absences.forEach(item => {
          this.tAbsences.push(
            this.fb.group({
              id_absence: [item.id_absence],
              dateAbs: [item.date_absence],
              absenceNj: [item.nbre_heures_nj],
              absence: [item.nbre_heures_j, Validators.required]
            })
          );
        });
      }
    }).catch(reason => {
      console.log(reason);
    });
  }

  private onloadDataFilter() {
    this.genericsService.getResource(`admin/absence/filtre/saisie`).then((result: any) => {
      if (result.success === true) {
        this.formSelect = result.data;
      } else {
        this.formSelect = result.data;
        this.genericsService.confirmResponseAPI(result.message, 'error');
      }
      if (!(this.routeMatricule == '0' || this.routeClasse == 0 || this.routeSequence == 0)) {
        this.currentSequence = this.formSelect.sequences.find(item => item.id == this.routeSequence);
      }
      console.log(this.formSelect);
    }).catch((reason: any) => {
      console.log(reason);
      this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des classes', 'error');
    });
  }

  // gestion du clavier sur les input /api/admin/absence/justifier
  @HostListener('window:keyup', ['$event']) keyUp(e: KeyboardEvent) {
    if ((e.code == 'Insert' || e.code == 'Enter')) {
      const listToDelete = [];
      const dto = new JustifierAbsenceModel(this.sequence.id, this.eleve.matricule, []);
      this.tAbsences.controls.forEach((fg, i) => {
        if (fg.value.absence > 0) {
          if (!this.tAbsences.at(i).invalid) {
            dto.absencesAJustifier.push(new AbsencesARemplir(fg.value.id_absence, this.f.matricule.value, fg.value.absence));
            listToDelete.push(i);
          }
        }
      });
      if (dto.absencesAJustifier.length > 0) {
        console.log(dto);
        this.genericsService.postResource(`admin/absence/justifier`, dto).then((response: any) => {
          listToDelete.forEach(i => this.tAbsences.removeAt(i));
          this.genericsService.confirmResponseAPI(response.message, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
        });
      }
    }
  }

  onResetSaisieAbsences() {
    this.router.navigate(['/discipline']).then(() => {
      this.modalService.dismissAll();
    });
  }

  ngOnDestroy(): void {
    this.onResetSaisieAbsences();
  }

  onChanStateUpdate(i) {

  }

  onRefreshAbsence() {
    this.tAbsences.clear();
    this.onloadAbsences(this.routeMatricule, this.routeSequence);
  }
}
