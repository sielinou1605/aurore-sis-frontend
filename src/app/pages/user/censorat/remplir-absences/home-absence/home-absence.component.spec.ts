import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAbsenceComponent } from './home-absence.component';

describe('HomeAbsenceComponent', () => {
  let component: HomeAbsenceComponent;
  let fixture: ComponentFixture<HomeAbsenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAbsenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAbsenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
