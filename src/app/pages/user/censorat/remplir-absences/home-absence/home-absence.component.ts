/* tslint:disable:triple-equals */
import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FiltreAbsenceModel} from '../../../../../shared/_models/response-dto/graduation/filtre-note-absence-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {AbsencesARemplir, RemplirAbsenceModel} from '../../../../../shared/_models/request-dto/graduation/gestion-eleve-absence-model';

@Component({
  selector: 'app-home-absence',
  templateUrl: './home-absence.component.html',
  styleUrls: ['./home-absence.component.scss']
})
export class HomeAbsenceComponent implements OnInit, OnDestroy {
  @ViewChild('modalSelectSequence', { static: true }) modalSequence: ElementRef;
  eleves: any;
  selectSequenceClasseForm: FormGroup;
  submitted: boolean;
  private base_url = 'admin/eleve/';
  public formSelect: FiltreAbsenceModel;
  currentClasse: any;
  currentSequence: any;
  private absencesForm: FormGroup;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private genericsService: GenericsService,  private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    private router: Router, public tokenStorage: TokenStorageService,
    public authService: AuthService,
    private translate: TranslateService
  ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(47);
  }

  ngOnInit() {
    this.onloadDataFilter();
    this.initFormSelectSequence();
    this.initAbsencesForm();
    this.modalService.open(this.modalSequence,
      {ariaLabelledBy: 'select-sequence', scrollable: true}).result.then((result) => {
    }, (reason) => {
      console.log(reason);
    });
  }

  private initFormSelectSequence() {
    this.selectSequenceClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      classe: ['', Validators.required]
    });
  }
  private initAbsencesForm() {
    this.absencesForm = this.fb.group({
      sequencesData: new FormArray([])
    });
  }
  get fSequences() { return this.absencesForm.controls; }
  get tSequences() { return this.fSequences.sequencesData as FormArray; }

  get f() { return this.selectSequenceClasseForm.controls; }

  onDisplayListeEleve() {
    this.submitted = true;
    if (this.selectSequenceClasseForm.invalid) {
      return;
    }
    this.genericsService.startLoadingPage();
    this.currentClasse = this.formSelect.classes.find(item => item.nomClasse == this.f.classe.value);
    this.currentSequence = this.formSelect.sequences.find(item => item.id == this.f.sequence.value);
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.onLoadListEleves(this.f.classe.value);
  }

  public onLoadListEleves(event) {
    this.genericsService.getResource(`${this.base_url}filtrer?predicat=${event}`).then((result: any) => {
      if (result.success === true) {
        this.eleves = result.data;
        this.eleves.forEach(item => {
          this.tSequences.push(
            this.fb.group({
              matricule: [item.matricule],
              noms: [item.nomEleve + ' ' + item.prenomEleve],
              absence: [0, Validators.min(0)],
              update: false,
              canUpdate: true
            })
          );
        });
        this.genericsService.stopLoadingPage();
      }
    }, err => {
      console.log(err);
    });
  }
  // gestion du clavier sur les input
  @HostListener('window:keyup', ['$event']) keyUp(e: KeyboardEvent) {
    if ((e.code == 'Insert' || e.code == 'Enter')) {
      const listToDelete = [];
      const dto = new RemplirAbsenceModel(this.currentClasse.id, this.currentSequence.id, []);
      this.tSequences.controls.forEach((fg, i) => {
        if (fg.value.absence > 0 && fg.value.update == true) {
          if (!this.tSequences.at(i).invalid) {
            console.log('testé valide', fg);
            dto.absencesARemplir.push(new AbsencesARemplir(null, fg.value.matricule, fg.value.absence));
            listToDelete.push(i);
          }
        }
      });
      if (dto.absencesARemplir.length > 0) {
        this.genericsService.postResource(`admin/absence/remplir`, dto).then((response: any) => {
          listToDelete.forEach(i => this.tSequences.removeAt(i));
          this.genericsService.confirmResponseAPI(response.message, 'success');
        }).catch(reason => {
          this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
        });
      }
    }
  }
  private onloadDataFilter() {
    this.genericsService.getResource(`admin/absence/filtre/saisie`).then((result: any) => {
      if (result.success === true) {
        this.formSelect = result.data;
      } else {
        this.formSelect = result.data;
        this.genericsService.confirmResponseAPI(result.message, 'error');
      }
      console.log(result);
    }).catch((reason: any) => {
      this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des classes', 'error');
    });
  }

  onResetSaisieAbsences(confirm: boolean) {
    // TODO on ne doit redirectionner vers la fenetre de discipline que lorsque l'utilisateur a cliquer sur annuler
    //     lorsqu'on veut avoir le detail sur un eleve, la methode onDestroy doit etre sans effet
    if (confirm) {
      this.router.navigate(['/discipline']).then(() => {
        this.modalService.dismissAll();
      });
    }
  }

  ngOnDestroy(): void {
    this.onResetSaisieAbsences(false);
  }

  onChanStateUpdate(i: number) {
    if (this.tSequences.at(i).value.canUpdate == true) {
      this.tSequences.at(i).patchValue({update: true});
    }
  }

  onloadEditAbsence(matricule: string, id: number) {
    this.router.navigateByUrl(`/discipline/remplir-absences/${matricule}/${id}/${this.currentSequence.id}`);
  }

  onRefreshAbsence() {
    this.tSequences.clear();
    this.onLoadListEleves(this.f.classe.value);
  }
}
