/* tslint:disable:triple-equals */
import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {AbsencesARemplir, RemplirAbsenceModel} from '../../../../shared/_models/request-dto/graduation/gestion-eleve-absence-model';
import {AuthService} from '../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-remplir-absences',
  templateUrl: './remplir-absences.component.html',
  styleUrls: ['./remplir-absences.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class RemplirAbsencesComponent {
}
