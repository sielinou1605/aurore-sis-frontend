import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EditerAbsenceComponent} from './editer-absence/editer-absence.component';
import {HomeAbsenceComponent} from './home-absence/home-absence.component';
import {RemplirAbsencesComponent} from './remplir-absences.component';


const routes: Routes = [
  {
    path: '', component: RemplirAbsencesComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Absences',
      // titre: 'userEleveAjaout.Absences_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Absences | Aurore' : 'Absences | Aurore',
      icon: 'icofont-quill-pen bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Saisie_des_heures_d_absences_pour_une_classe_et_pour_une_séquence_Absences',
      status: true,
      current_role: 'censeur'
    },
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: ':matricule/:classe/:sequence', component: EditerAbsenceComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Justification_absence',
          // titre: 'userEleveAjaout.Justification_absence_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Justification for absence | Aurore' : 'Justification absence | Aurore',
          icon: 'icofont-bell-alt bg-c-pink',
          breadcrumb_caption: 'userEleveAjaout.Justification_des_heures_d_absences_Justification_absence',
          status: true,
          current_role: 'censeur'
        }
      },
      {
        path: 'home', component: HomeAbsenceComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Absences',
          // titre: 'userEleveAjaout.Absences_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Absences | Aurore' : 'Absences | Aurore',
          icon: 'icofont-quill-pen bg-c-pink',
          breadcrumb_caption: 'userEleveAjaout.Saisie_des_heures_d_absences_pour_une_classe_et_pour_une_séquence_Absences',
          status: true,
          current_role: 'censeur'
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RemplirAbsencesRoutingModule { }
