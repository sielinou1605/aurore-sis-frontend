import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RemplirAbsencesRoutingModule} from './remplir-absences-routing.module';
import {RemplirAbsencesComponent} from './remplir-absences.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { HomeAbsenceComponent } from './home-absence/home-absence.component';
import { EditerAbsenceComponent } from './editer-absence/editer-absence.component';
import {HttpLoaderFactory} from '../../../../app.module';
import {HttpClient} from '@angular/common/http';


@NgModule({
  declarations: [
    RemplirAbsencesComponent,
    HomeAbsenceComponent,
    EditerAbsenceComponent
  ],
  imports: [
    CommonModule,
    RemplirAbsencesRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ]
})
export class RemplirAbsencesModule { }
