import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RemplirAbsencesComponent} from './remplir-absences.component';

describe('RemplirAbsencesComponent', () => {
  let component: RemplirAbsencesComponent;
  let fixture: ComponentFixture<RemplirAbsencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemplirAbsencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemplirAbsencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
