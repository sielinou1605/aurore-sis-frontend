import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CensoratRoutingModule} from './censorat-routing.module';
import {CensoratComponent} from './censorat.component';
import {HelpComponent} from './help/help.component';
import {SharedModule} from '../../../shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../../app.module';
import {HttpClient} from '@angular/common/http';


@NgModule({
  declarations: [CensoratComponent, HelpComponent],
  imports: [
    CommonModule,
    CensoratRoutingModule,
    SharedModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ]
})
export class CensoratModule { }
