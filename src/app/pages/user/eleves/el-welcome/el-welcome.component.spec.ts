import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ElWelcomeComponent} from './el-welcome.component';

describe('ElWelcomeComponent', () => {
  let component: ElWelcomeComponent;
  let fixture: ComponentFixture<ElWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
