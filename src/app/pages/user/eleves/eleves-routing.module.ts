import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ElevesComponent} from './eleves.component';
import {ElWelcomeComponent} from './el-welcome/el-welcome.component';
import {AjoutEleveComponent} from './ajout-eleve/ajout-eleve.component';


const routes: Routes = [
  {
    path: '', component: ElevesComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Eleves',
     // titre: 'userEleveAjaout.Eleves_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Students | Aurore' : 'Eleves | Aurore',
      icon: 'icofont-users-alt-5 bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_eleves_Eleves',
      status: true,
      current_role: 'eleve',
      action: 'bouton-eleve'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome', component: ElWelcomeComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Eleves',
          // titre: 'userEleveAjaout.Eleves_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Students | Aurore' : 'Eleves | Aurore',
          icon: 'icofont-file bg-c-pink',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_eleves_Eleves',
          status: true,
          current_role: 'eleve'
        }
      },
      {
        path: 'ajout-eleve', component: AjoutEleveComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Ajout_élève',
         // titre: 'userEleveAjaout.Ajout_élève_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? ' Add Students | Aurore' : 'Ajout Eleves | Aurore',
          icon: 'icofont-boy bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Recrutement_d_un_eleve_Ajout_élève',
          status: true,
          current_role: 'eleve'
        }
      },
      {
        path: 'liste-eleve', loadChildren: () => import('./liste-eleve/liste-eleve.module')
          .then(m => m.ListeEleveModule)
      },
      {
        path: 'liste-inscription', loadChildren: () => import('./liste-inscription/liste-inscription.module')
          .then(m => m.ListeInscriptionModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ElevesRoutingModule { }
