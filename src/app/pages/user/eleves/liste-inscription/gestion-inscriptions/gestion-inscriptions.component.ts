/* tslint:disable:triple-equals */
import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericsService } from '../../../../../shared/_services/generics.service';
import { ConveertDateService } from '../../../../../shared/_helpers/conveert-date.service';
// tslint:disable-next-line:import-spacing
import { InscriptionRequestModel, UpdateInscriptionModel }
from '../../../../../shared/_models/request-dto/finance/inscription-request-model';
import { TokenStorageService } from '../../../../../shared/_services/token-storage.service';
import { AuthService } from '../../../../../shared/_services/auth.service';
import { AppConstants } from '../../../../../shared/element/app.constants';
import { HttpEventType } from '@angular/common/http';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { log } from 'util';

@Component({
  selector: 'app-gestion-inscriptions',
  templateUrl: './gestion-inscriptions.component.html',
  styleUrls: ['./gestion-inscriptions.component.scss'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ])
  ],
  providers: [NgbModalConfig, NgbModal]
})
export class GestionInscriptionsComponent implements OnInit, OnDestroy {
  public servicesForm: FormGroup;
  public parentForm: FormGroup;
  public eleveForm: FormGroup;
  public isTransfert: any = false;
  public idEleve: any;
  public submitted: boolean;
  private base_url_eleve = 'admin/eleves/';
  private base_url = 'admin/inscription/';
  public eleves: any;
  public currentEleve: any;
  public modeleEcheanciers: any;
  public modeleEcheanciersDispo: any[];
  public classes: any;
  public echeanciersEleve: any;
  private transfertModel: any;
  public niveaux: any;
  public searchForm: FormGroup;
  private idInscription: any;
  private canModifier: boolean;
  public disabledButtonSave: boolean;
  public updateStudent = false;
  private reloadMatricule: any;
  public urlImage = AppConstants.API_URL + 'admin/recuperation/image/';
  public timestamp: number;
  public selectedFiles: any;
  public progress: number;
  public currentFileUpload: any;
  public canUploadImage: boolean;
  public pageSize = 20;
  public page = 0;
  public oldEleveClasse: any;
  public totalElements: number;
  private dataFilter: any;
  private classeDestination: any;
  private EcheancierDelete = [];
  public modeleEcheanciersObligatoire: any;
  public modeleEcheanciersFacultatif: any;
  private canAddService = false;
  public canSave = false;
  public newModel = false;
  private baseEcheancier: number;

  public pageSizeClasse = 500;
  public pageClasse = 0;
  disableFormParent = true;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private fb: FormBuilder, private modalService: NgbModal, config: NgbModalConfig,
    private calendar: NgbCalendar, private router: Router, private route: ActivatedRoute,
    private genericsService: GenericsService, private dateConvert: ConveertDateService,
    private tokenStorage: TokenStorageService, public authService: AuthService,
    private _location: Location,
    private translate: TranslateService
  ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(16);
  }

  ngOnInit() {
    console.log('gestion inscription');
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.initServiceForm();
    this.initParentForm();
    this.initEleveForm();
    this.onLoadListEleves('');
    this.loadListClasses();
    this.loadListNiveau();
    this.idEleve = this.route.snapshot.params.idEleve;
    this.idInscription = this.route.snapshot.params.idInscription;
    if (this.idEleve) {

      this.canAddService = true;
      this.onGetOnEleve(this.idEleve);
      this.timestamp = Date.now();
    }


  }
  get search() { return this.searchForm.controls; }
  // recuperation de la liste d'élève
  public onLoadListEleves(event, reInitPage?: boolean) {
    if (reInitPage == true) {
      this.page = 0;
    }
    this.genericsService.startLoadingPage();
    this.dataFilter = event;
    this.genericsService.getResource(`admin/eleve/liste?predicat=${event}&size=${this.pageSize}&page=${this.page}`).then((result: any) => {
      this.eleves = result.data.content;
      this.totalElements = result.data.totalElements;
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      this.genericsService.stopLoadingPage();
      console.log(reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI(AppConstants.KNOW_ERROREN, 'error')
        : this.genericsService.confirmResponseAPI(AppConstants.KNOW_ERRORFR, 'error');
    });
  }
  onAnnulerInscrireEleves() {
    this._location.back();
  }
  setPageChange(event) {
    this.page = event - 1;
    this.onLoadListEleves(this.dataFilter);
  }
  // recuperation de la liste de niveau pour une classse
  onGetNiveauByClasse(classe: number) {
    this.genericsService.getResource(`classes/${classe}/niveau`).then((result: any) => {
      this.listModelEcheancierNiveau(this.tokenStorage.getUser().codeAnnee, result.id);
    });
  }
  // Recuperation de l'eleve grace a son matricule
  private onGetOnEleve(idEleve) {
    this.genericsService.getResource(`${this.base_url_eleve}matricule/${idEleve}`).then((result: any) => {
      this.currentEleve = result.data;
      this.canUploadImage = true;
      this.updateView(this.currentEleve);
      this.listModelEcheancierNiveau(this.tokenStorage.getUser().codeAnnee, this.currentEleve.classe.niveau.id);
      this.listModelEcheancierDisponiblePourEleve(this.tokenStorage.getUser().codeAnnee, this.currentEleve.matricule);
      this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, this.currentEleve.matricule);
    });

  }

  // Liste du model echeancier par niveau
  private listModelEcheancierNiveau(annee: number, niveau: number) {
    this.genericsService.getResource(`admin/modele-echeancier/annee/${annee}/niveau/${niveau}/list`)
      .then((result: any) => {
        if (result.success === true) {
          this.modeleEcheanciers = result.data;
          const echeancierF = [];
          result.data.forEach(elt => (elt.service.obligatoire === 'N') ? echeancierF.push(elt) : '');
          this.modeleEcheanciersFacultatif = echeancierF;
          console.log('niveau', niveau);
          console.log(this.modeleEcheanciersFacultatif, ' **** init : ', this.t, '  *** ME : ', this.modeleEcheanciers);
          if (this.t.value.length < this.modeleEcheanciers.length) {
            this.canAddService = false;
          }
        }
      });
  }
  // Liste du model echeancier non souscrit par un eleve
  private listModelEcheancierDisponiblePourEleve(annee: number, matricule: number) {
    this.genericsService.getResource(`admin/echeancier/disponible/matricule/${matricule}/annee/${annee}/list`)
      .then((result: any) => {
        if (result.success === true) {
          this.modeleEcheanciersDispo = result.data;
          console.log('this.modeleEcheanciersDispo', this.modeleEcheanciersDispo);
        }
      });
  }
  // /echeancier/disponible/matricule/{matricule}/annee/{annee}/list

  // Valider apres les informations entrer sur le modal informations personelle utilisateurs
  onValiderEleve(eleve: any) {
    this.listModelEcheancierNiveau(this.tokenStorage.getUser().codeAnnee, this.currentEleve.classe.niveau.id);
    this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, eleve.matricule);
    if (this.authService.isActif('bouton-valider')) {
      this.canModifier = false;
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
    }
  }

  // mise a jour des elements du formulaire parent eleve
  updateView(eleve: any) {
    this.parentForm.patchValue({
      dossier: eleve.matricule,
      nomParent: eleve.nomParent,
      telParent: eleve.telephonePere,
      emailParent: eleve.emailParent,
      residenceParent: eleve.residence_parent,
      telephoneMere: eleve.telephoneMere,
      nomMere: eleve.nomMere,
    });
  }

  // lancer le modl pour l'ajout d'un mouvel eleve
  onCreateNewEleve(addeleve: TemplateRef<any>) {
    this.canModifier = true;
    this.disableFormParent = false;
    this.canUploadImage = false;
    this.t.clear();
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.generateNextMatricule();
    this.modalService.open(addeleve,
      { ariaLabelledBy: 'add-eleve', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => { console.log(reason); });
  }

  // les elements du formulaire globale
  get f() { return this.servicesForm.controls; }

  // les elements du formulaire local
  get t() { return this.f.servicesData as FormArray; }

  // Ajouter un element dans le array des echeanciers selectionné
  onAddLineToServiceSelect() {
    this.listModelEcheancierDisponiblePourEleve(this.tokenStorage.getUser().codeAnnee, this.currentEleve.matricule);

    console.log('init : ', this.t, ' ME : ', this.modeleEcheanciers);
    this.canSave = true;
    this.newModel =true;
    this.t.push(
      this.fb.group({
        service: [''],
        intitule: [],
        montant: [],
        dateEch: [this.calendar.getToday()],
        quantite:[],
        obligatoire: ['N']
      })
    );
  }

  // supprimer une ligne de formulaire locale
  onDeleleLineToServiceSelect(position) {
    console.log('AVANT : Position : ' + position, 'Object : ', this.t.value, ' Eleve echeance : ', this.echeanciersEleve);
    const obDelete = this.t.value[position];
    const IsExist = this.echeanciersEleve.find((o) => {
      return o.modeleEcheancier.id === obDelete.service;
    });

    console.log(IsExist);
    if (IsExist !== undefined) {
      // supp dans la BD
      const IsExistDel = this.EcheancierDelete.find((o) => {
        return o.modeleEcheancier.id === obDelete.service;
      });
      if (IsExistDel === undefined) { this.EcheancierDelete.push(IsExist); }
      this.canAddService = true;
      this.onDeleleLineToServiceSelectApi(position);
    } else {
      this.onDeleleLineToServiceSelectApi(position);
    }
    // this.onDeleleLineToServiceSelectApi(position)
  }
  onDeleleLineToServiceSelectApi(position) {
    this.t.removeAt(position);
    if (this.t.length == this.baseEcheancier) {
      this.canSave = false;
    }
  }

  // reinitialiser le formulaire des echeanciers
  onReset() {
    this.t.clear();
    this.initServiceForm();
  }
  // lancer le modal permettant de choisir l'eleve a modifier ou de creer un mouvel eleve
  openEleves(contenteleves: TemplateRef<any>) {
    console.log(this.idEleve);
    if (!this.idEleve) {
      this.modalService.open(contenteleves,
        { ariaLabelledBy: 'add-bourse', size: 'lg', scrollable: true }).result.then((result) => {
      }, (reason) => {
      });
    }
  }
  // Methode d'ajout d'un service a une inscription
  onAddServiceToInscription() {
    let isSend = true;
    const echeancierTotal = this.t.value;
    console.log('echeancier total : ', echeancierTotal);
    const echeancierNewAdd = [];

    const i = 0;
    echeancierTotal.forEach(elt => {
      // Recherche des services a jouter
      if (elt.service !== parseInt(elt.service, 0)) {
        echeancierNewAdd.push(elt);
      }

    });

    console.log('Echeance a ajouter : ', echeancierNewAdd, 'Echeance a Dele : ', this.EcheancierDelete);
// 
    const echeancier = [];
    const isAdd = false;
    if (echeancierNewAdd.length > 0) {
      echeancierNewAdd.forEach(elt => {
        if (parseInt(elt.service, 0) > 0) {
          echeancier.push({
            dateEcheance: elt.dateEch,
            idModeleEcheancier: elt.service,
            montant: elt.montant,
            quantite: elt.quantite
          });
        }
      });

      this.genericsService.putResource(`admin/inscription/${this.idInscription}/ajout-services`,
        echeancier).then((result) => {
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.SERVICE_CREATEDEN, 'success') :
          this.genericsService.confirmResponseAPI(AppConstants.SERVICE_CREATEDFR, 'success');
        this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, this.idEleve);
        this.listModelEcheancierDisponiblePourEleve(this.tokenStorage.getUser().codeAnnee, this.currentEleve.matricule);
      }).catch(reason => {
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_OPERATIONEN, reason.error.message, 'error')
          : this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_OPERATIONFR, reason.error.message, 'error');
      });
    } else if (this.EcheancierDelete.length > 0) {
      this.EcheancierDelete.forEach(elt => {
        echeancier.push({
          dateEcheance: elt.modeleEcheancier.date_ech,
          idModeleEcheancier: elt.modeleEcheancier.id,
          montant: elt.modeleEcheancier.montant_a_ech,
          quantite:elt.quantite
        });
      });
      this.genericsService.putResource(`admin/inscription/${this.idInscription}/supprimer-services`,
        echeancier).then((result) => {
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.SERVICE_DELETEEN, 'success')
          : this.genericsService.confirmResponseAPI(AppConstants.SERVICE_DELETEFR, 'success');
        this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, this.idEleve);
        this.listModelEcheancierDisponiblePourEleve(this.tokenStorage.getUser().codeAnnee, this.currentEleve.matricule);
      }).catch(reason => {

        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_OPERATIONEN, reason.error.message, 'error')
          : this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_OPERATIONFR, reason.error.message, 'error');
        this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, this.idEleve);
        this.listModelEcheancierDisponiblePourEleve(this.tokenStorage.getUser().codeAnnee, this.currentEleve.matricule);
        this.EcheancierDelete = [];
      });
    } else {
      isSend = false;
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI(AppConstants.SERVICE_OPTIONNEL_ERROREN, 'error')
        : this.genericsService.confirmResponseAPI(AppConstants.SERVICE_OPTIONNEL_ERRORFR, 'error');
    }
  }
  // ajout de service, ajout eleve ou modifications
  onRecapEleve() {
    if (this.authService.isActif('bouton-enregistrer-continuer')) {
      if (this.canAddService && this.updateStudent != true) {
        this.onAddServiceToInscription();
      } else if (!this.canAddService && this.updateStudent != true) {
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.SERVICE_OPTIONNEL_ERROREN, 'error')
          : this.genericsService.confirmResponseAPI(AppConstants.SERVICE_OPTIONNEL_ERRORFR, 'error');
      } else {
        this.submitted = true;
        if (this.parentForm.invalid) {
          return;
        }
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
        const echeancier = [];
        let detailsTransfert;
        const parent = this.parentForm.value;
        this.t.value.forEach(elt => {
          if (parseInt(elt.service, 0) > 0) {
            echeancier.push({
              dateEcheance: elt.dateEch,
              idModeleEcheancier: elt.service,
              montant: elt.montant,
              quantite:elt.quantite
            });
          }
        });
        if (this.transfertModel) {
          detailsTransfert = {
            codeEtabProvenance: this.transfertModel.codeEtabProv,
            idNiveau: this.transfertModel.niveauEleve,
            montantRemise: this.transfertModel.montantRemise,
            motifTransfert: this.transfertModel.motifte,
            nomEtabProvenance: this.transfertModel.nomEtabProv,
            villeEtabProvenance: this.transfertModel.villeEtabProv
          };
        }
        this.disabledButtonSave = true;
        console.log('this.updateStudent', this.updateStudent);
        if (this.updateStudent == true) {
          const dto = new UpdateInscriptionModel(
            this.currentEleve.date_naissance,
            parent.emailParent,
            this.currentEleve.handicape,
            // this.classeDestination,
            this.oldEleveClasse == this.classeDestination ? null : this.classeDestination,
            // this.oldEleveClasse == this.classeDestination ? null : this.classeDestination,
            this.currentEleve.lieuNais ? this.currentEleve.lieuNais : '',
            this.currentEleve.matricule,
            'changement de classe',
            this.currentEleve.nationalite ? this.currentEleve.nationalite : '',

            this.currentEleve.nomEleve,
            parent.nomParent,
            this.currentEleve.prenomEleve,
            this.currentEleve.religion,
            this.currentEleve.residence_eleve,
            parent.resParent,
            echeancier,
            this.currentEleve.sexe,
            (parent.identifiantPortail && parent.identifiantPortail.length == 0) ? null : parent.telParent,
            parent.nomMere,
            (parent.telephoneMere && parent.telephoneMere.length == 0) ? null : parent.telephoneMere);
          console.log('dto', dto);
          this.genericsService.postResource(`${this.base_url}eleve/modifier`, dto).then((result: any) => {
            this.generateNextMatricule();
            this.updateStudent = false;
            if (result.success === true) {
              this.redirectAfterSuccess(result.data);
            } else {
              localStorage.getItem('activelang') === 'en'
                ? this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_MODIFICATIONEN, result.message, 'error')
                : this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_MODIFICATIONFR, result.message, 'error');
            }
            this.disabledButtonSave = false;
          }).catch(reason => {
            localStorage.getItem('activelang') === 'en'
              ? this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_MODIFICATIONEN, reason.error.message, 'error')
              : this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_MODIFICATIONFR, reason.error.message, 'error');
            this.disabledButtonSave = false;
          });
        } else {
          const dto = new InscriptionRequestModel(
            this.currentEleve.date_naissance, parent.emailParent, this.currentEleve.handicape,
            this.tokenStorage.getUser().codeAnnee, this.currentEleve.classe.id,
            this.currentEleve.lieu_naissance ? this.currentEleve.lieu_naissance : '',
            this.currentEleve.matricule, this.currentEleve.nationalite ? this.currentEleve.nationalite : '',
            this.currentEleve.nomEleve, parent.nomParent, null,
            '', this.currentEleve.prenomEleve, this.currentEleve.religion,
            this.currentEleve.residence_eleve, parent.resParent, this.currentEleve.sexe,
            this.currentEleve.statut, (parent.identifiantPortail && parent.identifiantPortail.length == 0)
              ? null : parent.identifiantPortail, !!this.transfertModel,

            echeancier, detailsTransfert, this.currentEleve.numeroEleve, '', parent.nomMere,
            (parent.telephoneMere && parent.telephoneMere.length == 0) ? null : parent.telephoneMere);
          this.genericsService.postResource(`${this.base_url}create`, dto).then((result: any) => {
            this.generateNextMatricule();
            if (result.success === true) {
              this.redirectAfterSuccess(result.data);
            }
            this.disabledButtonSave = false;
          }).catch(reason => {
            localStorage.getItem('activelang') === 'en'
              ? this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_MODIFICATIONEN, reason.message, 'error')
              : this.genericsService.confirmModalResponseAPI(AppConstants.FAIL_MODIFICATIONFR, reason.message, 'error');
            this.disabledButtonSave = false;
          });
        }
      }
    }
  }

  // redirection vers la nouvelle page apres finalisation des manipulation des infos eleve
  private redirectAfterSuccess(data: any) {
    this.router.navigate(['/eleves/liste-inscription/recap-eleve/' + data.id]).then(() => {
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Creation of a successful student registration form !', 'success')
        : this.genericsService.confirmResponseAPI('Creation fiche inscription élève reussi !', 'success');
      this.t.clear();
      this.disabledButtonSave = false;
      this.currentEleve = null;
      this.transfertModel = null;
      this.parentForm.reset();
      this.eleveForm.reset();
    });
  }
  // initialisation du formulaire array sur les echeanciers
  private initServiceForm() {
    this.servicesForm = this.fb.group({
      servicesData: new FormArray([])
    });
  }
  // initialisation du formulaire infos parent de l'eleve
  private initParentForm() {
    this.parentForm = this.fb.group({
      telephoneMere: ['', Validators.pattern('^6[0-9]{8}$')],
      nomMere: [''],
      nomParent: [''],
      telParent: ['', Validators.pattern('^6[0-9]{8}$')],
      emailParent: ['', Validators.email],
      residenceParent: [''],
      photo: ['']
    });
  }

  get p() { return this.parentForm.controls; }
  // Initialisation du formulaire infos personnell eleve
  private initEleveForm() {
    this.eleveForm = this.fb.group({
      nomEleve: ['', Validators.required],
      prenomEleve: [''],
      birthday: ['', Validators.required],
      sexeEleve: ['', Validators.required],
      classeEleve: ['', Validators.required],
      matriculeE: ['', Validators.required],
      statut: ['', Validators.required],
      residenceEleve: [''],
      handicape: [false],
      codeEtabProv: ['', Validators.maxLength(5)],
      nomEtabProv: [''],
      villeEtabProv: [''],
      niveauEleve: [''],
      montantRemise: [''],
      ancienMatricule: [''],
      motifte: [''],
      religion: ['', Validators.required],
      lieuNais: ['']
    });
  }
  // getter pour la gestion de contenu du formulaire eleve
  get elf() { return this.eleveForm.controls; }
  // Mise a jour des conrols de champs en fonction
  onChangeOptions() {
    if (this.isTransfert === true) {
      this.eleveForm.controls['codeEtabProv'].setValidators([Validators.required, Validators.min(1)]);
      this.eleveForm.controls['nomEtabProv'].setValidators([Validators.required, Validators.min(1)]);
      this.eleveForm.controls['niveauEleve'].setValidators([Validators.required, Validators.min(1)]);
      this.eleveForm.controls['montantRemise'].setValidators([Validators.required, Validators.min(1)]);
      this.eleveForm.controls['ancienMatricule'].setValidators([Validators.required, Validators.min(1)]);
      this.eleveForm.controls['matriculeE'].clearValidators();
    } else {
      this.eleveForm.controls['codeEtabProv'].clearValidators();
      this.eleveForm.controls['nomEtabProv'].clearValidators();
      this.eleveForm.controls['niveauEleve'].clearValidators();
      this.eleveForm.controls['montantRemise'].clearValidators();
      this.eleveForm.controls['ancienMatricule'].clearValidators();
      this.eleveForm.controls['matriculeE'].setValidators([Validators.required, Validators.min(1)]);
    }
    this.eleveForm.get('codeEtabProv').updateValueAndValidity();
    this.eleveForm.get('nomEtabProv').updateValueAndValidity();
    this.eleveForm.get('niveauEleve').updateValueAndValidity();
    this.eleveForm.get('montantRemise').updateValueAndValidity();
    this.eleveForm.get('ancienMatricule').updateValueAndValidity();
    this.eleveForm.get('ancienMatricule').updateValueAndValidity();
  }
  // Mise a jour de l'interface apres la selection d'un eleve de la plate forme
  onSelectCurrentEleve(eleve: any) {
    // this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, eleve.matricule);
    // this.onGetNiveauByClasse(eleve.classe.id);
    this.onGetOnEleve(eleve.matricule);
    // this.updateView(eleve);
  }

  // Liste du model echeancier par eleve sur une année
  private listEcheancierParEleve(annee: number, matricule: string) {
    this.canAddService = false;
    const url = `admin/echeancier/matricule/${matricule}/annee/${annee}/list`;
    this.genericsService.getResource(url).then((result: any) => {
      if (result.success === true) {
        this.t.clear();
        if (result.data.length == 0) {
          this.t.push(
            this.fb.group({
              service: [''],
              intitule: [],
              montant: [],
              dateEch: [this.calendar.getToday()],
              quantite:[],
              obligatoire: ['N']
            })
          );
        } else {
          result.data.forEach(elt => {
            this.t.push(
              this.fb.group({
                service: [elt.modeleEcheancier.id],
                intitule: [elt.modeleEcheancier.intitule],
                montant: [elt.montant_ech],
                dateEch: [elt.dateEch],
                quantite: [elt.quantite],
                obligatoire: [elt.modeleEcheancier.service.obligatoire]
                
              })
            );
            console.log('serges kamga',  result.data);
            
          });
          this.baseEcheancier = this.t.length;
        }
        this.echeanciersEleve = result.data;
        // this.modeleEcheanciersObligatoire = result.data;
      }
    });

  }


  // Mise a jour de l'interface apres une quelconque manoeuvre
  onSetDataInputToView() {
    if (this.reloadMatricule) {
      clearInterval(this.reloadMatricule);
    }
    this.submitted = true;
    if (this.eleveForm.invalid) {
      return;
    }
    console.log(this.elf.classeEleve.value);
    this.genericsService.getResource(`classes/${this.elf.classeEleve.value}/niveau`).then((result: any) => {
      this.listModelEcheancierNiveau(this.tokenStorage.getUser().codeAnnee, result.id);
      this.genericsService
        .getResource(`admin/modele-echeancier/annee/${this.tokenStorage.getUser().codeAnnee}/niveau/${result.id}/obligatoire`)
        .then((result1: any) => {
          this.modeleEcheanciersObligatoire = result1.data;
          console.log(this.modeleEcheanciersObligatoire);
          this.t.clear();
          if (result1.success === true) {
            if (result1.data.length == 0) {
              this.t.push(
                this.fb.group({
                  service: [''],
                  montant: [],
                  intitule: [],
                  quantite:[],
                  dateEch: [this.calendar.getToday()],
                  obligatoire: ['N']
                })
              );
            } else {
              this.modeleEcheanciersObligatoire.forEach(elt => {
                this.t.push(
                  this.fb.group({
                    service: [elt.id],
                    intitule: [elt.intitule],
                    montant: [elt.montant_a_ech],
                    dateEch: [elt.date_ech],
                    quantite:[elt.quantite],
                    obligatoire: [elt.service.obligatoire]
                   
                  })
                );
              });
            }
          }
        });
    });
    this.oldEleveClasse = this.currentEleve ? this.currentEleve.classe.id : null;
    this.classeDestination = this.elf.classeEleve.value;
    this.currentEleve = {
      matricule: this.isTransfert == true ? this.elf.ancienMatricule.value : this.elf.matriculeE.value,
      nomEleve: this.elf.nomEleve.value,
      prenomEleve: this.elf.prenomEleve.value,
      date_naissance: this.elf.birthday.value,
      residence_eleve: this.elf.residenceEleve.value,
      sexe: this.elf.sexeEleve.value,
      statut: this.elf.statut.value,
      handicape: this.elf.handicape.value,
      religion: this.elf.religion.value,
      numeroEleve: 0,
      residenceParent: '',
      transfert: this.isTransfert,
      classe: this.classes.find(elt => elt.id == this.elf.classeEleve.value),
      lieuNais: this.elf.lieuNais.value,
      logo: null
    };
    this.parentForm.patchValue({
      dossier: this.elf.matriculeE.value
    });
    if (this.isTransfert == true) {
      this.transfertModel = {
        codeEtabProv: this.elf.codeEtabProv.value,
        nomEtabProv: this.elf.nomEtabProv.value,
        villeEtabProv: this.elf.villeEtabProv.value,
        niveauEleve: this.elf.niveauEleve.value,
        montantRemise: this.elf.montantRemise.value,
        ancienMatricule: this.elf.ancienMatricule.value,
        motifte: this.elf.motifte.value
      };
    }
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    console.log(this.currentEleve);
  }
  // Generer le matricule disponble
  private generateNextMatricule() {
    this.genericsService.getResource(`${this.base_url}generer-matricule`).then((result: any) => {
      if (result.success === true) {
        this.eleveForm.patchValue({ matriculeE: result.data });
      }
    });
  }

  public isExist(elt: number) {
    const test = this.modeleEcheanciers.find(item => item.id == elt);
    if (test!==undefined) {
      return true;
    }else return false;
  }
  // mettre a jour le montant echeancier au changement de classe ou a l'ajout de l'echeance
  public setMontant(model: any, i: number) {
    //  const test = this.t.value.find( elt=> elt.service === model );
    const test = this.echeanciersEleve.find(elt => elt.modeleEcheancier.id === parseInt(model, 0));
    const test1 = this.t.value.filter(elt => parseInt(elt.service, 0) === parseInt(model, 0));
    // console.log(test,"Test : ",this.t.value.find((elt)=>{elt.service === model}))
    console.log(test1, 'AVANT : ' + model + ' ****GET init : ', this.t, '  ***GET ME : ', this.echeanciersEleve);

    if (test !== undefined || test1.length > 1) {
      localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.SERVICE_OPTIONNEL_EXISTEEN, 'error') :
        this.genericsService.confirmResponseAPI(AppConstants.SERVICE_OPTIONNEL_EXISTEFR, 'error');
    } else {
      this.genericsService.getResource(`modeleEcheanciers/${model}`).then((result: any) => {
        this.t.at(i).patchValue({
          intitule: result.intitule,
          montant: result.montant_a_ech,
          dateEch: result.date_ech,
          quantite: result.quantite
        });
        console.log(this.t.at(i), ' ****GET init : ', this.t.value, '  ***GET ME : ', this.echeanciersEleve);
        this.canAddService = true;
      });
    }
  }

  // recuperer la liste de classe
  private loadListClasses() {
    this.genericsService.getResource(`admin/classe/liste?size=${this.pageSizeClasse}&page=${this.pageClasse}&sortBy=nomClasse`)
      .then((response: any) => {
        this.classes = response.data.content;
        this.totalElements = response.data.totalElements;
      }, err => {
        console.log(err);
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmResponseAPI('Unknown error for class loading', 'error')
          : this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des classes', 'error');
      });
  }

  // fermer le formulaire de modification des infos personnels de l'utilisateur
  public onResetFormEleve() {
    this.initEleveForm();
    if (!this.idEleve) {
      this.canModifier = false;
      this.currentEleve = null;
    }
    this.initParentForm();
    this.initServiceForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  public onResetFormEleveModal() {
    /*this.initEleveForm();
    if (!this.idEleve) {
      this.canModifier = false;
      this.currentEleve = null;
    }
    this.initParentForm();
    this.initServiceForm();*/
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  // lire la liste de niveau backend
  private loadListNiveau() {
    this.genericsService.getResource(`niveaus?size=500`).then((result: any) => {
      this.niveaux = result._embedded.niveaus;
    });
  }

  // afficher le formulaire de modification des attributs personnels de l'élève
  onModifierInfo(eleve: any, addeleve: TemplateRef<any>) {
    if (eleve) {
      this.eleveForm.patchValue({
        nomEleve: eleve.nomEleve,
        prenomEleve: eleve.prenomEleve,
        birthday: eleve.date_naissance,
        sexeEleve: eleve.sexe,
        classeEleve: eleve.classe.id,
        matriculeE: eleve.matricule,
        statut: eleve.statut,
        residenceEleve: eleve.residence_eleve,
        handicape: eleve.handicape,
        religion: eleve.religion,
        lieuNais: eleve.lieu_naissance
      });
    }
    if (this.idEleve) {
      this.updateStudent = true;
    }
    this.modalService.open(addeleve,
      { ariaLabelledBy: 'add-eleve', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => {
    });
  }
  // destruction du contexte du composant
  ngOnDestroy(): void {
    this.onResetFormEleve();
    if (this.reloadMatricule) {
      clearInterval(this.reloadMatricule);
    }
  }

  getTs() {
    return this.timestamp;
  }

  onselectedFile(event: any) {
    this.selectedFiles = event.target.files;
  }

  uploadPhotoEleve(matricule: string) {
    this.progress = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    console.log(this.currentFileUpload);
    this.genericsService.uploadImage(this.currentFileUpload,
      `admin/upload/image/${matricule}/eleve`).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else {
          this.progress = undefined;
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmResponseAPI('Registered profile picture', 'success')
            : this.genericsService.confirmResponseAPI('Photo de profil enregistré', 'success');
          this.timestamp = Date.now();
          this.onGetOnEleve(this.idEleve);
          // location.reload();
        }
      },
      errorPhoto => {
        console.log(errorPhoto);
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmResponseAPI('Profile picture not taken into account', 'error')
          : this.genericsService.confirmResponseAPI('Photo de profil pas prise en compte', 'error');
      });
    this.selectedFiles = undefined;
  }

  onFilmerEleve(currentEleve: any, photoeleve: TemplateRef<any>) {
    this.modalService.open(photoeleve,
      { ariaLabelledBy: 'photo-eleve', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => { console.log(reason); });
  }

  onUpdateViewAfterTakePhoto(event: boolean) {
    console.log(event);
    if (event == true) {
      this.timestamp = Date.now();
      this.onGetOnEleve(this.idEleve);
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
      location.reload();
    } else {

    }
  }
}
