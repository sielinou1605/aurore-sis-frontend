/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ReglementRequestModel} from '../../../../../shared/_models/request-dto/finance/reglement-request-model';
import {AttributionBourseRequestModel} from '../../../../../shared/_models/request-dto/finance/attribution-bourse-request-model';
import {ConveertDateService} from '../../../../../shared/_helpers/conveert-date.service';
import {BourseModel} from '../../../../../shared/_models/response-dto/finance/bourse-model';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {CaisseModel} from '../../../../../shared/_models/response-dto/finance/caisse-model';
import {ModeReglementModel} from '../../../../../shared/_models/response-dto/finance/mode-reglement-model';
import {AuthService} from '../../../../../shared/_services/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-menu-inscription',
  templateUrl: './menu-inscription.component.html',
  styleUrls: ['./menu-inscription.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class MenuInscriptionComponent implements OnInit, OnDestroy {
  public scolariteForm: FormGroup;
  public bourseForm: FormGroup;
  public submitted: boolean;
  public isAuto = true;
  private base_url = 'admin/inscription/';
  private base_url_caisse = 'admin/caisse/';
  private base_url_mode_reglement = 'admin/mode-reglement/';
  public inscriptions: any;
  public echeanciersEleve: any;
  public modeReglements: ModeReglementModel[];
  public caisses: CaisseModel[];
  private base_url_bourse = 'admin/bourse/';
  public idEleve: string;
  public bourses: BourseModel[];
  public annees: any;
  searchForm: FormGroup;
  public currentInscription: any;
  public modeRgl: any;
  private defaultCaisse: any;
  validated: boolean;
  public resteAVerser: any;
  impressionReleveForm: FormGroup;
  page = 0;
  pageSize = 40;
  public totalElements = 0;
  private dataFilter = '';
  listInscriptionFiltre: any;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private router: Router, private modalService: NgbModal,
    config: NgbModalConfig, private fb: FormBuilder,
    private genericsService: GenericsService, private dateConvert: ConveertDateService,
    private calendar: NgbCalendar, private tokenStorage: TokenStorageService,
    public authService: AuthService,
    private translate: TranslateService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(13);
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.initScolariteForm();
    this.initBourseForm();
    this.onFilterInscription('');
    this.onLoadListCaisse();
    this.onLoadListModeReglement();
    this.onLoadListBourse();
  }

  private initImpressionReleveForm() {
    this.impressionReleveForm = this.fb.group({
      dateDebut: [this.calendar.getToday(), Validators.required],
      dateFin: ['', Validators.required]
    });
  }

  public onFilterInscription(event, reInitPage?: boolean) {
    if (reInitPage == true) {
      this.page = 0;
    }
    let predicat: string;
    if (event) {
      predicat = this.genericsService.cleanToken(event);
    } else {
      predicat = event;
    }

    console.log(predicat);
    this.dataFilter = predicat;
    this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(`admin/inscription/liste?predicat=${predicat}&size=${this.pageSize}&page=${this.page}`)
      .then((response: any) => {
        this.inscriptions = response.data.content;
        this.listInscriptionFiltre = response.data.content;
        this.totalElements = response.data.totalElements;
        console.log(this.inscriptions);
        this.genericsService.stopLoadingPage();
      }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      this.genericsService.confirmResponseAPI(reason.message, 'error');
    });
  }
  onreloadListInscription(event, reInitPage?: boolean) {
    /*if (reInitPage == true) {
      this.page = 0;
    }
    this.dataFilter = event;
    this.genericsService
      .getResource(`admin/inscription/liste?predicat=${event}&size=${this.pageSize}&page=${this.page}`)
      .then((response: any) => {
        this.listInscriptionFiltre = response.data.content;
      }).catch(reason => {
      console.log(reason);
    });*/
  }
  setPageChange(event) {
    console.log(event);
    this.page = event - 1;
    this.onFilterInscription(this.dataFilter);
  }
  onAjouterListeInscription() {
    if (this.authService.isActif('bouton-ajouter-liste-inscription')) {
      this.router.navigate(['']).then(() => {});
    }
  }
  onAttribuerBourseEleve(idEleve) {
    this.idEleve = idEleve;
    this.submitted = true;
    if (this.bourseForm.invalid) {
      return;
    }
    const b = this.bourses.find(elt => elt.id == this.b.bourse.value) as BourseModel;
    const bourseRequest = new AttributionBourseRequestModel(
      this.b.anneeAc.value, this.dateConvert.format(this.calendar.getToday()),
      null, b.id, b.intitule_bourse, idEleve, b.montant_bourse
    );
    this.genericsService.postResource(`admin/attribution-bourse/create`, bourseRequest).then(() => {
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmResponseAPI('Scholarship awarded successfully', 'success')
      : this.genericsService.confirmResponseAPI('Attribution de bourse effectué avec succes', 'success');
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
    });
  }

  onReglerInscription(idEleve: string) {
    this.submitted = true;
    this.validated = true;
    if (this.scolariteForm.invalid) {
      this.validated = false;
      return;
    }
    const idEcheancier = (this.isAuto === false) ? this.s.service.value : null;
    const reglement = new ReglementRequestModel(
      this.tokenStorage.getUser().codeAnnee, this.tokenStorage.getCaisse() ?
        this.tokenStorage.getCaisse().id : this.defaultCaisse.id,  this.modeRgl.id,
      idEleve, this.s.montantAVerser.value, this.isAuto, idEcheancier);
    this.genericsService.postResource(`admin/reglement/create`, reglement).then((result: any) => {
      this.router.navigate(['/eleves/liste-inscription/recap-eleve', this.currentInscription.idInscription]).then(() => {
        localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Payment made successfully', 'success')
        : this.genericsService.confirmResponseAPI('Paiement effectué avec succes', 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
        this.onResetScolarite();
        this.genericsService.reportPostResource(
          `admin/impression/reglement/${result.data.idReglement}/service/${result.data.idServiceImpression}`).then((response: any) => {
          this.genericsService.getByteArrayAndSaveReportPDF(response, 'Recu_' + idEleve + '_');
        });
      });
    });
  }

  private initScolariteForm() {
    this.scolariteForm = this.fb.group({
      montantAVerser: ['', [Validators.required, Validators.min(100)]],
      service: [''],
    });
  }

  private listEcheancierParEleve(annee: number, matricule: string) {
    this.genericsService.getResource(`admin/echeancier/matricule/${matricule}/annee/${annee}/list`).then((result: any) => {
      console.log(result);
      if (result.success === true) {
        this.echeanciersEleve = result.data;
      }
    });
  }

  onPayerFraisInscriptions(inscription: any, reglerfrais: TemplateRef<any>) {
    this.currentInscription = inscription;
    this.idEleve = inscription.matricule;
    this.submitted = false;
    if (this.authService.isActif('bouton-payer-frais-inscription-eleve')) {
      this.genericsService.getResource(`${this.base_url + inscription.idInscription}/get-recap`).then((result: any) => {
        if (result.success === true) {
          console.log(result);
          this.resteAVerser = result.data.resteGlobalApayer;
          if (this.resteAVerser > 0) {
            this.updateControls();
            this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, this.idEleve);
            this.modalService.open(reglerfrais, {ariaLabelledBy: 'add-bourse', size: 'lg'}).result.then((modal) => {
            }, (reason) => {});
          }
        }
      });
    }
  }


  
  onUpdateResteAverser() {
    this.genericsService.getResource(`admin/echeancier/${this.s.service.value}/get-details`).then((result: any) => {
      console.log(result);
      if (result.success === true) {
        this.resteAVerser = result.data.solde_ech;
        this.updateControls();
      }
    });
  }
  updateControls() {
    this.scolariteForm.patchValue({
      montantAVerser: this.resteAVerser
    });
    this.scolariteForm.controls['montantAVerser'].clearValidators();
    this.scolariteForm.controls['montantAVerser']
      .setValidators([Validators.required, Validators.min(1), Validators.max(this.resteAVerser)]);
    this.scolariteForm.get('montantAVerser').updateValueAndValidity();
  }

  onOpenModalBourseEleve(matricule: string, attribuerbourse: TemplateRef<any>) {
    this.idEleve = matricule;
    // this.onLoadListAnnees();
    this.submitted = false;
    this.modalService.open(attribuerbourse,
      {ariaLabelledBy: 'attribuer-bourse'}).result.then((result) => {
    }, (reason) => {});
  }

  private onLoadListModeReglement() {
    this.genericsService.getResource(`${this.base_url_mode_reglement}list`).then((result: any) => {
      if (result.success === true) {
        this.modeReglements = result.data;
        this.modeRgl = result.data[0];
      }
    });
  }

  private onLoadListCaisse() {
    this.genericsService.getResource(`${this.base_url_caisse}list`).then((result: any) => {
      if (result.success === true) {
        this.caisses = result.data;
        this.defaultCaisse = result.data[0];
      }
    });
  }
  private onLoadListBourse() {
    this.genericsService.getResource(`${this.base_url_bourse}list`).then((result: any) => {
      if (result.success === true) {
        this.bourses = result.data;
      }
    });
  }

  onChangeOptions() {
    if (this.isAuto === false) {
      if (this.s.service.value > 0) {
        this.onUpdateResteAverser();
      }
      this.scolariteForm.controls['service'].setValidators([Validators.required, Validators.min(1)]);
    } else {
      this.genericsService.getResource(`${this.base_url + this.currentInscription.idInscription}/get-recap`).then((result: any) => {
        if (result.success === true) {
          this.resteAVerser = result.data.resteGlobalApayer;
          if (this.resteAVerser > 0) {
            this.updateControls();
          }
        }
      });
      this.scolariteForm.controls['service'].clearValidators();
    }
    this.scolariteForm.get('service').updateValueAndValidity();
  }

  private initBourseForm() {
    this.bourseForm = this.fb.group({
      bourse: ['', Validators.required],
      anneeAc: [this.tokenStorage.getUser().codeAnnee, Validators.required],
    });
  }

  get s() { return this.scolariteForm.controls; }

  onResetScolarite() {
    this.submitted = false;
    this.validated = false;
    this.isAuto = true;
    this.initScolariteForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  get b() { return this.bourseForm.controls; }

  onResetBourse() {
    this.submitted = false;
    this.bourseForm.reset();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onInscrireEleve(inscription: any) {
    this.idEleve = inscription.matricule;
    if (this.authService.isActif('bouton-dossier-inscription-eleve')) {
      this.router.navigate(['/eleves/liste-inscription/gestion-inscriptions/'
      + this.idEleve + '/' + inscription.idInscription]).then(() => {});

      // const url = this.router.serializeUrl(
      //   this.router.createUrlTree(['/eleves/liste-inscription/gestion-inscriptions/'
      //   + this.idEleve + '/' + inscription.idInscription])
      // );
      // console.log(url);
      // window.open(url, '_blank');
    }

  }
  // private onLoadListAnnees() {
  //   this.genericsService.getResource(`admin/annees/encours?size=50`).then((result: any) => {
  //     this.bourseForm = this.fb.group({
  //       bourse: ['', Validators.required],
  //       anneeAc: [result.data[0].code_annee, Validators.required],
  //     });
  //   });
  // }

  onInscrireNouvelEleve() {
    if (this.authService.isActif('bouton-inscrire-eleve')) {
      this.router.navigate(['/eleves/liste-inscription/gestion-inscriptions' ]).then(() => {});
    }
  }
  onOpenRecapPage(inscription: any) {
    if (inscription.anneeAcademique) {
      this.router.navigate(['/eleves/liste-inscription/recap-eleve/' + inscription.idInscription]).then(() => {
        localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Full upload of student file !!!', 'success')
        : this.genericsService.confirmResponseAPI('Chargement complet fiche étudiant !!!', 'success');
      });
    }
  }
  get search() { return this.searchForm.controls; }

  onImpressionEtatFinancier(idInscription: any, matricule) {
    this.genericsService.reportPostResource(`admin/impression/inscription/${idInscription}/etat-financier`).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Etat_Financier_' + matricule);
    });
  }

  openFormToImportListEleve(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'add-list-eleve', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
      console.log(reason);
    });
  }
  onUploadListEleve() {

  }

  onResetUploadFile() {

  }

  onUploadList() {

  }

  onResetImport() {

  }
  get f() { return this.impressionReleveForm.controls; }
  onReset() {
    this.initImpressionReleveForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  ngOnDestroy(): void {
    this.onResetScolarite();
    this.onResetBourse();
    this.onResetImport();
    this.onResetUploadFile();
  }

  onDeleteInscription(inscription: any) {
    if (this.authService.isActif('bouton-suprimer-inscription')) {
      Swal.fire({
        title: localStorage.getItem('activelang') === 'en' ? 'Are you sure ?' : 'Etes vous sure ?',
        // tslint:disable-next-line:max-line-length
        text: localStorage.getItem('activelang') === 'en' ? 'The removal of ' + inscription.nomEleve + ' is irreversible !' : 'La suppression de ' + inscription.nomEleve + ' est irreversible !',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: localStorage.getItem('activelang') === 'en' ? 'Yes, Delete!' : 'Oui, Supprimer!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.getResource(`admin/inscription/${inscription.idInscription}/supprimable`).then((response: any) => {
            console.log(response);
            if (response.data == true) {
              Swal.fire({
                // tslint:disable-next-line:max-line-length
                title: localStorage.getItem('activelang') === 'en' ? inscription.nomEleve + ' ' + inscription.prenomEleve + ' to already pay money !' : inscription.nomEleve + ' ' + inscription.prenomEleve + ' à déjà verser de l\'argent !',
                html: '<em>Rassurez-vous que cet acte de suppression est légitime '
                  + ' cette opération etant irreversible. </em>' +
                  '<span class="text-bold text-danger">Vous risquez de ne plus le voir parmis les inscrits</span> ',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: localStorage.getItem('activelang') === 'en' ? 'Yes, Delete!' : 'Oui, Supprimer!'
              }).then((result1) => {
                if (result1.isConfirmed) {
                  this.onProcessDeleleInscription(inscription);
                }
              });
            } else {
              this.onProcessDeleleInscription(inscription);
            }
          }).catch(reason => { console.log(reason); });
        }
      });
    }
  }

  onProcessDeleleInscription(inscription) {
    this.genericsService.deleteResource(`admin/inscription/${inscription.idInscription}/delete`).then((response: any) => {
      this.onFilterInscription(this.dataFilter);
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmModalResponseAPI(response.message, 'Delete!', 'success')
      : this.genericsService.confirmModalResponseAPI(response.message, 'Supprimé!', 'success');
    }).catch(reason => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en'
     ? this.genericsService.confirmModalResponseAPI('The registration has not been deleted.', 'Failure!', 'error')
      : this.genericsService.confirmModalResponseAPI('L\'inscription n\'a pas été supprimé.', 'Echec!', 'error');
    });
  }
}
