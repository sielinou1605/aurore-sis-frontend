/* tslint:disable:triple-equals */
import {Component, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ConveertDateService} from '../../../../../shared/_helpers/conveert-date.service';
import {ReglementRequestModel} from '../../../../../shared/_models/request-dto/finance/reglement-request-model';
import {BourseModel} from '../../../../../shared/_models/response-dto/finance/bourse-model';
import {AttributionBourseRequestModel} from '../../../../../shared/_models/request-dto/finance/attribution-bourse-request-model';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {CaisseModel} from '../../../../../shared/_models/response-dto/finance/caisse-model';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-etat-financier-eleve',
  templateUrl: './etat-financier-eleve.component.html',
  styleUrls: ['./etat-financier-eleve.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class EtatFinancierEleveComponent implements OnInit {
  public idInscription: string;
  private base_url = 'admin/inscription/';
  public inscription: any;
  public scolariteForm: FormGroup;
  public bourseForm: FormGroup;
  public dispenseForm: FormGroup;
  public submitted: boolean;
  public isAuto = true;
  private base_url_caisse = 'admin/caisse/';
  private base_url_mode_reglement = 'admin/mode-reglement/';
  private base_url_bourse = 'admin/bourse/';
  public caisses: CaisseModel[];
  public bourses: BourseModel[];
  public modeReglements: any;
  public echeanciersEleve: any;
  public modeRgl: any;
  private defaultCaisse: any;
  validated: boolean;
  resteAVerser = null;
  public services: any;
  public urlImage  = AppConstants.API_URL + 'admin/recuperation/image/';
  private timestamp: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private router: Router, private modalService: NgbModal,
    config: NgbModalConfig, private fb: FormBuilder,
    public genericsService: GenericsService, private dateConvert: ConveertDateService,
    private calendar: NgbCalendar, private route: ActivatedRoute, private tokenStorage: TokenStorageService,
    public authService: AuthService,
    private translate: TranslateService
  ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(57);
  }

  ngOnInit() {
    this.initBourseForm();
    this.initDispenseForm();
    this.initScolariteForm();
    this.route.paramMap.subscribe(params => {
      this.idInscription = params.get('idInscription');
      console.log(this.idInscription);
      this.onLoadRecapInscription(this.idInscription);
      this.onLoadListBourse();
      this.onLoadListCaisse();
      this.onLoadListModeReglement();
      this.onLoadListService();
      this.timestamp = Date.now();
    });
  }

  private onLoadRecapInscription(idInscription: string) {
    this.genericsService.getResource(`${this.base_url + idInscription}/get-recap`).then((result: any) => {
      if (result.success === true) {
        this.inscription = result.data;
        console.log(this.inscription);
        this.resteAVerser = result.data.resteGlobalApayer;
        this.initDispenseForm(this.resteAVerser);
        this.updateControls();
        this.scolariteForm.controls['montantAVerser'].clearValidators();
        this.scolariteForm.controls['montantAVerser']
          .setValidators([Validators.required, Validators.min(100), Validators.max(this.inscription.resteGlobalApayer)]);
        this.scolariteForm.get('montantAVerser').updateValueAndValidity();
      }
    });
  }
  onPayerFraisInscriptions(reglerfrais: TemplateRef<any>) {
    this.submitted = false;
    this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, this.inscription.eleve.matricule);
    this.modalService.open(reglerfrais,
      {ariaLabelledBy: 'add-bourse', size: 'lg'}).result.then((result) => {
    }, (reason) => {});
  }
  onOpenModalBourseEleve(attribuerbourse: TemplateRef<any>) {
    this.submitted = false;
    this.modalService.open(attribuerbourse,
      {ariaLabelledBy: 'attribuer-bourse'}).result.then((result) => {
    }, (reason) => {});
  }
  get b() { return this.bourseForm.controls; }
  get dis() { return this.dispenseForm.controls; }
  get s() { return this.scolariteForm.controls; }
  onResetScolarite() {
    this.submitted = false;
    this.validated = false;
    this.isAuto = true;
    this.initScolariteForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    if (this.inscription) {
      this.updateControls();
      this.resteAVerser = this.inscription.resteGlobalApayer;
      this.scolariteForm.patchValue({
        montantAVerser: this.resteAVerser
      });
    }
  }
  onResetBourse() {
    this.submitted = false;
    this.initBourseForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  updateControls() {
    this.scolariteForm.patchValue({
      montantAVerser: this.resteAVerser
    });
    this.scolariteForm.controls['montantAVerser'].clearValidators();
    this.scolariteForm.controls['montantAVerser']
      .setValidators([Validators.required, Validators.min(1), Validators.max(this.resteAVerser)]);
    this.scolariteForm.get('montantAVerser').updateValueAndValidity();
  }
  onUpdateResteAverser() {
    this.genericsService.getResource(`admin/echeancier/${this.s.service.value}/get-details`).then((result: any) => {
      if (result.success === true) {
        this.resteAVerser = result.data.solde_ech;
        this.updateControls();
      }
    });
  }
  onChangeOptions() {
    if (this.isAuto === false) {
      if (this.s.service.value > 0) {
        this.onUpdateResteAverser();
      }
      this.scolariteForm.controls['service'].setValidators([Validators.required, Validators.min(1)]);
    } else {
      this.resteAVerser = this.inscription.resteGlobalApayer;
      this.scolariteForm.patchValue({
        montantAVerser: this.resteAVerser
      });
      this.scolariteForm.controls['service'].clearValidators();
    }
    this.scolariteForm.get('service').updateValueAndValidity();

  }
  private initBourseForm() {
    this.bourseForm = this.fb.group({
      bourse: ['', Validators.required],
      anneeAc: [this.tokenStorage.getUser().codeAnnee, Validators.required],
    });
  }

  private initDispenseForm(reste?: any) {
    if (this.inscription) {
      const maxDispense = reste && reste > 0 ? (reste * 2) / 3 : 0;
      this.dispenseForm = this.fb.group({
        montantDispense: [0, [Validators.required, Validators.max(maxDispense)]]
      });
    }
  }
  private initScolariteForm() {
    this.scolariteForm = this.fb.group({
      montantAVerser: ['', [Validators.required, Validators.min(100)]],
      service: [''],
    });
  }

  onAttribuerDispenseEleve() {
    this.submitted = true;
    this.validated = true;
    if (this.dispenseForm.invalid) {
      this.validated = false;
      return;
    }
    const url = `admin/dispense/matricule/${this.inscription.eleve.matricule}/montant/${this.dis.montantDispense.value}/appliquer`;
    this.genericsService.postResource(url, {}).then((result: any) => {
      this.validated = false;
      this.onLoadRecapInscription(this.idInscription);
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmResponseAPI('Exemption granted successfully', 'success')
      : this.genericsService.confirmResponseAPI('Attribution de la dispense effectué avec succes', 'success');
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
    }).catch(reason => {
      this.validated = false;
    });
  }
  private onLoadListModeReglement() {
    this.genericsService.getResource(`${this.base_url_mode_reglement}list`).then((result: any) => {
      if (result.success === true) {
        this.modeReglements = result.data;
        this.modeRgl = result.data[0];
      }
    });
  }
  private onLoadListCaisse() {
    this.genericsService.getResource(`${this.base_url_caisse}list`).then((result: any) => {
      if (result.success === true) {
        this.caisses = result.data;
        this.defaultCaisse = result.data[0];
      }
    });
  }
  private onLoadListBourse() {
    this.genericsService.getResource(`${this.base_url_bourse}list`).then((result: any) => {
      if (result.success === true) {
        this.bourses = result.data;
      }
    });
  }
  private listEcheancierParEleve(id: number, matricule: any) {
    this.genericsService.getResource(`admin/echeancier/matricule/${matricule}/annee/${id}/list`).then((result: any) => {
      if (result.success === true) {
        this.echeanciersEleve = result.data;
      }
    });
  }
  onReglerInscription() {
    console.log('register');
    this.submitted = true;
    this.validated = true;
    if (this.scolariteForm.invalid) {
      this.validated = false;
      return;
    }
    const idEcheancier = (this.isAuto === false) ? this.s.service.value : null;
    const reglement = new ReglementRequestModel(
      this.tokenStorage.getUser().codeAnnee, this.tokenStorage.getCaisse() ?
        this.tokenStorage.getCaisse().id : this.defaultCaisse.id,  this.modeRgl.id,
      this.inscription.eleve.matricule, this.s.montantAVerser.value, this.isAuto, idEcheancier);
    this.genericsService.postResource(`admin/reglement/create`, reglement).then((result: any) => {
      this.onLoadRecapInscription(this.idInscription);
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmResponseAPI('Payment made successfully', 'success')
      : this.genericsService.confirmResponseAPI('Paiement effectué avec succes', 'success');
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
      this.onResetScolarite();
      console.log(result.data);
      this.onImprimerRecuGlobal(result.data, result.data.idServiceImpression, true);
    });
  }
  onImprimerRecuGlobal(reglement: any, service: number, withApe: boolean) {
    console.log(reglement);
    console.log(service);
    console.log(this.inscription);
    this.genericsService.reportPostResource(
      `admin/impression/reglement/${reglement.idReglement}/service/${service}?withApe=${withApe}`
    ).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Recu_' + this.inscription.eleve.matricule + ' ');
    }).catch(reason => {
      localStorage.getItem('activelang') === 'en'
      ? Swal.fire('Echec!', 'Receipt unavailable!!!', 'error')
      : Swal.fire('Echec!', 'Recu insdiponible!!!', 'error');
    });
  }

  onAttribuerBourseEleve() {
    this.submitted = true;
    this.validated = true;
    if (this.bourseForm.invalid) {
      this.validated = false;
      return;
    }
    const b = this.bourses.find(elt => elt.id == this.b.bourse.value) as BourseModel;
    const bourseRequest = new AttributionBourseRequestModel(
      this.b.anneeAc.value, this.dateConvert.format(this.calendar.getToday()),
      null, b.id, b.intitule_bourse, this.inscription.eleve.matricule, b.montant_bourse
    );
    this.genericsService.postResource(`admin/attribution-bourse/create`, bourseRequest).then((result: any) => {
      this.validated = false;
      this.onLoadRecapInscription(this.idInscription);
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmResponseAPI('Scholarship awarded successfully', 'success')
      : this.genericsService.confirmResponseAPI('Attribution de bourse effectué avec succes', 'success');
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
    }).catch(reason => {
      this.validated = false;
    });
  }
  onImpressionRecu(reglement: any) {
    this.genericsService.reportPostResource(`admin/impression/reglement/${reglement.idReglement}/eleve`).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Recu_Versement_' + reglement.service);
    });
  }
  private onLoadListService() {
    this.genericsService.getResource(`admin/inscription/${this.idInscription}/service-regle`).then((result: any) => {
      this.services = result.data;
      console.log(this.services);
    });
  }

  onImpressionEtatFinancier(matricule: any) {
    this.genericsService.reportPostResource(`admin/impression/inscription/${this.idInscription}/etat-financier`).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Etat_Financier_' + matricule);
    });
  }

  getTs() {
    return this.timestamp;
  }
}
