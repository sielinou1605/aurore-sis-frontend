import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EtatFinancierEleveComponent} from './etat-financier-eleve.component';

describe('EtatFinancierEleveComponent', () => {
  let component: EtatFinancierEleveComponent;
  let fixture: ComponentFixture<EtatFinancierEleveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtatFinancierEleveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatFinancierEleveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
