import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MenuInscriptionComponent} from './menu-inscription/menu-inscription.component';
import {ListeInscriptionComponent} from './liste-inscription.component';
import {GestionInscriptionsComponent} from './gestion-inscriptions/gestion-inscriptions.component';
import {EtatFinancierEleveComponent} from './etat-financier-eleve/etat-financier-eleve.component';

const routes: Routes = [
  {
    path: '', component: ListeInscriptionComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Inscriptions',
      titre: 'userEleveAjaout.Inscriptions_Aurore',
     // titre: localStorage.getItem('activelang') === 'en' ? 'Inscription | Aurore' : 'Inscriptions | Aurore',
      icon: 'icofont-users-alt-5 bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_Inscription_Inscriptions',
      status: true,
      current_role: 'eleve'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome', component: MenuInscriptionComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Inscriptions',
          titre: localStorage.getItem('activelang') === 'en' ? 'Inscription | Aurore' : 'Inscriptions | Aurore',
          icon: 'icofont-file bg-c-pink',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_inscriptions_Inscriptions',
          status: true,
          current_role: 'eleve'
        }
      },
      {
        path: 'gestion-inscriptions', component: GestionInscriptionsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Gestion_des_inscriptions',
         // titre: 'userEleveAjaout.Gestion_des_inscriptions_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Registration management | Aurore' : 'Gestion des inscriptions | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gerer_ici_les_dossiers_d_inscriptions_d_un_élève_Gest_Inscriptions',
          status: true,
          current_role: 'eleve'
        }
      },
      {
        path: 'gestion-inscriptions/:idEleve', component: GestionInscriptionsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Gestion_des_inscriptions',
          // titre: 'userEleveAjaout.Gestion_des_inscriptions_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Registration management | Aurore' : 'Gestion des inscriptions | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gerer_ici_les_dossiers_d_inscriptions_d_un_élève_Gest_Inscriptions',
          status: true,
          current_role: 'eleve'
        }
      },
      {
        path: 'gestion-inscriptions/:idEleve/:idInscription', component: GestionInscriptionsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Gestion_des_inscriptions',
          // titre: 'userEleveAjaout.Gestion_des_inscriptions_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Registration management | Aurore' : 'Gestion des inscriptions | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gerer_ici_les_dossiers_d_inscriptions_d_un_élève_Gest_Inscriptions',
          status: true,
          current_role: 'eleve'
        }
      },
      {
        path: 'recap-eleve/:idInscription', component: EtatFinancierEleveComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Etat_financier_de_l_élève',
          // titre: 'userEleveAjaout.Etat_financier_de_l_élève_Aurore',
          // tslint:disable-next-line:max-line-length
          titre: localStorage.getItem('activelang') === 'en' ? 'Student\'s financial status | Aurore' : 'Etat financier de l\'élève | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          // tslint:disable-next-line:max-line-length
          breadcrumb_caption: 'userEleveAjaout.Visualiser_ici_un_recapitulatif_des_paiements_élèves_pour_une_année_cible_Etat_financier_de_l_élève',
          status: true,
          current_role: 'eleve'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListeInscriptionRoutingModule { }
