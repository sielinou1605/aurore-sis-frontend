import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ListeInscriptionRoutingModule} from './liste-inscription-routing.module';
import {ListeInscriptionComponent} from './liste-inscription.component';
import {MenuInscriptionComponent} from './menu-inscription/menu-inscription.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../../shared/shared.module';
import {GestionInscriptionsComponent} from './gestion-inscriptions/gestion-inscriptions.component';
import {EtatFinancierEleveComponent} from './etat-financier-eleve/etat-financier-eleve.component';
import {AvatarModule} from 'ngx-avatar';
import {NgxPaginationModule} from 'ngx-pagination';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../../../app.module';
import {HttpClient} from '@angular/common/http';


@NgModule({
  declarations: [
    ListeInscriptionComponent,
    MenuInscriptionComponent,
    GestionInscriptionsComponent,
    EtatFinancierEleveComponent
  ],
    imports: [
        CommonModule,
        ListeInscriptionRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        AvatarModule,
        NgxPaginationModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    ]
})
export class ListeInscriptionModule { }
