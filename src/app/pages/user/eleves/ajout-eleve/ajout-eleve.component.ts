/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {
  InscriptionRequestModel,
  ServiceInscriptionRequestModel
} from '../../../../shared/_models/request-dto/finance/inscription-request-model';
import {ConveertDateService} from '../../../../shared/_helpers/conveert-date.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../shared/_services/auth.service';
import Stepper from 'bs-stepper';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-ajout-eleve',
  templateUrl: './ajout-eleve.component.html',
  styleUrls: ['./ajout-eleve.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class AjoutEleveComponent implements OnInit, OnDestroy {
  public ajoutEleveForm: FormGroup;
  private base_url = 'admin/inscription/';
  public submitted: boolean;
  public classes: any;
  public inscriptionRequest: InscriptionRequestModel;
  public activeButton = true;
  private reloadMatricule: any;
  public pageSizeClasse = 500;
  public pageClasse = 0;
  public totalElements: number;
  private stepper: Stepper;
  public modeleEcheanciersObligatoire: any;
  public modeleEcheanciersOptionel: any;
  public listServiceEleve: ServiceInscriptionRequestModel[] = [];

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(private genericsService: GenericsService, private fb: FormBuilder,
              private dateConvert: ConveertDateService, private router: Router,
              private tokenStorage: TokenStorageService, public authService: AuthService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(4);
  }

  next() {
    console.log('ajouter eleve');
    this.stepper.next();
  }

  initStepper() {
    this.stepper = new Stepper(document.querySelector('#stepper1'), {
      linear: false,
      animation: true
    });
  }

  ngOnInit() {
    this.initStepper();
    this.initFormAjoutEleve();
    this.loadListClasses();
    this.generateNextMatricule();
    // this.reloadMatricule = setInterval(() => {
    //   this.generateNextMatricule();
    // }, 3000);
  }

  private initFormAjoutEleve() {
    this.ajoutEleveForm = this.fb.group({
      nomEleve: ['', [Validators.required]],
      prenomEleve: [''],
      classeEleve: ['', [Validators.required]],
      matricule: ['', [Validators.required]],
      birthday: ['', [Validators.required]],
      lieuNais: ['', [Validators.required]],
      sexe: ['', Validators.required],
      statut: ['', Validators.required],
      religion: ['', Validators.required],
      telEleve: ['', Validators.pattern('^6[0-9]{8}$')],
      resEleve: [''],
      nationalite: [''],
      handicape: [false],
      nomParent: [''],
      emailParent: ['', Validators.email],
      resParent: [''],
      telParent: ['', [Validators.required, Validators.pattern('^6[0-9]{8}$')]],
      nomMere: [''],
      telephoneMere: ['', [Validators.required, Validators.pattern('^6[0-9]{8}$')]]
    });
  }
  get f() { return this.ajoutEleveForm.controls; }

  onAjoutEleve() {
    this.activeButton = false;
    this.submitted = true;
    if (this.ajoutEleveForm.invalid) {
      this.activeButton = true;
      this.initStepper();
      return;
    }
    if (this.listServiceEleve.length > 0) {
      this.modeleEcheanciersObligatoire.forEach(elt => {
        this.listServiceEleve.push(new ServiceInscriptionRequestModel(elt.date_ech, elt.id, elt.montant_a_ech));
      });
    }
    this.inscriptionRequest = new InscriptionRequestModel(
      this.f.birthday.value, this.f.emailParent.value, this.f.handicape.value,
      this.tokenStorage.getUser().codeAnnee, this.f.classeEleve.value, this.f.lieuNais.value ? this.f.lieuNais.value : '',
      this.f.matricule.value, this.f.nationalite.value ? this.f.nationalite.value : '', this.f.nomEleve.value,
      this.f.nomParent.value, null, '', this.f.prenomEleve.value,
      this.f.religion.value, this.f.resEleve.value, this.f.resParent.value,
      this.f.sexe.value, this.f.statut.value, (this.f.telParent.value.length == 0 ? null : this.f.telParent.value),
      false, this.listServiceEleve, null,
      (this.f.telEleve.value.length == 0 ? null : this.f.telEleve.value), '', this.f.nomMere.value,
      (this.f.telephoneMere.value.length == 0 ? null : this.f.telephoneMere.value));
    this.genericsService.postResource(`${this.base_url}create`, this.inscriptionRequest).then((result: any) => {
      this.inscriptionRequest = null;
      this.initFormAjoutEleve();
      this.generateNextMatricule();
      if (result.success === true) {
        this.router.navigate(['/eleves/liste-inscription/welcome']).then(() => {
          if (this.reloadMatricule) {
            clearInterval(this.reloadMatricule);
          }
          this.genericsService.confirmResponseAPI('Ajout de l\'élève reussi !', 'success');
        });
      } else  {
        this.genericsService.confirmModalResponseAPI('Echec de l\'ajout de l\'élève', result.message, 'error');
      }
    }).catch(reason =>  this.genericsService.confirmModalResponseAPI('Echec de l\'ajout de l\'élève', reason.error.message, 'error'));
  }

  private loadListClasses() {
    this.genericsService.getResource(`admin/classe/liste?size=${this.pageSizeClasse}&page=${this.pageClasse}&sortBy=nomClasse`)
      .then((response: any) => {
        this.classes = response.data.content;
        this.totalElements = response.data.totalElements;
      }, err => {
        console.log(err);
        this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des classes', 'error');
      });
  }

  private generateNextMatricule() {
    this.genericsService.getResource(`${this.base_url}generer-matricule`).then((result: any) => {
      if (result.success === true) {
        this.ajoutEleveForm.patchValue({ matricule: result.data });
      } else {
      }
    });
  }

  public listModelEcheancierClasse(idClasse: any) {
    this.genericsService.getResource(
      `admin/modele-echeancier/annee/${(this.tokenStorage.getUser().codeAnnee)}/classe/${idClasse}/list`
    ).then((result: any) => {
      if (result.success === true) {
        this.modeleEcheanciersObligatoire = result.data.filter(elt => elt.service.obligatoire == 'O');
        this.modeleEcheanciersOptionel = result.data.filter(elt => elt.service.obligatoire != 'O');
      }
      console.log(this.modeleEcheanciersObligatoire);
      console.log(this.modeleEcheanciersOptionel);
    });
  }

  onEleveCheckboxChange(event: any) {
    const modele: any = this.modeleEcheanciersOptionel.find((modeleEcheancier) => modeleEcheancier.id == event.target.value);
    const service: ServiceInscriptionRequestModel = new ServiceInscriptionRequestModel(modele.date_ech, modele.id, modele.montant_a_ech);
    const i = this.listServiceEleve.findIndex((mat) => mat.idModeleEcheancier == event.target.value);
    if (event.target.checked) {
      if (i == -1) {
        this.listServiceEleve.push(service);
      }
    } else {
      if (i > -1) {
        this.listServiceEleve.splice(i, 1);
      }
    }
  }

  ngOnDestroy(): void {
    if (this.reloadMatricule) {
      // clearInterval(this.reloadMatricule);
    }
  }
}
