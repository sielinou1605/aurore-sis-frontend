import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MenuElevesComponent} from './menu-eleves/menu-eleves.component';
import {ListeEleveComponent} from './liste-eleve.component';


const routes: Routes = [
  {
    path: '', component: ListeEleveComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Liste_Eleve',
     // titre: 'userEleveAjaout.Liste_Eleve_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Student List | Aurore' : 'Liste Eleve | Aurore',
      icon: 'icofont-users-alt-5 bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_eleves_Eleves',
      status: true,
      current_role: 'eleve'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome', component: MenuElevesComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Liste_Eleve',
          // titre: 'userEleveAjaout.Liste_Eleve_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Student List | Aurore' : 'Liste Eleve | Aurore',
          icon: 'icofont-file bg-c-pink',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_eleves_Eleves',
          status: true,
          current_role: 'eleve'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListeEleveRoutingModule { }
