import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ListeEleveRoutingModule} from './liste-eleve-routing.module';
import {ListeEleveComponent} from './liste-eleve.component';
import {SharedModule} from '../../../../shared/shared.module';
import {MenuElevesComponent} from './menu-eleves/menu-eleves.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../../../app.module';
import {HttpClient} from '@angular/common/http';


@NgModule({
  declarations: [ListeEleveComponent, MenuElevesComponent],
    imports: [
        CommonModule,
        ListeEleveRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    ]
})
export class ListeEleveModule { }
