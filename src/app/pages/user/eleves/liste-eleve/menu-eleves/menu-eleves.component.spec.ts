import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MenuElevesComponent} from './menu-eleves.component';

describe('MenuElevesComponent', () => {
  let component: MenuElevesComponent;
  let fixture: ComponentFixture<MenuElevesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuElevesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuElevesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
