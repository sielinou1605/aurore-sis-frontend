/* tslint:disable:triple-equals quotemark */
import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {UpdateInscriptionModel} from '../../../../../shared/_models/request-dto/finance/inscription-request-model';
import {TranslateService} from '@ngx-translate/core';
import { AppConstants } from '../../../../../shared/element/app.constants';

@Component({
  selector: 'app-menu-eleves',
  templateUrl: './menu-eleves.component.html',
  styleUrls: ['./menu-eleves.component.scss']
})
export class MenuElevesComponent implements OnInit, OnDestroy {
  private base_url = 'admin/eleve/';
  public searchForm: FormGroup;
  public listEleve: any[] = [];
  public inscriptionEleveForm: FormGroup;
  public classes: any;
  public modeleEcheanciersParClasse = [];
  public montantMinimalParAnnee = [];
  public montantTotalEcheancier: number;
  public submitted: boolean;
  public listMatricule = [];
  allSelectEleve: any;
  public currentClasseName: any;
  public pageSize = 40;
  public page = 0;
  public totalElements: number;
  private dataFilter: any;
  public listEleveFiltre: any;
  private subscription: any;
  public eleve: string;
  // @ts-ignore
  @ViewChild('elevesSearch') elevesSearch: ElementRef;
  public currentEleve: any;
  private echeanciers: any[];
  public institution: any;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(private genericsService: GenericsService, private fb: FormBuilder, config: NgbModalConfig,
              private modalService: NgbModal, private calendar: NgbCalendar,
              private router: Router, private tokenStorage: TokenStorageService,
              public authService: AuthService, private translate: TranslateService) {
    localStorage.getItem('activelang') === 'en' ? this.eleve = "The students" : this.eleve = "Les eleves";
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(8);
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.onLoadListEleves('');
    this.onLoadListClasse();
    this.initFormInscription();
    this.onloadListInstitutions();
    this.onLoadListMontantMinimal(this.tokenStorage.getUser().codeAnnee);
  }

  get search() { return this.searchForm.controls; }
  get f() { return this.inscriptionEleveForm.controls; }
  public onLoadListEleves(event, reInitPage?: boolean) {
    if (reInitPage == true) {
      this.page = 0;
    }
    let predicat: string;
    if (event) {
      predicat = this.genericsService.cleanToken(event);
    } else {
      predicat = event;
    }
    this.dataFilter = predicat;
    this.genericsService.startLoadingPage();
    this.listMatricule = [];
    this.genericsService
      .getResource(`${this.base_url}liste?predicat=${predicat}&size=${this.pageSize}&page=${this.page}`).then((result: any) => {
      if (result.success === true) {
        this.listEleve = result.data.content;
        this.listEleveFiltre = result.data.content;
        this.totalElements = result.data.totalElements;
        console.log(this.listEleveFiltre);
      }
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en'
        // tslint:disable-next-line:max-line-length
        ? this.genericsService.confirmResponseAPI('Unknown error !!! ', 'error')
        : this.genericsService.confirmResponseAPI('Erreur inconnue !!! ', 'error');
    });
  }

  public onReloadListEleves(event, reInitPage?: boolean) {
    /* if (reInitPage == true) {
      this.page = 0;
    }
    const params = `predicat=${event}&size=${this.pageSize}&page=${this.page}`;
    this.genericsService
      .getResource(`${this.base_url}liste?${params}`).then((result: any) => {
      if (result.success === true) {
        this.listEleveFiltre = result.data.content;
        console.log(this.listEleveFiltre);
      }
    }).catch(reason => {
      console.log(reason);
    }); */
  }
  onOpenModalInscrireEleve(eleve: any, inscriptionEleve) {
    if (eleve) {
      this.listMatricule = [eleve.matricule];
      this.currentEleve = eleve;
      this.inscriptionEleveForm.patchValue({
        classe: this.currentEleve.classe.id
      });
    }
    if (this.authService.isActif('bouton-inscription-eleve')) {
      this.modalService.open(inscriptionEleve,
        {ariaLabelledBy: 'inscription-eleve', scrollable: true}).result.then((result) => {
      }, (reason) => {
        console.log(reason);
      });
    }
  }

  private initFormInscription() {
    this.inscriptionEleveForm = this.fb.group({
      classe: ['', Validators.required]
    });
  }
  onResetClasse() {
    this.initFormInscription();
    this.submitted = false;
    this.currentEleve = undefined;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onInscrireEleveToClasse() {
    console.log("currentEleve : ", this.currentEleve);
    console.log("this.f.classe.value : ", this.f.classe.value);
    this.submitted = true;
    if (this.inscriptionEleveForm.invalid) {
      return;
    }
    this.montantTotalEcheancier = 0;
    // this.listModelEcheancierClasse(this.tokenStorage.getUser().codeAnnee,this.f.classe.value);
    this.genericsService
      .getResource(`admin/modele-echeancier/annee/${this.tokenStorage.getUser().codeAnnee}/classe/${this.f.classe.value}/list`)
      .then((result: any) => {
        if (result.success === true) {
          this.modeleEcheanciersParClasse = result.data;
          this.montantTotalEcheancier = 0;
          result.data.forEach( echeance => {
            this.montantTotalEcheancier += echeance.montant_a_ech;
          });

          // console.log("modeleEcheanciersParClasse : ",this.modeleEcheanciersParClasse);
          // console.log("montantTotalEcheancier : ",this.montantTotalEcheancier);
          // console.log("montantMinimalParAnnee : ",this.montantMinimalParAnnee);
          // let montantMinClasse = this.montantMinimalParAnnee.filter(o=>this.f.classe.value==o.id_classe);
          // console.log("montantMinClasse ",montantMinClasse);
          /*
          if(montantMinClasse.length>0){
            if(this.montantTotalEcheancier < montantMinClasse[0].montant_min){
              localStorage.getItem('activelang') === 'en'
              ? this.genericsService.confirmModalResponseAPI(AppConstants.CONTENT_FAIL_CONFIG_INSCIPTIONEN,
              AppConstants.TITLE_CONFIG_INSCIPTIONEN,  'error')
              // tslint:disable-next-line:max-line-length
              : this.genericsService.confirmModalResponseAPI(AppConstants.CONTENT_FAIL_CONFIG_INSCIPTIONFR,
              AppConstants.TITLE_CONFIG_INSCIPTIONFR,  'error');
              return;
            }
          }else{
            localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmModalResponseAPI(AppConstants.CONTENT_FAIL_CONFIG_MIN_INSCIPTIONEN,
            AppConstants.TITLE_FAIL_INSCIPTIONEN,  'error')
            : this.genericsService.confirmModalResponseAPI(AppConstants.CONTENT_FAIL_CONFIG_MIN_INSCIPTIONFR,
            AppConstants.TITLE_FAIL_INSCIPTIONFR,  'error');
            return;
          }
          */

          if (this.currentEleve && this.currentEleve.inscrit == true) {
            if (this.currentEleve.classe.id != this.f.classe.value) {
              const dto = new UpdateInscriptionModel(
                this.currentEleve.date_naissance, this.currentEleve.emailParent, this.currentEleve.handicape,
                this.f.classe.value,
                this.currentEleve.lieuNais ? this.currentEleve.lieuNais : '',
                this.currentEleve.matricule, localStorage.getItem('activelang') === 'en' ? 'class change' : 'changement de classe',
                this.currentEleve.nationalite ? this.currentEleve.nationalite : '',
                this.currentEleve.nomEleve, this.currentEleve.nomParent, this.currentEleve.prenomEleve, this.currentEleve.religion,
                this.currentEleve.residence_eleve, this.currentEleve.resParent, this.echeanciers,
                this.currentEleve.sexe, this.currentEleve.telephonePere, this.currentEleve.nomMere, this.currentEleve.telephoneMere);
              console.log('cet eleve a deja une fiche d\'inscription', this.currentEleve);
              this.genericsService.postResource(`admin/inscription/eleve/modifier`, dto).then(( result : any) => {
                if (result.success === true) {
                  this.redirectAfterSuccess(result.data);
                } else {
                  localStorage.getItem('activelang') === 'en'
                    ?  this.genericsService.confirmModalResponseAPI('Modification failure', result.message, 'error')
                    : this.genericsService.confirmModalResponseAPI('Echec de Modification', result.message, 'error');
                }
              }).catch(reason =>  {
                localStorage.getItem('activelang') === 'en'
                  ?  this.genericsService.confirmModalResponseAPI('Modification failure', reason.error.message, 'error')
                  : this.genericsService.confirmModalResponseAPI('Echec de Modification', reason.error.message, 'error');
              });
            } else {
              this.genericsService.confirmModalResponseAPI('cet eleve a deja une fiche d\'inscription dans cette classe', "Echec d'inscription",  'error');
            }
          } else {
            console.log('cet eleve n\'a pas encore de fiche d\'inscription', this.currentEleve);
            if (this.listMatricule.length > 1) {
              if (this.modalService.hasOpenModals()) {
                this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
              }
              localStorage.getItem('activelang') === 'en'
                ? Swal.fire({
                  title: 'Are you sure you want to continue ?',
                  text: 'You are about to launch a transaction on several students, the way back may be painful, make sure there are no errors',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Apply the change !'
                }).then(( result ) => {
                  if (result.isConfirmed) {
                    this.saveInscription();
                  }
                })
                :
                Swal.fire({
                  title: 'Etes vous sure de continuer ?',
                  text: 'Vous etes sur le point lancer une transaction sur plusieurs élèves le' +
                    ' chemin retour risque etre penible rassurez-vous qu\'il n\'y a pas d\'erreur',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Appliquer le changement !'
                }).then(( result ) => {
                  if (result.isConfirmed) {
                    this.saveInscription();
                  }
                });
            } else if (this.listMatricule.length == 1) {
              this.genericsService
                .getResource(`admin/echeancier/matricule/${this.listMatricule[0]}/annee/${this.tokenStorage.getUser().codeAnnee}/list`)
                .then(( result: any) => {
                  if (result.success === true) {
                    if (result.data.length == 0) {
                      this.saveInscription();
                    } else {
                      this.router.navigate(['/eleves/liste-inscription/gestion-inscriptions/' + this.listMatricule[0] ])
                        .then(() => {});
                    }
                  }
                });
            }
          }

        }
      }).catch(err => {
      console.log(err);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmModalResponseAPI(err.error.message, AppConstants.FAIL_OPERATIONEN,  'error')
        : this.genericsService.confirmModalResponseAPI(err.error.message, AppConstants.FAIL_OPERATIONFR,  'error');

    });

  }
  // redirection vers la nouvelle page apres finalisation des manipulation des infos eleve
  private redirectAfterSuccess(data: any) {
    this.router.navigate(['/eleves/liste-inscription/recap-eleve/' + data.id]).then(() => {
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Creation of a successful student registration form !', 'success')
        : this.genericsService.confirmResponseAPI('Creation fiche inscription élève reussi !', 'success');
      this.currentEleve = null;
    });
  }

  private saveInscription() {
    this.genericsService.postResource(`admin/inscription/classe/${this.f.classe.value}/create`,
      this.listMatricule).then((response: any) => {
      this.onResetClasse();
      if (response.success === true) {
        this.allSelectEleve = false;
        localStorage.getItem('activelang') === 'en' ?
          Swal.fire('Transfer!', response.message, 'success') : Swal.fire('Transféré!', response.message, 'success');
        if (this.listMatricule.length > 1) {
          this.searchForm.patchValue({
            search: this.currentClasseName
          });
          this.listMatricule = [];
          this.onLoadListEleves(this.currentClasseName);
        } else {
          this.router.navigate([`/eleves/liste-inscription/recap-eleve/${response.data[0].id}`]).then(() => {
            console.log(response);
          });
        }
      }
    }, err => {
      /*localStorage.getItem('activelang') === 'en'
        ? Swal.fire('Failed!', 'Failed to change student class !', 'error')
        : Swal.fire('Echec!', 'Echec de changement de classe élève !', 'error');*/
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmModalResponseAPI(err.error.message,AppConstants.TITLE_FAIL_INSCIPTIONEN,  'error')
        : this.genericsService.confirmModalResponseAPI(err.error.message,AppConstants.TITLE_FAIL_INSCIPTIONFR,  'error');

      console.log(err);
    });
  }
  private onLoadListClasse() {
    this.genericsService.getResource(`admin/classes`).then((response: any) => {
      if (response.success === true) {
        this.classes = response.data;
      }
    }).catch(reason => {
      console.log(reason);
    });
  }

  onDeleteEleve(eleve: any) {

    if (this.authService.isActif('bouton-suprimer-eleve')) {
      localStorage.getItem('activelang') === 'en'
        ? Swal.fire({
          title: 'Are you sure ?',
          text: 'The removal of ' + eleve.nomEleve + ' is irreversible !',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Delete!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.genericsService.deleteResource(`admin/inscription/eleve/${eleve.matricule}/delete`).then((response: any) => {
              console.log(response);
              this.onLoadListEleves('');
              Swal.fire('Delete!', response.message, 'success');
            }).catch(reason => {
              console.log(reason);
              Swal.fire('Failure', 'The student has not been deleted.', 'error');
            });
          }
        })
        : Swal.fire({
          title: 'Etes vous sure ?',
          text: 'La suppression de ' + eleve.nomEleve + ' est irreversible !',
          icon: 'Avertissement',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oui, Supprimer!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.genericsService.deleteResource(`admin/inscription/eleve/${eleve.matricule}/delete`).then((response: any) => {
              console.log(response);
              this.onLoadListEleves('');
              Swal.fire('Supprimé!', response.message, 'success');
            }).catch(reason => {
              console.log(reason);
              Swal.fire('Echec!', 'L\'élève n\'a pas été supprimé.', 'error');
            });
          }
        });
    }
  }

  onOpenEtatFinancier(eleve: any) {
    this.router.navigate(['/eleves/liste-inscription/gestion-inscriptions/'
    + eleve.matricule ]).then(() => {});
  }

  onAllEleveCheckboxChange(event: any) {
    if (event.target.checked) {
      this.listMatricule = [];
      this.allSelectEleve = true;
      this.listEleve.forEach((eleve, i) => {
        this.listMatricule.push(eleve.matricule);
      });
    } else {
      this.listMatricule = [];
      this.allSelectEleve = false;
    }
  }

  onEleveCheckboxChange(event: any) {
    const i = this.listMatricule.findIndex((mat) => mat == event.target.value);
    if (event.target.checked) {
      if (i == -1) {
        this.listMatricule.push(event.target.value);
      }
    } else {
      if (i > -1) {
        this.listMatricule.splice(i, 1);
      }
    }
  }

  ngOnDestroy(): void {
    this.onResetClasse();
    clearInterval(this.subscription);
  }

  onSelectClasse(event) {
    this.echeanciers = [];
    const classe = this.classes.find(item => item.id == event.target.value);
    this.currentClasseName = classe.nomClasse;
    const url = `admin/modele-echeancier/annee/${(this.tokenStorage.getUser().codeAnnee)}/niveau/${classe.idNiveau}/list`;
    this.genericsService.getResource(url).then((result: any) => {
      if (result.success === true) {
        result.data.forEach(elt => {
          this.echeanciers.push({
            dateEcheance: elt.date_ech,
            idModeleEcheancier: elt.id,
            montant: elt.montant_a_ech
          });
        });
        console.log(this.echeanciers);
      }
    });
  }

  // Liste du model echeancier par classe
  public listModelEcheancierClasse(annee: number, classe: number) {
    this.genericsService.getResource(`admin/modele-echeancier/annee/${annee}/classe/${classe}/list`)
      .then((result: any) => {
        if (result.success === true) {
          this.modeleEcheanciersParClasse = result.data;
          console.log("result.data: ",result.data)
          this.montantTotalEcheancier = 0;
          result.data.forEach( echeance =>{
            this.montantTotalEcheancier += echeance.montant_a_ech;
          });
        }
      });
    console.log("Montant Tolal a payer : "+this.montantTotalEcheancier);
  }

  // Liste des montants minimal  par annee
  public onLoadListMontantMinimal(annee: number) {
    this.genericsService.getResource(`admin/inscription/montant-min/classe/annee/${annee}/liste`)
      .then((result: any) => {
        if (result.success === true) {
          this.montantMinimalParAnnee = result.data;
          // console.log("result.data: ",result.data);
        }
      });
    // console.log("montantMinimalParAnnee : "+this.montantMinimalParAnnee);
  }

  setPageChange(event) {
    this.page = event - 1;
    this.onLoadListEleves(this.dataFilter);
  }

  onGenerateQrCode(eleve: any) {
    localStorage.getItem('activelang') === 'en'
      ? this.genericsService.startLoadingPage('In the process of loading')
      : this.genericsService.startLoadingPage('En cours de chargement');
    this.genericsService.postResource(`admin/eleve/qrcode`,
      { matricule: eleve.matricule, width: 250, height: 250 })
      .then((response: any) => {
        this.genericsService.stopLoadingPage();
        // tslint:disable-next-line:max-line-length
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmModalResponseAPI('Qr code generated with success', 'Success', 'success')
          : this.genericsService.confirmModalResponseAPI('Qr code généré avec success', 'Success', 'success');
      }).catch(err => {
      console.log(err);
    });
  }

  private onloadListInstitutions() {
    this.genericsService.getResource(`admin/institutions`).then((response: any) => {
      this.institution = response.data[0];
    }).catch(err => {
      console.log(err);
    });
  }

  onGenerateQrCodeForAll() {
    this.genericsService.startLoadingPage('En cours de chargement');
    this.genericsService.postResource(`admin/all/eleve/qrcode`,
      { matricule: null, width: 250, height: 250 })
      .then((response: any) => {
        this.genericsService.stopLoadingPage();
        // tslint:disable-next-line:max-line-length
        localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmModalResponseAPI('Qr code generated with success', 'Success', 'success')
          : this.genericsService.confirmModalResponseAPI('Qr code généré avec success', 'Success', 'success');
      }).catch(err => {
      console.log(err);
    });
  }
}
