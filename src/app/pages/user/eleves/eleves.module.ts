import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ElevesRoutingModule} from './eleves-routing.module';
import {ElevesComponent} from './eleves.component';
import {SharedModule} from '../../../shared/shared.module';
import {ElWelcomeComponent} from './el-welcome/el-welcome.component';
import {AjoutEleveComponent} from './ajout-eleve/ajout-eleve.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../../app.module';
import {HttpClient} from '@angular/common/http';


@NgModule({
  declarations: [ElevesComponent, ElWelcomeComponent, AjoutEleveComponent],
    imports: [
        CommonModule,
        ElevesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    ]
})
export class ElevesModule { }
