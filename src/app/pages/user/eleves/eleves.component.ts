import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SharedModule} from '../../../shared/shared.module';

@Component({
  selector: 'app-eleves',
  templateUrl: './eleves.component.html',
  styleUrls: ['./eleves.component.scss']
})
export class ElevesComponent implements OnInit {
lang = 'en';
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
        private translate: TranslateService
  ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
  }

}
