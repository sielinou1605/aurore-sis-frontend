import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FinancesRoutingModule} from './finances-routing.module';
import {FinancesComponent} from './finances.component';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReglementDiversComponent} from './reglement-divers/reglement-divers.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {TranslateModule} from '@ngx-translate/core';
import {JournalFinancesComponent} from './journal-finances/journal-finances.component';

@NgModule({
  declarations: [FinancesComponent, ReglementDiversComponent, JournalFinancesComponent],
    imports: [
        CommonModule,
        FinancesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        TranslateModule
    ]
})
export class FinancesModule { }
