import { Component, OnInit } from '@angular/core';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {HistoriqueEleveNoteResponse} from '../../../../shared/_models/response-dto/systeme/data-module-response-model';
import {AuthService} from '../../../../shared/_services/auth.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-journal-finances',
  templateUrl: './journal-finances.component.html',
  styleUrls: ['./journal-finances.component.scss']
})
export class JournalFinancesComponent implements OnInit {
  public searchForm: FormGroup;
  public pageSize = 15;
  public page = 0;
  public sortBy = '';
  public totalElements: number;
  private dataFilter: string;
  listNewsPeper: any;
  predicate = '';
  historique = localStorage.getItem('activelang') === 'fr' ? 'Historique des finances' : 'History of finance';

  constructor(
    public genericsService: GenericsService,
    public fb: FormBuilder,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ''
    });
    this.genericsService.startLoadingPage();
    this.onloadListHistorique(this.predicate, this.sortBy, this.page, this.pageSize);
  }

  get f() {
    return this.searchForm.controls;
  }
  onAfficherHistorique() {
    let url = 'admin/get-all/historique/finance/eleves';
    this.predicate = this.f.search.value;
    console.log(this.predicate);
    this.genericsService.getResource(`${url}?predicat=${this.predicate}&sortBy=${this.sortBy}&page=${this.page}&size=${this.pageSize}`);

    this.onloadListHistorique(this.predicate, this.sortBy, this.page, this.pageSize);
  }
  onDetailHistorique(note: HistoriqueEleveNoteResponse) {

  }

  setPageChange(event: any) {
    this.page = event - 1;
    this.onloadListHistorique(this.predicate, this.sortBy, this.page, this.pageSize);
  }

  private onloadListHistorique(predicat: string, sortBy: string, page: number, size: number) {
    let url = 'admin/get-all/historique/finance/eleves';
    this.genericsService.getResource(`${url}?predicat=${predicat}&sortBy=${sortBy}&page=${page}&size=${size}`)
      .then((result: any) => {
        this.genericsService.stopLoadingPage();
        console.log(result);
        this.listNewsPeper = result.data.content;
        this.totalElements = result.data.totalElements;
      }).catch(err => {
      console.log(err);
    });
  }

}
