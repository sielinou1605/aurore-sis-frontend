import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalFinancesComponent } from './journal-finances.component';

describe('JournalFinancesComponent', () => {
  let component: JournalFinancesComponent;
  let fixture: ComponentFixture<JournalFinancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JournalFinancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalFinancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
