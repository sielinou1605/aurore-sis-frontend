import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FinancesComponent} from './finances.component';
import {ReglementDiversComponent} from './reglement-divers/reglement-divers.component';
import {JournalFinancesComponent} from './journal-finances/journal-finances.component';
import {AcompteComponent} from '../administration/finances/acompte/acompte.component';

const routes: Routes = [
  {
    path: '',
    component: FinancesComponent,
    data: {
      breadcrumb: 'Finances',
      titre: localStorage.getItem('activelang') === 'en' ? 'Finance | Aurore' : 'Finances | Aurore',
      icon: 'icofont-fountain-pen bg-c-orenge',
      breadcrumb_caption: 'userEleveAjaout.Espace_de_paiement_des_frais_d_inscription_Finances',
      status: true,
      current_role: 'finances',
      action: 'bouton-finance'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome',
        loadChildren: () => import('./welcome/welcome.module')
          .then(m => m.WelcomeModule)
      },
      {
        path: 'liste-inscription',
        redirectTo: '/eleves/liste-inscription/welcome',
        pathMatch: 'full'
      },
      {
        path: 'reglements-divers', component: ReglementDiversComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Autres_versements',
          titre: localStorage.getItem('activelang') === 'en' ? 'Other payments | Aurore' : 'Autres versements | Aurore',
          icon: 'icofont-money bg-c-green',
          breadcrumb_caption: 'userEleveAjaout.Versements_libres_et_pour_services_para_scolaire_Caisse',
          status: true,
          current_role: 'finances'
        }
      },
      {
        path: 'historique-finance', component: JournalFinancesComponent,
        data: {
          breadcrumb: 'Historique des finances',
          titre: localStorage.getItem('activelang') === 'en' ? 'Journal Finances | Aurore' : 'Finance Newspaper | Aurore',
          icon: 'icofont-money bg-c-green',
          breadcrumb_caption: 'Historique des finances',
          status: true,
          current_role: 'finances'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancesRoutingModule { }
