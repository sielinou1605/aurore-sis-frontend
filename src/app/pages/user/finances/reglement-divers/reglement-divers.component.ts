import {Component, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CaisseModel} from '../../../../shared/_models/response-dto/finance/caisse-model';
import {ModeReglementModel} from '../../../../shared/_models/response-dto/finance/mode-reglement-model';
import {ReglementDiversRequest} from '../../../../shared/_models/request-dto/finance/reglement-divers-request';
import {ConveertDateService} from '../../../../shared/_helpers/conveert-date.service';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {AppConstants} from '../../../../shared/element/app.constants';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-reglement-divers',
  templateUrl: './reglement-divers.component.html',
  styleUrls: ['./reglement-divers.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ReglementDiversComponent implements OnInit {
  reglementDiversForm: FormGroup;
  public submitted: boolean;
  private url_mode_reglement = 'admin/mode-reglement/';
  private url_caisse = 'admin/caisse/';
  private url_reglement = 'admin/reglement/';
  private url_reglement_service = 'admin/reglement/service/';
  public modeReglements: ModeReglementModel[];
  toUpdate: boolean;
  public caisses: CaisseModel[];
  public reglements: any;
  private defaultCaisse: any;
  private modeRgl: any;
  validated: boolean;

  public pageSize = 40;
  public page = 0;
  public token = '';
  public totalElements: number;
  public idPaginate  = 'reglements-divers';

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  private base_url = 'admin/reglement/';

  constructor(
    private modalService: NgbModal, private tokenStorage: TokenStorageService,
    config: NgbModalConfig, private genericsService: GenericsService,
    private fb: FormBuilder, private dateConvert: ConveertDateService,
    private calendar: NgbCalendar, public authService: AuthService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(12);
  }

  ngOnInit() {
    this.onLoadListModeReglement();
    this.onLoadListCaisse();
    this.onLoadListReglement(this.token);
    this.initFormReglementDivers();
  }

  open(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'add-mode-reglement', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  private onLoadListModeReglement() {
    this.genericsService.getResource(`${this.url_mode_reglement}list`).then((result: any) => {
      if (result.success === true) {
        this.modeReglements = result.data;
        this.modeRgl = result.data[0];
      }
    });
  }

  private onLoadListCaisse() {
    this.genericsService.getResource(`${this.url_caisse}list`).then((result: any) => {
      if (result.success === true) {
        this.caisses = result.data;
        this.defaultCaisse = result.data[0];
      }
    });
  }

  private onLoadListReglement(predicat:any) {
    const annee = this.tokenStorage.getUser().codeAnnee;
    this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(`${this.url_reglement}list/type/0?annee=${annee}&token=${predicat}&size=${this.pageSize}&page=${this.page}`)
      .then((result: any) => {
        if (result.success === true) {
          this.reglements = result.data.content;
          this.totalElements = result.data.totalElements;
        }
        this.genericsService.stopLoadingPage();
      }).catch(reason => {
      this.genericsService.stopLoadingPage();
    });
  }

  setPageChange(event) {
    this.page = event - 1;
    this.onLoadListReglement(this.token);
  }

  get f() { return this.reglementDiversForm.controls; }

  onReset() {
    this.submitted = false;
    this.toUpdate = false;
    this.validated = false;
    this.initFormReglementDivers();
    this.modalService.dismissAll(ModalDismissReasons.ESC);
  }

  private initFormReglementDivers() {
    this.reglementDiversForm = this.fb.group({
      search: [''],
      montant: [0, [Validators.required, Validators.min(100)]],
      motifReglement: [''],
      nomPartieVersante: ['', Validators.required],
      numeroCni: ['']
    });
  }

  onSaveReglementDivers() {
    if (this.authService.isActif('bouton-enregistrer')) {
      this.submitted = true;
      this.validated = true;
      if (this.reglementDiversForm.invalid) {
        this.validated = false;
        return;
      }
      const reglementDivers = new ReglementDiversRequest(this.tokenStorage.getUser().codeAnnee,
        this.tokenStorage.getCaisse() ? this.tokenStorage.getCaisse().id : this.defaultCaisse.id,
        this.modeRgl.id, this.f.montant.value, this.f.motifReglement.value,
        this.f.nomPartieVersante.value, this.f.numeroCni.value);
      this.genericsService.postResource(`${this.url_reglement_service}create`, reglementDivers).then((result: any) => {
        console.log(result);
        this.onLoadListReglement(this.token);
        this.onReset();
        this.genericsService.confirmResponseAPI('Votre reglement a été effectué avec succes', 'success');
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        this.onImpressionRecu(result.data);
      }, err => {
        console.log(err);
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmModalResponseAPI(err.error.message, AppConstants.FAIL_OPERATIONEN,  'error')
          : this.genericsService.confirmModalResponseAPI(err.error.message, AppConstants.FAIL_OPERATIONFR,  'error');
      });
    }
  }

  onLoadDetailsReglement(reglement: any, content: TemplateRef<any>) {
    this.toUpdate = true;
    this.reglementDiversForm.patchValue({
      idCaisse: reglement.caisse.id,
      idModeReglement: reglement.mode_reglement.id,
      montant: reglement.montant_rgl,
      motifReglement: reglement.motifReglement,
      nomPartieVersante: reglement.nomPartieVersante,
      numeroCni: reglement.numeroCni
    });
    this.modalService.open(content, {ariaLabelledBy: 'add-mode-reglement', size: 'lg', scrollable: true})
      .result.then((response) => { console.log(response); },
      (reason) => { console.log(reason); });
  }

  onImpressionRecu(reglement: any) {
    this.genericsService.reportPostResource(`admin/impression/reglement/${reglement.id}/divers`).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, reglement.service);
    });
  }

  onDeleteVersement(id: number) {
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: AppConstants.ARE_U_SUREEN,
        text: AppConstants.IRREVERSIBLEEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITEN
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            console.log(resultDelete);
            this.onLoadListReglement(this.token);
            this.toUpdate = false;
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success'
            );
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
          });
        }
      }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            console.log(resultDelete);
            this.onLoadListReglement(this.token);
            this.toUpdate = false;
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success'
            );
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }

  onSearchReglDivers() {
    this.onLoadListReglement(this.token);
  }

  onChangeValueSearch(event: any) {
    console.log(event.target.value);
    this.token = event.target.value;
  }
}
