import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReglementDiversComponent} from './reglement-divers.component';

describe('ReglementDiversComponent', () => {
  let component: ReglementDiversComponent;
  let fixture: ComponentFixture<ReglementDiversComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReglementDiversComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReglementDiversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
