import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Finances',
      titre: localStorage.getItem('activelang') === 'en' ? 'Finance | Aurore' : 'Finances | Aurore',
      icon: 'icofont-fountain-pen bg-c-orenge',
      breadcrumb_caption: 'userEleveAjaout.Espace_de_paiement_des_frais_d_inscription_Finances',
      status: true,
      current_role: 'finances'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
