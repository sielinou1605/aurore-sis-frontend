import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../shared/_services/auth.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  public menuCompact: any = [];

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(public authService: AuthService, private router: Router,
              private translate: TranslateService
              ) {
    authService.returnListAction(10);
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
  }

  isActif(url: string, id: string) {
    if (this.authService.isActif(id)) {
      this.router.navigate([url]).then(() => {});
    }
  }

}
