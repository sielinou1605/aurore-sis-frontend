import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListReleveImpressionsComponent} from './list-releve-impressions.component';

describe('ListReleveImpressionsComponent', () => {
  let component: ListReleveImpressionsComponent;
  let fixture: ComponentFixture<ListReleveImpressionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListReleveImpressionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListReleveImpressionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
