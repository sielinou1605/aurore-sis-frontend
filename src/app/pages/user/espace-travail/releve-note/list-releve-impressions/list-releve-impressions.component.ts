/* tslint:disable:triple-equals */
import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-releve-impressions',
  templateUrl: './list-releve-impressions.component.html',
  styleUrls: ['./list-releve-impressions.component.scss']
})
export class ListReleveImpressionsComponent implements OnInit, OnDestroy {
  @ViewChild('releveNote', { static: true }) pdfTable: ElementRef;
  @ViewChild('modalSelectSequence', { static: true }) modalSequence: ElementRef;
  eleves: any;
  selectSequenceClasseForm: FormGroup;
  changeClassForm: FormGroup;
  submitted: boolean;
  public formSelect: any;
  public timestamp: any;
  public institution: any;
  public classe: any;
  public sequence: any;
  public urlImage  = AppConstants.API_URL + 'admin/load/logo/';
  public currentClasse: any;
  public currentSequence: any;
  public entete: any;
  public moyennes: any;
  public notesEleves: any;
  public nbMoyenne: any;
  public nbSousMoyenne: any;
  public tauxReussite: any;
  public moyennePremier: any;
  public moyenneDernier: any;
  public moyenneGenerale: any;

  public type: string;
  private idType: string;
  private idClasse: string;
  public trimestre: any;
  public currentSwitch = 'classe';
  public listEleve: any;
  public classes: any;
  public currentTrimestre: any;
  private sortby: string;
  private sortBy: string;

  public pageSize = 200;
  public page = 0;
  public totalElements = 0;
  listClasses: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    public genericsService: GenericsService,  private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    public authService: AuthService, public tokenStorage: TokenStorageService,
    private route: ActivatedRoute, private router: Router,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.onloadDataFilter();
    this.initFormSelectSequence();
    this.onloadInstitution();
    this.initChangeClasseForm();
    // tslint:disable-next-line:max-line-length
     localStorage.getItem('activelang') === 'en' ? this.sortBy = AppConstants.SORT_BY_NOM_CLASSE_VALUEEN : this.sortBy = AppConstants.SORT_BY_NOM_CLASSE_VALUEFR;
    this.onLoadListClasse(this.sortBy);
    this.route.paramMap.subscribe(params => {
      this.type = params.get('type');
      this.idType = params.get('idType');
      this.idClasse = params.get('idClasse');
      this.sortby = params.get('sortby');
      if (this.idClasse) {
        this.onloadClasseById(this.idClasse);
      }
      this.onDisplayReleveNotes(false);
      this.genericsService.getResource(`admin/eleves/classe/${this.idClasse}/valider`)
        .then((response: any) => {
          console.log(response);
        }).catch(err => {
        console.log(err);
      });
    });
  }
  private onloadClasseById(idClasse: string) {
    this.genericsService.getResource(`classes/${idClasse}`).then((response: any) => {
      this.classe = response;
      this.genericsService.getResource(`admin/eleve/filtrer?predicat=${response.nomClasse}`).then((responseEleve: any) => {
        if (responseEleve.success === true) {
          this.listEleve = responseEleve.data;
        }
      }).catch(reasonEleve => console.log(reasonEleve));
    }).catch(reason => console.log(reason));
  }

  onDisplayReleveNotes(toPrint: boolean) {
    this.genericsService.getResource(`admin/eleves/classe/${this.idClasse}/valider`)
      .then((response: any) => {
        console.log(response);
        console.log(this.type);
        if (this.type == 'sequence' && this.idType) {
          if (toPrint) {
            this.generatePDF(this.classe, this.idType);
          } else {
            this.onLoadReleveNotes(this.idType, this.idClasse);
          }
        } else if (this.type == 'trimestre') {
          if (toPrint) {
            this.generatePDFTrimestre(this.classe, this.idType);
          } else {
            this.onLoadReleveNotesTrimestre(this.idType, this.idClasse);
          }
        } else if (this.type == 'annuel') {
          console.log(toPrint);
          if (toPrint) {
            this.generatePDFAnnuel(this.classe);
          } else {
            this.onLoadReleveNotesAnnuel(this.idClasse);
          }
        }
      }).catch(err => {
      console.log(err);
    });
  }
  private initFormSelectSequence() {
    this.selectSequenceClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      classe: ['', Validators.required],
      sortby: ['nom', Validators.required]
    });
  }
  get f() { return this.selectSequenceClasseForm.controls; }

  onDisplayListeEleve() {
    this.submitted = true;
    if (this.selectSequenceClasseForm.invalid) {
      return;
    }
    this.sequence = this.formSelect.sequences.find(elt => elt.id_sequence == this.f.sequence.value);
    this.classe =  this.formSelect.classes.find(elt => elt.id_classe == this.f.classe.value);
    this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    this.onDisplayReleveNotes(false);
    this.onLoadReleveNotes(this.f.sequence.value, this.f.classe.value);
  }

  public onLoadReleveNotes(sequence, classe) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/classe/${classe}/sequence/${sequence}/releve-notes?sortBy=${this.sortby}`)
      .then((response: any) => {
        this.currentClasse = response.data.classe;
        this.currentSequence = response.data.sequence;
        this.entete = response.data.enTete;
        this.moyennes = response.data.moyennes;
        this.notesEleves = response.data.notesEleve;
        this.moyenneDernier = response.data.moyenneDernier;
        this.moyennePremier = response.data.moyennePremier;
        this.nbMoyenne = response.data.nbMoyenne;
        this.nbSousMoyenne = response.data.nbSousMoyenne;
        this.tauxReussite = response.data.tauxReussite;
        this.moyenneGenerale = response.data.moyenneGenerale;
        this.genericsService.stopLoadingPage();
      }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
    });
  }

  public onLoadReleveNotesTrimestre(trimestre, classe) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/classe/${classe}/trimestre/${trimestre}/releve-notes?sortBy=${this.sortby}`)
      .then((response: any) => {
        console.log(response);
        this.currentClasse = response.data.classe;
        this.currentTrimestre = response.data.trimestreDto;
        this.entete = response.data.enTete;
        this.moyennes = response.data.moyennes;
        this.notesEleves = response.data.notesEleve;
        this.moyenneDernier = response.data.moyenneDernier;
        this.moyennePremier = response.data.moyennePremier;
        this.nbMoyenne = response.data.nbMoyenne;
        this.nbSousMoyenne = response.data.nbSousMoyenne;
        this.tauxReussite = response.data.tauxReussite;
        this.moyenneGenerale = response.data.moyenneGenerale;
        this.genericsService.stopLoadingPage();
      }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
    });
  }

  private onLoadReleveNotesAnnuel(idClasse: string) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(
      `admin/classe/${idClasse}/annee/${this.tokenStorage.getUser().codeAnnee}/releve-notes?sortBy=${this.sortby}`
    ).then((response: any) => {
      console.log(response);
      this.currentClasse = response.data.classe;
      this.entete = response.data.enTete;
      this.moyennes = response.data.moyennes;
      this.notesEleves = response.data.notesEleve;
      this.moyenneDernier = response.data.moyenneDernier;
      this.moyennePremier = response.data.moyennePremier;
      this.nbMoyenne = response.data.nbMoyenne;
      this.nbSousMoyenne = response.data.nbSousMoyenne;
      this.tauxReussite = response.data.tauxReussite;
      this.moyenneGenerale = response.data.moyenneGenerale;
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
    });
  }

  private onloadDataFilter() {
    this.genericsService.getResource(`admin/bulletin/filtre/sequence`).then((response: any) => {
      this.formSelect = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  onResetSaisieNote() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }
  ngOnDestroy(): void {
    if (this.modalService.hasOpenModals()) {
      this.onResetSaisieNote();
    }
  }
  private onloadInstitution() {
    this.genericsService.getResource(`institutions`).then((result: any) => {
      this.institution = result._embedded.institutions[0];
      this.timestamp = new Date();
    });
  }
  public traiterLogoInstitution(nom: string) {
    if (nom) {
      const idx = nom.indexOf(' ');
      if (idx > 0) {
        return nom.slice(0, idx + 2);
      } else {
        return nom;
      }
    }
  }
  generatePDF(classe, sequence) {
    this.genericsService
      .printReport(`admin/classe/${classe.id}/sequence/${sequence}/releve-notes/print?sortBy=${this.sortby}`,
        classe.nomClasse + '_' + 'Releve_Notes');
  }
  generatePDFTrimestre(classe, trimestre) {
    this.genericsService
      .printReport(`admin/classe/${classe.id}/trimestre/${trimestre}/releve-notes/print?sortBy=${this.sortby}`,
        classe.nomClasse + '_' + 'Releve_Notes');
  }
  private generatePDFAnnuel(classe: any) {
    // /classe/{id_classe}/releve-notes/print
    this.genericsService
      .printReport(`admin/classe/${classe.id}/annee/${this.tokenStorage.getUser().codeAnnee}/releve-notes/print?sortBy=${this.sortby}`,
        classe.nomClasse + '_' + 'Releve_Notes');
  }

  onSelectChangeClasse(content: TemplateRef<any>) {
    if (this.listClasses) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-sequence', scrollable: true}).result.then((result) => {
      }, (reason) => {
      });
    }
  }

  private initChangeClasseForm() {
    this.changeClassForm = this.fb.group({
      classe: ['', Validators.required],
      sortby: ['nom']
    });
  }

  private onLoadListClasse(sortBy?: string) {
    // tslint:disable-next-line:max-line-length
    localStorage.getItem('activelang') === 'en' ? this.genericsService.startLoadingPage(AppConstants.LOADING_CLASSEEN) : this.genericsService.startLoadingPage(AppConstants.LOADING_CLASSEFR);
    this.genericsService.getResource(`admin/classe/liste?size=${this.pageSize}&page=${this.page}&sortBy=${sortBy}`)
      .then((response: any) => {
        this.listClasses = response.data.content;
        this.totalElements = response.data.totalElements;
        this.genericsService.stopLoadingPage();
      }, err => {
        console.log(err);
        this.genericsService.stopLoadingPage();
        // tslint:disable-next-line:max-line-length
        localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
      });
  }

  get bs() { return this.changeClassForm.controls; }

  onResetChangeClasse() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initChangeClasseForm();
  }

  onRefreshReleveNote() {
    if (!this.changeClassForm.invalid) {
      this.idClasse = this.bs.classe.value;
      this.sortby = this.bs.sortby.value;
      this.onDisplayReleveNotes(false);
      this.onResetChangeClasse();
      this.onloadClasseById(this.idClasse);
    }
  }
}
