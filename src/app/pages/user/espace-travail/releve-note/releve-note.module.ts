import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReleveNoteRoutingModule} from './releve-note-routing.module';
import {ReleveNoteComponent} from './releve-note.component';
import {SharedModule} from '../../../../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {AvatarModule} from 'ngx-avatar';
import {ListReleveNoteComponent} from './list-releve-note/list-releve-note.component';
import {ListReleveImpressionsComponent} from './list-releve-impressions/list-releve-impressions.component';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ReleveNoteComponent, ListReleveNoteComponent, ListReleveImpressionsComponent],
    imports: [
        CommonModule,
        ReleveNoteRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        AvatarModule,
        TranslateModule
    ]
})
export class ReleveNoteModule { }
