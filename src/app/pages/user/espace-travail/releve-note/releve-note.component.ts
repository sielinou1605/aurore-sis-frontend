import {Component, OnInit} from '@angular/core';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {TranslateService} from '@ngx-translate/core';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-releve-note',
  templateUrl: './releve-note.component.html',
  styleUrls: ['./releve-note.component.scss']
})
export class ReleveNoteComponent implements OnInit {

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor( private translate: TranslateService) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {}
}
