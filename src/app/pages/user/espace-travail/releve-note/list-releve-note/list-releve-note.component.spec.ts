import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListReleveNoteComponent} from './list-releve-note.component';

describe('ListReleveNoteComponent', () => {
  let component: ListReleveNoteComponent;
  let fixture: ComponentFixture<ListReleveNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListReleveNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListReleveNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
