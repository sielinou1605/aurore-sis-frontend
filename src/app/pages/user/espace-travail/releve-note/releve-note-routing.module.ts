import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReleveNoteComponent} from './releve-note.component';
import {ListReleveNoteComponent} from './list-releve-note/list-releve-note.component';
import {ListReleveImpressionsComponent} from './list-releve-impressions/list-releve-impressions.component';


const routes: Routes = [
  {
    path: '', component: ReleveNoteComponent,
    data: {
      breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Transcript of notes' : 'Relevé de notes',
      titre: localStorage.getItem('activelang') === 'en' ? 'Transcript of notes | Aurore' : 'Relevé de notes | Aurore',
      icon: 'icofont-pen-alt-4 bg-c-lite-green',
      breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Viewing and Printing Transcripts - Transcript' : 'Affichage et impression des rélevés de note - Rélevé de notes',
      status: true,
      current_role: 'enseignant'
    },
    children: [
      {
        path: '',
        redirectTo: 'list-releve-note',
        pathMatch: 'full'
      },
      {
        path: 'list-releve-note', component: ListReleveNoteComponent,
        data: {
          breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Transcript of notes' : 'Relevé de notes',
          titre: localStorage.getItem('activelang') === 'en' ? 'Transcript of notes | Aurore' : 'Relevé de notes | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Transcript Management - Transcript' : 'Gestion des  Relevé de notes - Relevé',
          status: true,
          current_role: 'enseignant'
        }
      },
      {
        path: 'releve-note-impression/:type/classe/:idClasse/:sortby', component: ListReleveImpressionsComponent,
        data: {
          breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Transcript of notes' : 'Relevé de notes',
          titre: localStorage.getItem('activelang') === 'en' ? 'Transcript of notes | Aurore' : 'Relevé de notes | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Printing of Sequential, Quarterly and or Annual Transcripts - Report' : 'Impression des releves de notes Sequentiel, Trimestriel et ou Annuel - Reléve',
          status: true,
          current_role: 'enseignant'
        }
      },
      {
        path: 'releve-note-impression/:type/:idType/classe/:idClasse/:sortby', component: ListReleveImpressionsComponent,
        data: {
          breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Transcript of notes' : 'Relevé de notes',
          titre: localStorage.getItem('activelang') === 'en' ? 'Transcript of notes | Aurore' : 'Relevé de notes | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Printing of Sequential, Quarterly and or Annual Transcripts - Report' : 'Impression des releves de notes Sequentiel, Trimestriel et ou Annuel - Reléve',
          status: true,
          current_role: 'enseignant'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReleveNoteRoutingModule { }
