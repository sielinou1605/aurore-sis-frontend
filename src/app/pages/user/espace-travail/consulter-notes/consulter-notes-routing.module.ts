import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConsulterNotesComponent} from './consulter-notes.component';


const routes: Routes = [
  {
    path: '', component: ConsulterNotesComponent,
    data: {
      breadcrumb: localStorage.getItem('activelang') === 'en' ? 'View notes' : 'Consulter notes',
      titre: localStorage.getItem('activelang') === 'en' ? 'View notes | Aurore' : 'Consulter notes | Aurore',
      icon: 'icofont-certificate-alt-1 bg-c-lite-green',
      breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elit - View notes' : 'Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elit - Consulter notes',
      status: true,
      current_role: 'enseignant'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsulterNotesRoutingModule { }
