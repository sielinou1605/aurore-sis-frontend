import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ConsulterNotesRoutingModule} from './consulter-notes-routing.module';
import {ConsulterNotesComponent} from './consulter-notes.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ConsulterNotesComponent],
    imports: [
        CommonModule,
        ConsulterNotesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class ConsulterNotesModule { }
