/* tslint:disable:triple-equals */
import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {Router} from '@angular/router';
import {
  EleveNoteModel,
  ModifierNoteModel,
  RemplirNoteModel
} from '../../../../shared/_models/request-dto/graduation/gestion-notes-enseignant_model';
import {AuthService} from '../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-consulter-notes',
  templateUrl: './consulter-notes.component.html',
  styleUrls: ['./consulter-notes.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ConsulterNotesComponent implements OnInit, OnDestroy {
  @ViewChild('modalSelectSequence', { static: true }) modalSequence: ElementRef;
  eleves: any;
  selectSequenceClasseForm: FormGroup;
  submitted: boolean;
  private base_url = 'admin/inscription/';
  public formSelect: any;
  private notesForm: FormGroup;
  public currentSequence: any;
  public currentClasse: any;
  public editionNoteForm: FormGroup;
  private toUpdate: boolean;
  public currentEleve: any;
  private currentIndex: number;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private genericsService: GenericsService,  private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    private router: Router, public authService: AuthService,
    public translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activlang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    }else{
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(62);
  }

  ngOnInit() {
    this.onloadDataFilter();
    this.initFormSelectSequence();
    this.initNotesForm();
    this.initFormEditNote();
    this.modalService.open(this.modalSequence,
      {ariaLabelledBy: 'select-sequence', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  private initFormSelectSequence() {
    this.selectSequenceClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      classe: ['', Validators.required]
    });
  }
  get f() { return this.selectSequenceClasseForm.controls; }

  onDisplayListeEleve() {
    this.submitted = true;
    if (this.selectSequenceClasseForm.invalid) {
      return;
    }
    const classeMatiere = this.f.classe.value.split('___');
    this.currentClasse = this.formSelect.classes.find(item =>
      (item.classe.nomClasse == classeMatiere[0] && item.matiere.id == classeMatiere[1]));
    this.currentSequence = this.formSelect.sequences.find(item => item.id_sequence == this.f.sequence.value);
    this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    console.log(this.currentClasse);
    this.onLoadListEleves(classeMatiere[0]);
  }

  public onLoadListEleves(event) {
    this.genericsService.getResource(`${this.base_url}filtrer?predicat=${event}`).then((result: any) => {
      if (result.success === true) {
        this.eleves = result.data;
        this.eleves.forEach(item => {
          this.tNotes.push(
            this.fb.group({
              matricule: [item.matricule],
              noms: [item.noms],
              idNote: [item.id_note],
              note: [0, [Validators.min(0),
                Validators.max(this.currentClasse ? this.currentClasse.baremeEvaluation : 20)]],
              observation: ['-'],
              update: false
            })
          );
        });
      }
      if (this.currentClasse && this.currentClasse.notes.length > 0) {
        this.currentClasse.notes.forEach(item => {
          const idx = this.tNotes.controls.findIndex(elt => elt.value.matricule == item.eleve.matricule
            && item.sequenc.id_sequence == this.f.sequence.value);
          if (idx > -1) {
            this.tNotes.at(idx).patchValue({ note: item.noteatt, idNote: item.id_note, observation: item.observation});
          }
        });
      }
    }, err => {
      console.log(err);
    });
  }
  private onloadDataFilter() {
    this.genericsService.getResource(`admin/note/enseignant/filtre/saisie`).then((result: any) => {
      if (result.success === true) {
        this.formSelect = result.data;
      } else {
        this.genericsService.confirmResponseAPI(result.message, 'error');
        this.formSelect = result.data;
      }
    }).catch((reason: any) => {
      this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des classes', 'error');
    });
  }

  onResetSaisieNote() {
    this.router.navigate(['/gestion-notes']).then(() => {
      this.modalService.dismissAll();
    });
  }

  private initNotesForm() {
    this.notesForm = this.fb.group({
      notesData: new FormArray([])
    });
  }

  // les elements du formulaire globale
  get fNotes() { return this.notesForm.controls; }
  // les elements du formulaire local
  get tNotes() { return this.fNotes.notesData as FormArray; }
  // les elements du formulaire edition de note
  get g() { return this.editionNoteForm.controls; }

  ngOnDestroy(): void {
    this.onReset();
    if (this.modalService.hasOpenModals()) {
      this.onResetSaisieNote();
    }
  }

  onPrintReleveDeNote() {
    this.genericsService
      .printReport(`admin/classe/${this.currentClasse.classe.id}/sequence/${this.currentSequence.id_sequence}/releve-notes/print`,
        this.currentClasse.classe.nomClasse + '_' + 'Releve_Notes');
  }

  onPrintFicheStatistique() {

  }

  onEditNote(eleve: any, content: TemplateRef<any>, i: number) {
    this.currentIndex = i;
    this.toUpdate =  true;
    this.currentEleve = eleve;
    this.editionNoteForm.patchValue({
      matricule: eleve.matricule,
      noms: eleve.noms,
      note: eleve.note,
      observation: eleve.observation,
      idNote: eleve.idNote
    });
    this.modalService.open(content,
      {ariaLabelledBy: 'edit-note', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onSaveEditionNote() {
    this.submitted = true;
    if (this.editionNoteForm.invalid) {
      return;
    }
    let dto;
    if (this.g.idNote.value == null) {
      dto = new RemplirNoteModel(this.currentClasse.id, this.currentSequence.id_sequence, []);
      dto.eleveNotes.push(new EleveNoteModel(this.g.matricule.value, this.g.note.value, this.g.observation.value));
      this.genericsService.postResource(`admin/note/saisie`, dto).then((response: any) => {
        this.genericsService.confirmResponseAPI(response.message, 'success');
      }).catch(reason => {
        this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
      });
    } else {
      dto = new ModifierNoteModel(this.currentClasse.id, this.g.idNote.value,
        this.currentSequence.id_sequence, this.g.matricule.value, this.g.note.value, this.g.observation.value);
      this.genericsService.postResource(`admin/note/modifier`, dto).then((response: any) => {
        this.genericsService.confirmResponseAPI(response.message, 'success');
        this.tNotes.at(this.currentIndex).patchValue({ note: this.g.note.value,  observation: this.g.observation.value});
        this.onReset();
      }).catch(reason => {
        console.log(reason);
        this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
      });
    }
  }
  private initFormEditNote() {
    this.editionNoteForm = this.fb.group({
      matricule: [''],
      noms: [''],
      idNote: [null],
      note: [0, [Validators.min(0),
        Validators.max(this.currentClasse ? this.currentClasse.baremeEvaluation : 20)]],
      observation: ['-'],
    });
  }
  onReset() {
    this.submitted = false;
    this.toUpdate = false;
    this.initFormEditNote();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
}
