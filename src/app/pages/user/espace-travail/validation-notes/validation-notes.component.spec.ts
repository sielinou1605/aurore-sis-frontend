import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ValidationNotesComponent} from './validation-notes.component';

describe('ValidationNotesComponent', () => {
  let component: ValidationNotesComponent;
  let fixture: ComponentFixture<ValidationNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidationNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
