import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VWelcomeComponent} from './v-welcome/v-welcome.component';
import {ProcessValidationComponent} from './process-validation/process-validation.component';
import {ValidationNotesComponent} from './validation-notes.component';

const routes: Routes = [
  {
    path: '', component: ValidationNotesComponent,
    data: {
      breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Validation of notes' : 'Validation de notes',
     titre: localStorage.getItem('activelang') === 'en' ? 'Validation of notes | Aurore' : 'Validation de notes | Aurore',
      icon: 'icofont-certificate-alt-2 bg-c-blue',
      breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Display the filling status and validate the filling of notes - Notes' : 'Afficher l\'état de remplissage et valider le remplissage de notes - Notes',
      status: true,
      current_role: 'enseignant'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome', component: VWelcomeComponent,
        data: {
          breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Validation of notes' : 'Validation de notes',
          titre: localStorage.getItem('activelang') === 'en' ? 'Validation of notes | Aurore' : 'Validation de notes | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Display the filling status and validate the filling of notes - Notes' : 'Afficher l\'état de remplissage et valider le remplissage de notes - Notes',
          status: true,
          current_role: 'enseignant'
        }
      },
      {
        path: 'process-validation/:type/:idType', component: ProcessValidationComponent,
        data: {
          breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Validations' : 'Validations',
          titre: localStorage.getItem('activelang') === 'en' ? 'Validations | Aurore' : 'Validations | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Validation of grades Sequential, Quarterly and or Annual - Validation' : 'Validation des notes Sequentiel, Trimestriel et ou Annuel - Validation',
          status: true,
          current_role: 'enseignant'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ValidationNotesRoutingModule { }
