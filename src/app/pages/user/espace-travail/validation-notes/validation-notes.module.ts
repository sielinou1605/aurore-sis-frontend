import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ValidationNotesRoutingModule} from './validation-notes-routing.module';
import {ValidationNotesComponent} from './validation-notes.component';
import {VWelcomeComponent} from './v-welcome/v-welcome.component';
import {SharedModule} from '../../../../shared/shared.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {ReactiveFormsModule} from '@angular/forms';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {ProcessValidationComponent} from './process-validation/process-validation.component';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ValidationNotesComponent, VWelcomeComponent, ProcessValidationComponent],
    imports: [
        CommonModule,
        ValidationNotesRoutingModule,
        SharedModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        NgMultiSelectDropDownModule,
        TranslateModule
    ]
})
export class ValidationNotesModule { }
