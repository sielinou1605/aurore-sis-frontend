/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-v-welcome',
  templateUrl: './v-welcome.component.html',
  styleUrls: ['./v-welcome.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class VWelcomeComponent implements OnInit, OnDestroy {
  submitted: boolean;
  bulletinSequentielForm: FormGroup;
  bulletinTrimestreForm: FormGroup;
  public filtreSequence: any;
  public filtreTrimestre: any;
  public bulletinAnnuelForm: any;
  public annees: any;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    public genericsService: GenericsService, private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    private router: Router, public authService: AuthService,
    private translate: TranslateService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(26);

      this.siteLanguage = localStorage.getItem('activelang');
      this.translate.addLangs(this.supportLanguages);
      if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
        this.translate.setDefaultLang(this.siteLanguage);
      } else {
        this.translate.setDefaultLang(this.supportLanguages[0]);
      }
  }

  ngOnInit() {
    this.initBulletinSequentielleForm();
    this.initBulletinTrimestrielleForm();
    this.initBulletinAnnuelForm();
    this.onloadBulletinSequenceFiltre();
    this.onloadBulletinTrimestreFiltre();
    this.onloadListAnnees();
  }

  private initBulletinSequentielleForm() {
    this.bulletinSequentielForm = this.fb.group({
      sequence: ['', Validators.required]
    });
  }
  private initBulletinTrimestrielleForm() {
    this.bulletinTrimestreForm = this.fb.group({
      trimestre: ['', Validators.required]
    });
  }

  private initBulletinAnnuelForm() {
    this.bulletinAnnuelForm = this.fb.group({
      annee: ['', Validators.required]
    });
  }
  private onloadBulletinSequenceFiltre() {
    this.genericsService.getResource(`admin/bulletin/filtre/sequence`).then((response: any) => {
      this.filtreSequence = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadBulletinTrimestreFiltre() {
    this.genericsService.getResource(`admin/bulletin/filtre/trimestre`).then((response: any) => {
      this.filtreTrimestre = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  onDisplayListBulletin(type: string) {
    this.submitted = true;
    if (type == 'sequence') {
      if (this.bulletinSequentielForm.invalid) {
        return;
      }
      this.router.navigate(
        [`gestion-notes/validation-notes/process-validation/${type}/${this.bs.sequence.value}`]
      ).then((result: any) => {
        this.onResetBulletinSequentielle();
      });
    } else if (type == 'trimestre') {
      if (this.bulletinTrimestreForm.invalid) {
        return;
      }
      this.router.navigate(
        [`gestion-notes/validation-notes/process-validation/${type}/${this.bt.trimestre.value}`]
      ).then((result: any) => {
        this.onResetBulletinTrimestre();
      });
    } else {
      if (this.bulletinAnnuelForm.invalid) {
        return;
      }
      this.router.navigate(
        [`gestion-notes/validation-notes/process-validation/${type}/${this.ba.annee.value}`]
      ).then((result: any) => {
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    }
  }
  get bs() { return this.bulletinSequentielForm.controls; }
  get bt() { return this.bulletinTrimestreForm.controls; }
  get ba() { return this.bulletinAnnuelForm.controls; }
  onResetBulletinTrimestre() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initBulletinTrimestrielleForm();
  }
  onResetBulletinSequentielle() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initBulletinSequentielleForm();
  }
  onResetBulletinAnnuel() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onSelectSequence(content: TemplateRef<any>) {
    if (this.filtreSequence && this.authService.isActif('bouton-bulletin-sequentiel')) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-sequence', scrollable: true}).result.then((result) => {
      }, (reason) => {
      });
    }
  }
  onSelectTrimestre(content: TemplateRef<any>) {
    if (this.filtreTrimestre && this.authService.isActif('bouton-bulletin-trimestriel')) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-trimestre', scrollable: true}).result.then((result) => {
      }, (reason) => {
      });
    }
  }

  onSelectAnnuel(content: TemplateRef<any>) {
    if (this.filtreTrimestre && this.authService.isActif('bouton-bulletin-annuel')) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-annuel', scrollable: true}).result.then((result) => {
      }, (reason) => {
      });
    }
  }

  ngOnDestroy(): void {
    this.onResetBulletinTrimestre();
    this.onResetBulletinSequentielle();
    this.onResetBulletinAnnuel();
  }

  private onloadListAnnees() {
    this.genericsService.getResource(`admin/annees`).then((response: any) => {
      this.annees = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
}
