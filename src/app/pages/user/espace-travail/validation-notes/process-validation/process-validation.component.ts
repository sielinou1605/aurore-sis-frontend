/* tslint:disable:triple-equals */
import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-process-validation',
  templateUrl: './process-validation.component.html',
  styleUrls: ['./process-validation.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ProcessValidationComponent implements OnInit {
  @ViewChild('modalValiderNotes', { static: true }) validerNote: ElementRef;
  selectSequenceForm: FormGroup;
  validerNoteForm: FormGroup;
  submitted: boolean;
  public sequences: any;
  public pageSize = 20;
  public page = 0;
  public totalElements = 0;
  public classes: any;
  public remplissages: any;
  public settings: any;
  public currentPeriode: any;
  public listClasse: any;
  public type: string;
  public idType: string;
  public annees: any;
  private trimestres: any;
  public choix: string;
  constructor(
    public genericsService: GenericsService, private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    public authService: AuthService, private route: ActivatedRoute, private router: Router
  ) {
    this.choix = localStorage.getItem('activelang') === 'en' ? 'Choose value' : 'Choisir la valeur';
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.settings = {
      singleSelection: false,
      idField: 'id',
      textField: 'nomClasse',
      enableCheckAll: true,
      selectAllText: 'Tout cocher',
      unSelectAllText: 'Tout decocher',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 220,
      itemsShowLimit: 10,
      searchPlaceholderText: 'Rechercher',
      noDataAvailablePlaceholderText: 'Aucune donnée disponible',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    };
    this.initFormSelectSequence();
    this.initFormValiderNotesClasses();
    this.route.paramMap.subscribe( params => {
      this.type = params.get('type');
      this.idType = params.get('idType');
      this.onloadCurrentData();
      this.onValiderSelectSequence();
    });
  }

  private onloadCurrentData() {
    if (this.type == 'sequence') {
      this.genericsService.getResource(`admin/bulletin/filtre/sequence`).then((response: any) => {
        this.sequences = response.data.sequences;
        this.classes = response.data.classes;
        this.currentPeriode = this.sequences.find(elt => elt.id == this.idType);
        console.log(this.classes);
        this.listClasse = [];
        this.classes.forEach(elt => {
          this.listClasse.push({id: elt.id, nomClasse: elt.nomClasse});
        });
      }).catch(reason => {
        console.log(reason);
      });
    } else if (this.type == 'trimestre') {
      this.genericsService.getResource(`admin/bulletin/filtre/trimestre`).then((response: any) => {
        this.trimestres = response.data.trimestres;
        this.currentPeriode = this.trimestres.find(elt => elt.id_trimestre == this.idType);
      }).catch(reason => {
        console.log(reason);
      });
    } else {
      this.genericsService.getResource(`admin/annees`).then((response: any) => {
        this.annees = response.data;
        this.currentPeriode = this.annees.find(elt => elt.id == this.idType);
      }).catch(reason => {
        console.log(reason);
      });
    }
  }
  onValiderNoteModal(content: TemplateRef<any>) {
    this.submitted = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'valider-notes'}).result.then((result) => {
    }, (reason) => {
    });
  }

  onValiderSelectSequence() {
    let url;
    if (this.type == 'sequence') {
      url  = `admin/etat-remplissage/sequence/${this.idType}/general`;
    } else if (this.type == 'trimestre') {
      url  = `admin/etat-remplissage/trimestre/${this.idType}/general`;
    } else {
      url  = `admin/etat-remplissage/annee/${this.idType}/general`;
    }
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(url).then((response: any) => {
      this.remplissages = response.data;
      console.log(this.remplissages);
      this.genericsService.stopLoadingPage();
      this.onResetModal();
    }).catch(reason => {
      console.log(reason);
    });
  }

  onResetModal() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }

  onSaveValiderNotes() {
    this.submitted = true;
    if (this.validerNoteForm.invalid) {
      return;
    }
    const classes = [];
    this.ns.classes.value.forEach(elt => classes.push(elt.id));
    this.onProcessValidateNotes(classes);
  }

  onProcessValidateNotes(classes: number[]) {
    let url;
    if (this.type == 'sequence') {
      url = `admin/note/sequence/${this.currentPeriode.id}/synchroniser`;
    } else if (this.type == 'trimestre') {
      url = `admin/note/trimestre/${this.currentPeriode.id_trimestre}/synchroniser`;
    } else {
      url = `admin/note/annee/synchroniser`;
    }
    this.genericsService.startLoadingPage();
    this.genericsService.postResource(url, classes).then((response: any) => {
      this.genericsService.stopLoadingPage();
      this.genericsService.confirmResponseAPI(response.message, 'success', 8000);
    }).catch(reason => {
      this.genericsService.confirmResponseAPI('Operation échouée !!!', 'error', 8000);
      this.genericsService.stopLoadingPage();
    });
  }

  onValidateNoteClasse(classe: any) {
    if (classe.tauxClasse < 100) {
      this.genericsService.confirmResponseAPI('Il faut remplir les notes a 100% dans cette classe !!!', 'error', 8000);
    } else {
      this.onProcessValidateNotes([classe.idClasse]);
    }
  }

  private initFormSelectSequence() {
    this.selectSequenceForm = this.fb.group({
      sequence: ['', Validators.required]
    });
  }
  private initFormValiderNotesClasses() {
    this.validerNoteForm = this.fb.group({
      classes: ['', Validators.required],
    });
  }
  get fs() { return this.selectSequenceForm.controls; }
  get ns() { return this.validerNoteForm.controls; }
}
