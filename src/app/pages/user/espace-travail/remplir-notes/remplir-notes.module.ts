import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RemplirNotesRoutingModule} from './remplir-notes-routing.module';
import {RemplirNotesComponent} from './remplir-notes.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [RemplirNotesComponent],
    imports: [
        CommonModule,
        RemplirNotesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class RemplirNotesModule { }
