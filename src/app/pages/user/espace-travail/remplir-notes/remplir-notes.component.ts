/* tslint:disable:triple-equals */
import { Component, ElementRef, HostListener, Inject, LOCALE_ID, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { GenericsService } from '../../../../shared/_services/generics.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EleveNoteModel,
  ModifierNoteModel,
  RemplirNoteModel
} from '../../../../shared/_models/request-dto/graduation/gestion-notes-enseignant_model';
import { AuthService } from '../../../../shared/_services/auth.service';
import { EleveNoteResponse } from '../../../../shared/_models/response-dto/graduation/filtre-note-absence-model';
import { TokenStorageService } from '../../../../shared/_services/token-storage.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-remplir-notes',
  templateUrl: './remplir-notes.component.html',
  styleUrls: ['./remplir-notes.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class RemplirNotesComponent implements OnInit, OnDestroy {
  @ViewChild('modalSelectSequence', { static: true }) modalSequence: ElementRef;
  public selectSequenceClasseForm: FormGroup;
  public formSelect: any;
  public notesForm: FormGroup;
  public noteGlobaleForm: FormGroup;
  public currentSequence: any;
  private idmatiereclasse: string;
  private idsequence: string;
  public currentMatiereClasse: any;
  public elevesNote: EleveNoteResponse[];
  public submitted = false;
  istrue: boolean;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  fileToUpload: File | null = null;
   matierDestination: any;
  filtreNote: any;

  constructor(private genericsService: GenericsService, private fb: FormBuilder,
              private modalService: NgbModal, config: NgbModalConfig,
              public tokenStorage: TokenStorageService, private route: ActivatedRoute,
              private router: Router, public authService: AuthService,
              @Inject(LOCALE_ID) locale: string,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(60);
    console.log('locale', locale);
  }

  ngOnInit() {
    this.initNotesForm();
    this.route.paramMap.subscribe(params => {
      this.idmatiereclasse = params.get('matiereclasse');
      this.idsequence = params.get('sequence');
      this.initHeader(this.idmatiereclasse, this.idsequence);
    });

    this.getClassDestination();
  }

  private initNotesForm() {
    this.notesForm = this.fb.group({
      notesData: new FormArray([])
    });
  }

  // les elements du formulaire globale
  get fNotes() { return this.notesForm.controls; }
  // les elements du formulaire local
  get tNotes() { return this.fNotes.notesData as FormArray; }
  // les elements du formulaire edition de note
  get g() { return this.noteGlobaleForm.controls; }

  private initHeader(matiereclasse: string, sequence: string) {
    const url1 = `admin/consulter/${matiereclasse}/matiereclasse`;
    const url2 = `prefet/note/matiereclasse/${matiereclasse}/sequence/${sequence}/saisie`;
    const url3 = `admin/consulter/${sequence}/sequence`;
    this.genericsService.getResource(url1).then((response: any) => {
      this.currentMatiereClasse = response.data;
    }).catch(err => {
      console.log(err);
    });
    const bareme = this.currentMatiereClasse ? parseInt(this.currentMatiereClasse.bareme_evaluation, 0) : 20;
    this.genericsService.getResource(url2).then((response: any) => {
      this.elevesNote = response.data;
      console.log('eleves', this.elevesNote);
      this.elevesNote.forEach(item => {
        // console.log(item)
        if (item.noteAtt > 0) {
          this.istrue = true;
        }
        this.tNotes.push(
          this.fb.group({
            idNote: [item.idNote],
            matricule: [item.matricule],
            noms: [item.nomEleve],
            note: [item.noteAtt, [Validators.min(0), Validators.max(bareme)]],
            observation: [item.observation],
            update: false,
            canUpdate: item.canUpdate
          })
        );
      });
    }).catch(err => {
      console.log(err);
    });
    this.genericsService.getResource(url3).then((response: any) => {
      this.currentSequence = response.data;
      console.log('Seq : ', response.data);
    }).catch(err => {
      console.log(err);
    });
  }

  onEditNoteGlobale(content: TemplateRef<any>, classe) {
    const bareme = classe ? classe.bareme_evaluation : 20;
    this.noteGlobaleForm = this.fb.group({
      matricule: [''],
      noms: [''],
      idNote: [null],
      note: [0, [Validators.min(0), Validators.max(bareme)]],
      observation: ['-'],
    });
    this.modalService.open(content,
      { ariaLabelledBy: 'edit-note', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => {
    });
  }

  onSaveNoteGlobale() {
    this.submitted = true;
    if (this.noteGlobaleForm.invalid) {
      return;
    }
    let j;
    j = 1;
    this.elevesNote.forEach((elt, i) => {
      const dto = new RemplirNoteModel(this.currentMatiereClasse.id_matiereclasse, this.currentSequence.id_sequence, []);
      dto.eleveNotes.push(new EleveNoteModel(elt.matricule, this.g.note.value, this.g.observation.value));
      this.genericsService.postResource(`admin/note/saisie`, dto).then((response: any) => {
        if (j === this.elevesNote.length) {
          this.genericsService.confirmResponseAPI(response.message + ' ' + elt.matricule, 'success');
        }
        j = j + 1;
      }).catch(reason => {
        if (j === this.elevesNote.length) {
          this.genericsService.confirmResponseAPI('Enregistrement échoué ' + elt.matricule, 'error');
        }
        j = j + 1;
      });
    });
    this.initNotesForm();
  }

  onReset() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }

  onObservation(i: number, obs: string) {
    this.tNotes.at(i).patchValue({ observation: obs, update: true });
  }

  // gestion du clavier sur les input
  @HostListener('window:keyup', ['$event']) keyUp(e: KeyboardEvent) {
    if ((e.code == 'Insert' || e.code == 'Enter')) {
      this.onSubmitNotes();
    }
  }

  onSubmitNotes() {
    const listToDelete = [];


    console.log('current_sequence', this.currentSequence);
    const dto = new RemplirNoteModel(this.currentMatiereClasse.id_matiereclasse, this.currentSequence.id_sequence, []);

    this.tNotes.controls.forEach((fg, i) => {
      if ((fg.value.note > 0 || fg.value.observation == 'malade' || fg.value.observation == 'absent') && fg.value.update == true) {
        if (!this.tNotes.at(i).invalid) {
          if (fg.value.canUpdate) {
            console.log(this.currentSequence);
            const dtoUpdate = new ModifierNoteModel(this.currentMatiereClasse.id_matiereclasse, fg.value.idNote,
              this.currentSequence.id_sequence, fg.value.matricule, fg.value.note, fg.value.note > 0 ? '-' : fg.value.observation);
            this.genericsService.postResource(`admin/note/modifier`, dtoUpdate).then((response: any) => {
              console.log('Mise a jour d\'une note existante');
              this.genericsService.confirmResponseAPI(response.message, 'success');
              this.tNotes.removeAt(i);
            }).catch(reason => {
              console.log(reason);
              this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
            });
          } else if (this.tNotes.at(i).value.update) {
            this.tNotes.at(i).patchValue({ update: false });
            dto.eleveNotes.push(new EleveNoteModel(fg.value.matricule, fg.value.note, fg.value.observation));
            listToDelete.push(i);
          }
        }
      }
    });
    if (dto.eleveNotes.length > 0) {
      this.genericsService.postResource(`admin/note/saisie`, dto).then((response: any) => {
        console.log('Enregistrement nouvelle note');
        listToDelete.forEach(i => this.tNotes.removeAt(i));
        this.genericsService.confirmResponseAPI(response.message, 'success');
      }).catch(reason => {
        this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
      });
    }
  }


  onSelectedFile(event) {
    console.log(event);
    this.fileToUpload = event.target.files[0];

    console.log(this.fileToUpload);
    this.onImportExport();
  }

  onImportExport() {

    console.log(this.fileToUpload);
    const renameFile = this.fileToUpload.name.substring(0, this.fileToUpload.name.indexOf('3') + 1) + '.xls';
    console.log(renameFile);
    const formData = new FormData();
    if (this.fileToUpload != null) {
      formData.append('file', this.fileToUpload, this.fileToUpload.name);
      this.genericsService.uploadFile(`admin/import/notes/eleves`, formData).subscribe((response: any) => {
        console.log(response);
        this.genericsService.confirmResponseAPI(response.message, 'success');
      }, error => {
        console.error(error);
        this.genericsService.confirmResponseAPI(error.error.message, 'error');
      });
    }
    console.log(this.fileToUpload);

  }

  export() {
    this.genericsService.reportPostResource(`admin/export/releve-note/matiere-classe/${this.idmatiereclasse}/sequence/${this.idsequence}`)
      .then((result: any) => {
        console.log(result);
        // this.genericsService.startLoadingPage();
        const date = new Date().getDate();
        // const renameFile = this.fileToUpload.name.substring(0, this.fileToUpload.name.indexOf('3') + 1) + '.xls';
        const fileName = 'Note_exporte_' + this.idmatiereclasse + '_' + this.idsequence;
        this.genericsService.getByteArrayAndSaveReportPDFNote(result, fileName, true);
        // this.genericsService.stopLoadingPage();
      }).catch((err) => {
      console.log(err);
      this.genericsService.confirmResponseAPI(err.message, 'error');
    });
  }


  openImportExport(modalImportExport: TemplateRef<any>, currentMatiereClasse: any) {
    console.log(currentMatiereClasse);
    this.onRefreshNotes();
    this.modalService.open(modalImportExport,
      {ariaLabelledBy: 'edit-note', size: 'lg', scrollable: true}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  getClassDestination() {
    console.log(this.currentMatiereClasse);
    console.log(this.matierDestination);
    const idClasse = this.currentMatiereClasse ? this.currentMatiereClasse.classe.id : null;
    this.genericsService.getResource(`admin/classe/${idClasse}/matieres`).then((responses: any) => {
      if (responses.success == true) {
        this.filtreNote = responses.data;
        this.matierDestination = this.filtreNote;
        console.log(responses);
      }
    }).catch(err => {
      console.log(err);
    });
  }

  onChanStateUpdate(i: number) {
    console.log(i);
    this.tNotes.at(i).patchValue({ update: true });
  }

  ngOnDestroy(): void {
    if (this.modalService.hasOpenModals()) {
      this.onReset();
    }
  }

  onRefreshNotes() {
    this.tNotes.clear();
    this.initHeader(this.idmatiereclasse, this.idsequence);
  }
}
