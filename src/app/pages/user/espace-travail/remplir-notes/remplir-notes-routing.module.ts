import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RemplirNotesComponent} from './remplir-notes.component';


const routes: Routes = [
  {
    path: '', component: RemplirNotesComponent,
    data: {
      breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Entering notes' : 'Saisie de notes',
      titre: localStorage.getItem('activelang') === 'en' ? 'Entering notes | Aurore' : 'Saisie de notes | Aurore',
      icon: 'icofont-pen-alt-4 bg-c-lite-green',
      breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Entering grades for a class and in a specific sequence - Entering grades' : 'Saisie des notes pour une classe et dans une séquence précise - Saisie de notes',
      status: true,
      current_role: 'enseignant'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RemplirNotesRoutingModule { }
