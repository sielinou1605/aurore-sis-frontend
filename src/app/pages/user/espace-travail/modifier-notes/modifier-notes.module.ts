import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ModifierNotesRoutingModule} from './modifier-notes-routing.module';
import {ModifierNotesComponent} from './modifier-notes.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ModifierNotesComponent],
    imports: [
        CommonModule,
        ModifierNotesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class ModifierNotesModule { }
