import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModifierNotesComponent} from './modifier-notes.component';

describe('ModifierNotesComponent', () => {
  let component: ModifierNotesComponent;
  let fixture: ComponentFixture<ModifierNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
