/* tslint:disable:triple-equals */
import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {ModifierNoteModel} from '../../../../shared/_models/request-dto/graduation/gestion-notes-enseignant_model';
import {AuthService} from '../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-modifier-notes',
  templateUrl: './modifier-notes.component.html',
  styleUrls: ['./modifier-notes.component.scss']
})
export class ModifierNotesComponent implements OnInit , OnDestroy {
  @ViewChild('modalSelectSequence', { static: true }) modalSequence: ElementRef;
  eleves: any;
  selectSequenceClasseForm: FormGroup;
  submitted: boolean;
  private base_url = 'admin/eleve/';
  public formSelect: any;
  public currentSequence: any;
  public currentClasse: any;
  private notesForm: FormGroup;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(private genericsService: GenericsService,  private fb: FormBuilder,
              private modalService: NgbModal, config: NgbModalConfig,
              private router: Router, public authService: AuthService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1){
      this.translate.setDefaultLang(this.siteLanguage);
    }else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(61);
  }

  ngOnInit() {
    this.onloadDataFilter();
    this.initFormSelectSequence();
    this.initNotesForm();
    this.modalService.open(this.modalSequence,
      {ariaLabelledBy: 'select-sequence', scrollable: true}).result.then((result) => {
    }, (reason) => {
      console.log(reason);
    });
  }

  private initFormSelectSequence() {
    this.selectSequenceClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      classe: ['', Validators.required],
      matricule: ['', Validators.required]
    });
  }
  get f() { return this.selectSequenceClasseForm.controls; }

  onDisplayListeEleve() {
    this.submitted = true;
    if (this.selectSequenceClasseForm.invalid) {
      return;
    }
    const classeMatiere = this.f.classe.value.split('___');
    this.currentClasse = this.formSelect.classes.find(item =>
      (item.classe.nomClasse == classeMatiere[0] && item.matiere.id == classeMatiere[1]));
    this.currentSequence = this.formSelect.sequences.find(item => item.id_sequence == this.f.sequence.value);
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.onLoadListEleves(this.f.matricule.value);
  }

  private initNotesForm() {
    this.notesForm = this.fb.group({
      notesData: new FormArray([])
    });
  }

  public onLoadListEleves(event) {
    this.genericsService.getResource(`${this.base_url}filtrer?predicat=${event}`).then((result: any) => {
      if (result.success === true) {
        this.eleves = result.data;
        this.eleves.forEach(item => {
          this.tNotes.push(
            this.fb.group({
              matricule: [item.matricule],
              noms: [item.nomEleve + ' ' + item.prenomEleve],
              note: [0, [Validators.min(0),
                Validators.max(this.currentClasse ? this.currentClasse.baremeEvaluation : 20)]],
              observation: ['-'],
              idNote: 0,
              update: false
            })
          );
        });
      }
      if (this.currentClasse && this.currentClasse.notes.length > 0) {
        this.currentClasse.notes.forEach(item => {
          const idx = this.tNotes.controls.findIndex(elt => elt.value.matricule == item.eleve.matricule
            && item.sequenc.id_sequence == this.f.sequence.value);
          if (idx > -1) {
            this.tNotes.at(idx).patchValue({ note: item.noteatt, idNote: item.id_note, observation: item.observation});
          }
        });
      }
    }, err => {
      console.log(err);
    });
  }
  private onloadDataFilter() {
    this.genericsService.getResource(`admin/note/enseignant/filtre/saisie`).then((result: any) => {
      if (result.success === true) {
        this.formSelect = result.data;
      } else {
        this.genericsService.confirmResponseAPI(result.message, 'error');
        this.formSelect = result.data;
      }
      console.log(this.formSelect.classes);
    }).catch((reason: any) => {
      console.log(reason);
      this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des classes', 'error');
    });
  }

  // les elements du formulaire globale
  get fNotes() { return this.notesForm.controls; }
  // les elements du formulaire local
  get tNotes() { return this.fNotes.notesData as FormArray; }

  onObservation(i: number, obs: string) {
    this.tNotes.at(i).patchValue({
      observation: (this.tNotes.at(i).value.observation == 'malade'
        || this.tNotes.at(i).value.observation == 'absent') ? '-' :  obs, update: true});
    console.log(this.tNotes.at(i).value.observation);
  }

  onResetSaisieNote() {
    this.router.navigate(['/gestion-notes']).then(() => {
      this.modalService.dismissAll();
    });
  }

  onChanStateUpdate(i: number) {
    this.tNotes.at(i).patchValue({update: true});
  }

  // gestion du clavier sur les input
  @HostListener('window:keyup', ['$event']) keyUp(e: KeyboardEvent) {
    if ((e.code == 'Insert' || e.code == 'Enter')) {
      const dto = new ModifierNoteModel(this.currentClasse.id, this.tNotes.at(0).value.idNote,
        this.currentSequence.id_sequence, this.tNotes.at(0).value.matricule,
        this.tNotes.at(0).value.note, this.tNotes.at(0).value.observation);
       this.genericsService.postResource(`admin/note/modifier`, dto).then((response: any) => {
         this.genericsService.confirmResponseAPI(response.message, 'success');
       }).catch(reason => {
         console.log(reason);
         this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
       });
    }
  }
  ngOnDestroy(): void {
    if (this.modalService.hasOpenModals()) {
      this.onResetSaisieNote();
    }
  }
}
