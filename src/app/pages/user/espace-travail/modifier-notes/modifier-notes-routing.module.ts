import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ModifierNotesComponent} from './modifier-notes.component';


const routes: Routes = [
  {
    path: '', component: ModifierNotesComponent,
    data: {
      breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Note' : 'Notes',
      titre: localStorage.getItem('activelang') === 'en' ? 'Note | Aurore' : 'Notes | Aurore',
      icon: 'icofont-pen-alt-4 bg-c-lite-green',
      breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Changing the student\'s grade - Grades' : 'Modification de la note de l\'élève - Notes',
      status: true,
      current_role: 'enseignant'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModifierNotesRoutingModule { }
