import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FichesProgressionComponent} from './fiches-progression.component';

describe('FichesProgressionComponent', () => {
  let component: FichesProgressionComponent;
  let fixture: ComponentFixture<FichesProgressionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichesProgressionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichesProgressionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
