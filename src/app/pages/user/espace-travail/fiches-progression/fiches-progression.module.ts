import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FichesProgressionRoutingModule} from './fiches-progression-routing.module';
import {FichesProgressionComponent} from './fiches-progression.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [FichesProgressionComponent],
    imports: [
        CommonModule,
        FichesProgressionRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class FichesProgressionModule { }
