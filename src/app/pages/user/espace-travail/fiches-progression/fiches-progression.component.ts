/* tslint:disable:triple-equals */
import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../shared/_services/auth.service';
import {AddFicheProgressionModel} from '../../../../shared/_models/request-dto/graduation/gestion-fiche-progression-request-model';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-fiches-progression',
  templateUrl: './fiches-progression.component.html',
  styleUrls: ['./fiches-progression.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class FichesProgressionComponent implements OnInit, OnDestroy {
  @ViewChild('modalSelectSequence', { static: true }) modalSequence: ElementRef;
  eleves: any;
  selectSequenceClasseForm: FormGroup;
  submitted: boolean;
  public sequences: any;
  public classes: any;
  public currentSequence: any;
  public progressions: any[] = [];
  progressionForm: FormGroup;
  public matiereclasses: any;
  private toUpdate: boolean;
  public pageSize = 15;
  public page = 0;
  public totalElements: number;
  public enseignants: any;
  public matierecClassesTmp: any[] = [];
  selectedItem: any;
  enseignantSelected: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  private dataFilter = '';
  public nomEnseignant = '';
   matiereClassesSelected: any;
   isEnseignant = false;
   matiereClasseSelect: any;
   enseignantConected: any;
   enseignantListe: any;
   enseignantsTmp: any;
   progressionsTmp: any;
   progressionsCurent: any[];
   matiereclassesPatch: any;
   progressionsListe: any[] = [];
  matiereProgressionsListe: any[] = [];
   progressionSelected: any;
  constructor(private genericsService: GenericsService,  private fb: FormBuilder,
              private modalService: NgbModal, config: NgbModalConfig,
              private router: Router, public tokenStorage: TokenStorageService,
              public authService: AuthService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(63);
  }

  ngOnInit() {
    this.progressionsListe = [];
    this.matiereProgressionsListe = [];

    this.onloadDataFilter();
    this.onloadListeClasse();
    this.initFormSelectSequence();
    this.initFormFicheProgression();
    this.modalService.open(this.modalSequence,
      {ariaLabelledBy: 'select-sequence', scrollable: true}).result.then((result) => {
    }, (reason) => {
      console.log(reason);
    });
    this.onLoadListEleves(null);
    console.log(this.f.sequence.value);
  }

  private initFormSelectSequence() {
    this.selectSequenceClasseForm = this.fb.group({
      sequence: ['', Validators.required],
    });
  }
  get f() { return this.selectSequenceClasseForm.controls; }
  private onloadListeClasse() {
    this.genericsService.getResource(`admin/consulter/classes/enseignant`).then((response: any) => {
      console.log(response);
      if (response.success === true) {
        this.matiereclasses = response.data;
      }
    }).catch(reason => {
      console.log(reason);
    });
  }
  onDisplayListeEleve() {
    this.submitted = true;
    if (this.selectSequenceClasseForm.invalid) {
      return;
    }
    console.log('Seq : ', this.sequences)
    this.currentSequence = this.sequences.find(item => item.id == this.f.sequence.value);
    console.log(this.currentSequence);
    this.onloadListFicheProgression();

  }
  private onloadDataFilter() {
    this.genericsService.getResource(`admin/bulletin/filtre/sequence`).then((result: any) => {
      this.sequences = result.data.sequences;
      if (result.success === true) {
        this.classes = result.data.classes;
      }
    }).catch((reason: any) => {
      console.log(reason);
      this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des classes', 'error');
    });
  }
  private onloadListFicheProgression() {
    this.genericsService.getResource(`admin/fiche-progression/sequence/${this.f.sequence.value}/liste`)
      .then((response: any) => {
        console.log(response);
        this.isEnseignant = !!response.data.enseignant;
        this.enseignantConected = response.data.enseignant;
        // console.log(this.isEnseignant);
        // console.log(this.enseignants);
        // console.log(this.enseignantConected);
        // tslint:disable-next-line:no-shadowed-variable
        this.enseignants.forEach((item: any) => {
          this.enseignants.push(item);

        });
        this.progressions = response.data.progessions;
        console.log(this.progressions);

        if ((this.dataFilter != '' && this.dataFilter != null) && !this.isEnseignant) {
          this.progressionsListe = [];
          this.matiereProgressionsListe = [];
          this.enseignantSelected = this.enseignants[0];

          // console.log(this.enseignantSelected);
          // console.log(this.progressions);

          this.enseignantSelected.matiereClasses.forEach((item2: any) => {
            const objetDuplique = this.progressions.find((item1: any) => {
              return item1.matiereClasse.idMatiereClasse === item2.idMatiereClasse;
            });
            // console.log(objetDuplique);
            if (objetDuplique) {
              this.progressionsListe.push(objetDuplique);
            } else {
              this.matiereProgressionsListe.push(item2);
            }
          });
        }
        if ((this.dataFilter != '' && this.dataFilter != null) || this.isEnseignant) {
          this.progressionsListe = [];
          this.matiereProgressionsListe = [];
          // console.log(this.tokenStorage.getUser());
          const idUser = parseInt(String(this.tokenStorage.getUser().id), 10);
          // console.log(idUser);
          this.enseignantSelected = this.enseignants.find((item: any) => item.enseignant.idUtilisateur === idUser);
          console.log(this.enseignantSelected);
          this.enseignantSelected.matiereClasses.forEach((item2: any) => {
            const objetDuplique = this.progressions.find((item1: any) => {
              return item1.matiereClasse.idMatiereClasse === item2.idMatiereClasse;
            });
            console.log(objetDuplique);
            if (objetDuplique) {
              this.progressionsListe.push(objetDuplique);
            } else {
              this.matiereProgressionsListe.push(item2);
            }
          });
        }

        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      }).catch(reason => {
      this.onResetSaisieNote();
      this.genericsService.confirmResponseAPI(reason.error.data.valeurFr, 'error', 10000);
    });
  }
  onResetSaisieNote() {
    this.router.navigate(['/gestion-notes']).then(() => {
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
    });
  }
  ngOnDestroy(): void {
    if (this.modalService.hasOpenModals()) {
      this.onResetSaisieNote();
    }
  }

  private initFormFicheProgression() {
    this.progressionForm = this.fb.group({
      autorisation_remplissage: true,
      id_ficheprogression: [null],
      matiereclasse_id: [''],
      nbre_heures_faites: [0, Validators.required],
      nbre_heures_prevues: [0, Validators.required],
      nbre_lecons_faites: [0, Validators.required],
      nbre_lecons_prevues: [0, Validators.required],
      nom_ficheprogression: ['', Validators.required],
      sequence_id: [null, Validators.required],
      enseignantSelect: ['']
    });

  }
  get p() { return this.progressionForm.controls; }

  @HostListener('window:keyup', ['$event']) keyUp(e: KeyboardEvent) {
    if ((e.code == 'Insert' || e.code == 'Enter')) {
      // console.log(this.isEnseignant);

      // console.log(this.tokenStorage.getUser());
      // console.log(this.p);
      // console.log(this.matiereclasses);
      // console.log(this.enseignants);
      // console.log(this.progressionSelected);
      // console.log(this.p.matiereclasse_id.value);
      let matiereClasse = this.matiereclasses.find(elt => elt.id == this.progressionSelected.idMatiereClasse);
      // console.log(matiereClasse);
      if (matiereClasse != undefined) {
        this.progressionForm.patchValue({
          nom_ficheprogression: 'Fiche progression ' + matiereClasse.matiere.nomMatiere + ' ' + matiereClasse.classe.nomClasse,
          sequence_id:  this.currentSequence.id
        });
      } else {
        matiereClasse = this.enseignants[0].matiereClasses.find(elt => elt.idMatiereClasse == this.progressionSelected.idMatiereClasse);
        // console.log(matiereClasse);

        if (matiereClasse === undefined) {
          matiereClasse = this.enseignantSelected.matiereClasses.find(elt => elt.idMatiereClasse == this.progressionSelected.matiereClasse.idMatiereClasse);
          // console.log(matiereClasse);
          this.progressionForm.patchValue({
            nom_ficheprogression: 'Fiche progression ' + matiereClasse.nomMatiere + ' ' + matiereClasse.nomClasse,
            sequence_id:  this.currentSequence.id
          });
        } else {
          this.progressionForm.patchValue({
            nom_ficheprogression: 'Fiche progression ' + matiereClasse.nomMatiere + ' ' + matiereClasse.nomClasse,
            sequence_id:  this.currentSequence.id
          });
        }
      }

      console.log(this.p.matiereclasse_id.value);
      if (!this.p.matiereclasse_id.value) {
        return;
      }

      console.log(this.p);
      if (this.progressionForm.invalid) {
        return;
      }
      // console.log(this.matiereclasses);
      // let matiereClasse = this.matiereclasses.find(item => item.idMatiereClasse === this.progressionForm.)
      // console.log(this.p.enseignantSelect.value);
      // console.log(this.enseignants[0].enseignant.id_enseignant);

      // console.log(this.p.matiereclasse_id.value);
      // console.log(this.p.nbre_lecons_prevues.value);
      if (this.p.nbre_lecons_prevues.value <= 0) {
        // tslint:disable-next-line:max-line-length
        this.genericsService.confirmResponseAPI(localStorage.getItem('activelang') === 'fr' ? 'Le nombre de leçon prévue doit être supérieure  à 0' : 'The number of lessons is want to greater than 0' , 'error', 8000);
        return;
      }

      console.log(matiereClasse);
      const dto = new AddFicheProgressionModel(
        (!(this.p.nbre_heures_faites.value == this.p.nbre_heures_prevues.value &&
          this.p.nbre_lecons_faites.value == this.p.nbre_lecons_prevues.value)),
        this.toUpdate ? this.p.id_ficheprogression.value :  0,
        this.isEnseignant ? matiereClasse.id : matiereClasse.idMatiereClasse,
        this.p.nbre_heures_faites.value,
        this.p.nbre_heures_prevues.value,
        parseInt(this.p.nbre_lecons_faites.value, 10),
        this.p.nbre_lecons_prevues.value,
        this.p.nom_ficheprogression.value,
        this.p.sequence_id.value,
        this.enseignantSelected.enseignant.id_enseignant
      );

      // console.log('this.p.nom_ficheprogression.value', this.p.nom_ficheprogression.value);
      // console.log(dto);
      // console.log(this.toUpdate);
      const urlUpdate = 'admin/fiche-progression/update';
      const urlCreate = 'admin/fiche-progression/ajouter';

      if (this.p.nbre_heures_faites.value > this.p.nbre_heures_prevues.value) {
        this.genericsService.confirmResponseAPI(localStorage.getItem('activelang') === 'fr' ? 'Le nombre d\'heure(s) éffectuée(s) doit être inférieure ou égale au nombre d\'heure(s) prévue(s)' : 'The number of heur done must be less than or equal to the number of heur planned.' , 'error', 8000);
        return;
      }
      if (this.p.nbre_lecons_faites.value > this.p.nbre_lecons_prevues.value) {
        this.genericsService.confirmResponseAPI(localStorage.getItem('activelang') === 'fr' ? 'Le nombre de leçon(s) éffectuée(s) doit être inférieure ou égale au nombre de leçon(s) prévue(s)' : 'The number of lessons performed must be less than or equal to the number of lessons planned.' , 'error', 8000);
        return;
      }

      if ((this.isEnseignant == false) && (!this.tokenStorage.getUser().displayName)) {
        this.genericsService.confirmResponseAPI('veuillez choisir l\'enseignant', 'error', 8000);
      } else {
        this.genericsService.postResource(this.toUpdate ?  urlUpdate : urlCreate, dto).then((response: any) => {
          console.log(response);
          this.genericsService.confirmResponseAPI(response.message, 'success', 6000);
          this.onloadListFicheProgression();
          this.initFormFicheProgression();
          this.progressionForm.patchValue({
            enseignantSelect: this.dataFilter
          });
          this.toUpdate = false;
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmResponseAPI('Erreur lors de l\'édition de la fiche de progression', 'error', 8000);
        });
      }
    } else if ((e.code == 'Escape')) {
      this.toUpdate = false;
      this.initFormFicheProgression();
    }
  }

  onUpdateFicheProgression(progression: any) {
    // console.log(progression);
    this.progressionSelected = progression;
    // console.log(this.matiereProgressionsListe);
    // console.log(this.enseignants);
    // console.log(this.progressionsListe);
    // console.log(this.p.matiereclasse_id.value);
    // this.toUpdate = true;
    if (progression.id_ficheprogression) {
      this.toUpdate = true;
      this.progressionForm.patchValue({
        autorisation_remplissage: progression.autorisation_remplissage,
        id_ficheprogression: progression.id_ficheprogression,
        matiereclasse_id: progression.matiereClasse.nomMatiere,
        nbre_heures_faites: progression.nbre_heures_faites,
        nbre_heures_prevues: progression.nbre_heures_prevues,
        nbre_lecons_faites: progression.nbre_lecons_faites,
        nbre_lecons_prevues: progression.nbre_lecons_prevues,
        nom_ficheprogression: progression.nom_ficheprogression,
        sequence_id: progression.sequence_id,
      });
    } else {
      this.progressionForm.patchValue({
        autorisation_remplissage: [0],
        id_ficheprogression: [0],
        matiereclasse_id: progression.nomMatiere,
        nbre_heures_faites: 0,
        nbre_heures_prevues: 0,
        nbre_lecons_faites: 0,
        nbre_lecons_prevues: 0,
        nom_ficheprogression: 0,
        sequence_id: [0],
      });
    }
    // this.toUpdate = false;


  }

  getItem(item) {
    console.log(item);
  }
  // get search() { return this.progressionForm.controls; }
  public onLoadListEleves(item, reInitPage?: boolean) {
    // console.log('****************************************');
    // console.log(item.target.value);
    // console.log(this.p.enseignantSelect);
    this.nomEnseignant = item ? item.target.value : this.dataFilter;
    let elt;
      elt = item ? item.target.value : this.dataFilter;
      // console.log(elt);
    if (reInitPage == true) {
      this.page = 0;
    }
    let predicat: string;
    if (elt) {
      predicat = this.genericsService.cleanToken(elt);
    } else {
      predicat = elt;
    }
    this.dataFilter = predicat;
    // console.log(this.dataFilter);
    // this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(`admin/enseignant/listes?predicat=${this.dataFilter}&size=${this.pageSize}&page=${this.page}`).then((result: any) => {
      if (result.success === true) {
        console.log(result);
        this.progressionsListe = [];
        this.matiereProgressionsListe = [];

        this.enseignants = result.data.content;
        // console.log(this.enseignants);
        // console.log(this.dataFilter);
        if (this.dataFilter != null && this.dataFilter != '') {
          this.enseignantSelected = this.enseignants[0];

          // console.log(this.enseignantSelected);
          // console.log(this.progressions);

          this.enseignantSelected.matiereClasses.forEach((item2: any) => {
            const objetDuplique = this.progressions.find((item1: any) => {
              return item1.matiereClasse.idMatiereClasse === item2.idMatiereClasse;
            });
            console.log(objetDuplique);
            if (objetDuplique) {
              this.progressionsListe.push(objetDuplique);
            } else {
              this.matiereProgressionsListe.push(item2);
            }
          });
        } else if (this.dataFilter == '') {
          this.matiereProgressionsListe = [];

          this.progressionForm.patchValue({
            autorisation_remplissage: [0],
            id_ficheprogression: [0],
            matiereclasse_id: [''],
            nbre_heures_faites: [0],
            nbre_heures_prevues: [0],
            nbre_lecons_faites: [0],
            nbre_lecons_prevues: [0],
            nom_ficheprogression: [0],
            sequence_id: [0],
          });
        }

        // console.log(this.progressionsListe);
        // console.log(this.matiereProgressionsListe);
        // console.log(this.isEnseignant);

        this.enseignants.forEach((item: any) => {
          // console.log(item);
          this.matierecClassesTmp.push(item.matiereClasses[0]);

        });
        // console.log(this.matierecClassesTmp);
        // console.log(this.matierecClassesTmp);
        this.matierecClassesTmp.forEach((item: any) => {
          if (this.isEnseignant && item != undefined) {
            this.matiereclasses.push(item);
          }
        });
        // console.log(this.matiereclasses);

        // console.log(this.matiereclasses);
        this.totalElements = result.data.totalElements;
      }
      // this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);

    });
  }
  onSelect(event) {
    console.log(event);
  }
}
