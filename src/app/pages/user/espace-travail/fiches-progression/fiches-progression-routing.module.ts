import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FichesProgressionComponent} from './fiches-progression.component';


const routes: Routes = [
  {
    path: '', component: FichesProgressionComponent,
    data: {
      breadcrumb: localStorage.getItem('activelang') === 'en' ? 'Progress cards' : 'Fiches progression',
      titre: localStorage.getItem('activelang') === 'en' ? 'Progress cards | Aurore' : 'Fiches progression | Aurore',
      icon: 'icofont-paper bg-c-lite-green',
      breadcrumb_caption: localStorage.getItem('activelang') === 'en' ? 'Filling in the progress form - Progress forms' :  'Remplisssage de la fiche de progression - Fiches progression',
      status: true,
      current_role: 'enseignant'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichesProgressionRoutingModule { }
