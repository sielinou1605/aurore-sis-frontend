import {RouterModule, Routes} from '@angular/router';
import {NotesComponent} from './notes.component';
import {AffecterNoteMatieresComponent} from './affecter-note-matieres/affecter-note-matieres.component';
import {NgModule} from '@angular/core';
import {NotesWelcomeComponent} from './notes-welcome/notes-welcome.component';

const routes: Routes = [
  {
    path: '', component: NotesComponent,
    data: {
      breadcrumb: 'Notes_et_affectation_note_matiere',
      titre: localStorage.getItem('activelang') === 'en' ? 'Note management | Aurore' : 'Gestion de notes | Aurore',
      icon: 'icofont-teacher bg-c-lite-green',
      breadcrumb_caption: 'Remplir_notes_et_affecter_note_matieres_meme_classe',
      status: true,
      current_role: 'enseignant',
      action: 'bouton-gestion-note-matiere'
    },
    children: [
      {
        path: '',
        redirectTo: 'notes-welcome',
        pathMatch: 'full'
      },
      // {
      //   path: 'welcome',
      //   loadChildren: () => import('./welcome/welcome.module')
      //     .then(m => m.WelcomeModule)
      // },
      {
        path: 'notes-welcome', component: NotesWelcomeComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Accueil',
          titre: localStorage.getItem('activelang') === 'en' ? 'Welcome | Aurore' : 'Accueil | Aurore',
          icon: 'icofont-teacher bg-c-lite-green',
          breadcrumb_caption: 'Remplir_notes_et_affecter_note_matieres_meme_classe',
          status: true,
          current_role: 'enseignant'
        },
      },
      {
        path: 'affecter-note-matieres', component: AffecterNoteMatieresComponent,
        data: {
          breadcrumb: 'userEleveAjaout.affecter',
          titre: localStorage.getItem('activelang') === 'en' ? 'Affect | Aurore' : 'Affect | Aurore',
          icon: 'icofont-question-circle bg-c-green',
          breadcrumb_caption: 'Aide - Gestion de notes',
          status: true,
          current_role: 'enseignant'
        }
      }
      ]
   }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotesRoutingModule {}
