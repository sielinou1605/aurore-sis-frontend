import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotesComponent } from './notes.component';
import { AffecterNoteMatieresComponent } from './affecter-note-matieres/affecter-note-matieres.component';
// @ts-ignore
import {SharedModule} from '../../../../shared/shared.module';
import { TranslateModule} from '@ngx-translate/core';
import {NotesRoutingModule} from './notes-routing.module';
import { NotesWelcomeComponent } from './notes-welcome/notes-welcome.component';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [NotesComponent, AffecterNoteMatieresComponent, NotesWelcomeComponent],
  imports: [
    CommonModule,
    NotesRoutingModule,
    SharedModule,
    TranslateModule,
    ReactiveFormsModule
  ]
})
export class NotesModule { }
