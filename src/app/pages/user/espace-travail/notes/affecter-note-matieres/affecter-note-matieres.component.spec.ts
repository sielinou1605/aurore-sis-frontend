import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffecterNoteMatieresComponent } from './affecter-note-matieres.component';

describe('AffecterNoteMatieresComponent', () => {
  let component: AffecterNoteMatieresComponent;
  let fixture: ComponentFixture<AffecterNoteMatieresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffecterNoteMatieresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffecterNoteMatieresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
