import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FiltreNoteModel} from '../../../../../shared/_models/response-dto/graduation/filtre-note-absence-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-notes-welcome',
  templateUrl: './notes-welcome.component.html',
  styleUrls: ['./notes-welcome.component.scss']
})
export class NotesWelcomeComponent implements OnInit, OnDestroy {

  selectNoteClasseForm: FormGroup;
  filtreNote: FiltreNoteModel;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  submitted: boolean;
  constructor(
    private genericsService: GenericsService,  private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    private tokenStorage: TokenStorageService,
    public authService: AuthService,
    private router: Router,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.initFormNote();
    this.onloadFiltres();
  }

  activeRout() {
    console.log('active');
    this.router.navigate(['/gestion-notes/notes-affectation/affecter-note-matieres']).then();
  }
  private initFormNote() {
    this.selectNoteClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      matiereclasse: ['', Validators.required]
    });
  }

  get f() { return this.selectNoteClasseForm.controls; }

  onDisplayModalToNote(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'select-note', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onDisplayListeEleve() {
    // [routerLink]="authService.isActif('bouton-saisie-note')? '/gestion-notes/remplir-notes' : ''"
    this.submitted = true;
    if (this.selectNoteClasseForm.invalid) {
      return;
    }
    console.log(this.f.matiereclasse.value, this.f.sequence.value);
    this.genericsService.startLoadingPage();
    this.router.navigate([`/gestion-notes/remplir-notes/${this.f.matiereclasse.value}/sequence/${this.f.sequence.value}`])
      .then(() => {
        this.genericsService.stopLoadingPage();
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
  }

  onReset() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }

  private onloadFiltres() {
    this.genericsService.getResource(`admin/lite/note/filtre/saisie`).then((responses: any) => {
      if (responses.success == true) {
        this.filtreNote = responses.data;
      }
    }).catch(err => {
      console.log(err);
    });
  }

  ngOnDestroy(): void {
    this.onReset();
  }
}
