import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesWelcomeComponent } from './notes-welcome.component';

describe('NotesWelcomeComponent', () => {
  let component: NotesWelcomeComponent;
  let fixture: ComponentFixture<NotesWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
