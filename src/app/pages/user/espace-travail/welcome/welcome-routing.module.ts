import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome.component';


const routes: Routes = [
  {
    path: '', component: WelcomeComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Accueil',
      // titre: 'userEleveAjaout.Accueil_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Welcome | Aurore' : 'Accueil | Aurore',
      icon: 'icofont-teacher bg-c-lite-green',
      breadcrumb_caption: 'userEleveAjaout.Saisie_notes_et_impression_de_bulletin_Accueil',
      status: true,
      current_role: 'enseignant'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
