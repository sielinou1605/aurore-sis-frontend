import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EspaceTravailComponent} from './espace-travail.component';
import {HelpComponent} from './help/help.component';
import {UserGuardService} from '../../../shared/_helpers/user-guard.service';


const routes: Routes = [
  {
    path: '', component: EspaceTravailComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Gestion_de_notes',
      // titre: 'userEleveAjaout.Gestion_de_notes_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Note management | Aurore' : 'Gestion de notes | Aurore',
      icon: 'icofont-teacher bg-c-lite-green',
      breadcrumb_caption: 'Saisie, modification, consultation et impressions bulletins - Espace Travail',
      status: true,
      current_role: 'enseignant',
      action: 'bouton-gestion-note'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome',
        loadChildren: () => import('./welcome/welcome.module')
          .then(m => m.WelcomeModule)
      },
      {
        path: 'consulter-classes',
        loadChildren: () =>
          import('./consulter-classes/consulter-classes.module').then(m => m.ConsulterClassesModule)
      },
      {
        path: 'remplir-notes/:matiereclasse/sequence/:sequence',
        loadChildren: () =>
          import('./remplir-notes/remplir-notes.module').then(m => m.RemplirNotesModule)
      },
      {
        path: 'modifier-notes',
        loadChildren: () =>
          import('./modifier-notes/modifier-notes.module').then(m => m.ModifierNotesModule)
      },
      {
        path: 'consulter-notes',
        loadChildren: () =>
          import('./consulter-notes/consulter-notes.module').then(m => m.ConsulterNotesModule)
      },
      {
        path: 'fiches-progression',
        loadChildren: () =>
          import('./fiches-progression/fiches-progression.module').then(m => m.FichesProgressionModule)
      },
      {
        path: 'bulletins',
        loadChildren: () =>
          import('./bulletins/bulletins.module').then(m => m.BulletinsModule)
      },
      {
        path: 'releve-note',
        loadChildren: () =>
          import('./releve-note/releve-note.module').then(m => m.ReleveNoteModule)
      },
      {
        path: 'validation-notes',
        loadChildren: () =>
          import('./validation-notes/validation-notes.module').then(m => m.ValidationNotesModule)
      },
      {
        path: 'notes',
        loadChildren: () =>
          import('./notes/notes.module').then(m => m.NotesModule)
      },
      {
        path: 'help', component: HelpComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Aide',
          // titre: 'userEleveAjaout.Aide_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Help | Aurore' : 'Aide | Aurore',
          icon: 'icofont-question-circle bg-c-green',
          breadcrumb_caption: 'Aide - Gestion de notes',
          status: true,
          current_role: 'enseignant'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EspaceTravailRoutingModule { }
