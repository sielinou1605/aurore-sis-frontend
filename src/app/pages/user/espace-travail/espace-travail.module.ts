import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EspaceTravailRoutingModule} from './espace-travail-routing.module';
import {EspaceTravailComponent} from './espace-travail.component';
import {HelpComponent} from './help/help.component';
import {SharedModule} from '../../../shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../../app.module';
import {HttpClient} from '@angular/common/http';


@NgModule({
  declarations: [EspaceTravailComponent, HelpComponent],
    imports: [
        CommonModule,
        EspaceTravailRoutingModule,
        SharedModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    ],
})
export class EspaceTravailModule { }
