import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ConsulterClassesComponent} from './consulter-classes.component';

describe('ConsulterClassesComponent', () => {
  let component: ConsulterClassesComponent;
  let fixture: ComponentFixture<ConsulterClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsulterClassesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsulterClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
