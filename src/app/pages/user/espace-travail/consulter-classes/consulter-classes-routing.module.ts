import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConsulterClassesComponent} from './consulter-classes.component';


const routes: Routes = [
  {
    path: '', component: ConsulterClassesComponent,
    data: {
      breadcrumb: 'userAdminConfNum.Consulter_classes',
      titre: localStorage.getItem('activelang') === 'en' ? 'View classes | Aurore' : 'Consulter classes | Aurore',
      icon: 'icofont-group-students bg-c-lite-green',
      breadcrumb_caption: 'userAdminConfNum.Liste_des_classes_vous_etes_programmé_consulter_classes',
      status: true,
      current_role: 'enseignant'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsulterClassesRoutingModule { }
