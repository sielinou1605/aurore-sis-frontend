import {Component, OnInit} from '@angular/core';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {AuthService} from '../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-consulter-classes',
  templateUrl: './consulter-classes.component.html',
  styleUrls: ['./consulter-classes.component.scss']
})
export class ConsulterClassesComponent implements OnInit {
  public matiereclasses: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(private genericsService: GenericsService,
              public authService: AuthService,
              private translate: TranslateService
              ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(59);
  }

  ngOnInit() {
    this.onloadListeClasse();
  }

  private onloadListeClasse() {
    this.genericsService.getResource(`admin/consulter/classes/enseignant`).then((response: any) => {
      console.log(response);
      if (response.success === true) {
        this.matiereclasses = response.data;
      }
    }).catch(reason => {
      console.log(reason);
    });
  }
}
