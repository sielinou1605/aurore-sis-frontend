import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ConsulterClassesRoutingModule} from './consulter-classes-routing.module';
import {ConsulterClassesComponent} from './consulter-classes.component';
import {SharedModule} from '../../../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ConsulterClassesComponent],
    imports: [
        CommonModule,
        ConsulterClassesRoutingModule,
        SharedModule,
        TranslateModule
    ]
})
export class ConsulterClassesModule { }
