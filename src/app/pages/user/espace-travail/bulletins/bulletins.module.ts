import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BulletinsRoutingModule} from './bulletins-routing.module';
import {BulletinsComponent} from './bulletins.component';
import {SharedModule} from '../../../../shared/shared.module';
import {ListeBulletinsComponent} from './liste-bulletins/liste-bulletins.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BulletinsImpressionsComponent} from './bulletins-impressions/bulletins-impressions.component';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [
    BulletinsComponent,
    ListeBulletinsComponent,
    BulletinsImpressionsComponent
  ],
    imports: [
        CommonModule,
        BulletinsRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class BulletinsModule { }
