/* tslint:disable:triple-equals */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-bulletins-impressions',
  templateUrl: './bulletins-impressions.component.html',
  styleUrls: ['./bulletins-impressions.component.scss']
})
export class BulletinsImpressionsComponent implements OnInit {
  public type: string;
  private idType: string;
  private idClasse: string;
  public sequence: any;
  public trimestre: any;
  public classe: any;
  public currentSwitch = 'classe';
  public listEleve: any;
  public classes: any;
  private annee: any;
  private idAnnee: string;
  public currentYear: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private route: ActivatedRoute,
    private genericsService: GenericsService,
    public tokenStorage: TokenStorageService,
    public authService: AuthService,
    private translate: TranslateService
  ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    authService.returnListAction(28);
  }

  ngOnInit() {
    this.onloadAnneeEncours();
    this.onloadListClasse();
    this.route.paramMap.subscribe(params => {
      this.type = params.get('type');
      this.idType = params.get('idType');
      this.idClasse = params.get('idClasse');
      this.idAnnee = params.get('idAnnee');
      if (this.idClasse) {
        this.onloadClasseById(this.idClasse, this.idAnnee);
      }
      if (this.idAnnee && parseInt(this.idAnnee, 0) > 0) {
        this.genericsService.getResource(`admin/annee/${this.idAnnee}/consulter`)
          .then((result: any) => {
            console.log('aanee courate...', result);
            this.currentYear = result.data;
          }).catch((err) => console.log(err));
      }
      if (this.type == 'sequence' && this.idType) {
        this.onloadSequenceById(this.idType);
      } else if (this.type == 'trimestre') {
        this.onloadTrimestreById(this.idType);
      }
      this.genericsService.getResource(`admin/eleves/classe/${this.idClasse}/valider`)
        .then((response: any) => {
          console.log(response);
        }).catch(err => {
        console.log(err);
      });
    });
  }

  private onloadClasseById(idClasse: string, idAnnee: String) {
    this.genericsService.getResource(`classes/${idClasse}`).then((response: any) => {
      this.classe = response;
      this.genericsService.getResource(`admin/annee/${idAnnee}/classe/${idClasse}/eleves`).then((responseEleve: any) => {
        if (responseEleve.success === true) {
          this.listEleve = responseEleve.data;
        }
      }).catch(reasonEleve => console.log(reasonEleve));
    }).catch(reason => console.log(reason));
  }
  private onloadTrimestreById(idType: string) {
    this.genericsService.getResource(`trimestres/${idType}`).then((response: any) => {
      this.trimestre = response;
    }).catch(reason => console.log(reason));
  }
  private onloadSequenceById(idType: string) {
    this.genericsService.getResource(`sequencs/${idType}`).then((response: any) => {
      this.sequence = response;
      this.trimestre = response._embedded.trimestre;
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadListClasse() {
    this.genericsService.getResource(`admin/classes`).then((response: any) => {
      this.classes = response.data;
    }).catch(reason => console.log(reason));
  }
  swicthTo(type: string) {
    this.currentSwitch = type;
  }
  private onloadAnneeEncours() {
    this.genericsService.getResource(`admin/annees`).then((response: any) => {
      this.annee = response.data.find(item => item.codeAnnee == this.tokenStorage.getUser().codeAnnee);
    }).catch(reason => console.log(reason));
  }
  onPrintPVConseilClasse(classe: any) {
    this.genericsService.getResource(`admin/eleves/classe/${classe.id}/valider`)
      .then((response: any) => {
        console.log(response);
        let param;
        let url;
        let returnFileName;
        if (this.type == 'trimestre') {
          param = this.trimestre.id_trimestre + '_' + classe.id;
          url = `admin/pv/trimestre/${param}/imprimer`;
          returnFileName = 'PVs_' + classe.nomClasse + '_' + 'Trimestre_' + this.trimestre.id_trimestre;
        } else {
          if (this.currentYear && this.currentYear.annee) {
            param = this.currentYear.annee.id_annee + '_' + classe.id;
          } else {
            param = this.annee.id + '_' + classe.id;
          }
          url = `admin/pv/annuel/${param}/imprimer`;
          returnFileName = 'PVs_' + classe.nomClasse + '_' + 'Annee_' + this.annee.codeAnnee;
        }
        this.printReport(url, returnFileName);
      }).catch(err => {
      console.log(err);
    });
  }
  onPrintBulletins(classe: any) {
    this.genericsService.getResource(`admin/eleves/classe/${classe.id}/valider`)
      .then((response: any) => {
        console.log(response);
        let param;
        let url;
        let returnFileName;
        if (this.type == 'sequence') {
          param = this.sequence.id_sequence + '_' + classe.id;
          url = `admin/bulletin/sequence/${param}/imprimer`;
          returnFileName = 'Bulletins_' + classe.nomClasse + '_' + 'Sequence_' + this.sequence.id_sequence;
        } else if (this.type == 'trimestre') {
          param = this.trimestre.id_trimestre + '_' + classe.id;
          url = `admin/bulletin/trimestre/${param}/imprimer`;
          returnFileName = 'Bulletins_' + classe.nomClasse + '_' + 'Trimestre_' + this.trimestre.id_trimestre;
        } else {
          if (this.currentYear && this.currentYear.annee) {
            param = this.currentYear.annee.id + '_' + classe.id;
            // console.log(this.currentYear);
            // console.log(param);
          } else {
            param = this.annee.id + '_' + classe.id;
          }
          url = `admin/bulletin/annuel/${param}/imprimer`;
          returnFileName = 'Bulletins_' + classe.nomClasse + '_' + 'Annee_' + this.annee.codeAnnee;
        }
        this.printReport(url, returnFileName);
      }).catch(err => {
      console.log(err);
    });

  }
  onPrintBulletinFromEleve(eleve: any) {
    let periode;
    let idPeriode;
    if (this.type == 'sequence') {
      periode = 'SEQUENCE';
      idPeriode = this.sequence.id_sequence;
    } else if (this.type == 'trimestre') {
      periode = 'TRIMESTRE';
      idPeriode = this.trimestre.id_trimestre;
    } else {
      idPeriode = this.annee.id;
      periode = 'ANNUEL';
    }
    const returnName = 'Bulletion_' + eleve.matricule + '_' + this.type + '_' + idPeriode;
    const dto = {
      idClasse: this.idClasse, idPeriode: idPeriode,
      matricule: eleve.matricule, periode: periode
    };
    const url = `admin/bulletin/eleve/imprimer`;
    this.printPostReport(url, dto, returnName);
  }

  onPrintRelevesNotes(classe: any) {
    this.genericsService.getResource(`admin/eleves/classe/${classe.id}/valider`)
      .then((response: any) => {
        console.log(response);
        let param;
        if (this.currentYear && this.currentYear.annee) {
          param = this.currentYear.annee.id_annee + '_' + classe.id;
        } else {
          param = this.annee.id + '_' + classe.id;
        }
        const url = `admin/releve/annuel/${param}/imprimer`;
        const returnName = 'ReleveNote_' + classe.nomClasse + '_Annee' + this.annee.codeAnnee;
        this.printReport(url, returnName);
      }).catch(err => {
      console.log(err);
    });
  }

  private printReport(url: string, nameFile: string) {
    this.genericsService.startLoadingPage();
    this.genericsService.reportPostResource(url).then((result: any) => {
      this.genericsService.stopLoadingPage();
      this.genericsService.getByteArrayAndSaveReportPDF(result, nameFile);
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      this.genericsService.confirmModalResponseAPI('Rassurez-vous qu\'un titulaire existe pour cette classe !',
        'Erreur impression bulletin', 'error');
    });
  }
  private printPostReport(url, data, nameFile) {
    this.genericsService.startLoadingPage();
    this.genericsService.reportPostPostResource(url, data).then((result: any) => {
      this.genericsService.stopLoadingPage();
      this.genericsService.getByteArrayAndSaveReportPDF(result, nameFile);
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      this.genericsService.confirmModalResponseAPI('Rassurez-vous qu\'un titulaire existe pour cette classe !',
        'Erreur impression bulletin', 'error');
    });
  }
}
