import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BulletinsImpressionsComponent} from './bulletins-impressions.component';

describe('BulletinsImpressionsComponent', () => {
  let component: BulletinsImpressionsComponent;
  let fixture: ComponentFixture<BulletinsImpressionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulletinsImpressionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulletinsImpressionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
