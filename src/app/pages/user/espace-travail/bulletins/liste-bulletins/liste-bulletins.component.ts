/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-liste-bulletins',
  templateUrl: './liste-bulletins.component.html',
  styleUrls: ['./liste-bulletins.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListeBulletinsComponent implements OnInit, OnDestroy {
  submitted: boolean;
  bulletinSequentielForm: FormGroup;
  bulletinTrimestreForm: FormGroup;
  public filtreSequence: any;
  public filtreTrimestre: any;
  public bulletinAnnuelForm: any;
  private idAnnee: string;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private genericsService: GenericsService, private fb: FormBuilder,
              private modalService: NgbModal, config: NgbModalConfig,
              private router: Router, public authService: AuthService,
              private route: ActivatedRoute,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    }else{
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(26);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idAnnee = params.get('idAnnee');
      console.log(this.idAnnee);
    });
    this.initBulletinSequentielleForm();
    this.initBulletinTrimestrielleForm();
    this.initBulletinAnnuelForm();
    this.onloadBulletinSequenceFiltre();
    this.onloadBulletinTrimestreFiltre();
  }
  private initBulletinSequentielleForm() {
    this.bulletinSequentielForm = this.fb.group({
      sequence: ['', Validators.required],
      classe: ['', Validators.required]
    });
  }
  private initBulletinTrimestrielleForm() {
    this.bulletinTrimestreForm = this.fb.group({
      trimestre: ['', Validators.required],
      classe: ['', Validators.required]
    });
  }

  private initBulletinAnnuelForm() {
    this.bulletinAnnuelForm = this.fb.group({
      classe: ['', Validators.required]
    });
  }
  private onloadBulletinSequenceFiltre() {
    this.idAnnee  = this.idAnnee ?  this.idAnnee : '0';
    this.genericsService.getResource(`admin/bulletin/filtre/sequence?idAnnee=${this.idAnnee}`).then((response: any) => {
      this.filtreSequence = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadBulletinTrimestreFiltre() {
    this.idAnnee  = this.idAnnee ?  this.idAnnee : '0';
    this.genericsService.getResource(`admin/bulletin/filtre/trimestre?idAnnee=${this.idAnnee}`).then((response: any) => {
      this.filtreTrimestre = response.data;
      console.log(this.filtreTrimestre);
    }).catch(reason => {
      console.log(reason);
    });
  }
  onDisplayListBulletin(type: string) {
    this.idAnnee  = this.idAnnee ?  this.idAnnee : '0';
    this.submitted = true;
    if (type == 'sequence') {
      if (this.bulletinSequentielForm.invalid) {
        return;
      }
      this.router.navigate(
        ['gestion-notes/bulletins/bulletins-impression/' + type + '/'
        + this.bs.sequence.value + '/classe/' + this.bs.classe.value + '/' + this.idAnnee]).then((result: any) => {
        this.onResetBulletinSequentielle();
      });
    } else if (type == 'trimestre') {
      if (this.bulletinTrimestreForm.invalid) {
        return;
      }
      this.router.navigate(['gestion-notes/bulletins/bulletins-impression/' + type + '/'
      + this.bt.trimestre.value + '/classe/' + this.bt.classe.value + '/' + this.idAnnee]).then((result: any) => {
        this.onResetBulletinTrimestre();
      });
    } else {
      if (this.bulletinAnnuelForm.invalid) {
        return;
      }
      this.router.navigate(
        ['gestion-notes/bulletins/bulletins-impression/' + type + '/classe/' +  this.ba.classe.value + '/' + this.idAnnee]
      ).then((result: any) => {
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    }
  }
  get bs() { return this.bulletinSequentielForm.controls; }
  get bt() { return this.bulletinTrimestreForm.controls; }
  get ba() { return this.bulletinAnnuelForm.controls; }
  onResetBulletinTrimestre() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initBulletinTrimestrielleForm();
  }
  onResetBulletinSequentielle() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initBulletinSequentielleForm();
  }
  onResetBulletinAnnuel() {
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onSelectSequence(content: TemplateRef<any>) {
    if (this.filtreSequence && this.authService.isActif('bouton-bulletin-sequentiel')) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-sequence', scrollable: true}).result.then((result) => {
      }, (reason) => {
      });
    }
  }
  onSelectTrimestre(content: TemplateRef<any>) {
    if (this.filtreTrimestre && this.authService.isActif('bouton-bulletin-trimestriel')) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-trimestre', scrollable: true}).result.then((result) => {
      }, (reason) => {
      });
    }
  }

  onSelectAnnuel(content: TemplateRef<any>) {
    if (this.filtreTrimestre && this.authService.isActif('bouton-bulletin-annuel')) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-annuel', scrollable: true}).result.then((result) => {
      }, (reason) => {
      });
    }
  }

  ngOnDestroy(): void {
    this.onResetBulletinTrimestre();
    this.onResetBulletinSequentielle();
    this.onResetBulletinAnnuel();
  }
}
