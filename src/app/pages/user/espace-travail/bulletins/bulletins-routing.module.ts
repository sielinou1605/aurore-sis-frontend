import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BulletinsComponent} from './bulletins.component';
import {ListeBulletinsComponent} from './liste-bulletins/liste-bulletins.component';
import {BulletinsImpressionsComponent} from './bulletins-impressions/bulletins-impressions.component';

const routes: Routes = [
  {
    path: '', component: BulletinsComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Bulletins',
      titre: localStorage.getItem('activelang') === 'en' ? 'Bulletins | Aurore' : 'Bulletins | Aurore',
      icon: 'icofont-certificate-alt-2 bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Menu_des_bulletins_Bulletin',
      status: true,
      current_role: 'enseignant'
    },
    children: [
      {
        path: '',
        redirectTo: 'list-bulletins',
        pathMatch: 'full'
      },
      {
        path: 'list-bulletins', component: ListeBulletinsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Bulletins',
          titre: localStorage.getItem('activelang') === 'en' ? 'Bulletins | Aurore' : 'Bulletins | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_bulletins_de_note_Bulletin',
          status: true,
          current_role: 'enseignant'
        }
      },
      {
        path: 'list-bulletins/:idAnnee', component: ListeBulletinsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Bulletins',
          titre: localStorage.getItem('activelang') === 'en' ? 'Bulletins | Aurore' : 'Bulletins | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_bulletins_de_note_Bulletin',
          status: true,
          current_role: 'enseignant'
        }
      },
      {
        path: 'bulletins-impression/:type/classe/:idClasse', component: BulletinsImpressionsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Bulletins',
          titre: localStorage.getItem('activelang') === 'en' ? 'Bulletins | Aurore' : 'Bulletins | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Impression_des_bulletins_Sequentiel_Trimestriel_et_ou_Annuel_Bulletins',
          status: true,
          current_role: 'enseignant'
        }
      },
      {
        path: 'bulletins-impression/:type/:idType/classe/:idClasse', component: BulletinsImpressionsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Bulletins',
          titre: localStorage.getItem('activelang') === 'en' ? 'Bulletins | Aurore' : 'Bulletins | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Impression_des_bulletins_Sequentiel_Trimestriel_et_ou_Annuel_Bulletins',
          status: true,
          current_role: 'enseignant'
        }
      },
      {
        path: 'bulletins-impression/:type/classe/:idClasse/:idAnnee', component: BulletinsImpressionsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Bulletins',
          titre: localStorage.getItem('activelang') === 'en' ? 'Bulletins | Aurore' : 'Bulletins | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Impression_des_bulletins_Sequentiel_Trimestriel_et_ou_Annuel_Bulletins',
          status: true,
          current_role: 'enseignant'
        }
      },
      {
        path: 'bulletins-impression/:type/:idType/classe/:idClasse/:idAnnee', component: BulletinsImpressionsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Bulletins',
          titre: localStorage.getItem('activelang') === 'en' ? 'Bulletins | Aurore' : 'Bulletins | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Impression_des_bulletins_Sequentiel_Trimestriel_et_ou_Annuel_Bulletins',
          status: true,
          current_role: 'enseignant'
        }
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BulletinsRoutingModule { }
