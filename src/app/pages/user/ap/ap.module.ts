import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ApRoutingModule} from './ap-routing.module';
import {ApComponent} from './ap.component';
import {HelpComponent} from './help/help.component';
import {SharedModule} from '../../../shared/shared.module';


@NgModule({
  declarations: [ApComponent, HelpComponent],
    imports: [
        CommonModule,
        ApRoutingModule,
        SharedModule
    ]
})
export class ApModule { }
