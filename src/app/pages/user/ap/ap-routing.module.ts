import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ApComponent} from './ap.component';
import {HelpComponent} from './help/help.component';


const routes: Routes = [
  {
    path: '', component: ApComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Comptabilite',
      // titre: 'userEleveAjaout.Comptabilite_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Accounting | Aurore' : 'Comptabilite | Aurore',
      icon: 'icofont-user-suited bg-c-green',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_operations_comptable_Comptabilite',
      status: true,
      current_role: 'finances'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome',
        loadChildren: () => import('./welcome/welcome.module')
          .then(m => m.WelcomeModule)
      },
      {
        path: 'help', component: HelpComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Aide',
         // titre: 'userEleveAjaout.Aide_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Help | Aurore' : 'Aide | Aurore',
          icon: 'icofont-question-circle bg-c-green',
          breadcrumb_caption: 'userEleveAjaout.Lorem_Ipsum_Dolor_Sit_Amet_Consectetur_Adipisicing_Elit_Animateur_pédagogique',
          status: true,
          current_role: 'finances'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApRoutingModule { }
