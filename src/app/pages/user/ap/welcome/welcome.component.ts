/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModeReglementModel} from '../../../../shared/_models/response-dto/finance/mode-reglement-model';
import {CaisseModel} from '../../../../shared/_models/response-dto/finance/caisse-model';
import {BourseModel} from '../../../../shared/_models/response-dto/finance/bourse-model';
import {Router} from '@angular/router';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {ConveertDateService} from '../../../../shared/_helpers/conveert-date.service';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {ReglementRequestModel} from '../../../../shared/_models/request-dto/finance/reglement-request-model';
import {FiltreRequestModel} from '../../../../shared/_models/request-dto/common/filtre-request-model';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class WelcomeComponent implements OnInit, OnDestroy {

  public scolariteForm: FormGroup;
  public bourseForm: FormGroup;
  public submitted: boolean;
  public isAuto = true;
  private base_url = 'admin/inscription/';
  private base_url_caisse = 'admin/caisse/';
  private base_url_mode_reglement = 'admin/mode-reglement/';
  public inscriptions: any;
  public echeanciersEleve: any;
  public modeReglements: ModeReglementModel[];
  public caisses: CaisseModel[];
  private base_url_bourse = 'admin/bourse/';
  public idEleve: string;
  public bourses: BourseModel[];
  searchForm: FormGroup;
  private currentInscription: any;
  impressionReleveForm: FormGroup;
  annees: any;
  advancedSearchForm: FormGroup;
  public entites: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private router: Router, private modalService: NgbModal,
              config: NgbModalConfig, private fb: FormBuilder,
              private genericsService: GenericsService, private dateConvert: ConveertDateService,
              private calendar: NgbCalendar, private tokenStorage: TokenStorageService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.initScolariteForm();
    this.initBourseForm();
    this.initAdvancedFilter();
    this.initImpressionReleveForm();
    this.onLoadListCaisse();
    this.onLoadListModeReglement();
    this.onLoadListBourse();
    this.onLoadListAnnees();
    this.initFormFilter();
  }

  public onFilterInscription(event) {
    if (this.af.dateAdmissionFin.value.length > 0 || this.af.nomEntite.value.length > 0 ||
      this.af.dateAdmissionDebut.value.length > 0 || this.af.type.value.length > 0) {
      const filtreRequest = new FiltreRequestModel(
        this.af.annee.value, this.af.dateAdmissionDebut.value, this.af.dateAdmissionFin.value, event,
        this.af.nomEntite.value, this.af.idEntite.value,  this.af.type.value
      );
      this.genericsService.postResource(`${this.base_url}filtre-avance`, filtreRequest).then((result: any) => {
        if (result.success === true) {
          this.inscriptions = result.data;
        }
      });
    } else {
      this.genericsService.getResource(`${this.base_url}filtrer?predicat=${event}`).then((result: any) => {
        if (result.success === true) {
          this.inscriptions = result.data;
        }
      });
    }
  }
  onAjouterListeInscription() {
    this.router.navigate(['']).then(() => {

    });
  }
  onReglerInscription(idEleve: string) {
    this.submitted = true;
    if (this.scolariteForm.invalid) {
      return;
    }
    const idEcheancier = (this.isAuto === false) ? this.s.service.value : null;
    const reglement = new ReglementRequestModel(
      this.tokenStorage.getUser().codeAnnee, this.s.caisse.value,  this.s.modeReglement.value,
      idEleve, this.s.montantAVerser.value, this.isAuto, idEcheancier ,
    );
    this.genericsService.postResource(`admin/reglement/create`, reglement).then(() => {
      this.router.navigate(['/administration/inscriptions/recap-eleve', this.currentInscription.idInscription]).then(() => {
        localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Payment made successfully', 'success')
        : this.genericsService.confirmResponseAPI('Paiement effectué avec succes', 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    });
  }
  private initScolariteForm() {
    this.scolariteForm = this.fb.group({
      caisse: ['', Validators.required],
      modeReglement: ['', Validators.required],
      montantAVerser: ['', Validators.required],
      service: [''],
    });
  }
  private listEcheancierParEleve(annee: number, matricule: string) {
    this.genericsService.getResource(`admin/echeancier/matricule/${matricule}/annee/${annee}/list`).then((result: any) => {
      console.log(result);
      if (result.success === true) {
        this.echeanciersEleve = result.data;
      }
    });
  }
  onPayerFraisInscriptions(inscription: any, reglerfrais: TemplateRef<any>) {
    this.currentInscription = inscription;
    this.idEleve = inscription.matricule;
    this.submitted = false;
    this.listEcheancierParEleve(this.tokenStorage.getUser().codeAnnee, this.idEleve);
    this.modalService.open(reglerfrais,
      {ariaLabelledBy: 'add-bourse', size: 'lg'}).result.then((result) => {
    }, (reason) => {});
  }
  private onLoadListModeReglement() {
    this.genericsService.getResource(`${this.base_url_mode_reglement}list`).then((result: any) => {
      if (result.success === true) {
        this.modeReglements = result.data;
      }
    });
  }
  private onLoadListCaisse() {
    this.genericsService.getResource(`${this.base_url_caisse}list`).then((result: any) => {
      if (result.success === true) {
        this.caisses = result.data;
      }
    });
  }
  private onLoadListBourse() {
    this.genericsService.getResource(`${this.base_url_bourse}list`).then((result: any) => {
      if (result.success === true) {
        this.bourses = result.data;
      }
    });
  }
  private onLoadListAnnees() {
    this.genericsService.getResource(`annees?size=100`).then((result: any) => {
      this.annees = result._embedded.annees;
    });
  }
  onChangeOptions() {
    if (this.isAuto === false) {
      this.scolariteForm.controls['service'].setValidators([Validators.required, Validators.min(1)]);
    } else {
      this.scolariteForm.controls['service'].clearValidators();
    }
    this.scolariteForm.get('service').updateValueAndValidity();
  }
  private initBourseForm() {
    this.bourseForm = this.fb.group({
      bourse: ['', Validators.required],
      anneeAc: ['', Validators.required],
    });
  }
  get s() { return this.scolariteForm.controls; }
  onResetScolarite() {
    this.submitted = false;
    this.isAuto = true;
    this.scolariteForm.reset();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  get b() { return this.bourseForm.controls; }

  onInscrireNouvelEleve() {
    this.router.navigate(['/administration/inscriptions/gestion-inscriptions' ]).then(() => {});
  }
  onOpenRecapPage(id: any) {
    this.router.navigate(['/comptable/welcome/etats/' + id]).then(() => {
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmResponseAPI('Full upload of student file !!!', 'success')
      : this.genericsService.confirmResponseAPI('Chargement complet fiche étudiant !!!', 'success');
    });
  }
  get search() { return this.searchForm.controls; }
  onImpressionEtatFinancier(idInscription: any, matricule) {
    this.genericsService.reportPostResource(`admin/impression/inscription/${idInscription}/etat-financier`).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Etat_Financier_' + matricule);
    });
  }
  onPrintReleveCaisse() {
    this.submitted = true;
    if (this.impressionReleveForm.invalid) {
      return;
    }
    const filter = {
      dateDebut: this.f.dateDebut.value,
      dateFin: this.f.dateFin.value
    };
    console.log(filter);
    this.genericsService.reportPostPostResource(`admin/impression/releve-caisse`, filter).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Releve_caisse_');
      this.onReset();
    });
  }
  onPrintGenericsResult() {}
  private initImpressionReleveForm() {
    this.impressionReleveForm = this.fb.group({
      dateDebut: [this.calendar.getToday(), Validators.required],
      dateFin: ['', Validators.required]
    });
  }
  get f() { return this.impressionReleveForm.controls; }
  get af() { return this.advancedSearchForm.controls; }
  onConfigImpressionReleve(impressionReleve: TemplateRef<any>) {
    this.modalService.open(impressionReleve,
      {ariaLabelledBy: 'print-releve', scrollable: true}).result.then((result) => {
    }, (reason) => {});
  }
  onReset() {
    this.initImpressionReleveForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  private initAdvancedFilter() {
    this.advancedSearchForm = this.fb.group({
      annee: ['2021'],
      dateAdmissionDebut: [''],
      dateAdmissionFin: [''],
      type: [''],
      nomEntite: ['INSTITUTION'],
      idEntite: ['']
    });
  }
  public initFormFilter() {
    if (this.af.nomEntite.value == 'ETABLISSEMENT') {
      this.onLoadListEtablissement();
    } else if (this.af.nomEntite.value == 'SECTION') {
      this.onLoadListSections();
    } else if (this.af.nomEntite.value == 'CYCLE') {
      this.onLoadListCycles();
    } else if (this.af.nomEntite.value == 'NIVEAU') {
      this.onLoadListNiveau();
    } else if (this.af.nomEntite.value == 'CLASSE') {
      this.onLoadListClasses();
    } else {
      this.onLoadListInstitution();
    }
  }
  private onLoadListInstitution() {
    this.genericsService.getResource(`institutions`).then((result: any) => {
      this.entites = result._embedded.institutions;
    });
  }
  private onLoadListEtablissement() {
    this.genericsService.getResource(`etablissements?size=100`).then((result: any) => {
      this.entites = result._embedded.etablissements;
    });
  }
  private onLoadListSections() {
    this.genericsService.getResource(`sectionses?size=100`).then((result: any) => {
      this.entites = result._embedded.sectionses;
    });
  }
  private onLoadListCycles() {
    this.genericsService.getResource(`cycleses?size=100`).then((result: any) => {
      this.entites = result._embedded.cycleses;
    });
  }
  private onLoadListNiveau() {
    this.genericsService.getResource(`niveaus?size=200`).then((result: any) => {
      this.entites = result._embedded.niveaus;
    });
  }
  private onLoadListClasses() {
    this.genericsService.getResource(`classes?size=200`).then((result: any) => {
      this.entites = result._embedded.classes;
    });
  }

  ngOnDestroy(): void {
    this.onReset();
    this.onResetScolarite();
  }
}
