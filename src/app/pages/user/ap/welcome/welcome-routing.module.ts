import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome.component';
import {DetailFinancierInscriptionComponent} from './detail-financier-inscription/detail-financier-inscription.component';


const routes: Routes = [
  {
    path: '', component: WelcomeComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Operation_comptable',
      titre: localStorage.getItem('activelang') === 'en' ? 'Accounting operation | Aurore' : 'Operation comptable | Aurore',
      icon: 'icofont-user-suited bg-c-green',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_rapports_et_bilans_financiers_Comptable',
      status: true,
      current_role: 'finances'
    },
  },
  {
    path: 'etats/:id', component: DetailFinancierInscriptionComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Operation_comptable',
      titre: localStorage.getItem('activelang') === 'en' ? 'Accounting operation | Aurore' : 'Operation comptable | Aurore',
      icon: 'icofont-user-suited bg-c-green',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_rapports_et_bilans_financiers_Comptable',
      status: true,
      current_role: 'finances'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
