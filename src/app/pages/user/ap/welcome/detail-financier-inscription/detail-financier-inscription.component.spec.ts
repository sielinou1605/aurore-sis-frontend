import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DetailFinancierInscriptionComponent} from './detail-financier-inscription.component';

describe('DetailFinancierInscriptionComponent', () => {
  let component: DetailFinancierInscriptionComponent;
  let fixture: ComponentFixture<DetailFinancierInscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailFinancierInscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailFinancierInscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
