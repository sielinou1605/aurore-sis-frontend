import {Component, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CaisseModel} from '../../../../../shared/_models/response-dto/finance/caisse-model';
import {BourseModel} from '../../../../../shared/_models/response-dto/finance/bourse-model';
import {ActivatedRoute, Router} from '@angular/router';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ConveertDateService} from '../../../../../shared/_helpers/conveert-date.service';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-detail-financier-inscription',
  templateUrl: './detail-financier-inscription.component.html',
  styleUrls: ['./detail-financier-inscription.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class DetailFinancierInscriptionComponent implements OnInit {
  public idInscription: string;
  private base_url = 'admin/inscription/';
  public inscription: any;
  public scolariteForm: FormGroup;
  public bourseForm: FormGroup;
  public submitted: boolean;
  public isAuto = true;
  private base_url_caisse = 'admin/caisse/';
  private base_url_mode_reglement = 'admin/mode-reglement/';
  private base_url_bourse = 'admin/bourse/';
  public caisses: CaisseModel[];
  public bourses: BourseModel[];
  public modeReglements: any;
  public echeanciersEleve: any;


  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private router: Router, private modalService: NgbModal,
    config: NgbModalConfig, private fb: FormBuilder,
    public genericsService: GenericsService, private dateConvert: ConveertDateService,
    private calendar: NgbCalendar, private route: ActivatedRoute, private tokenStorage: TokenStorageService

  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idInscription = params.get('id');
      console.log(this.idInscription);
      this.onLoadRecapInscription(this.idInscription);
    });
  }

  private onLoadRecapInscription(idInscription: string) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`${this.base_url + idInscription}/get-recap`).then((result: any) => {
      console.log(result);
      if (result.success === true) {
        this.inscription = result.data;
      }
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
    });
  }
  onResetScolarite() {
    this.submitted = false;
    this.isAuto = true;
    this.scolariteForm.reset();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onChangeOptions() {
    if (this.isAuto === false) {
      this.scolariteForm.controls['service'].setValidators([Validators.required, Validators.min(1)]);
    } else {
      this.scolariteForm.controls['service'].clearValidators();
    }
    this.scolariteForm.get('service').updateValueAndValidity();

  }
  onImpressionRecu(reglement: any) {
    this.genericsService.reportPostResource(`admin/impression/reglement/${reglement.idReglement}`).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, reglement.service);
    });
  }

  onImpressionEtatFinancier(idInscription: any, matricule) {
    this.genericsService.reportPostResource(`admin/impression/inscription/${idInscription}/etat-financier`).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Etat_Financier_' + matricule);
    });
  }
}
