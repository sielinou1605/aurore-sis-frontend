import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrationRoutingModule } from './administration-routing.module';
import { AdministrationComponent } from './administration.component';
import { UtilisateursModule } from './utilisateurs/utilisateurs.module';
import { AideComponent } from './aide/aide.component';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StatistiquesComponent } from './statistiques/statistiques.component';
//import { AdminNotesComponent } from './notes/admin-notes/admin-notes.component';
//import { AdminAbsencesComponent } from './discipline/admin-absences/admin-absences.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../../app.module';
import { PortailModule } from './portail/portail.module';
import { HttpClient } from '@angular/common/http';
//import { LogNotesComponent } from './notes/log-notes/log-notes.component';
import {NgxPaginationModule} from 'ngx-pagination';
//import { LogAbscenceComponent } from './discipline/log-abscence/log-abscence.component';

@NgModule({
  declarations: [
    AdministrationComponent,
    AideComponent,
    StatistiquesComponent,
    // AdminAbsencesComponent,
    // LogAbscenceComponent,
    // AdminNotesComponent,
    // LogNotesComponent,
  ],
    imports: [
        CommonModule,
        AdministrationRoutingModule,
        UtilisateursModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        PortailModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        NgxPaginationModule
    ]
})
export class AdministrationModule { }
