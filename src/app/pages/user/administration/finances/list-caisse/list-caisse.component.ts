/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CaisseModel} from '../../../../../shared/_models/response-dto/finance/caisse-model';
import {CaisseRequestModel} from '../../../../../shared/_models/request-dto/finance/caisse-request-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-caisse',
  templateUrl: './list-caisse.component.html',
  styleUrls: ['./list-caisse.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListCaisseComponent implements OnInit, OnDestroy {
  public caisseForm: FormGroup;
  public submitted: boolean;
  public caisses: CaisseModel[];
  private base_url = 'admin/caisse/';
  private toUpdate = false;
  private currentCaisse: CaisseModel;
  public errorResponse;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private translate: TranslateService,
    private modalService: NgbModal, private genericsService: GenericsService,
    config: NgbModalConfig, private fb: FormBuilder,
    public authService: AuthService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(19);
  }

  ngOnInit() {
    this.initCaisseForm();
    this.onLoadListCaisse();
  }

  open(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'add-caisse', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onSaveCaisse() {
    this.submitted = true;
    if (this.caisseForm.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const caisseRequest = new CaisseRequestModel(this.currentCaisse.id, this.f.nomCaisse.value,
        this.f.description.value);
      this.genericsService.putResource(`${this.base_url + this.currentCaisse.id}/update`,
       caisseRequest).then((result: any) => {
        this.onLoadListCaisse();
        this.toUpdate = false;
        this.submitted = false;
        this.currentCaisse = null;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.CAISSE_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.CAISSE_UPDATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    } else  {
    const caisseRequest = new CaisseRequestModel(null, this.f.nomCaisse.value, this.f.description.value);
      this.genericsService.postResource(`${this.base_url}create`, caisseRequest).then((result: any) => {
        this.onLoadListCaisse();
        this.initCaisseForm();
        this.submitted = false;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.CAISSE_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.CAISSE_CREATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      }, err => {
          this.errorResponse = err.error.message + ' ' + err.error.data;
      });
    }
  }

  private initCaisseForm() {
    this.caisseForm = this.fb.group({
      nomCaisse: ['', Validators.required],
      description: [''],
    });
  }

  get f() { return this.caisseForm.controls; }

  onReset() {
    this.submitted = false;
    this.toUpdate = false;
    this.initCaisseForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onLoadDetailsCaisse(caisse: CaisseModel,  content: TemplateRef<any>) {
    this.toUpdate =  true;
    this.currentCaisse = caisse;
    this.caisseForm.patchValue({
      nomCaisse: caisse.nomCaisse,
      description: caisse.description_caisse,
    });
    this.modalService.open(content,
      {ariaLabelledBy: 'add-caisse', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onDeleteCaisse(id: number) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
          this.onLoadListCaisse();
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            this.onLoadListCaisse();
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }
  private onLoadListCaisse() {
    this.genericsService.getResource(`${this.base_url}list`).then((result: any) => {
      if (result.success === true) {
        this.caisses = result.data;
        console.log(this.caisses);
      }
    });
  }

  ngOnDestroy(): void {
    this.onReset();
  }
}
