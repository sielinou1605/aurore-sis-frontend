import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FinancesRoutingModule} from './finances-routing.module';
import {FinancesComponent} from './finances.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MenuConfigComponent} from './menu-config/menu-config.component';
import {SharedModule} from '../../../../shared/shared.module';
import {ListRemiseNiveauComponent} from './list-remise-niveau/list-remise-niveau.component';
import {ListModeleEcheanceComponent} from './list-modele-echeance/list-modele-echeance.component';
import {ListBourseComponent} from './list-bourse/list-bourse.component';
import {ListServicesComponent} from './list-services/list-services.component';
import {ListRemiseComponent} from './list-remise/list-remise.component';
import {ListModeReglementComponent} from './list-mode-reglement/list-mode-reglement.component';
import {ListCaisseComponent} from './list-caisse/list-caisse.component';
import {ListJournalComponent} from './list-journal/list-journal.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {TransfertConfigurationComponent} from './transfert-configuration/transfert-configuration.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../../../app.module';
import {HttpClient} from '@angular/common/http';
import { AcompteComponent } from './acompte/acompte.component';
import { GestionDesAcomptesComponent } from './gestion-des-acomptes/gestion-des-acomptes.component';
import { ListAcomptesComponent } from './list-acomptes/list-acomptes.component';


@NgModule({
  declarations: [
    FinancesComponent,
    ListServicesComponent,
    ListRemiseComponent,
    ListBourseComponent,
    ListModeleEcheanceComponent,
    ListModeReglementComponent,
    ListCaisseComponent,
    ListJournalComponent,
    ListRemiseNiveauComponent,
    TransfertConfigurationComponent,
    MenuConfigComponent,
    AcompteComponent,
    GestionDesAcomptesComponent,
    ListAcomptesComponent
  ],
    imports: [
        CommonModule,
        FinancesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        NgMultiSelectDropDownModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    ], 
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class FinancesModule { }
