/* tslint:disable:triple-equals */
import { Component, OnDestroy, OnInit, TemplateRef } from "@angular/core";
import {
  ModalDismissReasons,
  NgbModal,
  NgbModalConfig,
} from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ServiceModel } from "../../../../../shared/_models/response-dto/finance/service-model";
import { GenericsService } from "../../../../../shared/_services/generics.service";
import { ServiceRequestModel } from "../../../../../shared/_models/request-dto/finance/service-request-model";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { AuthService } from "../../../../../shared/_services/auth.service";
import { TokenStorageService } from "../../../../../shared/_services/token-storage.service";
import { AppConstants } from "../../../../../shared/element/app.constants";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-list-services",
  templateUrl: "./list-services.component.html",
  styleUrls: ["./list-services.component.scss"],
  providers: [NgbModalConfig, NgbModal],
})
export class ListServicesComponent implements OnInit, OnDestroy {
  public serviceForm: FormGroup;
  public searchForm: FormGroup;
  public search = '';
  public predicat: any;
  serviceNiveauForm: FormGroup;
  public submitted: boolean;
  public services: ServiceModel[];
  public servicesData:any;
  private base_url = "admin/service/";
  private toUpdate: boolean;
  public currentService: ServiceModel;
   searchvalue = '';
  p = 0;
  itemPage = 15;
  public pageSize = 15;
  public page = 0;
  public pagine = 1;
  totalElements = 0;
  public niveaux: any;
  private annee: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private translate: TranslateService,
    private modalService: NgbModal,
    config: NgbModalConfig,
    private fb: FormBuilder,
    private genericsService: GenericsService,
    public authService: AuthService,
    private tokenStorage: TokenStorageService
  ) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = "static";
    config.keyboard = false;
    authService.returnListAction(17);
  }

  ngOnInit() {
    console.log(this.page);
    this.onLoadListService(this.search);
    this.initFormService();
    this.initFormServiceNiveau();
    this.onloadListNiveau();
    this.onloadAnneeEncours();


    this.searchForm = this.fb.group({
      search: ''
    });
  }

  open(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.modalService
      .open(content, {
        ariaLabelledBy: "add-service",
        size: "lg",
        scrollable: true,
      })
      .result.then(
        (result) => {
          console.log(result);
        },
        (reason) => {
          console.log(reason);
        }
      );
  }
  onSaveService() {
    this.submitted = true;
    if (this.serviceForm.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const serviceCreate = new ServiceRequestModel(
        this.f.description.value,
        this.currentService.id,
        this.f.nom.value,
        this.f.obligatoire.value === true ? "O" : "N",
        this.f.priorite.value,
        0
      );
      this.genericsService
        .putResource(
          `${this.base_url + this.currentService.id}/update`,
          serviceCreate
        )
        .then((result: any) => {
          console.log(result);
          this.onLoadListService(this.search);
          this.toUpdate = false;
          this.submitted = false;
          localStorage.getItem("activelang") === "en"
            ? this.genericsService.confirmResponseAPI(
                AppConstants.SERVICE_LOADEDEN,
                "success"
              )
            : this.genericsService.confirmResponseAPI(
                AppConstants.SERVICE_LOADEDFR,
                "success"
              );
          if (this.modalService.hasOpenModals()) {
            this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
          }
        });
    } else {
      const serviceCreate = new ServiceRequestModel(
        this.f.description.value,
        null,
        this.f.nom.value,
        this.f.obligatoire.value === true ? "O" : "N",
        this.f.priorite.value,
        0
      );
      this.genericsService
        .postResource(`${this.base_url}create`, serviceCreate)
        .then((result: any) => {
          this.onLoadListService(this.search);
          this.initFormService();
          this.submitted = false;
          localStorage.getItem("activelang") === "en"
            ? this.genericsService.confirmResponseAPI(
                AppConstants.SERVICE_CREATEDEN,
                "success"
              )
            : this.genericsService.confirmResponseAPI(
                AppConstants.SERVICE_CREATEDFR,
                "success"
              );
          if (this.modalService.hasOpenModals()) {
            this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
          }
        });
    }
  }
  setPageChange(e) {
    console.log(e);
    this.page = e - 1;
    this.onLoadListService(this.search);
  }

  onLoadListService(predicat) {
    this.genericsService
      .getResource(`${this.base_url}list?page=${this.page}&pagine=${this.pagine}&size=${this.pageSize}&predicat=${predicat}`)
      .then((result: any) => {
        if (result.success === true) {
          console.log(result);
          this.services = result.data.content;
          this.totalElements = result.data.totalElements;
          // this.page = result.data.number;
          console.log(this.totalElements);
          // this.totalElements = result.data.numberOfElements;
        }
      });
  }
  onLoadDetailsService(service: ServiceModel, content: TemplateRef<any>) {
    this.currentService = service;
    this.toUpdate = true;
    this.serviceForm.patchValue({
      nom: service.nomService,
      // tslint:disable-next-line:triple-equals
      obligatoire: service.obligatoire == "O",
      description: service.description,
      priorite: service.priorite,
    });
    this.modalService
      .open(content, {
        ariaLabelledBy: "add-service",
        size: "lg",
        scrollable: true,
      })
      .result.then(
        (resultModal) => {},
        (reason) => {}
      );
  }

  onDeleteService(id: number) {
    localStorage.getItem("activelang") === "en"
      ? Swal.fire({
          title: AppConstants.ARE_U_SUREEN,
          text: AppConstants.IRREVERSIBLEEN,
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: AppConstants.YES_DELETE_ITEN,
        }).then((result) => {
          if (result.isConfirmed) {
            this.genericsService
              .deleteResource(`${this.base_url}${id}/delete`)
              .then((resultDelete: any) => {
                this.onLoadListService(this.search);
                this.toUpdate = false;
                this.genericsService.confirmModalResponseAPI(
                  AppConstants.DELETEDEN,
                  AppConstants.DELETE_SUCCESSEN,
                  "success"
                );
              })
              .catch((reason) => {
                console.log(reason);
                this.genericsService.confirmModalResponseAPI(
                  AppConstants.NO_DELETEDEN,
                  AppConstants.DELETE_FAILEN,
                  "error"
                );
              });
          }
        })
      : Swal.fire({
          title: AppConstants.ARE_U_SUREFR,
          text: AppConstants.IRREVERSIBLEFR,
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: AppConstants.YES_DELETE_ITFR,
        }).then((result) => {
          if (result.isConfirmed) {
            this.genericsService
              .deleteResource(`${this.base_url}${id}/delete`)
              .then((resultDelete: any) => {
                this.onLoadListService(this.search);
                this.toUpdate = false;
                this.genericsService.confirmModalResponseAPI(
                  AppConstants.DELETEDFR,
                  AppConstants.DELETE_SUCCESSFR,
                  "success"
                );
              })
              .catch((reason) => {
                console.log(reason);
                this.genericsService.confirmModalResponseAPI(
                  AppConstants.NO_DELETEDFR,
                  AppConstants.DELETE_FAILFR,
                  "error"
                );
              });
          }
        });
  }

  private initFormService() {
    this.serviceForm = this.fb.group({
      nom: ["", Validators.required],
      obligatoire: [true],
      description: [""],
      priorite: ["1"],
    });
  }

  private initFormServiceNiveau() {
    this.serviceNiveauForm = this.fb.group({
      niveau: ["", Validators.required],
    });
  }

  private onloadListNiveau() {
    this.genericsService
      .getResource(`admin/niveaus`)
      .then((response: any) => {
        console.log(response);
        this.niveaux = response.data;
      })
      .catch((reason) => {
        console.log(reason);
      });
  }

  get f() {
    return this.serviceForm.controls;
  }
  get g() {
    return this.serviceNiveauForm.controls;
  }

  onReset() {
    this.submitted = false;
    this.initFormService();
    this.toUpdate = false;
    this.modalService.dismissAll(ModalDismissReasons.ESC);
  }

  ngOnDestroy(): void {
    this.onReset();
    this.onResetServiceNiveau();
  }

  onAppliquerNiveau(service: ServiceModel, content: TemplateRef<any>) {
    this.currentService = service;
    this.modalService
      .open(content, {
        ariaLabelledBy: "add-service-to-niveau",
        scrollable: true,
      })
      .result.then(
        (resultModal) => {},
        (reason) => {}
      );
  }

  onSearchService() {

    this.onLoadListService(this.search);
}


  onSaveServiceNiveau() {
    this.submitted = true;
    if (this.serviceNiveauForm.invalid) {
      return;
    }
    this.genericsService
      .postResource(
        `admin/service/${this.currentService.id}/niveau/${this.g.niveau.value}/annee/${this.annee.id}/attribuer`,
        {}
      )
      .then((response: any) => {
        if (response.success == true) {
          this.genericsService.confirmResponseAPI(
            response.message,
            "success",
            6000
          );
        }
      })
      .catch((reason) => {
        console.log(reason);
      });
  }

  onResetServiceNiveau() {
    this.submitted = false;
    this.initFormServiceNiveau();
    this.toUpdate = false;
    this.modalService.dismissAll(ModalDismissReasons.ESC);
  }

  private onloadAnneeEncours() {
    this.genericsService
      .getResource(`admin/annees`)
      .then((response: any) => {
        this.annee = response.data.find(
          (item) => item.codeAnnee == this.tokenStorage.getUser().codeAnnee
        );
      })
      .catch((reason) => console.log(reason));
  }
}
