import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListRemiseComponent} from './list-remise.component';

describe('ListRemiseComponent', () => {
  let component: ListRemiseComponent;
  let fixture: ComponentFixture<ListRemiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRemiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRemiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
