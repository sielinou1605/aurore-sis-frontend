/* tslint:disable:triple-equals */
import { Component, HostListener, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RemiseModel } from '../../../../../shared/_models/response-dto/finance/remise-model';
import { GenericsService } from '../../../../../shared/_services/generics.service';
import { RemiseRequestModel } from '../../../../../shared/_models/request-dto/finance/remise-request-model';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthService } from '../../../../../shared/_services/auth.service';
import { AppConstants } from '../../../../../shared/element/app.constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list-remise',
  templateUrl: './list-remise.component.html',
  styleUrls: ['./list-remise.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListRemiseComponent implements OnInit, OnDestroy {
  public remiseForm: FormGroup;
  public applicationBonusForm: FormGroup;
  public submitted: boolean;
  public remises: RemiseModel[];
  private base_url = 'admin/modele-remise-globale/';
  private toUpdate = false;
  private currentRemise: RemiseModel;
  public eric: string;

  public settings: any;
  dataEleve;
  // @ts-ignore
  @ViewChild('selectEleve') selectEleve;
  public pageSize = 150;
  public page = 0;
  public totalElements: number;

  public validated: boolean;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private translate: TranslateService,
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, private genericsService: GenericsService,
    public authService: AuthService) {
    this.eric = localStorage.getItem('activelang') === 'en' ? 'Select a student' : 'Sélectionnez un élève';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(18);
  }

  ngOnInit() {
    this.initFormRemise();
    this.onLoadListRemise();
    // tslint:disable-next-line:triple-equals
    this.settings = localStorage.getItem('activelang') == 'en' ? {
      singleSelection: false,
      idField: 'id',
      textField: AppConstants.DESCRIPTIONEN,
      enableCheckAll: true,
      selectAllText: AppConstants.SELECT_ALLEN,
      unSelectAllText: AppConstants.UNSELECT_ALLEN,
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 150,
      itemsShowLimit: 15,
      searchPlaceholderText: AppConstants.SEARCHEN,
      noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEEN,
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    } : {
      singleSelection: false,
      idField: 'id',
      textField: AppConstants.DESCRIPTIONFR,
      enableCheckAll: true,
      selectAllText: AppConstants.SELECT_ALLFR,
      unSelectAllText: AppConstants.UNSELECT_ALLFR,
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 150,
      itemsShowLimit: 15,
      searchPlaceholderText: AppConstants.SEARCHFR,
      noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEFR,
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    };

    this.onloadListEleves('');
    this.initFormApplyRemise();
  }

  onloadListEleves(predicat: string) {
    const params = `predicat=${predicat}&size=${this.pageSize}&page=${this.page}`;
    this.genericsService.getResource(`admin/inscription/liste?${params}`).then((result: any) => {
      if (result.data.content.length == 0) {
        this.onloadListEleves('');
      } else {
        if (this.dataEleve) {
          this.dataEleve = null;
          this.dataEleve = [];
        } else {
          this.dataEleve = [];
        }
        result.data.content.forEach(eleve => {
          this.dataEleve.push({ id: eleve.matricule, description: eleve.matricule + ' ' + eleve.noms });
        });
        this.settings.data = this.dataEleve;
      }
      console.log(this.settings);
    }).catch(reason => {
      console.log(reason);
      // tslint:disable-next-line:max-line-length
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error')
        : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }
  initFormApplyRemise() {
    this.applicationBonusForm = this.fb.group({
      eleve: [[], [Validators.required]],
    });
  }

  open(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.modalService.open(content,
      { ariaLabelledBy: 'add-remise', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => {
      console.log(reason);
    });
  }

  private initFormRemise() {
    this.remiseForm = this.fb.group({
      nomModele: ['', Validators.required],
      nomRemise: ['', Validators.required],
      type: ['1'],
      valeurRemise: ['', Validators.required]
    });
  }

  onSaveValidApplyBonus() {
    console.log(this.g.eleve);

    this.submitted = true;
    this.validated = true;
    if (this.applicationBonusForm.invalid) {
      this.validated = false;
      return;
    }
    const listEleves = [];
    this.g.eleve.value.forEach(elt => {
      listEleves.push(elt.id);
    });
    this.genericsService.postResource(`admin/bonus/appliquer`, listEleves).then((response: any) => {
      this.genericsService.confirmResponseAPI(response.message, 'success', 7000);
      this.onReloadListEleve('');
      this.applicationBonusForm.setValue({ eleve: [] });
      this.submitted = false;
      this.validated = false;
    }).catch(reason => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI(AppConstants.ARCHIVED_ERROREN, 'error', 7000)
        : this.genericsService.confirmResponseAPI(AppConstants.ARCHIVED_ERRORFR, 'error', 7000);
    });
  }

  onChange(event: any) {
    console.log(' E_________onChange Event : ', event.target.value);
    this.onloadListEleves(event.target.value);
  }
  onReloadListEleve(event: any) {
    // this.predicat += event.target.value;
    const elevesSelectionne = event;
    if (elevesSelectionne.length > this.g.eleve.value.length) {
      elevesSelectionne.forEach(e => {
        const isExist = this.g.eleve.value.filter(o => o.id === e.id);
        if (isExist.length === 0) { this.g.eleve.value.push(e); }
      });
    } else {
      console.log(' Pensez a retire les elts ');

      // this.g.eleve.value.forEach
    }
    console.log(' E_________ onReloadListEleve Event : ', event);

  }
  @HostListener('window:keyup', ['$event']) keyUp(e: KeyboardEvent) {
    if ((e.code == 'Insert' || e.code == 'Enter')) {
      this.onReloadListEleve('');
    }
  }

  onSaveRemise() {
    this.submitted = true;
    if (this.remiseForm.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const remiseRequest = new RemiseRequestModel(this.currentRemise.id, this.f.valeurRemise.value,
        this.f.nomModele.value, this.f.nomRemise.value, this.f.type.value);
      this.genericsService.putResource(`${this.base_url + this.currentRemise.id}/update`,
        remiseRequest).then((result: any) => {
        this.onLoadListRemise();
        this.toUpdate = false;
        this.submitted = false;
        this.currentRemise = null;
        this.initFormRemise();
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmResponseAPI(AppConstants.REMISE_UPDATEDEN, 'success')
          : this.genericsService.confirmResponseAPI(AppConstants.REMISE_UPDATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    } else {
      const remiseRequest = new RemiseRequestModel(null, this.f.valeurRemise.value,
        this.f.nomModele.value, this.f.nomRemise.value, this.f.type.value);
      this.genericsService.postResource(`${this.base_url}create`, remiseRequest).then((result: any) => {
        this.onLoadListRemise();
        this.initFormRemise();
        this.submitted = false;
        // tslint:disable-next-line:max-line-length
        localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.REMISE_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.REMISE_CREATEDFR, 'success');
        console.log(localStorage.getItem('activelang'));
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    }
  }

  get f() { return this.remiseForm.controls; }

  get g() { return this.applicationBonusForm.controls; }

  onReset() {
    this.initFormApplyRemise();
    this.submitted = false;
    this.toUpdate = false;
    this.initFormRemise();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onLoadDetailsRemise(remise: RemiseModel, content: TemplateRef<any>) {
    this.toUpdate = true;
    this.currentRemise = remise;
    this.remiseForm.patchValue({
      nomModele: remise.nom_modele,
      nomRemise: remise.nom_rg,
      type: remise.type_rg,
      valeurRemise: remise.montant_rg
    });
    this.modalService.open(content,
      { ariaLabelledBy: 'add-service', size: 'lg', scrollable: true }).result.then((resultModal) => { },
      (reason) => { });
  }

  onDeleteRemise(id: any) {
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: AppConstants.ARE_U_SUREEN,
        text: AppConstants.IRREVERSIBLEEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITEN
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            this.onLoadListRemise();
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
          });
        }
      }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            this.onLoadListRemise();
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }

  onLoadListRemise() {
    this.genericsService.getResource(`${this.base_url}list`).then((result: any) => {
      if (result.success === true) {
        this.remises = result.data;
        console.log(this.remises);
      }
    });
  }
  @HostListener('unloaded')
  ngOnDestroy(): void {
    this.onReset();
    // window.location.reload();
  }
}
