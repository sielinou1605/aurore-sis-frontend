import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListModeleEcheanceComponent} from './list-modele-echeance.component';

describe('ListModeleEcheanceComponent', () => {
  let component: ListModeleEcheanceComponent;
  let fixture: ComponentFixture<ListModeleEcheanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListModeleEcheanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListModeleEcheanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
