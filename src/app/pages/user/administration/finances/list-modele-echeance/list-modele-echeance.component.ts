import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {ModeleEcheancierModel} from '../../../../../shared/_models/response-dto/finance/modele-echeancier-model';
import {ModeleEcheancierRequestModel} from '../../../../../shared/_models/request-dto/finance/modele-echeancier-request-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ServiceModel} from '../../../../../shared/_models/response-dto/finance/service-model';
import {NiveauModel} from '../../../../../shared/_models/response-dto/common/niveau-model';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-modele-echeance',
  templateUrl: './list-modele-echeance.component.html',
  styleUrls: ['./list-modele-echeance.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListModeleEcheanceComponent implements OnInit, OnDestroy {
  modeleEcheanceForm: FormGroup;
  searchForm: FormGroup;
  public submitted: boolean;
  public modeleEcheanciers: ModeleEcheancierModel[];
  private base_url = 'admin/modele-echeancier/';
  private base_url_service = 'admin/service/';
  private toUpdate = false;
  private currentModeleEcheancier: ModeleEcheancierModel;
  public niveaux: NiveauModel[];
  public services: ServiceModel[];
  public annees: any;
  public isNotifie= false;
  public totalElements = 0;
  public pageSize = 20;
  public page = 0;
  public model: string;
  validated: boolean;
  currentSwitch = localStorage.getItem('activelang') === 'en' ? AppConstants.ALL_MODELESEN : AppConstants.ALL_MODELESFR;
  currNiveau = 0;
  allModels = localStorage.getItem('activelang') === 'en' ? AppConstants.ALL_MODELESEN : AppConstants.ALL_MODELESFR;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  public predicate = '';

  constructor(
    private translate: TranslateService,
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, private genericsService: GenericsService,
    public authService: AuthService,
    private tokenStorage: TokenStorageService
  ) {

    this.model = localStorage.getItem('activelang') === 'en' ? 'Model ' : 'Modèle ';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(23);
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.initModeleEcheance();
    this.onLoadListModeleEcheance(this.currNiveau);
    this.onLoadListService();
    this.onLoadListNiveau();
    this.onLoadListAnnees();
  }

  get fSearch() {
    return this.searchForm.controls;
  }

  onLoadListAnnees() {
    this.genericsService.getResource(`annees?size=100`).then((result: any) => {
      this.annees = result._embedded.annees;
    });
  }

  open(content: TemplateRef<any>) {
    this.initModeleEcheance();
    this.toUpdate = false;
    this.currentModeleEcheancier = null;
    this.modalService.open(content,
      {ariaLabelledBy: 'add-caisse', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  private onLoadListService() {
    this.genericsService.getResource(`${this.base_url_service}list`).then((result: any) => {
      if (result.success === true) {
        this.services = result.data;
      }
    });
  }

  private onLoadListNiveau() {
    this.genericsService.getResource(`niveaus?size=200`).then((result: any) => {
      this.niveaux = result._embedded.niveaus;
    });
  }

  private initModeleEcheance() {
    this.modeleEcheanceForm = this.fb.group({
      designation: ['', Validators.required],
      nomEch: ['', Validators.required],
      dateEch: ['', Validators.required],
      annee: ['', Validators.required],
      montant: ['', Validators.required],
      service: ['', Validators.required],
      niveau: ['', Validators.required],
      dispense: ['', Validators.required],
      bourse: ['', Validators.required],
      autreRemise: ['', Validators.required],
    });
  }

  onSaveModeleEcheance() {
    this.submitted = true;
    this.validated = true;

    if (this.modeleEcheanceForm.invalid) {
      this.validated = false;
      return;
    }
    if (this.toUpdate === true) {
      const modeleEcheancierRequest = new ModeleEcheancierRequestModel(
        this.f.annee.value, this.f.dateEch.value, this.currentModeleEcheancier.id,
        this.f.niveau.value, this.f.service.value, this.f.designation.value,
        this.f.montant.value,  this.f.nomEch.value, this.f.autreRemise.value,
        this.f.bourse.value,this.f.dispense.value
      );

      console.log("modeleEcheancierRequest",modeleEcheancierRequest);
      
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
      localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: AppConstants.PROPAGER_MODIFEN,
        text: AppConstants.PROPAGER_MODIF_DETAILEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Ne pas propager',
        confirmButtonText: AppConstants.PROPAGER_MODIF_CONFIRMEN
      }).then((resultSwal) => {
        if (resultSwal.isConfirmed) {
          this.onUpdateEcheancier(1, modeleEcheancierRequest);
        } else {
          this.onUpdateEcheancier(0, modeleEcheancierRequest);
        }
      }) : Swal.fire({
          title: AppConstants.PROPAGER_MODIFFR,
          text: AppConstants.PROPAGER_MODIF_DETAILFR,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Ne pas propager',
          confirmButtonText: AppConstants.PROPAGER_MODIF_CONFIRMFR
        }).then((resultSwal) => {
          if (resultSwal.isConfirmed) {
            this.onUpdateEcheancier(1, modeleEcheancierRequest);
          } else {
            this.onUpdateEcheancier(0, modeleEcheancierRequest);
          }
        });
    } else {
      const modeleEcheancierRequest = new ModeleEcheancierRequestModel(
        this.f.annee.value, this.f.dateEch.value, null,
        this.f.niveau.value, this.f.service.value, this.f.designation.value,
        this.f.montant.value,  this.f.nomEch.value, this.f.autreRemise.value,
        this.f.bourse.value,this.f.dispense.value
      );
      this.genericsService.postResource(`${this.base_url}create`,
        modeleEcheancierRequest).then((result: any) => {
        this.isNotifie = true;
        this.onLoadListModeleEcheance(this.currNiveau);
        this.initModeleEcheance();
        this.submitted = false;
        this.validated = false;
        this.toUpdate = false;
        this.currentModeleEcheancier = null;

        //localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.PROPAGER_MODIF_OKEN, 'success', 50000) : this.genericsService.confirmResponseAPI(AppConstants.PROPAGER_MODIF_OKFR, 'success', 50000);
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      }).catch(reason => {
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONEN,  'error')
        : this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONFR,  'error');


        this.validated = false;
      });
    }
  }

  onUpdateEcheancier(propager, dto) {
    this.genericsService.putResource(`admin/modele-echeancier/${this.currentModeleEcheancier.id}/update/propager/${propager}`,
      dto).then((result: any) => {
      this.isNotifie = true;
      this.onLoadListModeleEcheance(this.currNiveau);
      this.toUpdate = false;
      this.submitted = false;
      this.validated = false;
      this.initModeleEcheance();
      this.currentModeleEcheancier = null;

    }).catch(reason => {
      localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error') : this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
    });
  }

  get f() { return this.modeleEcheanceForm.controls; }

  onReset() {
    this.submitted = false;
    this.toUpdate = false;
    this.initModeleEcheance();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onLoadDetailsModeleEcheance(modeleEcheance: ModeleEcheancierModel, content: TemplateRef<any>) {
    const dateEch = modeleEcheance.date_ech;
    this.toUpdate = true;
    this.currentModeleEcheancier = modeleEcheance;
    this.modeleEcheanceForm.patchValue({
      designation: modeleEcheance.intitule,
      nomEch: modeleEcheance.nom_ech,
      dateEch: dateEch,
      annee: modeleEcheance.anneeAcademique,
      montant: modeleEcheance.montant_a_ech,
      service: modeleEcheance.service.id,
      niveau: modeleEcheance.niveau.id,
      dispense: modeleEcheance.autoriserRemiseDispense,
      bourse: modeleEcheance.autoriserRemiseBourse,
      autreRemise: modeleEcheance.autoriserRemiseAutres 
    });
console.log(this.modeleEcheanceForm);


    this.modalService.open(content,
      {ariaLabelledBy: 'add-service', size: 'lg', scrollable: true}).result.then((resultModal) => {},
      (reason) => {});
  }

  onDeleteModeleEcheance(id: any) {

    console.log(id);
    localStorage.getItem('activelang') === 'en' ? Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
          this.onLoadListModeleEcheance(this.currNiveau);
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
      title: AppConstants.ARE_U_SUREFR,
      text: AppConstants.IRREVERSIBLEFR,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITFR
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
          this.onLoadListModeleEcheance(this.currNiveau);
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
        }).catch(reason => {
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
        });
      }
    });
  }

  onLoadListModeleEcheance(niveau: number) {
    this.predicate = this.fSearch.search.value;
    this.currNiveau = niveau;
    this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(
        `admin/modele-echeancier/${niveau}
        /liste?predicat=${this.predicate}&size=${this.pageSize}&page=${this.page}&annee=${(this.tokenStorage.getUser().codeAnnee)}`)
      .then((result: any) => {
        console.log(result);
        if (result.success === true) {
          this.modeleEcheanciers = result.data.content;
          this.totalElements = result.data.totalElements;
        }
        this.genericsService.stopLoadingPage();
        if(this.isNotifie==true)
        {
          localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.MODELE_ECHEANCE_UPDATEDEN, 'success', 9000) : this.genericsService.confirmResponseAPI(AppConstants.MODELE_ECHEANCE_UPDATEDFR, 'success', 9000);
          this.isNotifie = false;
        }
      }).catch(reason => {
      this.genericsService.stopLoadingPage();
       localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }

  setPageChange(event) {
    this.page = event - 1;
    this.onLoadListModeleEcheance(this.currNiveau);
  }
  ngOnDestroy(): void {
    this.onReset();
  }

  swicthTo(niveau: number, type: string) {
    this.currNiveau = niveau;
    this.currentSwitch = type;
    this.onLoadListModeleEcheance(niveau);
  }
}
