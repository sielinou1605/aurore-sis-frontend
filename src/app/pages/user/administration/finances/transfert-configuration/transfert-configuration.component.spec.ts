import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransfertConfigurationComponent} from './transfert-configuration.component';

describe('TransfertConfigurationComponent', () => {
  let component: TransfertConfigurationComponent;
  let fixture: ComponentFixture<TransfertConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransfertConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransfertConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
