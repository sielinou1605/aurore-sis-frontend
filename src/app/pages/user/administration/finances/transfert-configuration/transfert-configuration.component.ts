/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NiveauModel} from '../../../../../shared/_models/response-dto/common/niveau-model';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-transfert-configuration',
  templateUrl: './transfert-configuration.component.html',
  styleUrls: ['./transfert-configuration.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class TransfertConfigurationComponent implements OnInit, OnDestroy {
  // @ts-ignore
  @ViewChild('selectNiveaux') selectNiveaux;
  public transfertConfigForm: FormGroup;
  public niveaux: NiveauModel[];
  currentNiveau: NiveauModel;
  submitted: boolean;
  settings: any;
  dataFiltre: any;
  private annee: any;
  public placeholder: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private modalService: NgbModal, config: NgbModalConfig,
    private genericsService: GenericsService,
    private fb: FormBuilder,
    private tokenStorage: TokenStorageService,
    private translate: TranslateService
  ) {
    localStorage.getItem('activelang') === 'en' ?
    this.placeholder = "Choose the levels" : this.placeholder = "Choisir les niveaux";
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    localStorage.getItem('activelang') === 'en' ?
    this.settings = {
      singleSelection: false, idField: 'id', textField: AppConstants.DESCRIPTIONEN,
      enableCheckAll: true, selectAllText: AppConstants.SELECT_ALLEN,
      unSelectAllText: AppConstants.UNSELECT_ALLEN, allowSearchFilter: true,
      limitSelection: -1, clearSearchFilter: true,
      maxHeight: 180, itemsShowLimit: 8,
      searchPlaceholderText: AppConstants.SEARCHEN,
      noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEEN,
      closeDropDownOnSelection: false, showSelectedItemsAtTop: false, defaultOpen: false
    } : this.settings = {
        singleSelection: false, idField: 'id', textField: AppConstants.DESCRIPTIONFR,
        enableCheckAll: true, selectAllText: AppConstants.SELECT_ALLFR,
        unSelectAllText: AppConstants.UNSELECT_ALLFR, allowSearchFilter: true,
        limitSelection: -1, clearSearchFilter: true,
        maxHeight: 180, itemsShowLimit: 8,
        searchPlaceholderText: AppConstants.SEARCHFR,
        noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEFR,
        closeDropDownOnSelection: false, showSelectedItemsAtTop: false, defaultOpen: false
      };
    this.onloadListNiveaus();
    this.initFormTransfertConfig();
    this.onloadAnneeEncours();
  }

  ngOnDestroy(): void {
    this.onResetTransfertConfig();
  }

  private onloadListNiveaus() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/niveaus`).then((response: any) => {
      this.genericsService.stopLoadingPage();
      this.niveaux = response.data;
      this.dataFiltre = [];
      this.niveaux.forEach(filtre => {
        this.dataFiltre.push({id: filtre.id, description: filtre.nomNiveau});
      });
    }).catch(reason => {
      console.log(reason);
    });
  }

  private initFormTransfertConfig() {
    this.transfertConfigForm = this.fb.group({
      niveauxDest: [[], Validators.required]
    });
  }
  get f() { return this.transfertConfigForm.controls; }

  onResetTransfertConfig() {
    this.currentNiveau = null;
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
    this.initFormTransfertConfig();
  }

  onValidTransfertConfig() {
    this.submitted = true;
    if (this.transfertConfigForm.invalid) {
      return;
    }
    this.genericsService.startLoadingPage();
    const dto = [];
    this.f.niveauxDest.value.forEach(elt => {
      dto.push(elt.id);
    });
    console.log(dto);
    const url = `admin/niveau/${this.currentNiveau.id}/annee-source/${this.annee.id}/annee-destination/${this.annee.id}/copier`;
    this.genericsService.postResource(url, dto).then((response: any) => {
      console.log(response);
      this.genericsService.stopLoadingPage();
      this.onResetTransfertConfig();
    }).catch(reason => {
      console.log(reason);
    });

  }

  onOpenModalTransfert(niveau: NiveauModel, modalTransfertConfig: TemplateRef<any>) {
    this.currentNiveau = niveau;
    this.modalService.open(modalTransfertConfig,
      {ariaLabelledBy: 'transfert-config'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  private onloadAnneeEncours() {
    this.genericsService.getResource(`admin/annees`).then((response: any) => {
      this.annee = response.data.find(item => item.codeAnnee == this.tokenStorage.getUser().codeAnnee);
    }).catch(reason => console.log(reason));
  }
}
