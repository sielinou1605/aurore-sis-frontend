import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-menu-config',
  templateUrl: './menu-config.component.html',
  styleUrls: ['./menu-config.component.scss']
})
export class MenuConfigComponent implements OnInit {

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private translate: TranslateService,
    public authService: AuthService, private router: Router) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(6);

  }

  ngOnInit() {
  }

  onloadPage(url: string, id: string) {
    console.log(url);
    console.log(id);
    if (this.authService.isActif(id)) {
      this.router.navigate([url]).then(() => {});
    }
  }
}
