import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {NgbCalendar, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  acompteListeRequestModel,
  acompteRequestModel,
  acompteResponseModel, AddAcompteRequestModel
} from '../../../../../shared/_models/request-dto/finance/acompte-request-model';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {FACTUREACCOMPTE, ParamImpressionModel} from '../../../../../shared/_models/request-dto/common/etats-model';


@Component({
  selector: 'app-acompte',
  templateUrl: './acompte.component.html',
  styleUrls: ['./acompte.component.scss']
})
export class AcompteComponent implements OnInit {
  public selectedFiles: any;
  readOnly = false;
  submitted = false;
  public idEleve: any;
  public acomptForm: FormGroup;
  public listeClass: any;
  public listeEleveClass: any[] = [];
  public elevedetail: any;
  public line: any;
  private eleveCourant: any;
   idClass = 0;
  public nomNiveau: any;
  public pageSize = 10;
  public page = 0;
  totalElements: any;
  line_temp: null;
  find: any;
  newMatricule = '';
   nomIdClasse: any;
   predicate: string;
  showPageCount = true;
   showErrorMessage = false;
   idAccompteEnregistre: any;
   accompteEnregistre: any;
  private base_url = 'admin/inscription/';
   inscription: any;

  constructor(
    public genericsService: GenericsService,
    public modalService: NgbModal,
    public fb: FormBuilder,
    public calendar: NgbCalendar,
    public router: Router,
    public routerNav: ActivatedRoute,
    public authService: AuthService
  ) { }

  ngOnInit() {
    // this.genericsService.getResource(`${this.base_url + ''}/get-recap`).then((resp: any) => {
    //   console.log(resp);
    //   this.inscription = resp.data;
    // });
    console.log(this.listeEleveClass);
    this.getListeClass();
    // this.getAcompte();
    this.initForm('');
    if (this.idClass >= 1) {
      this.routerNav.params.subscribe(() => {
        this.getListeClassOnTable(this.page, this.pageSize);
      });
    }
  }
  uploadListAccompte() {
    console.log('uploadListAccompte');
    console.log(this.f.matricule.value);
    const name = this.f.matricule.value;
    if (name === '') {
      this.router.navigate([`/administration/finances/list-acomptes`]).then((res: any) => {
        console.log(res);
      });
    } else {
      this.router.navigate([`/administration/finances/list-acomptes/${name}`]).then((res: any) => {
        console.log(res);
      });
    }
  }
  getAcompte() {
    this.genericsService.getResource(`admin/inscription/list`).then((response) => {
     // console.log(response);
    });
  }

  getListeClass() {
    this.genericsService.getResource(`admin/classes`).then((response: any) => {
      console.log(response);
      if (response) {
        this.listeClass = response.data;
      }
    });
  }
 get f() {
    return this.acomptForm.controls;
 }
  initForm(data) {
    this.showErrorMessage = false;
    const today = this.calendar.getToday();
    console.log(today);
    console.log(this.calendar);
    // const day = today.day;
    // const month = today.month;
    // const year = today.year;
    // const date = new Date(year, month - 1, day);
    // const formattedDate = `${date.getDate()}-${0}${date.getMonth() + 1}-${date.getFullYear()}`;
    const format1 = new Date();
    this.acomptForm = this.fb.group({
      id: [0],
      prenomEleve: [data ? data.prenomEleve : ''],
      nomEleve: [data ? data.nomEleve : ''],
      matricule: [data ? data.matricule : ''],
      // codeAcces: [data ? data.codeAcces : ''],
      // dateAdmission: [data ? data.date_admission : formattedDate],
      // dateNaissance: [data ? data.date_naissance : formattedDate],
      // dateAdmission: [data ? data.date_admission : this.calendar.getToday()],
      // dateNaissance: [data ? data.date_naissance : this.calendar.getToday()],
      dateAdmission: [data ? data.date_admission : format1.toISOString().split('T')[0]],
      dateNaissance: [data ? data.date_naissance : format1.toISOString().split('T')[0]],
      lieuNaissance: [data ? data.lieuNaissance : ''],
      nationalite: [data ? data.nationalite : ''],
      eleveTransferer: [data ? data.eleveTransferer : ''],
      emailEleve: [data ? data.emailEleve : '', Validators.email],
      emailParent: [data ? data.emailParent : '', Validators.email],
      search: [data ? data.id : ''],
      handicape: [data ? data.handicape : false],
      montantVerse: [data ? data.montantVerse : '', [Validators.required]],
      nomMere: [data ? data.nomMere : ''],
      nomParent: [data ? data.nomParent : ''],
      numeroEleve: [data ? data.numeroEleve : ''],
      photo: [data ? data.photo : ''],
      value: [data ? data.value : ''],
      religion: [data ? data.religion : ''],
      residenceEleve: [data ? data.residenceEleve : ''],
      residenceParent: [data ? data.residenceParent : ''],
      sexe: [data ? data.sexe : ''],
      statut: [data ? data.statut : ''],
      telephoneEleve: [data ? data.telephoneEleve : ''],
      telephoneMere: [data ? data.telephoneMere : ''],
      telephoneParent: [data ? data.telephoneParent : ''],
      telephonePere: [data ? data.telephonePere : '', [Validators.required, Validators.pattern('^6[0-9]{8}$')]],
      transfert: [data ? data.transfert : ''],
      idClasse: [data ? this.nomIdClasse : '', Validators.required]
    });
    console.log(this.f.dateAdmission.value);
  }
  getElt(data) {
    // console.log(data.target.value);
    console.log(data);
    console.log(this.listeClass);
    this.nomIdClasse = data.target.value;

    this.find = this.listeClass.find(x => x.nomClasse === data.target.value);
    console.log(this.find);
    // console.log(this.find.nomNiveau);
        this.idClass = this.find.id;
  }

  getListeClassOnTable(page: number, pageSize: number) {
    this.predicate = '';
    this.totalElements = 0;
    console.log(this.idClass);
    // tslint:disable-next-line:max-line-length
    this.genericsService.getResource(`admin/classe/${this.idClass}/eleves/liste?predicat=${this.predicate}&page=${page}&size=${pageSize}`).then((res: any) => {
      this.listeEleveClass = res.data.content;
      this.totalElements = res.data.totalElements;
      console.log(res);
      console.log(this.totalElements);
    });
    // console.log(this.listeClass[this.f.search.value]);
    this.nomNiveau = this.listeClass[this.f.search.value];
  }

  onSelectEleveDetail(item) {
    this.elevedetail = item;
    this.line = item.matricule;
    console.log('elevedetail', this.elevedetail);
    console.log('line', this.line);
    this.line_temp = this.line;
  }

  onOpenEleveDetail(item) {
    this.showErrorMessage = false;
    this.readOnly = true;
    this.line = item.matricule;
    this.newMatricule = this.line;
    console.log(this.listeEleveClass);
    this.eleveCourant = item;
    console.log(this.eleveCourant);
    this.initForm(item);

  }
  save() {
    console.log(this.eleveCourant);
    if (this.acomptForm.valid && (this.f.montantVerse.value > 100)) {

      this.newMatricule = this.f.matricule.value;
      console.log(this.newMatricule);
      let dto;
      console.log(this.newMatricule);
      const idClass = parseInt(String(this.idClass), 10);
      this.submitted = true;
      this.readOnly = false;
      console.log(this.newMatricule);
      console.log(this.f.dateAdmission.value);
      dto = new AddAcompteRequestModel(
        // tslint:disable-next-line:max-line-length
        this.eleveCourant ? (this.eleveCourant.dateAdmission ? this.eleveCourant.dateAdmission : this.eleveCourant.date_admission) : this.f.dateAdmission.value,
        // tslint:disable-next-line:max-line-length
        this.eleveCourant ? (this.eleveCourant.dateNaissance ? this.eleveCourant.dateNaissance : this.eleveCourant.date_naissance) : this.f.dateNaissance.value,
        // this.eleveCourant ? this.eleveCourant.eleveTransferer : (this.f.eleveTransferer.value ? this.f.eleveTransferer.value : false),
        false,
        this.eleveCourant ? this.eleveCourant.emailEleve : this.f.emailEleve.value,
        this.eleveCourant ? this.eleveCourant.emailParent : this.f.emailParent.value,
        this.eleveCourant ? this.eleveCourant.handicape : (this.f.handicape.value === 0 ? true : false),
        // this.eleveCourant ? this.eleveCourant.handicape : (this.f.handicape.value === false ? 0 : 1),
        0,
        // this.eleveCourant ? (this.eleveCourant.classe.id ? this.eleveCourant.classe.id : this.idClass) : 0,
        this.idClass,
        this.eleveCourant ? (this.eleveCourant.lieuNaissance ? this.eleveCourant.lieuNaissance : this.eleveCourant.lieu_naissance) : this.f.lieuNaissance.value,
        this.eleveCourant ?  this.eleveCourant.matricule : '',
        this.f.montantVerse.value,
        this.eleveCourant ? this.eleveCourant.nationalite : this.f.nationalite.value,
        this.eleveCourant ? this.eleveCourant.nomEleve : this.f.nomEleve.value,
        this.eleveCourant ? this.eleveCourant.nomMere : this.f.nomMere.value,
        this.eleveCourant ? this.eleveCourant.nomParent : this.f.nomParent.value,
        0,
        this.eleveCourant ? this.eleveCourant.photo : this.f.photo.value,
        this.eleveCourant ? this.eleveCourant.prenomEleve : this.f.prenomEleve.value,
        this.eleveCourant ? this.eleveCourant.religion : this.f.religion.value,
        '',
        '',
        this.eleveCourant ? this.eleveCourant.sexe : '',
        this.eleveCourant ? this.eleveCourant.statut : this.f.statut.value,
        '',
        '',
        '',
        this.eleveCourant ? this.eleveCourant.telephonePere : this.f.telephonePere.value,
        true,
      );
      // console.log(this.eleveCourant.sexe);
      console.log(dto);
      this.genericsService.postResource(`admin/add/inscription/accompte/eleve`, dto).then((res: any) => {
        console.log(res);
        this.idAccompteEnregistre = res.data.id;
        this.accompteEnregistre = res.data;
        console.log(this.accompteEnregistre);
        // tslint:disable-next-line:max-line-length
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmResponseAPI(AppConstants.SERVICE_LOADEDEN, 'success')
          : this.genericsService.confirmResponseAPI(AppConstants.SERVICE_LOADEDFR, 'success');

        console.log(this.accompteEnregistre);
        this.onImprimerRecuAccompte();
        this.initForm('');

        this.eleveCourant = '';

      }).catch(reason => {
        console.log(reason);
        this.genericsService.confirmResponseAPI(reason.error.error, 'error', 7000);
        // localStorage.getItem('activelang') === 'en' ?
        //   this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_ERROREN, 'error', 7000)
        //   : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_ERRORFR, 'error', 7000);
        // this.genericsService.stopLoadingPage();
      });


    } else {
      this.genericsService.confirmResponseAPI('Veuillez remplir les champs obligatoires', 'error', 7000);
      // this.onOpenEleveDetail(this.eleveCourant);
      this.showErrorMessage = true;
    }
  }


  onImprimerRecuAccompte() {
    console.log(this.accompteEnregistre);
    console.log(typeof FACTUREACCOMPTE);
    const idAccompte = this.accompteEnregistre.id;
    let dto: any;
    dto = new ParamImpressionModel(
      false,
      FACTUREACCOMPTE,
      [
        {
          texte: 'ID',
          valeur: idAccompte
        }
      ]
    );
    // console.log(accompte);
    // console.log(this.listeEleveClass);
    // console.log(idAccompteEnregistre);
    // const classeAccompte = accompte.classe;
    console.log(dto);
    this.genericsService.reportPostPostResource(
      `parametre/etat/imprimer`, dto
    ).then((result: any) => {
      console.log(result);
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Reçu_' + this.accompteEnregistre.matricule + ' ');
    }).catch(reason => {
      console.error(reason);
      localStorage.getItem('activelang') === 'en'
        ? Swal.fire('Echec!', 'Receipt unavailable!!!', 'error')
        : Swal.fire('Echec!', 'Recu insdiponible!!!', 'error');
    });
  }

  setPageChange(event) {
    this.page = event - 1;
    this.getListeClassOnTable(this.page, this.pageSize);
  }
  onselectedFile(event: any) {
    this.selectedFiles = event.target.files;
  }
  resetForm() {
    this.showErrorMessage = false
    this.line_temp = null;
    this.readOnly = false;
      this.initForm('');
    this.line = this.line_temp;
  }

  // ngOnDestroy() {
  //   this.getListeClass();
  //   this.getListeClassOnTable(this.page, this.pageSize);
  //   // if (this.idClass >= 1) {
  //   //   this.routerNav.params.subscribe(() => {
  //   //     this.getListeClassOnTable(this.page, this.pageSize);
  //   //   });
  //   // }
  //   // this.setPageChange(this.page - 1);
  //   // this.showPageCount = false;
  //   // this.getListeClassOnTable(this.page, this.pageSize);
  // }
}
