import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListBourseComponent} from './list-bourse.component';

describe('ListBourseComponent', () => {
  let component: ListBourseComponent;
  let fixture: ComponentFixture<ListBourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
