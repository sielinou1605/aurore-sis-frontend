import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BourseModel} from '../../../../../shared/_models/response-dto/finance/bourse-model';
import {BourseRequestModel} from '../../../../../shared/_models/request-dto/finance/bourse-request-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';
import {TokenStorageService} from "../../../../../shared/_services/token-storage.service";

@Component({
  selector: 'app-list-bourse',
  templateUrl: './list-bourse.component.html',
  styleUrls: ['./list-bourse.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListBourseComponent implements OnInit, OnDestroy {
  public bourseForm: FormGroup;
  public submitted: boolean;
  public bourses: BourseModel[];
  private base_url = 'admin/bourse/';
  private toUpdate = false;
  private currentBourse: BourseModel;
  public annees: any;
  public services: any;
  private base_url_service = 'admin/service/';

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  public reconduire = false;
  private codeAnneeSource: number;
  private codeAnnee: number;

  constructor(
    private modalService: NgbModal,
    config: NgbModalConfig, private genericsService: GenericsService,
    private fb: FormBuilder, public authService: AuthService, public tokenStorage: TokenStorageService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(22);
  }

  ngOnInit() {
    console.log(location.port);
    console.log(location.hostname);
    this.codeAnnee=this.tokenStorage.getUser().codeAnnee;
    this.initFormBourse();
    this.onLoadListBourse();
    this.onLoadListAnnees();
    this.onLoadListService();
  }

  open(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'add-bourse', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  private initFormBourse() {
    this.bourseForm = this.fb.group({
      intitule: ['', Validators.required],
      anneeAcc: [{value:this.codeAnnee, disabled: true}, Validators.required],
      modApplication: ['', Validators.required],
      montantBourse: ['', Validators.required],
      motif: ['', Validators.required],
      idService: ['', Validators.required]
    });
  }

  onSaveBourse() {
    this.submitted = true;
    if (this.bourseForm.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const bourseRequest = new BourseRequestModel(
        this.currentBourse.id, this.f.anneeAcc.value, this.f.intitule.value,
        this.f.modApplication.value, this.f.montantBourse.value, this.f.motif.value,
        this.f.idService.value
      );
      this.genericsService.putResource(`${this.base_url + this.currentBourse.id}/update`,
        bourseRequest).then((result: any) => {
        this.onLoadListBourse();
        this.toUpdate = false;
        this.submitted = false;
        this.currentBourse = null;
        this.initFormBourse();
       /* localStorage.getItem('activelang') === 'en' ?
          // tslint:disable-next-line:max-line-length
        this.genericsService.confirmResponseAPI(AppConstants.BOURSE_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.BOURSE_UPDATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }*/
      });
    } else {
      const bourseRequest = new BourseRequestModel(
        null, this.f.anneeAcc.value, this.f.intitule.value,
        this.f.modApplication.value, this.f.montantBourse.value, this.f.motif.value,
        this.f.idService.value
      );
      this.genericsService.postResource(`${this.base_url}create`, bourseRequest).then((result: any) => {
        this.onLoadListBourse();
        this.initFormBourse();
        this.submitted = false;

      /*  Swal.fire({
          title: AppConstants.BOURSE_CREATEDEN,
          text: AppConstants.IRREVERSIBLEEN,
          icon: 'warning',
          timer:5000,
         // showCancelButton: true,
         // confirmButtonColor: '#3085d6',
         // cancelButtonColor: '#d33',
          confirmButtonText: AppConstants.BOURSE_CREATEDEN
        })*/
       console.log("Ajout d'une bourse")
        localStorage.getItem('activelang') === 'en' ?
          // tslint:disable-next-line:max-line-length
        this.genericsService.confirmResponseAPI(AppConstants.BOURSE_CREATEDEN, 'success',2000) : this.genericsService.confirmResponseAPI(AppConstants.BOURSE_CREATEDFR, 'success',2000);
       if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    }
  }

  get f() { return this.bourseForm.controls; }

  onReset() {
    this.submitted = false;
    this.toUpdate = false;
    this.initFormBourse();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  private onLoadListBourse() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`${this.base_url}list`).then((result: any) => {
      if (result.success === true) {
        this.bourses = result.data;
        console.log(this.bourses);
      }
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      this.genericsService.stopLoadingPage();
    });
  }

  private onLoadListAnnees() {
    this.genericsService.getResource(`annees?size=50`).then((result: any) => {
      this.annees = result._embedded.annees.filter(item=>item.codeAnnee!=this.codeAnnee);
      this.annees = result._embedded.annees;
      this.codeAnneeSource = this.annees[0].codeAnnee;
    });
  }


  onLoadListService() {
    this.genericsService.getResource(`${this.base_url_service}list`).then((result: any) => {
      if (result.success === true) {
        this.services = result.data;
      }
    });
  }

  onLoadDetailsBourse(bourse: BourseModel, content: TemplateRef<any>) {
    this.toUpdate = true;
    this.currentBourse = bourse;
    this.bourseForm.patchValue({
      intitule: bourse.intitule_bourse,
      anneeAcc: bourse.anneeAcademique,
      modApplication: bourse.modalite_application,
      montantBourse: bourse.montant_bourse,
      motif: bourse.motif_bourse,
      idService: bourse.service.id
    });
    this.modalService.open(content,
      {ariaLabelledBy: 'add-bourse', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onDeleteBourse(id: number) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
          this.onLoadListBourse();
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            this.onLoadListBourse();
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }
  ngOnDestroy(): void {
    this.onReset();
  }

  onChangeAnnee(event: any) {
    this.codeAnneeSource = event.target.value;
  }

  onClickReconductionBourses() {
    console.log(this.codeAnneeSource);

    Swal.fire({
      title: localStorage.getItem('activelang') === 'en'? AppConstants.ARE_U_SUREEN : AppConstants.ARE_U_SUREFR,
      text: localStorage.getItem('activelang') === 'en'? AppConstants.IRREVERSIBLEEN : AppConstants.IRREVERSIBLEFR,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: localStorage.getItem('activelang') === 'en'? AppConstants.RECONDUIRE_ITEN:AppConstants.RECONDUIRE_ITFR
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.getResource(`admin/bourse/reconduire/${this.codeAnneeSource}`)
            .then((result: any) => {
              console.log(result);
              this.onLoadListBourse();
              this.genericsService.confirmResponseAPI(result.message, 'success');
            }).catch((reason: any) => {
          console.log(reason);
          localStorage.getItem('activelang') === 'en'
              ? this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONEN,  'error')
              : this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONFR,  'error');
        });
      }
    })

  }
}
