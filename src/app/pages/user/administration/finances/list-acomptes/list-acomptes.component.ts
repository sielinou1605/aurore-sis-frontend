import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {HistoriqueEleveNoteResponse} from '../../../../../shared/_models/response-dto/systeme/data-module-response-model';
import { AuthService } from 'app/shared/_services/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import {acompteRequestModel} from '../../../../../shared/_models/request-dto/finance/acompte-request-model';
import {AppConstants} from '../../../../../shared/element/app.constants';
import * as moment from 'moment';
import {ActivatedRoute, Router} from '@angular/router';
import {FACTUREACCOMPTE, FACTURELISTEACCOMPTE, ParamImpressionModel} from '../../../../../shared/_models/request-dto/common/etats-model';

@Component({
  selector: 'app-list-acomptes',
  templateUrl: './list-acomptes.component.html',
  styleUrls: ['./list-acomptes.component.scss']
})
export class ListAcomptesComponent implements OnInit {

  readOnly = false;
  submitted = false;
  public idEleve: any;
  public line: any;
  public pageSize = 15;
  public page = 0;
  totalElements: any;
  public sortBy = '';
  public searchForm: FormGroup;
  predicate = '';
  listAcomptes: any;
  dataFilter = '';
  acomptForm: FormGroup;
  modifElement = false;
  detailElt = false;
  detailModif: string;
  eleveCourant: any;
  line_temp: null;
  find: any;
  newMatricule = '';
  public listeClass: any;
  idClass: any;
  listeClassDetailModif: any;
  nomIdClasse: any;
  name: any;
  constructor(
    public genericsService: GenericsService,
    public fb: FormBuilder,
    public authService: AuthService,
    public route: ActivatedRoute,
    public calendar: NgbCalendar,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.name = params['name'];
      // Faites quelque chose avec le paramètre récupéré...
      console.log(this.name);
      this.dataFilter = this.name;
    });
    this.getListeClass();
    this.searchForm = this.fb.group({
      search: [this.dataFilter === undefined ? '' : this.dataFilter ]
    });
    // this.onAfficherHistorique(this.dataFilter, this.sortBy, this.page, this.pageSize);
    this.initForm('');
    console.log(this.dataFilter);
    this.onAfficherHistorique(this.dataFilter, this.sortBy, this.page, this.pageSize);
  }

  get f() {
    return this.acomptForm.controls;
  }
  initForm(data) {
    console.log(data);
    this.acomptForm = this.fb.group({
      id: [0],
      prenomEleve: [data ? data.prenomEleve : ''],
      nomEleve: [data ? data.nomEleve : ''],
      matricule: [data ? data.matricule : ''],
      // codeAcces: [data ? data.codeAcces : ''],
      dateAdmission: [data ? data.dateAdmission.split('T')[0] : this.calendar.getToday()],
      dateNaissance: [data ? data.dateNaissance.split('T')[0] : this.calendar.getToday()],
      lieuNaissance: [data ? data.lieuNaissance : ''],
      nationalite: [data ? data.nationalite : ''],
      eleveTransferer: [data ? (data.eleveTransferer === false ? 'NON' : 'OUI') : ''],
      emailEleve: [data ? data.emailEleve : '', Validators.email],
      emailParent: [data ? data.emailParent : '', Validators.email],
      search: [data ? data.id : ''],
      handicape: [data ? data.handicape : ''],
      montantVerse: [data ? data.montantVerse : '', [Validators.required]],
      nomMere: [data ? data.nomMere : ''],
      nomParent: [data ? data.nomParent : ''],
      numeroEleve: [data ? data.numeroEleve : ''],
      photo: [data ? data.photo : ''],
      value: [data ? data.value : ''],
      religion: [data ? data.religion : ''],
      residenceEleve: [data ? data.residenceEleve : ''],
      residenceParent: [data ? data.residenceParent : ''],
      sexe: [data ? data.sexe : ''],
      statut: [data ? data.statut : ''],
      telephoneEleve: [data ? data.telephoneEleve : ''],
      telephoneMere: [data ? data.telephoneMere : ''],
      telephoneParent: [data ? data.telephoneParent : ''],
      telephonePere: [data ? data.telephonePere : '', [Validators.required, Validators.pattern('^6[0-9]{8}$')]],
      transfert: [data ? data.transfert : ''],
      idClasse: [data ? data.classe.nomClasse : '']
    });
  }
  get fS() {
    return this.searchForm.controls;
  }
  getListeClass() {
    this.genericsService.getResource(`admin/classes`).then((response: any) => {
      console.log(response);
      if (response) {
        this.listeClassDetailModif = response.data;
      }
    });
  }

  getElt(data) {
    // console.log(data.target.value);
    console.log(data.target.value);
    console.log(this.listeClassDetailModif);
    this.nomIdClasse = data.target.value;

    this.find = this.listeClassDetailModif.find(x => x.nomClasse === data.target.value);
    console.log(this.find);
    // console.log(this.find.nomNiveau);
    // this.idClass = this.find.id;
  }

  setPageChange(event: any) {
    this.page = event - 1;
    this.onAfficherHistorique(this.dataFilter, this.sortBy, this.page, this.pageSize);
  }

  onAfficherHistorique(dataFilter: string, sortBy: string, page: number, pageSize: number) {
    // admin/get-all/inscription/accompte/eleve
    const url = 'admin/get-all/inscription/accompte/eleve';
    this.predicate = this.fS.search.value;
    console.log(this.predicate);
    this.genericsService.getResource(
      `${url}?predicat=${this.predicate}&sortBy=${sortBy}&page=${page}&size=${pageSize}`)
      .then((response: any) => {
        console.log(response);
        this.listAcomptes = response.data.content;
        this.totalElements = response.data.totalElements;
        console.log(this.listAcomptes);
      });

  }
  onDetailHistorique(note: HistoriqueEleveNoteResponse) {

  }

  getDetail(item: any, value: string) {
    this.readOnly = true;
    this.line = item.matricule;
    this.newMatricule = this.line;
    console.log(item);
    this.eleveCourant = item;
    console.log(this.eleveCourant);
    this.initForm(this.eleveCourant);

    if (value === 'detail') {
      this.detailElt = true;
      this.detailModif = 'Detail';
    } else if (value === 'modifier') {
      this.modifElement = true;
      this.detailModif = 'Modification';
    }
  }

  retourn() {
    this.detailElt = false;
    this.modifElement = false;

  }
  save() {
    console.log(this.find);
    console.log(this.eleveCourant);
    this.idClass = this.eleveCourant.classe.id;
    this.modifElement = false;
    this.detailElt = false;
    this.submitted = true;
    this.readOnly = false;
    this.newMatricule = this.f.matricule.value;
    console.log(this.eleveCourant);
    console.log(this.newMatricule);
    let dto;
    console.log(this.idClass);
    const idClass = parseInt(this.idClass, 10);
    console.log(this.newMatricule);
    console.log(idClass);

    if (this.f.montantVerse.value > 100) {
      console.log(this.newMatricule);
      console.log(this.f.dateAdmission.value);
      console.log(this.eleveCourant);
      dto = new acompteRequestModel(
        this.eleveCourant ?
          (this.eleveCourant.dateAdmission ? this.eleveCourant.dateAdmission
            : this.eleveCourant.date_admission) : this.f.dateAdmission.value,
        this.eleveCourant ? (this.eleveCourant.dateNaissance ?
          this.eleveCourant.dateNaissance : this.eleveCourant.date_naissance) :
          this.f.dateNaissance.value,
        // this.eleveCourant ? this.eleveCourant.eleveTransferer : (this.f.eleveTransferer.value ? this.f.eleveTransferer.value : false),
        this.f.eleveTransferer.value === 'OUI' ? true : false,
        // this.eleveCourant ? this.eleveCourant.emailEleve : this.f.emailEleve.value,
        '',
        this.eleveCourant ? this.eleveCourant.emailParent : this.f.emailParent.value,
        this.eleveCourant ? this.eleveCourant.handicape : (this.f.handicape.value === false ? 0 : 1),
        this.eleveCourant ? this.eleveCourant.id : 0,
        this.eleveCourant ? this.idClass : 0,
        this.eleveCourant ? (this.eleveCourant.lieuNaissance ?
          this.eleveCourant.lieuNaissance : (this.eleveCourant.lieu_naissance ?
            this.eleveCourant.lieu_naissance : '')) : this.f.lieuNaissance.value,
        this.eleveCourant ?  this.eleveCourant.matricule : '',
        this.f.montantVerse.value,
        this.eleveCourant ? this.eleveCourant.nationalite : this.f.nationalite.value,
        this.eleveCourant ? this.eleveCourant.nomEleve : this.f.nomEleve.value,
        this.eleveCourant ? this.eleveCourant.nomMere : this.f.nomMere.value,
        this.eleveCourant ? this.eleveCourant.nomParent : this.f.nomParent.value,
        0,
        this.eleveCourant ? this.eleveCourant.photo : this.f.photo.value,
        this.eleveCourant ? this.eleveCourant.prenomEleve : this.f.prenomEleve.value,
        this.eleveCourant ? this.eleveCourant.religion : this.f.religion.value,
        '',
        '',
        this.eleveCourant ? this.eleveCourant.sexe : '',
        this.eleveCourant ? this.eleveCourant.statut : this.f.statut.value,
        '',
        '',
        '',
        // this.eleveCourant ? this.eleveCourant.telephonePere : this.f.telephonePere.value,
        '',
        true,
      );
      console.log(this.eleveCourant.sexe);
      console.log(dto);
      this.genericsService.postResource(`admin/add/inscription/accompte/eleve`, dto).then((res: any) => {
        console.log(res.status);
        // tslint:disable-next-line:max-line-length
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmResponseAPI(AppConstants.SERVICE_LOADEDEN, 'success')
          : this.genericsService.confirmResponseAPI(AppConstants.SERVICE_LOADEDFR, 'success');
        this.onAfficherHistorique(this.dataFilter, this.sortBy, this.page, this.pageSize);
        this.initForm('');
      }).catch(reason => {
        console.log(reason);
        this.genericsService.confirmResponseAPI(reason.message, 'error', 7000);
        // localStorage.getItem('activelang') === 'en' ?
        //   this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_ERROREN, 'error', 7000)
        //   : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_ERRORFR, 'error', 7000);
        // this.genericsService.stopLoadingPage();
      });


    } else {
      this.genericsService.confirmResponseAPI('Le montant versé doit etre suppérieure à 100 francs XAF', 'error', 7000);
      // @ts-ignore
      this.getDetail(this.eleveCourant);
    }

    this.onAfficherHistorique(this.dataFilter, this.sortBy, this.page, this.pageSize);
  }
  ventillerLAcompte(item: any) {
    // admin/ventilletion/accompte/{idAccompte}/eleves
    console.log(item);
    this.genericsService.postResource(`admin/ventilletion/accompte/${item.id}/eleves`, '').then((res: any) => {
      console.log(res);
      if (res.success) {
        this.genericsService.confirmResponseAPI(res.message, 'success');
        this.onAfficherHistorique(this.dataFilter, this.sortBy, this.page, this.pageSize);
      } else {
        this.genericsService.confirmResponseAPI(res.message, 'error');
      }
    }).catch(reason => {
      console.log(reason);
      Swal.fire('Failure', reason.error.message, 'error');
    });
  }
  onDelete(acompt: any) {

    if (this.authService.isActif('bouton-suprimer-accompte')) {
      localStorage.getItem('activelang') === 'en'
        ? Swal.fire({
          title: 'Are you sure ?',
          text: 'The removal of ' + acompt.nomEleve + ' is irreversible !',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Delete!'
        }).then((result) => {
          if (result.isConfirmed) {
            // admin/delete/accompte/inscription/eleve/{idAccompte}
            this.genericsService.deleteResource(`admin/delete/accompte/inscription/eleve/${acompt.id}`).then((response: any) => {
              console.log(response);
              this.onAfficherHistorique(this.predicate, this.sortBy, this.page, this.pageSize );
              Swal.fire('Delete!', response.message, 'success');
            }).catch(reason => {
              console.log(reason);
              Swal.fire('Failure', 'The student has not been deleted.', 'error');
            });
          }
        })
        : Swal.fire({
          title: 'Etes vous sure ?',
          text: 'La suppression de ' + acompt.nomEleve + ' est irreversible !',
          icon: 'Avertissement',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oui, Supprimer!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.genericsService.deleteResource(`admin/delete/accompte/inscription/eleve/${acompt.id}`).then((response: any) => {
              console.log(response);
              this.onAfficherHistorique(this.predicate, this.sortBy, this.page, this.pageSize );
              Swal.fire('Supprimé!', response.message, 'success');
            }).catch(reason => {
              console.log(reason);
              Swal.fire('Echec!', 'L\'acompte n\'a pas été supprimé.', 'error');
            });
          }
        });
    }
  }

  onImprimerListe() {
    // console.log(item);
    // const idAccompte = item.id;
    let dto: any;
    dto = new ParamImpressionModel(
      false,
      FACTURELISTEACCOMPTE,
      []
    );
    // console.log(accompte);
    // console.log(this.listeEleveClass);
    // console.log(idAccompteEnregistre);
    // const classeAccompte = accompte.classe;
    console.log(dto);
    this.genericsService.reportPostPostResource(
      `parametre/etat/imprimer`, dto
    ).then((result: any) => {
      console.log(result);
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Reçu_' + ' ');
    }).catch(reason => {
      console.error(reason);
      localStorage.getItem('activelang') === 'en'
        ? Swal.fire('Echec!', 'Receipt unavailable!!!', 'error')
        : Swal.fire('Echec!', 'Recu insdiponible!!!', 'error');
    });
  }

  onImprimerRecuListe(item: any) {
    console.log(item);
    const idAccompte = item.id;
    let dto: any;
    dto = new ParamImpressionModel(
      false,
      FACTUREACCOMPTE,
      [
        {
          texte: 'ID',
          valeur: idAccompte
        }
      ]
    );
    // console.log(accompte);
    // console.log(this.listeEleveClass);
    // console.log(idAccompteEnregistre);
    // const classeAccompte = accompte.classe;
    console.log(dto);
    this.genericsService.reportPostPostResource(
      `parametre/etat/imprimer`, dto
    ).then((result: any) => {
      console.log(result);
      this.genericsService.getByteArrayAndSaveReportPDF(result, 'Reçu_' + item.matricule + ' ');
    }).catch(reason => {
      console.error(reason);
      localStorage.getItem('activelang') === 'en'
        ? Swal.fire('Echec!', 'Receipt unavailable!!!', 'error')
        : Swal.fire('Echec!', 'Recu insdiponible!!!', 'error');
    });
  }



}
