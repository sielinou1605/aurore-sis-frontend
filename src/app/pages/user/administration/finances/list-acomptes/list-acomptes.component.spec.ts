import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAcomptesComponent } from './list-acomptes.component';

describe('ListAcomptesComponent', () => {
  let component: ListAcomptesComponent;
  let fixture: ComponentFixture<ListAcomptesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAcomptesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAcomptesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
