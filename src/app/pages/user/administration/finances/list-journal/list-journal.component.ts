import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {JournalModel} from '../../../../../shared/_models/response-dto/finance/journal-model';
import {JournalRequestModel} from '../../../../../shared/_models/request-dto/finance/journal-request-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-journal',
  templateUrl: './list-journal.component.html',
  styleUrls: ['./list-journal.component.scss']
})
export class ListJournalComponent implements OnInit, OnDestroy {
  journalForm: FormGroup;
  fSearchGroup: FormGroup;
  public submitted: boolean;
  public journaux: JournalModel[];
  private base_url = 'admin/journal/';
  private toUpdate = false;
  private currentJournal: JournalModel;
  public pageSize = 20;
  public page = 0;
  totalElements: any;
  public sortBy = '';

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  public eltSearch = '';
   journauxFinance: any;

  constructor(
    private translate: TranslateService,
    private modalService: NgbModal, private genericsService: GenericsService,
    config: NgbModalConfig, private fb: FormBuilder, public authService: AuthService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(21);
  }

  ngOnInit() {
    this.fSearchGroup = this.fb.group({
      search: ['']
    });
    this.initJournalForm();
    // this.onLoadListJournaux();
    this.getListeClassOnTable();
  }

  open(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'add-journal', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  private initJournalForm() {
    this.journalForm = this.fb.group({
      code: ['', Validators.required],
      type: ['',  Validators.required],
      designation: ['', Validators.required],
    });
  }

  onSaveJournal() {
    this.submitted = true;
    if (this.journalForm.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const journalRequest = new JournalRequestModel(this.f.code.value,
        this.f.designation.value, this.currentJournal.id, this.f.type.value);
      this.genericsService.putResource(`${this.base_url + this.currentJournal.id}/update`,
        journalRequest).then((result: any) => {
        this.onLoadListJournaux();
        this.toUpdate = false;
        this.submitted = false;
        this.currentJournal = null;
        this.initJournalForm();
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.JOURNAL_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.JOURNAL_UPDATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    } else {
      const journalRequest = new JournalRequestModel(this.f.code.value,
        this.f.designation.value, null, this.f.type.value);
      this.genericsService.postResource(`${this.base_url}create`, journalRequest).then((result: any) => {
        this.onLoadListJournaux();
        this.initJournalForm();
        this.submitted = false;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.JOURNAL_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.JOURNAL_CREATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    }
  }

  get f() { return this.journalForm.controls; }

  onReset() {
    this.submitted = false;
    this.toUpdate = false;
    this.initJournalForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onLoadDetailsJournal(journal: JournalModel, content: TemplateRef<any>) {
    this.toUpdate = true;
    this.currentJournal = journal;
    this.journalForm.patchValue({
      code: journal.code_jrnl,
      designation: journal.designation_jrnl,
      type: journal.type_jrnl,
    });
    this.modalService.open(content,
      {ariaLabelledBy: 'add-service', size: 'lg', scrollable: true}).result.then((resultModal) => {},
      (reason) => {});
  }

  onDeleteJournal(id: any) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
          this.onLoadListJournaux();
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            this.onLoadListJournaux();
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }

  private onLoadListJournaux() {
    this.genericsService.getResource(`${this.base_url}list`).then((result: any) => {
      if (result.success === true) {
        this.journaux = result.data;
        console.log(this.journaux);
      }
    });
  }

  get fSearch() {
    return this.fSearchGroup.controls;
  }
  getListeClassOnTable() {
    this.eltSearch = this.fSearch.search.value;
    console.log(this.eltSearch);
    this.genericsService.getResource(`admin/get-all/historique/finance/eleves?predicat=${this.eltSearch}&page${this.page}&size=${this.pageSize}`).then((res: any) => {
      this.journauxFinance = res.data.content;
      console.log(this.journauxFinance);
    });
    // console.log(this.listeClass[this.f.search.value]);
    // this.nomNiveau = this.listeClass[this.f.search.value];
  }


  ngOnDestroy(): void {
    this.onReset();
  }
}
