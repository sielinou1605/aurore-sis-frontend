import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModeReglementModel} from '../../../../../shared/_models/response-dto/finance/mode-reglement-model';
import {ModeReglementRequestModel} from '../../../../../shared/_models/request-dto/finance/mode-reglement-request-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-mode-reglement',
  templateUrl: './list-mode-reglement.component.html',
  styleUrls: ['./list-mode-reglement.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListModeReglementComponent implements OnInit, OnDestroy {
  modeReglementForm: FormGroup;
  public submitted: boolean;
  public modeReglements: ModeReglementModel[];
  private base_url = 'admin/mode-reglement/';
  private toUpdate = false;
  private currentModeReglement: ModeReglementModel;
  public errorResponse: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private translate: TranslateService,
    private modalService: NgbModal,
    config: NgbModalConfig, private genericsService: GenericsService,
    private fb: FormBuilder, public authService: AuthService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(20);
  }

  ngOnInit() {
    console.log('liste mode de reglement');
    this.initFormModeReglement();
    this.onLoadListModeReglement();
  }

  open(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'add-mode-reglement', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  private initFormModeReglement() {
    this.modeReglementForm = this.fb.group({
      libelle: ['', Validators.required],
      description: [''],
    });
  }

  onSaveModeReglement() {
    this.submitted = true;
    if (this.modeReglementForm.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const modeReglementRequest = new ModeReglementRequestModel(
        this.currentModeReglement.id, this.f.libelle.value, this.f.description.value
      );
      this.genericsService.putResource(`${this.base_url + this.currentModeReglement.id}/update`,
        modeReglementRequest).then((result: any) => {
        this.onLoadListModeReglement();
        this.toUpdate = false;
        this.submitted = false;
        this.initFormModeReglement();
        this.currentModeReglement = null;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.MODE_REGLEMENT_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.MODE_REGLEMENT_CREATEDFR, 'success');
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      });
    } else {
      const modeReglementRequest = new ModeReglementRequestModel(null, this.f.libelle.value, this.f.description.value);
      this.genericsService.postResource(`${this.base_url}create`, modeReglementRequest).then((result: any) => {
        this.onLoadListModeReglement();
        this.initFormModeReglement();
        this.submitted = false;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.MODE_REGLEMENT_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.MODE_REGLEMENT_CREATEDFR, 'success');
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }, err => {
        console.log(err);
        localStorage.getItem('activelang') === 'en' ?
        this.errorResponse = AppConstants.ANOTHER_CAISSE_EXISTEN + this.f.libelle.value : this.errorResponse = AppConstants.ANOTHER_CAISSE_EXISTFR + this.f.libelle.value;
      });
    }
  }

  get f() { return this.modeReglementForm.controls; }

  onReset() {
    this.submitted = false;
    this.toUpdate = false;
    this.initFormModeReglement();
    this.modalService.dismissAll(ModalDismissReasons.ESC);
  }

  onLoadDetailsModeReglement(modeReglement: ModeReglementModel, content: TemplateRef<any>) {
    this.toUpdate = true;
    this.currentModeReglement = modeReglement;
    this.modeReglementForm.patchValue({
      libelle: modeReglement.libelle,
      description: modeReglement.description
    });
    this.modalService.open(content,
      {ariaLabelledBy: 'add-service', size: 'lg', scrollable: true}).result.then((resultModal) => {},
      (reason) => {});
  }

  onDeleteModeReglement(id: any) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
          this.onLoadListModeReglement();
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            this.onLoadListModeReglement();
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }
  private onLoadListModeReglement() {
    this.genericsService.getResource(`${this.base_url}list`).then((result: any) => {
      if (result.success === true) {
        this.modeReglements = result.data;
      }
    });
  }

  ngOnDestroy(): void {
    this.onReset();
  }
}
