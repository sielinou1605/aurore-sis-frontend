import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServiceModel} from '../../../../../shared/_models/response-dto/finance/service-model';
import {RemiseNiveauModel} from '../../../../../shared/_models/response-dto/finance/remise-niveau-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {RemiseModel} from '../../../../../shared/_models/response-dto/finance/remise-model';
import {NiveauModel} from '../../../../../shared/_models/response-dto/common/niveau-model';
import {RemiseNiveauRequestModel} from '../../../../../shared/_models/request-dto/finance/remise-niveau-request-model';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-remise-niveau',
  templateUrl: './list-remise-niveau.component.html',
  styleUrls: ['./list-remise-niveau.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListRemiseNiveauComponent implements OnInit, OnDestroy {
  remiseNiveauForm: FormGroup;
  public submitted: boolean;
  public remiseNiveaux: RemiseNiveauModel[];
  private base_url = 'admin/remise-niveau/';
  private base_url_service = 'admin/service/';
  private toUpdate: boolean;
  private currentRemiseNiveau: RemiseNiveauModel;
  public services: ServiceModel[];
  public remises: RemiseModel[];
  public sources: any;
  public niveaux: NiveauModel[];
  public annees: any;
  public totalElements = 0;
  public pageSize = 20;
  public page: number = 0;
  public searchForm: FormGroup;
  public predicat = '';

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private translate: TranslateService,
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, private genericsService: GenericsService,
    public authService: AuthService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(24);
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.initFormRemiseNiveau();
    this.onLoadListService();
    this.onLoadListRemise();
    this.onLoadListNiveau();
    this.onLoadListRemiseNiveau();
    this.onLoadListAnnees();
  }

  private initFormRemiseNiveau() {
    this.remiseNiveauForm = this.fb.group({
      nomRemise: ['', Validators.required],
      montant: ['', Validators.required],
      type: ['', Validators.required],
      modApp: [''],
      source: ['2', Validators.required],
      dateEch: ['', Validators.required],
      annee: [''],
      remise: ['', Validators.required],
      service: [''],
      niveau: ['', Validators.required],
      matricule: [''],
      globale: [true],
    });
  }
  get f() { return this.remiseNiveauForm.controls; }

  get fSearch() {
    return this.searchForm.controls;
  }
  private onLoadListService() {
    this.genericsService.getResource(`${this.base_url_service}list`).then((result: any) => {
      if (result.success === true) {
        this.services = result.data;
      }
    });
  }

  getListeClassOnTable(predicate: string, pageSize: number, page: number) {
    predicate = this.fSearch.search.value;
    // tslint:disable-next-line:max-line-length
    this.genericsService.getResource(`admin/remise-niveau/liste?predicat=${predicate}&size=${pageSize}&page=${page}`).then((result: any) => {
      if (result.success === true) {
        this.remiseNiveaux = result.data.content;
        console.log(this.remiseNiveaux);
        this.totalElements = result.data.totalElements;
        console.log(result.data);
      }
    });
  }

  private onLoadListRemise() {
    this.genericsService.getResource(`modeleRemiseGlobales?size=100`).then((result: any) => {
      this.remises = result._embedded.modeleRemiseGlobales;
    });
  }
  private onLoadListNiveau() {
    this.genericsService.getResource(`niveaus?size=100`).then((result: any) => {
      this.niveaux = result._embedded.niveaus;
    });
  }

  private onLoadListRemiseNiveau() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/remise-niveau/liste?size=${this.pageSize}&page=${this.page}`).then((result: any) => {
      if (result.success === true) {
        this.remiseNiveaux = result.data.content;
        this.totalElements = result.data.totalElements;
      }
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') :
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }
  setPageChange(event) {
    this.page = event - 1;
    this.onLoadListRemiseNiveau();
  }
  private onLoadListAnnees() {
    this.genericsService.getResource(`annees?size=50`).then((result: any) => {
      this.annees = result._embedded.annees;
    });
  }

  open(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'add-remise-niveau', size: 'lg', scrollable: true}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onChangeDate() {
    console.log(this.f.dateEch.value);
    // if (this.f.dateEch.errors) {
    //   this.remiseNiveauForm.patchValue({
    //     dateEch: this.convertDate.parseError(this.f.dateEch.value)
    //   });
    // }
  }
  onSaveRemiseNiveau() {
    this.submitted = true;
    if (this.remiseNiveauForm.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      console.log('update');
      const remiseNiveauRequest = new RemiseNiveauRequestModel(
        this.f.annee.value, this.f.dateEch.value,
        this.currentRemiseNiveau.id, this.f.niveau.value, this.f.remise.value,
        this.f.service.value, null, this.f.matricule.value,  this.f.modApp.value,
        this.f.montant.value, this.f.nomRemise.value, this.f.source.value, this.f.type.value
      );
      this.genericsService.putResource(`${this.base_url + this.currentRemiseNiveau.id}/update`, remiseNiveauRequest).then((result: any) => {
        this.onLoadListRemiseNiveau();
        this.currentRemiseNiveau = null;
        this.toUpdate = false;
        this.submitted = false;
        this.initFormRemiseNiveau();
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.REMISE_NIVEAU_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.REMISE_NIVEAU_UPDATEDFR, 'success');
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      });
    } else {
      console.log('create');
      const remiseNiveauRequest = new RemiseNiveauRequestModel(
        this.f.annee.value, `${this.f.dateEch.value}`,
        null, this.f.niveau.value, this.f.remise.value,
        this.f.service.value, null, this.f.matricule.value,  this.f.modApp.value,
        this.f.montant.value, this.f.nomRemise.value, this.f.source.value, this.f.type.value
      );
      this.genericsService.postResource(`${this.base_url}create`, remiseNiveauRequest).then((result: any) => {
        this.onLoadListRemiseNiveau();
        this.initFormRemiseNiveau();
        this.submitted = false;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.REMISE_NIVEAU_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.REMISE_NIVEAU_CREATEDFR, 'success');
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      });
    }
  }

  onLoadDetailsRemiseNiveau(remise: RemiseNiveauModel, content: TemplateRef<any>) {
    this.currentRemiseNiveau = remise;
    console.log(remise);
    this.toUpdate = true;
    this.remiseNiveauForm.patchValue({
      nomRemise: remise.nom_rn,
      montant: remise.montant_rn,
      type: remise.type_rn,
      modApp: remise.modaliteApplication,
      source: remise.sourceRemise,
      dateEch: remise.echeanceApplication,
      annee: remise.anneeAcademique,
      matricule: remise.matricule,
      niveau: remise.niveau.id,
      service:  remise.service ? remise.service.id : '',
      remise: remise.remise_globale ? remise.remise_globale.id : ''
    });
    this.modalService.open(content,
      {ariaLabelledBy: 'add-remise-niveau', size: 'lg', scrollable: true}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onDeleteRemiseNiveau(id: number) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
          this.onLoadListRemiseNiveau();
          this.toUpdate = false;
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${id}/delete`).then((resultDelete: any) => {
            this.onLoadListRemiseNiveau();
            this.toUpdate = false;
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }

  onReset() {
    this.submitted = false;
    this.initFormRemiseNiveau();
    this.toUpdate = false;
    this.modalService.dismissAll(ModalDismissReasons.ESC);
  }

  onChangeOptions() {
    // tslint:disable-next-line:triple-equals
    if (this.f.source.value == 0 || this.f.source.value == 1) {
      this.remiseNiveauForm.controls['matricule'].setValidators([Validators.required, Validators.min(1)]);
    } else {
      this.remiseNiveauForm.controls['matricule'].clearValidators();
    }
    this.remiseNiveauForm.get('matricule').updateValueAndValidity();
  }

  ngOnDestroy(): void {
    this.onReset();
  }
}
