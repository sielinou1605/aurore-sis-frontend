import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListRemiseNiveauComponent} from './list-remise-niveau.component';

describe('ListRemiseNiveauComponent', () => {
  let component: ListRemiseNiveauComponent;
  let fixture: ComponentFixture<ListRemiseNiveauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRemiseNiveauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRemiseNiveauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
