import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FinancesComponent} from './finances.component';
import {MenuConfigComponent} from './menu-config/menu-config.component';
import {ListServicesComponent} from './list-services/list-services.component';
import {ListRemiseComponent} from './list-remise/list-remise.component';
import {ListRemiseNiveauComponent} from './list-remise-niveau/list-remise-niveau.component';
import {ListBourseComponent} from './list-bourse/list-bourse.component';
import {ListCaisseComponent} from './list-caisse/list-caisse.component';
import {ListModeReglementComponent} from './list-mode-reglement/list-mode-reglement.component';
import {ListModeleEcheanceComponent} from './list-modele-echeance/list-modele-echeance.component';
import {ListJournalComponent} from './list-journal/list-journal.component';
import {TransfertConfigurationComponent} from './transfert-configuration/transfert-configuration.component';
import {AcompteComponent} from './acompte/acompte.component';
import {GestionDesAcomptesComponent} from './gestion-des-acomptes/gestion-des-acomptes.component';
import {ListAcomptesComponent} from './list-acomptes/list-acomptes.component';


const routes: Routes = [
  {
    path: '', component: FinancesComponent,
    data: {
      breadcrumb: 'userAdminFinancesTransfert.Finances',
      // titre: 'userAdminFinancesTransfert.Finances_Aurore',
      titre: 'Finances | Aurore',
      icon: 'icofont-ui-settings bg-c-blue',
      breadcrumb_caption: 'userAdminFinancesTransfert.Configuration_des_modeles_pour_la_gestion_des_inscriptions_Finances',
      status: true,
      current_role: 'userAdminFinancesTransfert.admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'menu-config',
        pathMatch: 'full'
      },
      {
        path: 'menu-config', component: MenuConfigComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Finances',
          // titre: 'userAdminFinancesTransfert.Finances_Aurore',
           titre: 'Finances | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Menu_des_configurations_de_la_finance_Finances',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'list-services', component: ListServicesComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Services',
          // titre: 'userAdminFinancesTransfert.Services_Aurore',
          titre: 'Services | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Configurer_les_services_payables_par_les_eleves_Services',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'list-remise', component: ListRemiseComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Remises',
          // titre: 'userAdminFinancesTransfert.Remises_Aurore',
          titre: 'Remises',
            icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Configurer_les_remises_pour_les_frais_d_inscriptions_de_l_eleve_Remise',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'list-remise-niveau', component: ListRemiseNiveauComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Remise_niveau',
          // titre: 'userAdminFinancesTransfert.Remise_niveau_Aurore',
          titre: 'Remise niveau | Aurore',
            icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Configuration_et_liaison_des_remises_a_un_niveau_Remise_Niveau',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'list-bourse', component: ListBourseComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Bourses',
         // titre: 'userAdminFinancesTransfert.Bourses_Aurore',
          titre: 'Bourses | Aurore',
            icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Ajout_suppression_modification_et_listing_des_bourses_Bourses',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'list-caisse', component: ListCaisseComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Caisse',
         // titre: 'userAdminFinancesTransfert.Caisse_Aurore',
          titre: 'Caisse | Aurore',
            icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Gestion_des_caisses_Caisses',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'list-mode-reglement', component: ListModeReglementComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Mode_reglements',
          // titre: 'userAdminFinancesTransfert.Mode_reglements_Aurore',
          titre: 'Mode reglements | Aurore',
            icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Gestion_des_mode_de_reglements_Mode_de_reglement',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'list-modele-echeance', component: ListModeleEcheanceComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Modele_echéance',
          // titre: 'userAdminFinancesTransfert.Modele_echéance_Aurore',
          titre: 'Modele echéance | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Gestion_des_modeles_d_échéance_Modele_écheance',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'transfert', component: TransfertConfigurationComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Transferer_un_service',
          // titre: 'userAdminFinancesTransfert.Transferer_un_service_Aurore',
          titre: 'Transferer un service | Aurore',
          icon: 'icofont-ui-copy bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Transfert_un_service_avec_echeancier_d_un_niveau_a_l_autre_Transfert',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'list-journal', component: ListJournalComponent,
        data: {
          breadcrumb: 'userAdminFinancesTransfert.Journaux',
         // titre: 'userAdminFinancesTransfert.Journaux_Aurore',
          titre: 'Journaux | Aurore',
            icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesTransfert.Gerer_les_journaux_Journaux',
          status: true,
          current_role: 'userAdminFinancesTransfert.admin'
        },
      },
      {
        path: 'acompte', component: AcompteComponent,
        data: {
          breadcrumb: 'Acompte',
          titre: localStorage.getItem('activelang') === 'en' ? 'Acompte | Aurore' : 'Acompte | Aurore',
          icon: 'icofont-bank bg-c-green',
          breadcrumb_caption: 'Acompte',
          status: true,
          current_role: 'finances'
        }
      },
      {
        path: 'gestion-des-acomptes', component: GestionDesAcomptesComponent,
        data: {
          breadcrumb: 'Gestion des acomptes',
          titre: 'Gestion des acomptes | Aurore',
          icon: 'icofont-bank bg-c-green',
          breadcrumb_caption: 'Gestion des acomptes',
          status: true,
          current_role: 'finances'
        }
      },
      {
        path: 'list-acomptes/:name', component: ListAcomptesComponent,
        data: {
          breadcrumb: 'Acomptes',
          titre: 'Finance Acomptes | Aurore',
          icon: 'icofont-bank bg-c-green',
          breadcrumb_caption: 'Finance Acomptes',
          status: true,
          current_role: 'finances'
        }
      },
      {
        path: 'list-acomptes', component: ListAcomptesComponent,
        data: {
          breadcrumb: 'Acomptes',
          titre: 'Finance Acomptes | Aurore',
          icon: 'icofont-bank bg-c-green',
          breadcrumb_caption: 'Finance Acomptes',
          status: true,
          current_role: 'finances'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancesRoutingModule { }
