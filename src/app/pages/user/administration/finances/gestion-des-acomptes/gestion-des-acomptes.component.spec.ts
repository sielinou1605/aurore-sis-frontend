import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionDesAcomptesComponent } from './gestion-des-acomptes.component';

describe('GestionDesAcomptesComponent', () => {
  let component: GestionDesAcomptesComponent;
  let fixture: ComponentFixture<GestionDesAcomptesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionDesAcomptesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionDesAcomptesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
