import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-gestion-des-acomptes',
  templateUrl: './gestion-des-acomptes.component.html',
  styleUrls: ['./gestion-des-acomptes.component.scss']
})
export class GestionDesAcomptesComponent implements OnInit {

  constructor(
    public authService: AuthService,
    public router: Router,
  ) { }

  ngOnInit() {
  }
  onloadPage(url: string, id: string) {
    console.log(url);
    console.log(id);
    if (this.authService.isActif(id)) {
      this.router.navigate([url]).then(() => {});
    }
  }
}
