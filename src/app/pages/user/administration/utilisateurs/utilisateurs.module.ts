import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UtilisateursRoutingModule} from './utilisateurs-routing.module';
import {UtilisateursComponent} from './utilisateurs.component';
import {SharedModule} from '../../../../shared/shared.module';
import {ListUtilisateurComponent} from './list-utilisateur/list-utilisateur.component';
import {EditerUtilisateurComponent} from './editer-utilisateur/editer-utilisateur.component';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AvatarModule} from 'ngx-avatar';
import {NgxPaginationModule} from 'ngx-pagination';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [UtilisateursComponent, ListUtilisateurComponent, EditerUtilisateurComponent],
    imports: [
        CommonModule,
        UtilisateursRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        /** /https://www.npmjs.com/package/ngx-avatar **/
        AvatarModule,
        NgMultiSelectDropDownModule.forRoot(),
        NgxPaginationModule,
        TranslateModule
    ]
})
export class UtilisateursModule { }
