/* tslint:disable:triple-equals */
import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MustMatch} from '../../../../../shared/_helpers/mustmatch-validator';
import {AddUserRequest} from '../../../../../shared/_models/auth/add-user-request';
import {ActivatedRoute, Router} from '@angular/router';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-editer-utilisateur',
  templateUrl: './editer-utilisateur.component.html',
  styleUrls: ['./editer-utilisateur.component.scss']
})
export class EditerUtilisateurComponent implements OnInit {
  UtilisateurForm: FormGroup;
  public roles: any;
  submitted: boolean;
  @Input() public currentUser: any;
  @Output() edit = new EventEmitter();
  public idUser: number;
  public caisses: any;
  private url_caisse = 'admin/caisse/';
  // @ts-ignore
  @ViewChild('selectRole') selectRole;
  // @ts-ignore
  @ViewChild('selectCaisse') selectCaisse;
  public loadContent = false;
  public name = 'Cricketers';
  public dataRole = [];
  public dataCaisse = [];
  public settings = {};
  public selectedRoles = [];
  public selectedCaisse = [];

  public placeholder: string;
  public Choisir_les_roles: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private genericsService: GenericsService,
              private fb: FormBuilder, private router: Router,
              private route: ActivatedRoute) {

    localStorage.getItem('activelang') === 'en'
    ? this.placeholder = " Choose the boxes "
      : this.placeholder = " Choisir les caisses ";

    localStorage.getItem('activelang') === 'en' ?
      this.Choisir_les_roles = ' Choose the roles '
      : this.Choisir_les_roles = ' Choisir les roles ';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

  }

  ngOnInit() {
    localStorage.getItem('activelang') === 'en' ?
    this.settings = {
      singleSelection: false,
      idField: 'id',
      textField: AppConstants.DESCRIPTIONEN,
      enableCheckAll: true,
      selectAllText: AppConstants.SELECT_ALLEN,
      unSelectAllText: AppConstants.UNSELECT_ALLEN,
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 10,
      searchPlaceholderText: AppConstants.SEARCHEN,
      noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEEN,
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    } : this.settings = {
        singleSelection: false,
        idField: 'id',
        textField: AppConstants.DESCRIPTIONFR,
        enableCheckAll: true,
        selectAllText: AppConstants.SELECT_ALLFR,
        unSelectAllText: AppConstants.UNSELECT_ALLFR,
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: AppConstants.SEARCHFR,
        noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEFR,
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };
    this.route.paramMap.subscribe(params => {
      this.idUser = params.get('id') != null ? parseInt(params.get('id'), 0) : null;
      console.log(this.idUser);
      if (this.idUser) {
        this.onLoadUser(this.idUser);
      }
    });
    if (this.currentUser != null) {
      this.loadUser(this.currentUser);
    }
    this.onLoadListRoles();
    this.onLoadListCaisses();
    this.initFormUser();
  }

  private onLoadListRoles() {
    this.genericsService.getResource(`habilitation/role/liste`).then((result: any) => {
      this.roles = result.data;
      this.roles.forEach(role => {
        this.dataRole.push({ id: role.roleId, description: role.description });
      });
    }, err => {
      console.log(err);
    });
  }

  private onLoadListCaisses() {
    this.genericsService.getResource(`${this.url_caisse}list`).then((result: any) => {
      if (result.success === true) {
        this.caisses = result.data;
        this.caisses.forEach(caisse => {
          this.dataCaisse.push({ id: caisse.id, description: caisse.nomCaisse });
        });
      }
    });
  }

  private initFormUser() {
    this.UtilisateurForm = this.fb.group({
      username: ['', Validators.required],
      displayName: ['', Validators.required],
      telephone: ['', [Validators.required, Validators.minLength(9)]],
      roles: [this.selectedRoles, Validators.required],
      caisses: [this.selectedCaisse, Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmation: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmation')
    }
      );
  }

  private loadUser(currentUser: any) {
    this.UtilisateurForm = this.fb.group({
      username: [currentUser.username, Validators.required],
      displayName: [currentUser.displayName, Validators.required],
      telephone: [currentUser.telephone, [Validators.required, Validators.minLength(9)]],
      roles: [this.selectedRoles, Validators.required],
      caisses: [this.selectedCaisse, Validators.required],
      email: [currentUser.email, [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmation: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmation')
    });
    console.log(this.UtilisateurForm);
    this.genericsService.getResource(`utilisateurs/${this.currentUser.id}/caisse`).then((caisses: any) => {
      console.log(caisses);
      this.UtilisateurForm.patchValue({
        caisses: [],
      });
    }, err => {
      console.log(err);
    });
  }

  private onLoadUser(idUser: number) {
    this.genericsService.getResource(`utilisateurs/${idUser}`).then((result: any) => {
        this.UtilisateurForm.patchValue({
          username: result.username,
          displayName: result.displayName,
          email: result.email
        });
        this.genericsService.getResource(`utilisateurs/${result.id}/caisse`).then((caisses: any) => {
          console.log(caisses);
          this.UtilisateurForm.patchValue({
            caisses: [],
          });
        }, err => {
          console.log(err);
        });
        this.UtilisateurForm.controls['roles'].clearValidators();
        this.UtilisateurForm.controls['password'].clearValidators();
        this.UtilisateurForm.controls['confirmation'].clearValidators();
        this.UtilisateurForm.get('roles').updateValueAndValidity();
        this.UtilisateurForm.get('password').updateValueAndValidity();
        this.UtilisateurForm.get('confirmation').updateValueAndValidity();
      },
      err => {
        console.log(err);
        // tslint:disable-next-line:max-line-length
        localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_ERROREN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_ERRORFR, 'error');
      });
  }

  get f() { return this.UtilisateurForm.controls; }

  onSaveUtilisateur() {
    this.submitted = true;
    if (this.UtilisateurForm.invalid) {
      return;
    }
    console.log(this.f);
    if (this.currentUser) {
      // const updateUserRequest = new UpdateUserRequestModel(
      //   this.f.displayName.value, this.f.password.value, this.idUser, this.f.caisses.value
      // );
      // this.genericsService.postResource(`auth/reset/password`, updateUserRequest).then((result: any) => {
      //   this.router.navigate(['/administration/utilisateurs/list-utilisateur']).then(() => {});
      // }, err => {
      //   console.log(err);
      // });
    } else {
      const userRequest = new AddUserRequest( this.f.displayName.value, this.f.email.value,
        this.f.password.value,  this.f.username.value, this.f.username.value, this.f.roles.value, this.f.caisses.value);
      console.log(userRequest);
      this.hideForm();
      // this.genericsService.postResource(`user/utilisateur/ajouter`, userRequest).then((result: any) => {
      //   this.router.navigate(['/administration/utilisateurs/list-utilisateur']).then(() => {});
      // }, err => {
      //   console.log(err);
      // });
    }
  }

  onResetUtilisateur() {
    this.initFormUser();
    this.selectRole.toggleSelectAll();
    this.hideForm();
  }

  onChangeOptions() {

    // tslint:disable-next-line:max-line-length
    if (this.f.roles.value.find(localStorage.getItem('avtivelang') === 'en' ? elt => elt.description == AppConstants.CAISSIEREN : elt => elt.description == AppConstants.CAISSIERFR ) ||
      // tslint:disable-next-line:max-line-length
      this.f.roles.value.find(localStorage.getItem('avtivelang') === 'en' ? elt => elt.description == AppConstants.ADMINISTRATEUREN : elt => elt.description == AppConstants.ADMINISTRATEURFR )) {
      this.UtilisateurForm.controls['caisses'].setValidators([Validators.required]);
    } else {
      this.UtilisateurForm.controls['caisses'].clearValidators();
    }
    this.UtilisateurForm.get('caisses').updateValueAndValidity();
  }

  displayForm() {
    this.edit.emit(true);
  }

  hideForm() {
    this.edit.emit(false);
  }

  public onFilterRoleChange(item: any) { console.log(item); }
  public onDropDownRoleClose(item: any) { console.log(item); }
  public onItemRoleSelect(item: any) { console.log(item); }
  public onDeRoleSelect(item: any) { console.log(item); }
  public onRoleSelectAll(items: any) { console.log(items); }
  public onRoleDeSelectAll(items: any) { console.log(items); }
  public onCaisseFilterChange(item: any) { console.log(item); }
  public onCaisseDropDownClose(item: any) { console.log(item); }
  public onCaisseItemSelect(item: any) { console.log(item); }
  public onCaisseDeSelect(item: any) { console.log(item); }
  public onCaisseSelectAll(items: any) { console.log(items); }
  public onCaisseDeSelectAll(items: any) { console.log(items); }
}
