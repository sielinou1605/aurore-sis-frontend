/* tslint:disable:triple-equals */
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { GenericsService } from '../../../../../shared/_services/generics.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../../../../../shared/_helpers/mustmatch-validator';
import { AddUserRequest, Caisse, RoleModel, UpdateUserRequestModel } from '../../../../../shared/_models/auth/add-user-request';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AjouterCaisseToUser, AjouterRoleToUser } from '../../../../../shared/_models/auth/request-role-management-model';
import { AuthService } from '../../../../../shared/_services/auth.service';
import { AppConstants } from '../../../../../shared/element/app.constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list-utilisateur',
  templateUrl: './list-utilisateur.component.html',
  styleUrls: ['./list-utilisateur.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListUtilisateurComponent implements OnInit, OnDestroy {
  public utilisateurs: any;
  public edit: boolean;
  currentUser = null;

  UtilisateurForm: FormGroup;
  searchForm: FormGroup;
  public roles: any;
  submitted: boolean;
  public idUser: number;
  public caisses: any;
  //public error_add_user: any;
  private url_caisse = 'admin/caisse/';
  // @ts-ignore
  @ViewChild('selectRole') selectRole;
  // @ts-ignore
  @ViewChild('selectCaisse') selectCaisse;
  public loadContent = false;
  public name = 'Cricketers';
  public dataRole = [];
  public dataCaisse = [];
  public settings = {};
  public selectedRoles = [];
  public selectedCaisse = [];
  public isNotifieAdd = false;
  public isNotifieEdit = false;
  public totalElements = 0;
  public pageSize = 20;
  public page = 0;

  public choix: string;
  public bloqueUser: string;
  public debloqueUser: string;
  public choix_2: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  private predicat = '';
  constructor(public genericsService: GenericsService, private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    public authService: AuthService,
    private translate: TranslateService) {

    this.bloqueUser = localStorage.getItem('activelang') === 'en' ? 'Lock this user' : 'Bloquer cet utilisateur';
    this.debloqueUser = localStorage.getItem('activelang') === 'en' ? 'Unlock this user' : 'Débloquer cet utilisateur';
    this.choix = localStorage.getItem('activelang') === 'en' ? 'Choose the roles' : 'Choisir les roles';
    this.choix_2 = localStorage.getItem('activelang') === 'en' ? 'Choose the boxes' : 'Choisir les caisses';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(5);
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.onLoadListUtilisateur();
    localStorage.getItem('activelang') === 'en' ?
    this.settings = {
      singleSelection: false,
      idField: 'id',
      textField: 'description',
      enableCheckAll: false,
      selectAllText: 'Tout cocher',
      unSelectAllText: 'Tout decocher',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 10,
      searchPlaceholderText: AppConstants.SEARCHEN,
      noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEEN,
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    } :  this.settings = {
        singleSelection: false,
        idField: 'id',
        textField: 'description',
        enableCheckAll: false,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: AppConstants.SEARCHFR,
        noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEFR,
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };
    this.onLoadListRoles();
    this.onLoadListCaisses();
    this.initFormUser();
  }

  get fSearch() {
    return this.searchForm.controls;
  }
  private onLoadListRoles() {
    this.genericsService.getResource(`habilitation/role/liste`).then((result: any) => {
      this.roles = result.data;
      this.roles.forEach(role => {
        this.dataRole.push({ id: role.roleId, description: role.description });
      });
    }, err => {
      console.log(err);
    });
  }

  listUpload() {
    this.predicat = this.fSearch.search.value;
    this.genericsService
      .getResource(`admin/management/users?predicat=${this.predicat}&size=${this.pageSize}&page=${this.page}`)
      .then((result: any) => {
        console.log('____________REsult List filter', result);
        this.utilisateurs = result.data.content.filter(item=>item.id!=1 && item.id!=2);
        this.totalElements = result.data.totalElements;
      });
  }
  onLoadListUtilisateur() {
    this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(`admin/management/users?size=${this.pageSize}&page=${this.page}`)
      .then((result: any) => {
        console.log('____________REsult List',result);
        this.utilisateurs = result.data.content.filter(item=>item.id!=1 && item.id!=2);
        this.totalElements = result.data.totalElements;
        this.genericsService.stopLoadingPage();
        if (this.isNotifieAdd == true) {
          this.isNotifieAdd = false;
          localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.USER_SAVEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.USER_SAVEDFR, 'success');
        }
        if (this.isNotifieEdit == true) {
          this.isNotifieEdit = false;
          localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.USER_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.USER_UPDATEDFR, 'success');
        }
      }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }

  onEditUser(utilisateur: any, modalEditUser: TemplateRef<any>) {
    console.log(utilisateur);
    this.selectedRoles = [];
    this.selectedCaisse = [];
    this.modalService.open(modalEditUser,
      { ariaLabelledBy: 'edit-user', scrollable: true, size: 'lg' }).result.then((result) => {
      }, (reason) => {
      });
    if (utilisateur != null) {
      this.UtilisateurForm.controls['password'].clearValidators();
      this.UtilisateurForm.controls['confirmation'].clearValidators();
      this.UtilisateurForm.get('password').updateValueAndValidity();
      this.UtilisateurForm.get('confirmation').updateValueAndValidity();
      this.selectedRoles = [];
      if (utilisateur && utilisateur.roleDTOS) {
        utilisateur.roleDTOS.forEach(role => {
          this.selectedRoles.push({ id: role.id, description: role.description });
        });
      }
      this.UtilisateurForm.patchValue({
        username: utilisateur.username,
        displayName: utilisateur.displayName,
        telephone: utilisateur.telephone,
        roles: this.selectedRoles,
        email: utilisateur.email
      });
      this.genericsService.getResource(`habilitation/utilisateur/${utilisateur.id}/caisse/liste`).then((result: any) => {
        if (result.success === true) {
          this.selectCaisse = [];
          result.data.caisses.forEach(caisse => {
            this.selectedCaisse.push({ id: caisse.idUserCaisse, description: caisse.caisse.nomCaisse });
          });
          this.UtilisateurForm.patchValue({
            caisses: this.selectedCaisse
          });
        }
      });
      this.currentUser = utilisateur;
    } else {
      this.UtilisateurForm.patchValue({
        username: '',
        displayName: '',
        telephone: '',
        roles: [],
        caisses: [],
        email: []
      });
    }
    this.currentUser = utilisateur;
    this.edit = true;
  }

  onChangeStatutUser(utilisateur: any){
    let lgEn = localStorage.getItem('activelang') === 'en';
    let statut = (utilisateur.enabled==true)? 'BLOQUER':'DEBLOQUER';
   // console.log(utilisateur);
    Swal.fire({
      title: lgEn ? AppConstants.ARE_U_SUREEN : AppConstants.ARE_U_SUREFR,
      text: lgEn ? AppConstants.IRREVERSIBLEEN : AppConstants.IRREVERSIBLEFR,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: lgEn ? (utilisateur.enabled==true ? AppConstants.YES_LOCK_EN : AppConstants.YES_UN_LOCK_EN) : (utilisateur.enabled==true ? AppConstants.YES_LOCK_FR : AppConstants.YES_UN_LOCK_FR)
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.
        getResource(`user/${utilisateur.id}/change?statut=${statut}`).then((result: any) => {
          this.onLoadListUtilisateur();
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(lgEn ? AppConstants.FAILEN : AppConstants.FAILFR, lgEn ? AppConstants.FAIL_OPERATIONEN : AppConstants.FAIL_OPERATIONFR, 'error');
        });
      }
    });
  }

  updateView(event: any) {
    this.edit = event;
  }
  private onLoadListCaisses() {
    // this.genericsService.getResource(`${this.url_caisse}list`).then((result: any) => {
    //   if (result.success === true) {
    //     this.caisses = result.data;
    //     this.caisses.forEach(caisse => {
    //       this.dataCaisse.push({ id: caisse.id, description: caisse.nomCaisse });
    //     });
    //   }
    // });

    this.genericsService.getResource(`caisses`).then((result: any) => {
      this.caisses = result._embedded.caisses;
      this.caisses.forEach(caisse => {
        this.dataCaisse.push({ id: caisse.id, description: caisse.nomCaisse });
      });
    });
  }

  private initFormUser() {
    this.UtilisateurForm = this.fb.group({
      username: ['', Validators.required],
      caisses: [this.selectedCaisse],
      displayName: ['', Validators.required],
      telephone: ['', [Validators.required, Validators.pattern('^6[0-9]{8}$')]],
      roles: [this.selectedRoles, Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmation: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmation')
    });
  }
  get f() { return this.UtilisateurForm.controls; }
  onSaveUtilisateur() {
    this.submitted = true;
    if (this.UtilisateurForm.invalid) {
      return;
    }
    if (this.currentUser) {
      const updateUserRequest = new UpdateUserRequestModel(
        this.f.displayName.value, this.f.email.value, this.currentUser.id,
        this.f.password.value.length > 0 ? this.f.password.value : null,
        this.f.telephone.value, this.f.username.value
      );
      console.log(updateUserRequest);
      this.genericsService.putResource(`habilitation/utilisateur/modifier`, updateUserRequest).then((result: any) => {
        this.isNotifieEdit = true;
        this.onResetUtilisateur();
      }, err => {
        console.log(err);
        this.genericsService.confirmModalResponseAPI(err.error.message, "Echec", 'error');
        // this.genericsService.confirmResponseAPI(err.error.message, 'error');
      });
    } else {
      const roles = [];
      this.f.roles.value.forEach(role => {
        roles.push({ roleId: role.id, description: role.description });
      });
      const userRequest = new AddUserRequest(this.f.displayName.value, this.f.email.value,
        this.f.password.value, this.f.username.value, this.f.telephone.value, roles, this.f.caisses.value
      );
      console.log(userRequest);
      this.genericsService.postResource(`habilitation/utilisateur/ajouter`, userRequest).then((result: any) => {
        this.isNotifieAdd = true;
        this.onResetUtilisateur();
      },
        err => {
          console.log(err);
          this.genericsService.confirmModalResponseAPI(err.error.message, "Echec", 'error');
          //this.error_add_user = err.error.message;
          //this.genericsService.confirmResponseAPI(err.error.message, 'error');
        });
    }
  }
  onResetUtilisateur() {
    this.onLoadListUtilisateur();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormUser();
    this.edit = false;
    this.currentUser = null;
    this.submitted = false;
  }

  onDeleteUser(currentUser: any) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.
        deleteResource(`habilitation/utilisateur/${currentUser.id}/supprimer`).then((resultDelete: any) => {
          this.onLoadListUtilisateur();
          this.edit = false;
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.
          deleteResource(`habilitation/utilisateur/${currentUser.id}/supprimer`).then((resultDelete: any) => {
            this.onLoadListUtilisateur();
            this.edit = false;
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }
  onDeselectRole(event: any) {
    if (this.currentUser) {
      const dto = new AjouterRoleToUser([new RoleModel(event.id, event.description, event.description)], this.currentUser.id);
      this.genericsService.postResource(`habilitation/utilisateur/role/supprimer`, dto).then((result: any) => {
        console.log(result);
      });
    }
  }
  onSelectRole(event: any) {
    if (this.currentUser) {
      const dto = new AjouterRoleToUser([new RoleModel(event.id, event.description, event.description)], this.currentUser.id);
      this.genericsService.postResource(`habilitation/utilisateur/role/ajouter`, dto).then((result: any) => {
        console.log(result);
      });
    }
  }
  onDeselectCaisse(event: any) {
    if (this.currentUser) {
      this.genericsService.deleteResource(`habilitation/utilisateur/caisse/${event.id}/retirer`)
        .then((result: any) => {
          console.log(result);
        });
    }
  }
  onSelectCaisse(event: any) {
    const dto = new AjouterCaisseToUser(this.currentUser.id, [new Caisse(event.id, event.description, event.description)]);
    if (this.currentUser) {
      this.genericsService.postResource(`habilitation/utilisateur/caisse/ajouter`, dto).then((result: any) => {
        console.log(result);
      });
    }
  }
  ngOnDestroy(): void {
    this.onResetUtilisateur();
  }
  setPageChange(event) {
    this.page = event - 1;
    this.onLoadListUtilisateur();
  }
}
