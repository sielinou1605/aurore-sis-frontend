import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UtilisateursComponent} from './utilisateurs.component';
import {ListUtilisateurComponent} from './list-utilisateur/list-utilisateur.component';
import {EditerUtilisateurComponent} from './editer-utilisateur/editer-utilisateur.component';

const routes: Routes = [
  {
    path: '', component: UtilisateursComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Utilisateurs',
      // titre: 'userEleveAjaout.Utilisateurs_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Users' : 'Utilisateurs',
      icon: 'icofont-user-alt-4 bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Les_utilisateurs_de_l_application_Utilisateurs',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'list-utilisateur',
        pathMatch: 'full'
      },
      {
        path: 'list-utilisateur', component: ListUtilisateurComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Utilisateurs',
          // titre: 'userEleveAjaout.Utilisateurs_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Users' : 'Utilisateurs',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Les_utilisateurs_de_l_application_Utilisateurs',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'editer-utilisateur', component: EditerUtilisateurComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Profil',
          // titre: 'userEleveAjaout.Profil_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Profil | Aurore' : 'Profile | Aurore',
            icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Ajouter_Modifer_et_changer_le_mot_de_passe_d_un_utilisateur_Profils',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'editer-utilisateur/:id', component: EditerUtilisateurComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Profil',
          //titre: 'userEleveAjaout.Profil_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Profil | Aurore' : 'Profile | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Ajouter_Modifer_et_changer_le_mot_de_passe_d_un_utilisateur_Profils',
          status: true,
          current_role: 'admin'
        }
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UtilisateursRoutingModule { }
