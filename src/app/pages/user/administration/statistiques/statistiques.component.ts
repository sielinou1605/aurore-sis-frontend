/* tslint:disable:triple-equals */
import {Component, OnInit} from '@angular/core';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {ClasseStatsResponseModel} from '../../../../shared/_models/response-dto/common/classe-stats-response-model';
import {AppConstants} from '../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-statistiques',
  templateUrl: './statistiques.component.html',
  styleUrls: ['./statistiques.component.scss']
})
export class StatistiquesComponent implements OnInit {
  public classes: ClasseStatsResponseModel[];
  public totalEleves: number;
  public totalElevesFilles: number;
  public totalElevesGarcons: number;
  public totalNetAttendu: number;
  public totalNetVerse: number;
  public totalNetRestant: number;
  public services: any;
  public currentSwitch: string;
  public  allServices = localStorage.getItem('activelang') === 'en' ? AppConstants.ALL_SERVICESEN : AppConstants.ALL_SERVICESFR;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private genericsService: GenericsService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

  }

  ngOnInit() {
    this.onloadListService();
    this.onloadStatFinancierEleve(0, this.allServices);
  }
  public onloadStatFinancierEleve(idService: any,  type: string) {
    this.currentSwitch = type;
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/inscription/stats/service?idService=${idService}`).then((response: any) => {
      this.computeState(response);
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
    });
  }

  computeState(data: any) {
    this.classes = data.data.filter((item: ClasseStatsResponseModel) => item.nbTotalEleves > 0);
    this.totalEleves  = this.classes.reduce(function(accumulator, currentValue) {
      return accumulator + currentValue.nbTotalEleves;
    }, 0);
    this.totalNetAttendu  = this.classes.reduce(function(accumulator, currentValue) {
      return accumulator + currentValue.montantAttendu;
    }, 0);
    this.totalElevesFilles  = this.classes.reduce(function(accumulator, currentValue) {
      return accumulator + currentValue.nbTotalElevesFilles;
    }, 0);
    this.totalElevesGarcons  = this.classes.reduce(function(accumulator, currentValue) {
      return accumulator + currentValue.nbTotalElevesGarcons;
    }, 0);
    this.totalNetVerse  = this.classes.reduce(function(accumulator, currentValue) {
      return accumulator + currentValue.montantRecu;
    }, 0);
    this.totalNetRestant  = this.classes.reduce(function(accumulator, currentValue) {
      return accumulator + currentValue.detteTotale;
    }, 0);
  }

  private onloadListService() {
    this.genericsService.getResource(`admin/service/list`).then((result: any) => {
      if (result.success === true) {
        this.services = result.data;
        console.log(this.services);
      }
    });
  }
}
