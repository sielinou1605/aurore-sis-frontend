import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-menu-config',
  templateUrl: './menu-config.component.html',
  styleUrls: ['./menu-config.component.scss']
})
export class MenuConfigComponent implements OnInit {

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(public authService: AuthService,
              public translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(68);
  }


  ngOnInit() {
  }

}
