/* tslint:disable:triple-equals */
import {Component, OnInit, TemplateRef} from '@angular/core';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {
  AjoutTypePenaliteModel,
  ModifierTypePenaliteModel
} from '../../../../../shared/_models/request-dto/graduation/gestion-sanction-request-model';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-penalite',
  templateUrl: './penalite.component.html',
  styleUrls: ['./penalite.component.scss']
})
export class PenaliteComponent implements OnInit {
  penalites: any;
  public typePenaliteForm: FormGroup;
  submitted: boolean;
  public toUpdate: boolean;
  totalElements;
  page = 0;
  pageSize = 20;
  private currentPenalite: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private genericsService: GenericsService,
              private modalService: NgbModal, config: NgbModalConfig,
              private fb: FormBuilder, private tokenStorage: TokenStorageService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;

  }
  ngOnInit() {
    this.onloadListTypePenalites();
    this.initFormTypePenalite();
  }
  private onloadListTypePenalites() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`parametre/type-penalite/liste/paged?size=${this.pageSize}`).then((response: any) => {
      console.log(response);
      this.penalites = response.data.content;
      this.totalElements = response.data.totalElements;
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_ERROREN, 'error', 7000) : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_ERRORFR, 'error', 7000);
      this.genericsService.stopLoadingPage();
    });
  }
  private initFormTypePenalite() {
    this.typePenaliteForm = this.fb.group({
      libelle: ['', Validators.required],
      gravite: [''],
      description: ['', Validators.required]
    });
  }
  get f() { return this.typePenaliteForm.controls; }
  onSaveTypePenalite() {
    this.submitted = true;
    if (this.typePenaliteForm.invalid) {
      return;
    }
    let dto;
    if (this.toUpdate == true && this.currentPenalite) {
      dto = new ModifierTypePenaliteModel(this.f.description.value, this.f.libelle.value, this.f.gravite.value, this.currentPenalite.id);
      this.genericsService.putResource(`parametre/type-penalite/modifier`, dto).then((response) => {
        this.onResetTypePenalite();
        this.onloadListTypePenalites();
      }).catch(reason => {
        console.log(reason);
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.UPDATE_PENALITE_FAILEDEN, 'error', 8000) : this.genericsService.confirmResponseAPI(AppConstants.UPDATE_PENALITE_FAILEDFR, 'error', 8000);
      });
    } else {
      dto = new AjoutTypePenaliteModel(this.f.description.value, this.f.libelle.value, this.f.gravite.value);
      this.genericsService.postResource(`parametre/type-penalite/ajouter`, dto).then((response: any) => {
        this.onResetTypePenalite();
        this.onloadListTypePenalites();
      }).catch(reason => {
        console.log(reason);
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.CREATE_PENALITE_FAILEDEN, 'error', 8000) : this.genericsService.confirmResponseAPI(AppConstants.CREATE_PENALITE_FAILEDFR, 'error', 8000);
      });
    }

  }

  onOpenDetailPenalite(penalite: any, detailPenalite: TemplateRef<any>, toEdit: boolean) {
    this.toUpdate = toEdit;
    this.currentPenalite = penalite;
    this.modalService.open(detailPenalite,
      {ariaLabelledBy: 'type-penalite', size: 'lg', scrollable: true}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
    if (penalite) {
      this.typePenaliteForm.patchValue({
        libelle: penalite.libelle,
        gravite: penalite.gravite,
        description: penalite.description
      });
    }
  }
  onResetTypePenalite() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
    this.currentPenalite = null;
    this.toUpdate = true;
    this.initFormTypePenalite();
  }
  onSearchPenalite() {

  }
  onDeleteTypePenalite(penalite: any) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`parametre/type-penalite/${penalite.id}/supprimer`)
          .then((resultDeleteEtat: any) => {
            console.log(resultDeleteEtat);
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
          }).catch((errEtat: any) => {
          console.log(errEtat);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`parametre/type-penalite/${penalite.id}/supprimer`)
            .then((resultDeleteEtat: any) => {
              console.log(resultDeleteEtat);
              this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
            }).catch((errEtat: any) => {
            console.log(errEtat);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }
  setPageChange(event: number) {
    this.page = event - 1;
    this.onloadListTypePenalites();
  }
}
