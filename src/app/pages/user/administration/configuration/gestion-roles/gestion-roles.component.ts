/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoleModel} from '../../../../../shared/_models/auth/add-user-request';
import {AjoutActionToRole, RoleAction} from '../../../../../shared/_models/auth/request-role-management-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {
  ActionResponse,
  DataFenetreResponse,
  DataModuleResponseModel,
  FenetreResponse
} from '../../../../../shared/_models/response-dto/systeme/data-module-response-model';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-gestion-roles',
  templateUrl: './gestion-roles.component.html',
  styleUrls: ['./gestion-roles.component.scss']
})
export class GestionRolesComponent implements OnInit, OnDestroy {
  roleForm: FormGroup;
  accessForm: FormGroup;
  public objectsModules: DataModuleResponseModel[];
  public privileges: RoleModel[];
  public rolesActions: RoleAction[] = [];
  availableActions: ActionResponse[];
  currentRole: RoleModel;
  allSelectEditable: boolean;
  allSelectVisible: boolean;
  allSelectActif: boolean;
  allSelectAction: boolean;
  submitted: boolean;
  private toUpdate: boolean;
  idCurrentFenetre: number;
  public roleActionsForm: FormGroup;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private genericsService: GenericsService, private modalService: NgbModal,
              config: NgbModalConfig, private fb: FormBuilder,
              public authService: AuthService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(7);
  }

  ngOnInit() {
    this.onLoadListRole();
    this.initFormRole();
    this.initAccessForm();
    this.initFormRoleActions();
  }

  private onLoadListRole() {
    this.genericsService.getResource(`habilitation/role/liste`).then((result: any) => {
        if (result.success === true) {
          this.privileges = result.data;
        }
      },
      err => {
        console.log(err);
        localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.ERROR_LOADED_PRIVILEGEEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.ERROR_LOADED_PRIVILEGEFR, 'error');
      });
  }
  get r() { return this.roleForm.controls; }
  private initFormRole() {
    this.roleForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
  }
  onOpenFormEditRole(role: RoleModel, modalEditRole: TemplateRef<any>) {
    this.currentRole = null;
    this.modalService.open(modalEditRole,
      {ariaLabelledBy: 'edit-roles', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
    if (role != null) {
      this.roleForm.patchValue({name: role.name, description: role.description});
    }
  }
  onResetRole() {
    this.initFormRole();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onSaveRole() {
    this.submitted = true;
    if (this.roleForm.invalid) {
      return;
    }
    const url = this.currentRole ? `habilitation/role/modifier` : `habilitation/role/ajouter`;
    const modelRequest = new RoleModel(this.currentRole ? this.currentRole.roleId : null, this.r.name.value, this.r.description.value);
    if (this.currentRole) {
      this.genericsService.putResource(url, modelRequest).then((resultUpdate: any) => {
        this.genericsService.confirmResponseAPI(resultUpdate.message, 'success');
        this.onLoadListRole();
        this.onResetRole();
      }).catch((errUpdate) => {
        console.log(errUpdate);
        if(errUpdate.status==404)
        this.genericsService.confirmResponseAPI(errUpdate.error.message, 'error');
      });
    } else {
      this.genericsService.postResource(url, modelRequest).then((resultCreate: any) => {
        this.genericsService.confirmResponseAPI(resultCreate.message, 'success');
        this.onLoadListRole();
        this.onResetRole();
      }).catch(errCreate => {
        console.log(errCreate);
        if(errCreate.status==404)
        this.genericsService.confirmResponseAPI(errCreate.error.message, 'error');
      });
    }

  }
  onDeleteRole(id: number) {
    console.log(id);
    if (this.authService.isActif('bouton-role-supprimer') && id != 1 && id != 2) {
      localStorage.getItem('activelang') === 'en' ?
        Swal.fire({
        title: AppConstants.ARE_U_SUREEN,
        text: AppConstants.IRREVERSIBLEEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITEN
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.
          deleteResource(`habilitation/role/${id}/supprimer`).then((resultDelete: any) => {
            this.genericsService.confirmResponseAPI(resultDelete.message, 'success');
            this.onLoadListRole();
            this.toUpdate = false;
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
          });
        }
      }) : Swal.fire({
          title: AppConstants.ARE_U_SUREFR,
          text: AppConstants.IRREVERSIBLEFR,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: AppConstants.YES_DELETE_ITFR
        }).then((result) => {
          if (result.isConfirmed) {
            this.genericsService.
            deleteResource(`habilitation/role/${id}/supprimer`).then((resultDelete: any) => {
              this.genericsService.confirmResponseAPI(resultDelete.message, 'success');
              this.onLoadListRole();
              this.toUpdate = false;
              this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
            }).catch(reason => {
              console.log(reason);
              this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
            });
          }
        });
    }
  }
  onSelectRoleToEdit(privilege: RoleModel) {
    if (this.authService.isActif('bouton-role-modifier')) {
      this.onloadListModule(privilege);
      this.currentRole = privilege;
      this.rolesActions = [];
      this.allSelectEditable = false;
      this.allSelectVisible =  false;
      this.allSelectActif = false;
      this.allSelectAction = false;
    }
  }
  /**_____________________ Ceci releve de la conception faite en par Oumar et que la conception actuelle
   * ne permet pas encore la mise en oeuvre _________________________________**/
  initAccessForm() {
    this.accessForm = this.fb.group({
      lineElements: this.fb.array([])
    });
    this.addCheckboxes();
  }

  get af() { return this.accessForm.controls; }

  get t() { return this.af.roleActionData as FormArray; }

  private addCheckboxes() {
    this.rolesActions.forEach((line, i) => {
      this.t.push(
        this.fb.group({
          actif: [''],
          visible: [''],
          editable: ['']
        })
      );
    });
  }

  private onloadListModule(role: any) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`systeme/role/${role.roleId}/module/liste-complete`).then((resultModule: any) => {
      console.log(this.objectsModules);
      if (resultModule.success === true) {
        this.objectsModules = resultModule.data.sort((a, b) => (a.module.id > b.module.id) ? 1 : ((b.module.id > a.module.id) ? -1 : 0));
      }
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      this.genericsService.stopLoadingPage();
    });
  }

  onAddActionsToRole() {
    const dto = new AjoutActionToRole(this.currentRole.roleId, this.rolesActions);
    this.genericsService.putResource(`habilitation/role/action/modifier`, dto).then((resultAction: any) => {
      this.rolesActions = [];
      this.onloadListModule(this.currentRole);
      this.allSelectEditable = false;
      this.allSelectVisible = false;
      this.allSelectActif = false;
      this.allSelectAction = false;
      console.log(resultAction);
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  onAllActionCheckboxChange(event: any, dataFenetre: DataFenetreResponse) {
    this.idCurrentFenetre = dataFenetre.fenetre.id;
    this.allSelectAction = this.allSelectActif = this.allSelectEditable = this.allSelectVisible = !!event.target.checked;
    this.rolesActions = [];
    dataFenetre.actions.forEach((action, i) => {
      if (event.target.checked) {
        this.rolesActions.push(new RoleAction('true', 'true', 'true', action.idRoleAction));
      } else {
        this.rolesActions.push(new RoleAction('false', 'false', 'false', action.idRoleAction));
      }
    });
  }

  onAllActifCheckboxChange(event: any, dataFenetre: DataFenetreResponse) {
    this.idCurrentFenetre = dataFenetre.fenetre.id;
    const isChecked = event.target.checked ? 'true' : 'false';
    this.allSelectActif = !!event.target.checked;
    this.rolesActions.forEach((roleAction, i) => {
      this.rolesActions[i].conditionActif = isChecked;
    });
  }
  onAllVisibleCheckboxChange(event: any, dataFenetre: DataFenetreResponse) {
    this.idCurrentFenetre = dataFenetre.fenetre.id;
    const isChecked = event.target.checked ? 'true' : 'false';
    this.allSelectVisible = !!event.target.checked;
    this.rolesActions.forEach((roleAction, i) => {
      this.rolesActions[i].conditionVisible = isChecked;
    });
  }
  onAllEditableCheckboxChange(event: any, dataFenetre: DataFenetreResponse) {
    this.idCurrentFenetre = dataFenetre.fenetre.id;
    const isChecked = event.target.checked ? 'true' : 'false';
    this.allSelectEditable = !!event.target.checked;
    this.rolesActions.forEach((roleAction, i) => {
      this.rolesActions[i].conditionEditable = isChecked;
    });
  }

  onActifCheckboxChange(event: any, itemAction: ActionResponse, fenetre: FenetreResponse) {
    this.idCurrentFenetre = fenetre.id;
    const indexAction = this.rolesActions.findIndex((roleAction) => roleAction.idRoleAction == itemAction.idRoleAction);
    const isChecked = event.target.checked ? 'true' : 'false';
    if (indexAction > -1) {
      this.rolesActions[indexAction].conditionActif = isChecked;
    } else {
      this.rolesActions.push(new RoleAction(isChecked, itemAction.conditionEditable, itemAction.conditionVisible, itemAction.idRoleAction));
    }
  }
  onVisibleCheckboxChange(event: any, itemAction: ActionResponse, fenetre: FenetreResponse) {
    this.idCurrentFenetre = fenetre.id;
    const indexAction = this.rolesActions.findIndex((roleAction) => roleAction.idRoleAction == itemAction.idRoleAction);
    const isChecked = event.target.checked ? 'true' : 'false';
    if (indexAction > -1) {
      this.rolesActions[indexAction].conditionVisible = isChecked;
    } else {
      this.rolesActions.push(new RoleAction(isChecked, itemAction.conditionEditable, isChecked, itemAction.idRoleAction));
    }
  }
  onEditableCheckboxChange(event: any, itemAction: ActionResponse, fenetre: FenetreResponse) {
    this.idCurrentFenetre = fenetre.id;
    const indexAction = this.rolesActions.findIndex((roleAction) => roleAction.idRoleAction == itemAction.idRoleAction);
    const isChecked = event.target.checked ? 'true' : 'false';
    if (indexAction > -1) {
      this.rolesActions[indexAction].conditionEditable = isChecked;
    } else {
      this.rolesActions.push(new RoleAction(itemAction.conditionActif, isChecked, itemAction.conditionVisible, itemAction.idRoleAction));
    }
  }

  ngOnDestroy(): void {
    this.onResetRole();
  }

  onloadFormTransfertAction(privilege: RoleModel, modalRoleActions: TemplateRef<any>) {
    this.currentRole = privilege;
    this.modalService.open(modalRoleActions,
      {ariaLabelledBy: 'transfert-actions', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onTransfertRoleActions() {
    this.submitted = true;
    if (this.roleActionsForm.invalid) {
      return;
    }
    this.genericsService.startLoadingPage();
    const dto = [new RoleModel(this.ta.role.value, null, null)];
    this.genericsService.postResource(`habilitation/role/${this.currentRole.roleId}/action/copier`, dto).then((response: any) => {
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmResponseAPI(AppConstants.ACTIONS_TRANSFEREEN, 'success', 7000) : this.genericsService.confirmResponseAPI(AppConstants.ACTIONS_TRANSFEREFR, 'success', 7000);
      this.onResetRoleAction();
      this.onLoadListRole();
      this.currentRole = null;
      this.objectsModules = null;
    }).catch(reason => {
      console.log(reason);
    });
  }

  onResetRoleAction() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
    this.initFormRoleActions();
  }

  private initFormRoleActions() {
    this.roleActionsForm = this.fb.group({
      role: ['', Validators.required]
    });
  }

  get ta() { return this.roleActionsForm.controls; }

  onRefreshRoleAction() {
    this.genericsService.startLoadingPage();
    this.genericsService.postResource(`habilitation/role/actualiser`, {}).then((response: any) => {
      console.log(response);
      this.genericsService.stopLoadingPage();
    }).catch(err => {
      this.genericsService.stopLoadingPage();
      console.log(err);
    });
  }
}
