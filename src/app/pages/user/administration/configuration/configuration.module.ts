// @ts-ignore
import {NgModule} from '@angular/core';
// @ts-ignore
import {CommonModule} from '@angular/common';

import {ConfigurationRoutingModule} from './configuration-routing.module';
import {ConfigurationComponent} from './configuration.component';
import {MenuConfigComponent} from './menu-config/menu-config.component';
import {GestionRolesComponent} from './gestion-roles/gestion-roles.component';
import {MatieresComponent} from './matieres/matieres.component';
import {SharedModule} from '../../../../shared/shared.module';
// @ts-ignore
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GestionEtatsComponent} from './gestion-etats/gestion-etats.component';
// @ts-ignore
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
// @ts-ignore
import {NgxPaginationModule} from 'ngx-pagination';
import {NumerotationComponent} from './numerotation/numerotation.component';
import {PenaliteComponent} from './penalite/penalite.component';
// @ts-ignore
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    ConfigurationComponent,
    MenuConfigComponent,
    GestionRolesComponent,
    MatieresComponent,
    GestionEtatsComponent,
    NumerotationComponent,
    PenaliteComponent,
  ],
    imports: [
        CommonModule,
        ConfigurationRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgMultiSelectDropDownModule,
        NgxPaginationModule,
        TranslateModule
    ]
})
export class ConfigurationModule { }
