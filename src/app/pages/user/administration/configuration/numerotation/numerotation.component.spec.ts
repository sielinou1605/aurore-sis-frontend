import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NumerotationComponent} from './numerotation.component';

describe('NumerotationComponent', () => {
  let component: NumerotationComponent;
  let fixture: ComponentFixture<NumerotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumerotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumerotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
