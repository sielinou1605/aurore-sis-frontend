import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {
  EditerNumerotation,
  NumerotationResponseModel
} from '../../../../../shared/_models/request-dto/common/gestion-numerotation-request-model';
import {GlobalVariableDto} from '../../../../../shared/_models/request-dto/common/global-variable-dto';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-numerotation',
  templateUrl: './numerotation.component.html',
  styleUrls: ['./numerotation.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class NumerotationComponent implements OnInit, OnDestroy {
  public numerotationForm: FormGroup;
  public submitted: boolean;
  private numerotation: NumerotationResponseModel;
  public variableGlobaleForm: FormGroup;
  public variableGlobales: any;
  public toUpdate: boolean;
  public currentVGlobale: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private modalService: NgbModal, private genericsService: GenericsService,
    config: NgbModalConfig, private fb: FormBuilder,
    public authService: AuthService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.initFormNumerotation();
    this.initFormVariableGlobale();
    this.consulterNumerotation();
    this.onloadListVariableGlobale();
  }

  private initFormNumerotation() {
    this.numerotationForm = this.fb.group({
      id: ['', Validators.required],
      numeroIndexMatricule: ['', Validators.required],
      numeroIndexReglementDivers: ['', Validators.required],
      numeroIndexReglementEleve: ['', Validators.required],
      soucheNumeroMatricule: ['', Validators.required],
      soucheNumeroReglementDivers: ['', Validators.required],
      soucheNumeroReglementEleve: ['', Validators.required]
    });
  }
  private initFormVariableGlobale() {
    this.variableGlobaleForm = this.fb.group({
      cle: [''],
      valeur: [''],
      description: ['']
    });
  }

  onSaveNumerotation() {
    console.log(this.f);
    this.submitted = true;
    if (this.numerotationForm.invalid) {
      return;
    }
    const dto = new EditerNumerotation(
      this.f.id.value,  this.f.numeroIndexMatricule.value, this.f.numeroIndexReglementDivers.value,
      this.f.numeroIndexReglementEleve.value, this.f.soucheNumeroMatricule.value,
      this.f.soucheNumeroReglementDivers.value, this.f.soucheNumeroReglementEleve.value,
    );
    this.genericsService.postResource(`parametre/numerotation/ajouter`, dto).then((response: any) => {
      this.genericsService.confirmResponseAPI(response.message,
        'success', 6000);
      this.submitted = false;
    }).catch(reason => {
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmResponseAPI(AppConstants.UPDATE_NUMEROTATION_FAILEN,
        'error', 8000) : this.genericsService.confirmResponseAPI(AppConstants.UPDATE_NUMEROTATION_FAILFR,
          'error', 8000);
    });
  }
  onSaveConfigVariableGlobale() {
    this.submitted = true;
    if (this.variableGlobaleForm.invalid) {
      return;
    }
    const dto = new GlobalVariableDto(this.currentVGlobale.id, this.g.valeur.value, this.g.description.value);
    this.genericsService.startLoadingPage();
    this.genericsService.putResource(`parametre/variable-globale/modifier`, dto).then((response: any) => {
      console.log(response);
      this.genericsService.stopLoadingPage();
      this.genericsService.confirmResponseAPI(response.message,
        'success', 6000);
      this.genericsService.getResource(`parametre/variable-globale/liste`).then((vg: any) => {
        this.variableGlobales = vg.data;
      });
      this.submitted = false;
      this.onReset();
    }).catch(err => {
      console.log(err);
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmResponseAPI(AppConstants.UPDATE_VAR_GLOBALE_FAILEN,
        'error', 8000) : this.genericsService.confirmResponseAPI(AppConstants.UPDATE_VAR_GLOBALE_FAILFR,
          'error', 8000);
    });
  }
  private consulterNumerotation() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`parametre/numerotation/consulter`).then((response: any) => {
      this.numerotation = response.data;
      this.genericsService.stopLoadingPage();
      this.numerotationForm.patchValue({
        id: this.numerotation.id,
        numeroIndexMatricule: this.numerotation.numeroIndexMatricule,
        numeroIndexReglementDivers:  this.numerotation.numeroIndexReglementDivers,
        numeroIndexReglementEleve:  this.numerotation.numeroIndexReglementEleve,
        soucheNumeroMatricule:  this.numerotation.soucheNumeroMatricule,
        soucheNumeroReglementDivers:  this.numerotation.soucheNumeroReglementDivers,
        soucheNumeroReglementEleve:  this.numerotation.soucheNumeroReglementEleve
      });
    }).catch(reason => {
      this.genericsService.stopLoadingPage();
      console.log(reason);
    });
  }
  get f() { return this.numerotationForm.controls; }

  get g() { return this.variableGlobaleForm.controls; }

  private onloadListVariableGlobale() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`parametre/variable-globale/liste`).then((response: any) => {
      this.variableGlobales = response.data;
      this.genericsService.stopLoadingPage();
    }).catch(err => {
      this.genericsService.stopLoadingPage();
      console.warn(err);
    });
  }
  onLoadDetailsVG(vglobale: any,  content: TemplateRef<any>) {
    this.toUpdate =  true;
    this.currentVGlobale = vglobale;
    this.variableGlobaleForm.patchValue({
      cle: vglobale.cle,
      valeur: vglobale.valeur,
      description: vglobale.description
    });
    this.modalService.open(content,
      {ariaLabelledBy: 'add-caisse', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onReset() {
    this.submitted = false;
    this.toUpdate = false;
    this.initFormVariableGlobale();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  ngOnDestroy(): void {
    this.onReset();
  }
}
