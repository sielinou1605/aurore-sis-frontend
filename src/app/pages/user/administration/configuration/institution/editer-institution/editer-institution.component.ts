import {Component, OnInit} from '@angular/core';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {InstitutionRequestModel} from '../../../../../../shared/_models/request-dto/common/institution-request-model';
import {HttpEventType} from '@angular/common/http';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-editer-institution',
  templateUrl: './editer-institution.component.html',
  styleUrls: ['./editer-institution.component.scss']
})
export class EditerInstitutionComponent implements OnInit {
  private idInstitution: string;
  public editInstitutionForm: FormGroup;
  public submitted: boolean;

  public selectedFiles: any;
  public progress: number;
  public currentFileUpload: any;
  public id: number;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private genericsService: GenericsService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public authService: AuthService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(38);
  }

  ngOnInit() {
    this.initFormEditEtablissement(null);
    this.route.paramMap.subscribe(params => {
      this.idInstitution = params.get('id');
      this.id = parseInt(this.idInstitution, 0);
      this.onLoadListInstitutionById(this.idInstitution);
    });
  }

  onSelectedFile(event) {
    this.selectedFiles = event.target.files;
    const fileReader = new FileReader();
    fileReader.onload = (e) => {};
    fileReader.readAsText(this.selectedFiles[0]);
  }

  uploadLogoInstitution(id: number) {
    if (this.authService.isActif('bouton-modifier-logo')) {
      this.progress = 0;
      this.currentFileUpload = this.selectedFiles.item(0);
      this.genericsService.uploadImage(this.currentFileUpload, `admin/upload/logo/${id}/institution`).subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          } else {
            this.progress = undefined;
            localStorage.getItem('activelang') === 'en' ?
              // tslint:disable-next-line:max-line-length
            this.genericsService.confirmResponseAPI(AppConstants.INSTITUTION_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.INSTITUTION_UPDATEDFR, 'success');
            // this.router.navigate(['/administration/institutions/list-institution']).then(() => {
            //   this.onLoadListInstitutionById(this.idInstitution);
            //   this.genericsService.confirmResponseAPI('L\'institution à été mise a jour avec succes', 'success');
            // });
          }
        },
        errorLogo => {
          // tslint:disable-next-line:max-line-length
          localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.LOGO_UPLOAD_FAILEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.LOGO_UPLOAD_FAILEDFR, 'error');
        });
      this.selectedFiles = undefined;
    }
  }

  private initFormEditEtablissement(institution: any) {
    console.log(institution);
    this.editInstitutionForm = this.fb.group({
      id: [institution ? institution.id : null],
      nomfr: [institution ? institution.nomInstitution : '', Validators.required],
      nomen: [institution ? institution.nomInstitutionEn : ''],
      code: [institution ? institution.code_institution : '', Validators.required],
      devise: [institution ? institution.deviseInstitution : '', Validators.required],
      deviseen: [institution ? institution.deviseInstitutionEn : ''],
      raisonsociale: [institution ? institution.raisonSociale : '', Validators.required],
      region: [institution ? institution.region : '', Validators.required],
      departement: [institution ? institution.departement : '', Validators.required],
      arrondissement: [institution ? institution.arrondissement : '', Validators.required],
      delegregionale: [institution ? institution.delegation : ''],
      delegregionaleen: [institution ? institution.delegationEn : ''],
      delegdepartementale: [institution ? institution.delegationDepFr : ''],
      delegdepartementaleen: [institution ? institution.delegationDepEn : ''],
      telephone1: [institution ? institution.telephone1 : '', Validators.required],
      telephone2: [institution ? institution.telephone2 : ''],
      email: [institution ? institution.email : '', Validators.required],
      siteweb: [institution ? institution.siteWeb : ''],
      boitepostale: [institution ? institution.boitePostale : '', Validators.required],
      logo: [institution ? institution.logoInstitution : ''],
    });
  }

  public onUpdateInstitution() {
    this.submitted = true;
    if (this.editInstitutionForm.invalid) {
      return;
    }
    const institutionRequest = new InstitutionRequestModel(this.f.arrondissement.value,
      this.f.boitepostale.value, this.f.code.value, this.f.delegregionale.value,
      this.f.delegdepartementaleen.value, this.f.delegdepartementale.value,
      this.f.delegregionaleen.value, this.f.departement.value, this.f.devise.value,
      this.f.deviseen.value, this.f.email.value, this.f.id.value, this.f.logo.value,
      this.f.nomfr.value, this.f.nomen.value, this.f.raisonsociale.value, this.f.region.value,
      this.f.siteweb.value, this.f.telephone1.value, this.f.telephone2.value, this.f.arrondissement.value
    );
    this.genericsService.postResource(`admin/ajouter/institution`, institutionRequest).then((result: any) => {
      if (this.selectedFiles) {
        this.uploadLogoInstitution(this.id);
      } else {
        this.router.navigate(['/administration/configuration/institutions/list-institution']).then(() => {
          // tslint:disable-next-line:max-line-length
          localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.INSTITUTION_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.INSTITUTION_UPDATEDFR, 'success');
        });
      }
    });
  }

  get f() { return this.editInstitutionForm.controls; }

  private onLoadListInstitutionById(id: string) {
    this.genericsService.getResource(`institutions/${id}`).then((result: any) => {
      this.initFormEditEtablissement(result);
    });
  }

  onResetInstitution() {
    this.submitted = false;
    this.editInstitutionForm.reset();
    this.genericsService.onBackClicked();
  }
}
