import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditerInstitutionComponent} from './editer-institution.component';

describe('EditerInstitutionComponent', () => {
  let component: EditerInstitutionComponent;
  let fixture: ComponentFixture<EditerInstitutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditerInstitutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditerInstitutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
