import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DisciplineRequestModel} from '../../../../../../shared/_models/request-dto/graduation/gestion-cours-enseignant-model';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-details-departement',
  templateUrl: './details-departement.component.html',
  styleUrls: ['./details-departement.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class DetailsDepartementComponent implements OnInit, OnDestroy {
  searchForm: FormGroup;
  idDep: any;
  submitted: boolean;
  disciplineForm: FormGroup;
  public toUpdate: boolean;
  detailsDepartement: any;
  validated: boolean;

  public eric: string;
  public romuald: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, private genericsService: GenericsService,
    private route: ActivatedRoute, private router: Router,
    private translate: TranslateService
  ) {

    this.eric = localStorage.getItem('avtivelang') === 'en' ? 'The Disciplines of the Department ' : 'Les Disciplines du departements ';
    this.romuald = localStorage.getItem('activelang') === 'en' ? 'The department\'s staff ' : 'Le personnel du departement ';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idDep = params.get('id');
      this.onLoadListEnseignant(this.idDep);
      this.onLoadDetailsDepartement(this.idDep);
      this.initDisciplineForm();
    });
  }

  onLoadListEnseignant(idDep: any) {

  }

  openEditFormDisciplice(discipline, content: TemplateRef<any>) {
    if (discipline) {
      this.toUpdate = true;
      this.disciplineForm.patchValue({
        id_discipline: discipline.id_discipline,
        nom: discipline.nom_discipline
      });
    } else  {
      this.toUpdate = false;
    }
    console.log(discipline);
    this.submitted = false;
    this.modalService.open(content, {ariaLabelledBy: 'add-discipline'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onSaveDiscipline() {
    this.submitted = true;
    this.validated = true;
    if (this.disciplineForm.invalid) {
      this.validated = false;
      return;
    }
    const disciplineRequest = new DisciplineRequestModel(parseInt(this.idDep, 0), this.f.id_discipline.value, this.f.nom.value);
    this.genericsService.postResource(`admin/ajouter/discipline`, disciplineRequest).then((response: any) => {
      console.log(response);
      this.onResetDiscipline();
      this.toUpdate = false;
      this.submitted = false;
      this.validated = false;
      this.onLoadDetailsDepartement(this.idDep);
    }).catch(reason => {
      console.log(reason);
      this.validated = false;
    });
  }
  onResetDiscipline() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.submitted = false;
    this.initDisciplineForm();
  }
  private initDisciplineForm() {
    this.disciplineForm = this.fb.group({
      id_discipline: [0],
      nom: ['', [Validators.required, Validators.minLength(3)]]
    });
  }
  get f() {
    return this.disciplineForm.controls;
  }

  private onLoadDetailsDepartement(idDep: any) {
    this.genericsService.getResource(`admin/consulter/${idDep}/departement`).then((response: any) => {
      if (response.success === true) {
        this.detailsDepartement = response.data;
        console.log(response.data);
      }
    }).catch(reason => {
      console.log(reason);
    });
  }

  ngOnDestroy(): void {
    this.onResetDiscipline();
  }
}
