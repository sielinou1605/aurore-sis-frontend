import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {InstitutionRoutingModule} from './institution-routing.module';
import {InstitutionComponent} from './institution.component';
import {SharedModule} from '../../../../../shared/shared.module';
import {ListEtablissementComponent} from './list-etablissement/list-etablissement.component';
import {ListSectionComponent} from './list-section/list-section.component';
import {ListCycleComponent} from './list-cycle/list-cycle.component';
import {ListNiveauComponent} from './list-niveau/list-niveau.component';
import {ListInstitutionComponent} from './list-institution/list-institution.component';
import {EditerInstitutionComponent} from './editer-institution/editer-institution.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DetailsDepartementComponent} from './details-departement/details-departement.component';
import {AvatarModule} from 'ngx-avatar';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [
    InstitutionComponent,
    ListEtablissementComponent,
    ListSectionComponent,
    ListCycleComponent,
    ListNiveauComponent,
    ListInstitutionComponent,
    EditerInstitutionComponent,
    DetailsDepartementComponent,
  ],
    imports: [
        CommonModule,
        InstitutionRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        AvatarModule,
        TranslateModule
    ]
})
export class InstitutionModule { }
