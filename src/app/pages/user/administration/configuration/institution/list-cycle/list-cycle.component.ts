import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {ActivatedRoute} from '@angular/router';
import {CycleRequestModel} from '../../../../../../shared/_models/request-dto/common/cycle-request-model';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-cycle',
  templateUrl: './list-cycle.component.html',
  styleUrls: ['./list-cycle.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListCycleComponent implements OnInit, OnDestroy {
  public cycles: any;
  submitted: boolean;
  formCycle: FormGroup;
  private toUpdate: boolean;
  private idSection: string;
  private currentCycle: any;
  public detailsSection: any;

  public eric: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, private genericsService: GenericsService,
    private route: ActivatedRoute, public authService: AuthService,
    private translate: TranslateService
  ) {

    this.eric = localStorage.getItem('activelang') === 'en' ? 'The cycles of the ' : 'Les cycles de la ';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(40);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idSection = params.get('id');
      this.onloadDetailsSection(this.idSection);
      this.onLoadListCycles(this.idSection);
      this.initFormCycle();
    });
  }

  openLoadFormAddCycle(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.submitted = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'add-cycle'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onSaveCycle() {
    this.submitted = true;
    if (this.formCycle.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const cycleRequest = new CycleRequestModel(this.currentCycle.id,
        this.f.nom.value, this.f.numOrdre.value, parseInt(this.idSection, 0));
      console.log(cycleRequest);
      this.genericsService.postResource(`admin/ajouter/cycle`, cycleRequest).then((result: any) => {
        this.onLoadListCycles(this.idSection);
        this.resetForm();
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.CYCLE_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.CYCLE_UPDATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    } else {
      const cycleRequest = new CycleRequestModel(null,
        this.f.nom.value, this.f.numOrdre.value, parseInt(this.idSection, 0));
      console.log(cycleRequest);
      console.log("cycleRequest");
      this.genericsService.postResource(`admin/ajouter/cycle`, cycleRequest).then((response: any) => {
        this.onLoadListCycles(this.idSection);
        this.resetForm();
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.CYCLE_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.CYCLE_CREATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      }).catch(reason => {
        this.genericsService.confirmModalResponseAPI(reason.error.message, "Echec", 'error');
        console.log(reason);
      });
    }
  }

  onLoadDetailsCycle(cycle: any, content: TemplateRef<any>) {
    this.currentCycle = cycle;
    this.toUpdate = true;
    this.formCycle.patchValue({
      numOrdre: cycle.numeroOrdre,
      nom: cycle.nomCycle
    });
    this.modalService.open(content, {ariaLabelledBy: 'add-cycle'})
      .result.then((resultModal) => {},
      (reason) => {});
  }

  get f() { return this.formCycle.controls; }

  private onLoadListCycles(id: string) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`sectionses/${id}/cycles`).then((result: any) => {
      this.cycles = result._embedded.cycleses;
      console.log(this.cycles);
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      this.genericsService.stopLoadingPage();
    });
  }

  private initFormCycle() {
    this.formCycle = this.fb.group({
      numOrdre: ['', Validators.required],
      nom: ['', Validators.required]
    });
  }

  onResetCycle() {
    this.resetForm();
    this.toUpdate = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  resetForm() {
    this.submitted = false;
    this.initFormCycle();
  }

  private onloadDetailsSection(idSection: string) {
    this.genericsService.getResource(`admin/consulter/${idSection}/section`).then((response: any) => {
      this.detailsSection = response.data;
      console.log(this.detailsSection);
    }).catch(reason => {
      console.log(reason);
    });
  }

  ngOnDestroy(): void {
    this.onResetCycle();
  }

  onDeleteCycle(cycle: any) {
    console.log(cycle);
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        if (cycle._embedded && cycle._embedded.niveaus) {
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.CYCLE_WITH_NIVEAUXEN, 'error');
        } else {
            this.genericsService.deleteResource(`cycleses/${cycle.id}`).then((resultDelete: any) => {
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETE_SUCCESSEN, AppConstants.DELETE_SUCCESSEN, 'success');
            this.genericsService.getResource(`sectionses/${this.idSection}/cycles`).then((response: any) => {
              this.cycles = response._embedded.cycleses;
            }).catch(reason => {
              console.log(reason);
            });
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
          });
        }
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          if (cycle._embedded && cycle._embedded.niveaus) {
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.CYCLE_WITH_NIVEAUXFR, 'error');
          } else {
            this.genericsService.deleteResource(`cycleses/${cycle.id}`).then((resultDelete: any) => {
              this.genericsService.confirmModalResponseAPI(AppConstants.DELETE_SUCCESSFR, AppConstants.DELETE_SUCCESSFR, 'success');
              this.genericsService.getResource(`sectionses/${this.idSection}/cycles`).then((response: any) => {
                this.cycles = response._embedded.cycleses;
              }).catch(reason => {
                console.log(reason);
              });
            }).catch(reason => {
              console.log(reason);
              this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
            });
          }
        }
      });
  }
}
