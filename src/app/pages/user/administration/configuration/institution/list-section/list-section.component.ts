import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SectionRequestModel} from '../../../../../../shared/_models/request-dto/common/section-request-model';
import {DepartementRequest} from '../../../../../../shared/_models/request-dto/common/departement-request';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-section',
  templateUrl: './list-section.component.html',
  styleUrls: ['./list-section.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListSectionComponent implements OnInit, OnDestroy {
  public sections: any;
  public sectionForm: FormGroup;
  public listEleve: any;
  public submitted: boolean;
  public idEtab: string;
  private toUpdate: boolean;
  private currentSection: any;
  private base_url = 'admin/eleve/';
  public searchForm: FormGroup;
  public departementForm: FormGroup;
  public departements: any;
  public currentDepartement: any;
  public enseignants: any;
  public nommerApForm: FormGroup;
  public detailsEtab: any;
  public ERIC: string;
  public ROMUALD: string;
  validated: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  public enseignantsList: any;

  constructor(
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, private genericsService: GenericsService,
    private route: ActivatedRoute, private router: Router,
    public authService: AuthService,
    private translate: TranslateService
  ) {

    this.ROMUALD = localStorage.getItem('activelang') === 'en' ? 'The institution\'s departments ' : 'Les départements de l\'établissement ';
    // tslint:disable-next-line:no-unused-expression
    this.ERIC = localStorage.getItem('activelang') === 'en' ? 'The institution\'s sections ' : 'Les sections de l\'établissement ';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(39);
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.route.paramMap.subscribe(params => {
      this.idEtab = params.get('id');
      this.onDisplayDetailsEtablissement(this.idEtab);
      this.onLoadListSections(this.idEtab);
      this.onLoadListDepartements(this.idEtab);
      this.initFormSection();
      this.initFormDepartement();
      this.initFormNommination();
    });
  }

  get search() { return this.searchForm.controls; }

  private onLoadListSections(id: string) {
    this.genericsService.getResource(`etablissements/${id}/sections`).then((result: any) => {
      this.sections = result._embedded.sectionses;
    });
  }

  openLoadFormAddSection(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.submitted = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'add-section'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  get f() { return this.sectionForm.controls; }
  get dp() { return this.departementForm.controls; }
  get nap() { return this.nommerApForm.controls; }

  onResetSection() {
    this.submitted = false;
    this.initFormSection();
    this.toUpdate = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onSaveSection() {
    this.submitted = true;
    if (this.sectionForm.invalid) {
      return;
    }
    const sectionRequest = new SectionRequestModel(parseInt(this.idEtab, 0), this.currentSection ? this.currentSection.id_section : null,
      this.f.langue.value, this.f.nom.value);
    this.genericsService.postResource(`admin/ajouter/section`, sectionRequest).then((result: any) => {
      this.onLoadListSections(this.idEtab);
      this.onResetSection();
      localStorage.getItem('activelang') === 'en' ?
        // tslint:disable-next-line:max-line-length
      this.genericsService.confirmResponseAPI(AppConstants.SECTION_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.SECTION_UPDATEDFR, 'success');
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
    }).catch(reason => {
      this.genericsService.confirmModalResponseAPI(reason.error.message, 'Echec', 'error');
      console.log(reason);
    });
  }

  onSaveDepartement() {
    this.submitted = true;
    this.validated = true;
    if (this.departementForm.invalid) {
      this.validated = false;
      return;
    }
    const departementRequest = new DepartementRequest(parseInt(this.idEtab, 0),
      this.currentDepartement ? this.currentDepartement.id : 0,
      this.dp.nomDep.value, this.dp.specialite.value, this.dp.ap.value);
      console.log('===================================');
      console.log(this.dp.nomDep.value);
      console.log(this.dp.specialite.value);
      console.log('===================================');
    this.saveNommination(departementRequest);
  }

  onLoadDetailsSections(section: any, content: TemplateRef<any>) {
    this.currentSection = section;
    this.toUpdate = true;
    this.sectionForm.patchValue({
      langue: section.langue,
      nom: section.nom_section
    });
    this.modalService.open(content, {ariaLabelledBy: 'add-section', size: 'lg'})
      .result.then((resultModal) => {},
      (reason) => {});
  }

  private initFormSection() {
    this.submitted = false;
    this.toUpdate = false;
    this.sectionForm = this.fb.group({
      langue: ['', Validators.required],
      nom: ['', Validators.required]
    });
  }

  private initFormDepartement() {
    this.submitted = false;
    this.toUpdate = false;
    this.departementForm = this.fb.group({
      nomDep: ['', Validators.required],
      specialite: ['', Validators.required],
      ap: [0]
    });
  }
  private initFormNommination() {
    this.nommerApForm = this.fb.group({
      nomDep: ['', Validators.required],
      specialite: ['', Validators.required],
      ap: ['', Validators.required]
    });
  }

  public onLoadListDepartements(idEtab: string) {
    this.genericsService.getResource(`admin/departements/${idEtab}`).then((response: any) => {
      if (response.success === true) {
        this.departements = response.data;
        console.log(this.departements);
      }
    }).catch(reason => {
      console.log(reason);
    });
  }

  onOpenModalToEditDepartement(departement, content: TemplateRef<any>) {
    if (departement) {
      this.genericsService.getResource(`admin/consulter/${departement.id}/departement`).then((response: any) => {
        this.enseignants = response.data.enseignants;
        this.currentDepartement = departement;
        this.toUpdate = true;
        this.departementForm.patchValue({
          nomDep: this.currentDepartement.nomDepartement,
          specialite: this.currentDepartement.specialite
        });
      }).catch(reason => {
        console.log(reason);
      });
    } else  {
      this.toUpdate = false;
    }
    this.modalService.open(content, {ariaLabelledBy: 'edit-departement',
      scrollable: true}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onResetDepartement() {
    this.initFormDepartement();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.submitted = false;
    this.toUpdate = false;
    this.currentDepartement = null;
  }

  onConsulterDepartement(departement: any) {
    this.router.navigate(['administration/configuration/institutions/departements',
      departement.id]).then(() => {});
  }

  private ondisplayDetailsDepartement(dep: any, content) {
    this.genericsService.getResource(`admin/consulter/${dep.id}/departement`).then((response: any) => {
      if (response.success === true) {
        this.enseignants = response.data.enseignants;
        if (dep) {
          this.currentDepartement = dep;
          this.toUpdate = true;
          this.nommerApForm.patchValue({
            nomDep: this.currentDepartement.nomDepartement,
            specialite: this.currentDepartement.specialite
          });
        }
        this.modalService.open(content, {ariaLabelledBy: 'nommer-ap'})
          .result.then((resultModal) => {},
          (reason) => {});
      }
    }).catch(reason => {
      console.log(reason);
    });
  }
  onDelete(departement: any) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`${this.base_url}${departement.id}/delete`).then((resultDelete: any) => {
          this.onLoadListDepartements(this.idEtab);
          this.toUpdate = false;
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}${departement.id}/delete`).then((resultDelete: any) => {
            this.onLoadListDepartements(this.idEtab);
            this.toUpdate = false;
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }

  onNommerAP(departement: any, contentAP: TemplateRef<any>) {
    this.ondisplayDetailsDepartement(departement, contentAP);
  }

  onResetNomination() {
    this.initFormNommination();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.submitted = false;
    this.toUpdate = false;
  }

  onSaveNomination() {
    this.submitted = true;
    if (this.nommerApForm.invalid) {
      return;
    }
    const departementRequest = new DepartementRequest(parseInt(this.idEtab, 0),
      this.currentDepartement ? this.currentDepartement.id : 0,
      this.nap.nomDep.value, this.nap.specialite.value, this.nap.ap.value);
    this.saveNommination(departementRequest);
  }

  private saveNommination(departementRequest) {
    this.genericsService.postResource(`admin/ajouter/departement`, departementRequest).then((result: any) => {
      this.onLoadListDepartements(this.idEtab);
      this.onResetDepartement();
      localStorage.getItem('activelang') === 'en' ?
        // tslint:disable-next-line:max-line-length
      this.genericsService.confirmResponseAPI(AppConstants.DEPARTEMENT_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.DEPARTEMENT_UPDATEDFR, 'success');
      this.validated = false;
    }).catch(reason => {
      this.genericsService.confirmModalResponseAPI(reason.error.message, 'Echec', 'error');
      console.log(reason);
    });
  }

  private onDisplayDetailsEtablissement(idEtab: string) {
    this.genericsService.getResource(`admin/consulter/${idEtab}/etablissement`).then((response: any) => {
      this.detailsEtab = response.data;
      console.log(this.detailsEtab);
    }).catch(reason => {
      console.log(reason);
    });
  }

  ngOnDestroy(): void {
    this.onResetDepartement();
    this.onResetNomination();
    this.onResetSection();
  }
}
