import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-institution',
  templateUrl: './list-institution.component.html',
  styleUrls: ['./list-institution.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListInstitutionComponent implements OnInit, OnDestroy {
  private toUpdate = false;
  institutionForm: FormGroup;
  submitted: boolean;
  public institutions: any;
  public timestamp: number;
  public urlImage  = AppConstants.API_URL + 'admin/load/logo/';

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private genericsService: GenericsService, private fb: FormBuilder,
              private modalService: NgbModal, config: NgbModalConfig,
              public authService: AuthService,
              private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(36);
  }

  ngOnInit() {
    this.onLoadListInstitution();
  }

  open(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'add-institution', size: 'lg'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }
  private onLoadListInstitution() {
    this.genericsService.getResource(`institutions`).then((result: any) => {
      const firstInstitution = result._embedded.institutions[0];
      this.institutions = [];
      this.institutions.push(firstInstitution);
      this.timestamp = Date.now();
    });
  }
  get f() { return this.institutionForm.controls; }

  public traiterLogoInstitution(nom: string) {
    if (nom) {
      const idx = nom.indexOf(' ');
      if (idx > 0) {
        return nom.slice(0, idx + 2);
      } else {
        return nom;
      }
    } else {
      return '';
    }

  }

  getTS() {
    return this.timestamp;
  }

  ngOnDestroy(): void {
  }

}
