import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericsService } from '../../../../../../shared/_services/generics.service';
import { ActivatedRoute } from '@angular/router';
import { NiveauRequestModel } from '../../../../../../shared/_models/request-dto/common/niveau-request-model';
import { NiveauModel } from '../../../../../../shared/_models/response-dto/common/niveau-model';
import { ClasseRequestModel } from '../../../../../../shared/_models/request-dto/common/classe-request-model';
import { AuthService } from '../../../../../../shared/_services/auth.service';
import { AppConstants } from '../../../../../../shared/element/app.constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list-niveau',
  templateUrl: './list-niveau.component.html',
  styleUrls: ['./list-niveau.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListNiveauComponent implements OnInit, OnDestroy {
  public niveaux: any;
  submitted: boolean;
  public formNiveau: FormGroup;
  public formClasse: FormGroup;
  private toUpdate: boolean;
  private idCycle: string;
  public currentNiveau: NiveauModel;
  public enseignants: any;
  public idNiveau: number;
  public isNotifieAdd = false;
  public isNotifieEdit = false;
  public isNotifieAddNiveau = false;
  public isNotifieEditNiveau = false;
  public currentClasse: any;
  private base_url = 'admin/ajouter/';
  public detailCycle: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
   titulaire: any;

  constructor(
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, private genericsService: GenericsService,
    private route: ActivatedRoute, public authService: AuthService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(41);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idCycle = params.get('id');
      this.onloadDetailsCycle(this.idCycle);
      this.onLoadListNiveaux(this.idCycle);
      this.onLoadListEnseignant();
      this.initFormNiveau();
      this.initFormClasse();
    });
  }
  private onLoadListNiveaux(id: string) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`cycleses/${id}/niveaus`).then((result: any) => {
      this.niveaux = result._embedded.niveaus;
      console.log('niveaux', this.niveaux);
      this.genericsService.stopLoadingPage();
      if (this.isNotifieAdd === true) {
        this.isNotifieAdd = false;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.CLASSE_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.CLASSE_CREATEDFR, 'success');
      }
      if (this.isNotifieEdit === true) {
        this.isNotifieEdit = false;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.CLASSE_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.CLASSE_UPDATEDFR, 'success');
      }
      if (this.isNotifieAddNiveau === true) {
        this.isNotifieAddNiveau = false;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.NIVEAU_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.NIVEAU_CREATEDFR, 'success');
      }
      if (this.isNotifieEditNiveau === true) {
        this.isNotifieEditNiveau = false;
        localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.NIVEAU_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.NIVEAU_UPDATEDFR, 'success');
      }


    }).catch(reason => {
      this.genericsService.stopLoadingPage();
    });
  }
  openLoadFormAddNiveau(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.submitted = false;
    this.modalService.open(content,
      { ariaLabelledBy: 'add-niveau' }).result.then((result) => {
        console.log(result);
      }, (reason) => {
        console.log(reason);
      });
  }

  openLoadFormAddClasseToNiveau(niveau: any, content: TemplateRef<any>) {
    this.toUpdate = false;
    this.submitted = false;
    this.currentNiveau = niveau;
    this.idNiveau = niveau.id;
    this.modalService.open(content,
      { ariaLabelledBy: 'add-classe', size: 'lg' }).result.then((result) => {
        console.log(result);
      }, (reason) => {
        console.log(reason);
      });
  }

  private initFormNiveau() {
    this.formNiveau = this.fb.group({
      nom: ['', Validators.required],
      numOrdre: ['', Validators.required]
    });
  }
  private initFormClasse() {
    this.formClasse = this.fb.group({
      nomClasse: ['', Validators.required],
      serieClasse: [''],
      optionClasse: [''],
      specialiteClasse: ['', Validators.required],
      titulaire: ['']
    });
  }
  onSaveNiveau() {
    this.submitted = true;
    if (this.formNiveau.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const niveauRequest = new NiveauRequestModel(parseInt(this.idCycle, 0),
        this.currentNiveau.id, this.n.nom.value, this.n.numOrdre.value);
      this.genericsService.postResource(`${this.base_url}niveau`, niveauRequest).then((result: any) => {
        this.isNotifieEditNiveau = true;
        this.onLoadListNiveaux(this.idCycle);
        this.resetForm();
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    } else {
      const niveauRequest = new NiveauRequestModel(parseInt(this.idCycle, 0),
        null, this.n.nom.value, this.n.numOrdre.value);
      this.genericsService.postResource(`${this.base_url}niveau`, niveauRequest).then((result: any) => {
        this.isNotifieAddNiveau = true;
        this.onLoadListNiveaux(this.idCycle);
        this.resetForm();
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      }).catch(reason => {
        this.genericsService.confirmModalResponseAPI(reason.error.message, "Echec", 'error');
        console.log(reason);
      });
    }
  }

  onSaveClasse() {
    this.submitted = true;
    if (this.formClasse.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const classeRequest = new ClasseRequestModel(
        this.currentClasse.id, this.cl.titulaire.value ? this.cl.titulaire.value : null,
        this.cl.nomClasse.value, this.cl.optionClasse.value, this.cl.serieClasse.value,
        this.cl.specialiteClasse.value
      );
      this.genericsService.postResource(`admin/ajouter/${this.currentNiveau.id}/classe`, classeRequest).then((result: any) => {
        this.isNotifieEdit = true;
        this.onLoadListNiveaux(this.idCycle);
        this.resetForm();
        //this.genericsService.confirmResponseAPI(AppConstants.CLASSE_UPDATED, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      }).catch(reason => {
        this.genericsService.confirmModalResponseAPI(reason.error.message, "Echec", 'error');
        console.log(reason);
      });
    } else {
      const classeRequest = new ClasseRequestModel(
        null, this.cl.titulaire.value ? this.cl.titulaire.value : null,
        this.cl.nomClasse.value, this.cl.optionClasse.value, this.cl.serieClasse.value,
        this.cl.specialiteClasse.value
      );
      this.genericsService.postResource(`admin/ajouter/${this.currentNiveau.id}/classe`, classeRequest).then((result: any) => {
        this.isNotifieAdd = true;
        this.onLoadListNiveaux(this.idCycle);
        this.resetForm();
        //this.genericsService.confirmResponseAPI(AppConstants.CLASSE_CREATED, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      }).catch(reason => {
        console.log(reason);
        this.genericsService.confirmResponseAPI(reason.error.message, 'error');
      });
    }
  }

  onLoadDetailsNiveaux(niveau: any, content: TemplateRef<any>) {
    this.currentNiveau = niveau;
    this.toUpdate = true;
    this.formNiveau.patchValue({
      nom: niveau.nomNiveau,
      numOrdre: niveau.numeroOrdre
    });
    this.modalService.open(content, { ariaLabelledBy: 'add-niveau' })
      .result.then((resultModal) => { },
        (reason) => { });
  }

  onLoadDetailsClasse(niveau: any, classe: any, content: TemplateRef<any>) {
    this.currentClasse = classe;
    this.currentNiveau = niveau;
    this.toUpdate = true;
    console.log(classe);
    console.log(niveau);
    // this.getTitulaire(classe.id);
    console.log(this.titulaire);

    this.genericsService.getResource(`classes/${classe.id}/titulaire`).then((response: any) => {
      console.log(response);
      this.titulaire = response;
      this.formClasse.patchValue({
        nomClasse: classe.nomClasse,
        serieClasse: classe.serie,
        optionClasse: classe.optionClasse,
        specialiteClasse: classe.specialite,
        titulaire: this.titulaire.id_enseignant
      });
    });
    this.modalService.open(content, { ariaLabelledBy: 'add-classe', size: 'lg' })
      .result.then((resultModal) => { },
        (reason) => { });
  }

  onResetNiveau() {
    this.resetForm();
    this.toUpdate = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onResetClasse() {
    this.resetForm();
    this.toUpdate = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  get n() { return this.formNiveau.controls; }
  get cl() { return this.formClasse.controls; }

  private onLoadListEnseignant() {
    this.genericsService.getResource(`enseignants`).then((result: any) => {
      this.enseignants = result._embedded.enseignants;
      console.log(this.enseignants);
    });
  }

  private resetForm() {
    this.submitted = false;
    this.initFormClasse();
    this.initFormNiveau();
  }

  private onloadDetailsCycle(idCycle: string) {
    this.genericsService.getResource(`admin/consulter/${idCycle}/cycle`).then((response: any) => {
      this.detailCycle = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
getTitulaire(idClass) {
    this.genericsService.getResource(`classes/${idClass}/titulaire`).then((response: any) => {
      console.log(response);
      this.titulaire = response;
    })
}
  ngOnDestroy(): void {
    this.onResetClasse();
    this.onResetNiveau();
  }
}
