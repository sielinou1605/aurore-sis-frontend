import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {ActivatedRoute} from '@angular/router';
import {EtablissementRequestModel} from '../../../../../../shared/_models/request-dto/common/etablissement-request-model';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-etablissement',
  templateUrl: './list-etablissement.component.html',
  styleUrls: ['./list-etablissement.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListEtablissementComponent implements OnInit, OnDestroy {
  public etablissements: any;
  private idInstitution: string;
  formEtablissement: FormGroup;
  private toUpdate: boolean;
  public submitted: boolean;
  private currentEtablissement: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private modalService: NgbModal, config: NgbModalConfig,
              private fb: FormBuilder, private genericsService: GenericsService,
              private route: ActivatedRoute,
              public authService: AuthService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(37);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idInstitution = params.get('id');
      this.onLoadListEtablissement(params.get('id'));
      this.initFormEtablissement();
    });
  }

  openLoadFormAddEtablissement(content: TemplateRef<any>) {
    this.toUpdate = false;
    this.submitted = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'add-etablissement', size: 'lg'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  private onLoadListEtablissement(id: string) {
    this.genericsService.getResource(`institutions/${id}/etablissements`).then((result: any) => {
      this.etablissements = result._embedded.etablissements;
      console.log(this.etablissements);
    });
  }

  get f() { return this.formEtablissement.controls; }

  onSaveEtablissement() {

    console.log("Sauvagarde de letablissement")
    this.submitted = true;
    if (this.formEtablissement.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const etabRequest = new EtablissementRequestModel(this.f.code.value,
        this.f.decret.value, this.f.email.value, this.currentEtablissement.id, this.idInstitution,
        this.f.nom.value, this.f.telephone1.value, this.f.telephone2.value);
      this.genericsService.postResource(`admin/ajouter/etablissement`, etabRequest).then((result: any) => {
        this.onLoadListEtablissement(this.idInstitution);
        this.resetForm();
        this.toUpdate = false;
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.ETABLISSEMENT_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.ETABLISSEMENT_UPDATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    } else {
      const etabRequest = new EtablissementRequestModel(this.f.code.value,
        this.f.decret.value, this.f.email.value, null, this.idInstitution,
        this.f.nom.value, this.f.telephone1.value, this.f.telephone2.value);
      this.genericsService.postResource(`admin/ajouter/etablissement`, etabRequest).then((result: any) => {
        this.onLoadListEtablissement(this.idInstitution);
        this.resetForm();
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.ETABLISSEMENT_CREATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.ETABLISSEMENT_CREATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      }).catch(reason => {
        this.genericsService.confirmModalResponseAPI(reason.error.message, "Echec", 'error');
        console.log(reason);
      });
    }
  }

  onLoadDetailsEtablissement(etab: any, content: TemplateRef<any>) {
    this.currentEtablissement = etab;
    this.toUpdate = true;
    this.formEtablissement.patchValue({
      nom: etab.nomEtablissement,
      code: etab.codeEtablissement,
      decret: etab.decretCreation,
      email: etab.email,
      telephone1: etab.telephone1,
      telephone2: etab.telephone2
    });
    this.modalService.open(content, {ariaLabelledBy: 'add-etablissement', size: 'lg'})
      .result.then((resultModal) => {},
      (reason) => {});
  }

  onResetEtablissement() {
    this.resetForm();
    this.toUpdate = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  resetForm() {
    this.submitted = false;
    this.initFormEtablissement();
  }

  private initFormEtablissement() {
    this.formEtablissement = this.fb.group({
      nom: ['', Validators.required],
      code: ['', Validators.required],
      decret: [''],
      email: [''],
      telephone1: ['', Validators.required],
      telephone2: ['']
    });
  }

  ngOnDestroy(): void {
    this.onResetEtablissement();
  }
}
