import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstitutionComponent} from './institution.component';
import {ListInstitutionComponent} from './list-institution/list-institution.component';
import {ListEtablissementComponent} from './list-etablissement/list-etablissement.component';
import {ListSectionComponent} from './list-section/list-section.component';
import {ListCycleComponent} from './list-cycle/list-cycle.component';
import {ListNiveauComponent} from './list-niveau/list-niveau.component';
import {EditerInstitutionComponent} from './editer-institution/editer-institution.component';
import {DetailsDepartementComponent} from './details-departement/details-departement.component';


const routes: Routes = [
  {
    path: '', component: InstitutionComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Institutions',
      // titre: 'userEleveAjaout.Institutions_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Institutions | Aurore' : 'Institutions | Aurore',
      icon: 'icofont-university bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Liste_des_institutions_de_la_plateforme_Institutions',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'list-institution',
        pathMatch: 'full'
      },
      {
        path: 'list-institution', component: ListInstitutionComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Institutions',
          // titre: 'userEleveAjaout.Institutions_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Institutions | Aurore' : 'Institutions | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_institutions_de_la_plateforme_Institutions',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'list-etablissement/:id', component: ListEtablissementComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Etablissements',
          // titre: 'userEleveAjaout.Etablissements_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Establishments | Aurore' : 'Etablissements | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_établissements_de_l_institution_Etablissements',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'list-section/:id', component: ListSectionComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Sections',
          // titre: 'userEleveAjaout.Sections_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Sections | Aurore' : 'Sections | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_sections_de_l_établissement_Sections',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'list-cycle/:id', component: ListCycleComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Cycles',
          titre: localStorage.getItem('activelang') === 'en' ? 'Cycle | Aurore' : 'Cycles | Aurore',
          // titre: 'userEleveAjaout.Cycles_Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_cycles_de_la_section_cycles',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'list-niveau/:id', component: ListNiveauComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Niveaux',
          // titre: 'userEleveAjaout.Niveaux_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Level | Aurore' : 'Niveaux | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_niveaux_du_cycle_Niveaux',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'editer-institution/:id', component: EditerInstitutionComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Institutions',
          // titre: 'userEleveAjaout.Institutions_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Institutions | Aurore' : 'Institutions | Aurore',
          icon: 'icofont-university bg-warning',
          breadcrumb_caption: 'userEleveAjaout.Modifier_les_attributs_de_l_institution_Institutions',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'departements/:id', component: DetailsDepartementComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Departements',
          // titre: 'userEleveAjaout.Departements_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Departments | Aurore' : 'Departements | Aurore',
          icon: 'icofont-university bg-warning',
          breadcrumb_caption: 'userEleveAjaout.Consulter_le_departement_Matieres_par_discipline_et_enseignants_Institutions',
          status: true,
          current_role: 'admin'
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstitutionRoutingModule { }
