/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {
  AjouterEtat,
  AjouterFiltreEtatModel,
  EtatImprimableModel,
  Filtre,
  ModifierEtatModel,
  ParamImpressionModel
} from '../../../../../shared/_models/request-dto/common/etats-model';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-gestion-etats',
  templateUrl: './gestion-etats.component.html',
  styleUrls: ['./gestion-etats.component.scss']
})
export class GestionEtatsComponent implements OnInit, OnDestroy {
  etats: any;
  etatForm: FormGroup;
  currentEtat: any;
  etatsData: any;
  submittedEdit: boolean;
  isExcel = false;
  // @ts-ignore
  @ViewChild('selectFiltre') selectFiltre;
  // @ts-ignore
  @ViewChild('selectVue') selectVue;
  public dataFiltre: any;
  public settings = {};
  public selectedFiltres = [];
  printEtatForm: FormGroup;
  submittedPrint: boolean;
  public availableFiltre: any;
  public fenetres: any;
  public modeleImpressions: any;
  public ownerFiltre: any;
  public settingsVue = {};
  public dataVue: any;

  public totalElements = 0;
  public pageSize = 20;
  public page = 0;
  validated: boolean;
  private searchvalue: string = '';
  public placeholder: string;
  public siteLanguage: string;
  public CHOIX: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private genericsService: GenericsService, private modalService: NgbModal,
              config: NgbModalConfig, private fb: FormBuilder,
              public authService: AuthService,
              private translate: TranslateService) {
  this.CHOIX = localStorage.getItem('activelang') === 'en' ? 'Choose the associated view' : 'Choisir la vue associée';
    localStorage.getItem('activelang') === 'en'
      ? this.placeholder = 'Choose filters'
      : this.placeholder = 'Choisir les filtres';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(82);
  }

  ngOnInit() {
    localStorage.getItem('activelang') === 'en' ?
    this.settings = {
      singleSelection: false, idField: 'id', textField: AppConstants.DESCRIPTIONEN, enableCheckAll: true,
      selectAllText: AppConstants.SELECT_ALLEN, unSelectAllText: AppConstants.UNSELECT_ALLEN, allowSearchFilter: true,
      limitSelection: -1, clearSearchFilter: true, maxHeight: 180, itemsShowLimit: 8,
      searchPlaceholderText: AppConstants.SEARCHEN, noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEEN,
      closeDropDownOnSelection: false, showSelectedItemsAtTop: false, defaultOpen: false
    } : this.settings = {
        singleSelection: false, idField: 'id', textField: AppConstants.DESCRIPTIONFR, enableCheckAll: true,
        selectAllText: AppConstants.SELECT_ALLFR, unSelectAllText: AppConstants.UNSELECT_ALLFR, allowSearchFilter: true,
        limitSelection: -1, clearSearchFilter: true, maxHeight: 180, itemsShowLimit: 8,
        searchPlaceholderText: AppConstants.SEARCHFR, noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEFR,
        closeDropDownOnSelection: false, showSelectedItemsAtTop: false, defaultOpen: false
      };
    localStorage.getItem('activelang') === 'en' ?
    this.settingsVue = {
      singleSelection: true, idField: 'id', textField: AppConstants.DESCRIPTIONEN,
      enableCheckAll: true, allowSearchFilter: true, limitSelection: -1,
      clearSearchFilter: true, maxHeight: 180, itemsShowLimit: 8,
      searchPlaceholderText: AppConstants.SEARCHEN, noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEEN,
      closeDropDownOnSelection: false, showSelectedItemsAtTop: true, defaultOpen: false
    } : this.settingsVue = {
        singleSelection: true, idField: 'id', textField: AppConstants.DESCRIPTIONFR,
        enableCheckAll: true, allowSearchFilter: true, limitSelection: -1,
        clearSearchFilter: true, maxHeight: 180, itemsShowLimit: 8,
        searchPlaceholderText: AppConstants.SEARCHFR, noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEFR,
        closeDropDownOnSelection: false, showSelectedItemsAtTop: true, defaultOpen: false
      };
    this.onloadListEtat();
    this.onloadListFenetre();
    this.onloadAvailableFiltre();
    this.initFormEtat();
    this.initFormPrintEtat();
  }


  onEditEtat(etat, modalEditEtat: TemplateRef<any>) {
    this.currentEtat = etat;
    if (etat) {
      this.onloadOwnerFiltre();
      this.etatForm.patchValue({
        libelle: etat.libelle ,
        description: etat.description,
        groupe: etat.groupe,
        vue: [{id: etat.fenetre.id, description: etat.fenetre.libelleFenetre}],
        chemin: etat.chemin,
      });
    }
    this.modalService.open(modalEditEtat,
      {ariaLabelledBy: 'edit-etat', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {});
  }

  onOpenPrintModal(etat, modalPrintEtat: TemplateRef<any>) {
    if (etat) {
      this.currentEtat = etat;
      this.onloadModelesImpressions();
      this.modalService.open(modalPrintEtat,
        {ariaLabelledBy: 'print-etat', size: 'lg', scrollable: true}).result.then((result) => {
      }, (reason) => {});
    }
  }


  private onloadListEtat() {
    this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(`parametre/etat/liste/paged?size=${this.pageSize}&page=${this.page}`)
      .then((resultEtat: any) => {
        if (resultEtat.success === true) {
          this.etats = resultEtat.data.content;
          this.etatsData = resultEtat.data.content;
          this.totalElements = resultEtat.data.totalElements;
        }
        this.genericsService.stopLoadingPage();
      }).catch((errEtat: any) => {
        this.genericsService.stopLoadingPage();
        console.log(errEtat);
      });
  }


  private onloadListEta() {
    this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(`parametre/etat/liste/paged?size=${this.pageSize}&page=${this.page}`)
      .then((resultEtat: any) => {
        if (resultEtat.success === true) {
          this.etats = resultEtat.data.content;
          this.totalElements = resultEtat.data.totalElements;
        }
        this.genericsService.stopLoadingPage();
      }).catch((reason: any) => {
      this.genericsService.stopLoadingPage();
      console.log(reason);
    });
  }

  onChangeValueSearch(event: any) {
    console.log(event.target.value);
    this.searchvalue = event.target.value;
  }

  setPageChange(event) {
    this.page = event - 1;
    this.onloadListEtat();
  }
  private onloadListFenetre() {
    this.genericsService.getResource(`systeme/fenetre/liste`).then((resultFenetre: any) => {
      if (resultFenetre.success === true) {
        this.dataVue = [];
        this.fenetres = resultFenetre.data;
        this.fenetres.forEach(fenetre => {
          this.dataVue.push({ id: fenetre.id, description: fenetre.libelleFenetre });
        });
      }
    }).catch((errFenetre: any) => {
      console.log(errFenetre);
    });
  }
  private onloadAvailableFiltre() {
    this.genericsService.getResource(`parametre/etat/filtre/disponible`)
      .then((resultFiltre: any) => {
        this.dataFiltre = [];
        this.availableFiltre = resultFiltre.data;
        this.availableFiltre.forEach(filtre => {
          this.dataFiltre.push({id: filtre.id, description: filtre.description});
        });
      }).catch((reason: any) => {
      console.log(reason);
    });
  }
  private onloadOwnerFiltre() {
    console.log(this.currentEtat);
    this.genericsService.getResource(`parametre/etat/${this.currentEtat.id}/filtre/liste`)
      .then((resultFiltre: any) => {
        this.ownerFiltre = resultFiltre.data.filtres;
        console.log('owner filtre', this.ownerFiltre);
        this.selectedFiltres = [];
        this.ownerFiltre.forEach(filtre => {
          this.selectedFiltres.push({id: filtre.filtre.id, description: filtre.filtre.description});
        });
        this.etatForm.patchValue({
          filtres: this.selectedFiltres
        });
      }).catch((errFiltre: any) => {
      console.log(errFiltre);
    });
  }
  private onloadModelesImpressions() {
    this.genericsService.getResource(`parametre/etat/${this.currentEtat.id}/modele/liste`).then((resultModeleI: any) => {
      this.modeleImpressions = resultModeleI.data;
      console.log(this.modeleImpressions);
    }).catch((reason: any) => {
      console.log(reason);
    });
  }
  public onDeleteEtat() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`parametre/etat/${this.currentEtat.id}/supprimer`)
          .then((resultDeleteEtat: any) => {
            console.log(resultDeleteEtat);
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
          }).catch((errEtat: any) => {
          console.log(errEtat);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) :  Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`parametre/etat/${this.currentEtat.id}/supprimer`)
            .then((resultDeleteEtat: any) => {
              console.log(resultDeleteEtat);
              this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
            }).catch((errEtat: any) => {
            console.log(errEtat);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }
  private initFormEtat() {
    this.etatForm = this.fb.group({
      libelle: ['', Validators.required],
      description: [''],
      groupe: ['', Validators.required],
      vue: [[], Validators.required],
      chemin: ['', Validators.required],
      filtres: [[]]
    });
  }
  get ef() { return this.etatForm.controls; }

  private initFormPrintEtat() {
    this.printEtatForm = this.fb.group({
      choisirEtat: ['', Validators.required],
      cat: ['', Validators.required],
      datePrint: ['']
    });
  }
  get ep() { return this.printEtatForm.controls; }
  onSaveEditEtat() {
    this.submittedEdit = true;
    this.validated = true;
    if (this.etatForm.invalid) {
      this.validated = false;
      return;
    }
    if (this.currentEtat) {
      const dto = new ModifierEtatModel(
        this.ef.chemin.value, this.ef.description.value, this.isExcel == true ? 1 : 0,
        this.ef.groupe.value, this.currentEtat.id, this.ef.vue.value[0].id, this.ef.libelle.value, 1);
      this.genericsService.putResource(`parametre/etat/modifier`, dto).then((resultSaveEtat: any) => {
        this.validated = false;
        this.onResetEditEtat();
        this.onloadListEtat();
        this.genericsService.confirmResponseAPI(resultSaveEtat.message, 'success');
      }).catch((errSaveEtat: any) => {
        this.validated = false;
        console.log(errSaveEtat);
      });
    } else {
      const dto = new AjouterEtat(
        this.ef.vue.value[0].id, [new EtatImprimableModel(
          this.ef.chemin.value, this.ef.description.value, this.isExcel == true ? 1 : 0,
          this.ef.groupe.value, this.ef.libelle.value, 1)]);
      this.genericsService.postResource(`parametre/etat/ajouter`, dto).then((resultSaveEtat: any) => {
        this.validated = false;
        this.onResetEditEtat();
        this.onloadListEtat();
        console.log('etat enregistré', resultSaveEtat);
        this.genericsService.confirmResponseAPI(resultSaveEtat.message, 'success');
      }).catch((errSaveEtat: any) => {
        console.log(errSaveEtat);
        this.validated = false;
      });
    }
  }


  onPrintEtatType(type: boolean) {
    console.log(type);
    this.submittedPrint = true;
    if (this.printEtatForm.invalid) {
      return;
    }
    const dto = new ParamImpressionModel(type, this.currentEtat.id, null);
    this.genericsService.printPostReport(`parametre/etat/imprimer`, dto, `CERTIFICAT_SCOLARITE`);
  }
  onResetEditEtat() {
    this.submittedEdit = false;
    this.initFormEtat();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onResetPrintEtat() {
    this.submittedPrint = false;
    this.initFormPrintEtat();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onSelectFiltre(event: any) {
    this.onAddFiltresToEtat([event]);
  }

  onDeselectFiltre(event: any) {
    const filtre = this.ownerFiltre.find(item => item.filtre.id == event.id);
    this.onDeleleFiltreEtat(filtre.idFiltreEtat);
  }

  onSelectAllFiltre(event: Array<any>) {
    this.onAddFiltresToEtat(event);
  }

  onDeSelectAllFiltre(event: Array<any>) {
    event.forEach((item, i) => {
      const filtre = this.ownerFiltre.find((elt) => item.filtre.id == elt.id);
      this.genericsService.deleteResource(`parametre/filtre-etat/${filtre.idFiltreEtat}/supprimer`)
        .then((response: any) => {
          console.log('Supprimé !!!', response);
          if (i == this.ownerFiltre.length - 1) {
            this.onloadOwnerFiltre();
          }
        }).catch((reason: any) => {
        console.log('erreur capturé', reason);
      });
      this.onDeleleFiltreEtat(filtre.id);
    });
  }

  onAddFiltresToEtat(filtres: Filtre[]) {
    const dto = new AjouterFiltreEtatModel(this.currentEtat.id, filtres);
    this.genericsService.postResource(`parametre/etat/${this.currentEtat.id}/filtre/ajouter`, dto)
      .then((response: any) => {
        console.log('ajouté', response);
        this.onloadOwnerFiltre();
      }).catch((errAddFiltre: any) => {
      console.log('erreur capturé', errAddFiltre);
    });
  }

  onDeleleFiltreEtat(event: any) {
    console.log(event);
    this.genericsService.deleteResource(`parametre/filtre-etat/${event}/supprimer`)
      .then((response: any) => {
        this.onloadOwnerFiltre();
        console.log('filtre supprimé', response);
      }).catch((reason: any) => {
      console.log('erreur capturé', reason);
    });
  }

  ngOnDestroy(): void {
    this.onResetEditEtat();
    this.onResetPrintEtat();
  }

  onChangeOptionsVue() {

  }

  onSelectVue(event: any) {

  }

  onDeselectVue(event: any) {

  }

  onChangeOptions() {

  }

  onSearchEtats() {
    if (this.authService.isActif('bouton-recherche-etat')) {
      console.log(this.etatsData);
      console.log("Recherche des etats")
    }
  }

  onSearchEtat() {
    console.log(this.etatsData)
    if (this.authService.isActif('bouton-recherche-etat')) {
      var predicat = new RegExp(this.searchvalue, 'i'); // prepare a regex object
      this.etats = this.etatsData.filter(
        item => (
          predicat.test((this.siteLanguage == 'en' )? item.description_en : item.description)
          || predicat.test((this.siteLanguage == 'en' ) ? item.libelle_en : item.libelle)
          || predicat.test(item.groupe))
)}
 }
}
