import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GestionEtatsComponent} from './gestion-etats.component';

describe('GestionEtatsComponent', () => {
  let component: GestionEtatsComponent;
  let fixture: ComponentFixture<GestionEtatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionEtatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionEtatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
