/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AnneeRequestModel} from '../../../../../../shared/_models/request-dto/common/annee-request-model';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {TokenStorageService} from '../../../../../../shared/_services/token-storage.service';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {UserLoggedModel} from '../../../../../../shared/_models/auth/user-logged-model';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-annees',
  templateUrl: './list-annees.component.html',
  styleUrls: ['./list-annees.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListAnneesComponent implements OnInit, OnDestroy {
  public annees: any;
  anneeForm: FormGroup;
  submitted: boolean;
  public toUpdate: boolean;
  private currentAnnee: any;
  private base_url = 'admin/';
  activateAnneeForm: FormGroup;
  validated: boolean;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  public lesAnneesScolairesDispo = localStorage.getItem('activelang') === 'fr' ? 'Les années scolaires disponibles' : 'Available school years';

  constructor(
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, public genericsService: GenericsService,
    public authService: AuthService, private tokenStorage: TokenStorageService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(43);
  }

  ngOnInit() {
    this.initFormAnnee();
    this.initActivateAnnee();
    this.onLoadListAnnee();
  }

  private initFormAnnee() {
    this.anneeForm = this.fb.group({
      codeAnnee: ['', Validators.required],
      ouverture: ['', Validators.required],
      fermeture: ['', Validators.required]
    });
  }
  private onLoadListAnnee() {
    this.genericsService.getResource(`annees`).then((result: any) => {
      this.annees = result._embedded.annees;
    });
  }

  open(content: TemplateRef<any>) {
    this.onResetAnnee();
    this.modalService.open(content,
      {ariaLabelledBy: 'add-annee', size: 'lg'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onLoadDetailsAnnee(annee: any, content: TemplateRef<any>) {
    this.currentAnnee = annee;
    this.toUpdate = true;
    console.log(annee);
    this.anneeForm.patchValue({
      codeAnnee: annee.codeAnnee,
      ouverture: annee.dateOuverture,
      fermeture: annee.dateFermeture
    });
    this.modalService.open(content, {ariaLabelledBy: 'add-annee', size: 'lg'})
      .result.then((resultModal) => {},
      (reason) => {});
  }

  onSaveAnnee() {
    this.submitted = true;
    if (this.anneeForm.invalid) {
      return;
    }
    if (this.toUpdate === true) {
      const anneUpdate = new AnneeRequestModel(
        this.f.codeAnnee.value, this.f.fermeture.value, this.f.ouverture.value, true, this.currentAnnee.id
      );
      console.log('anneUpdate', anneUpdate);
      this.genericsService.postResource(`${this.base_url}ajouter-annee`, anneUpdate).then((result: any) => {
        this.onLoadListAnnee();
        this.toUpdate = false;
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.ANNEE_UPDATEDEN, 'success')
          : this.genericsService.confirmResponseAPI(AppConstants.ANNEE_UPDATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    } else {
      const anneCreate = new AnneeRequestModel(
        this.f.codeAnnee.value, this.f.fermeture.value, this.f.ouverture.value, false, null
      );
      console.log('anneCreate', anneCreate);
      this.genericsService.postResource(`${this.base_url}ajouter-annee`, anneCreate).then((result: any) => {
        this.onLoadListAnnee();
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.ANNEE_CREATEDEN, 'success')
          : this.genericsService.confirmResponseAPI(AppConstants.ANNEE_CREATEDFR, 'success');
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
    }
  }

  get f() { return this.anneeForm.controls; }
  get g() { return this.activateAnneeForm.controls; }

  onResetAnnee() {
    this.submitted = false;
    this.toUpdate = false;
    this.validated = false;
    this.initFormAnnee();
    this.initActivateAnnee();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.ESC);
    }
  }

  onChangeStateAnnee(annee: any, content: TemplateRef<any>) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success mr-2',
        cancelButton: 'btn btn-danger ml-2'
      },
      buttonsStyling: false
    });


    localStorage.getItem('activelang') === 'en' ?
      swalWithBootstrapButtons.fire({
        title: AppConstants.ARE_U_SUREEN,
        text: AppConstants.IRREVERSIBLEEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: AppConstants.YES_CHANGE_ITEN,
        cancelButtonText: AppConstants.NO_DONT_CHANGE_ITEN,
        reverseButtons: true
      }).then((result1) => {
        if (result1.isConfirmed) {
          Swal.fire({
            title: AppConstants.BON_A_SAVOIREN,
            html: AppConstants.HTML_NOTIFICATION_TRANSFERTEN,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: AppConstants.YES_CHANGE_ITEN
          }).then((result2) => {
            if (result2.isConfirmed) {
              this.modalService.open(content, {ariaLabelledBy: 'active-annee'})
                .result.then((resultModal) => {},
                (reason) => {});
            }
          });
        } else if (result1.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            AppConstants.CANCELEN,
            AppConstants.AUCUNE_MODIF_APPORTEEN,
            'error'
          );
        }
      }) :  swalWithBootstrapButtons.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: AppConstants.YES_CHANGE_ITFR,
        cancelButtonText: AppConstants.NO_DONT_CHANGE_ITFR,
        reverseButtons: true
      }).then((result1) => {
        if (result1.isConfirmed) {
          Swal.fire({
            title: AppConstants.BON_A_SAVOIRFR,
            html: AppConstants.HTML_NOTIFICATION_TRANSFERTFR,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: AppConstants.YES_CHANGE_ITFR
          }).then((result2) => {
            if (result2.isConfirmed) {
              this.modalService.open(content, {ariaLabelledBy: 'active-annee'})
                .result.then((resultModal) => {},
                (reason) => {});
            }
          });
        } else if (result1.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            AppConstants.CANCELFR,
            AppConstants.AUCUNE_MODIF_APPORTEFR,
            'error'
          );
        }
      });
  }



  validChangeStateAnnee() {
    this.submitted = true;
    this.validated = true;
    if (this.activateAnneeForm.invalid) {
      this.validated = false;
      return;
    }
    const selectYear = this.annees.find(item => item.id == this.g.nextYear.value);
    if (selectYear.codeAnnee == this.tokenStorage.getUser().codeAnnee) {
      this.validated = false;
      this.onResetAnnee();
      localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmModalResponseAPI( AppConstants.CANNOT_CHANGEEN + selectYear.codeAnnee +
          AppConstants.PARTIAL_CURRENT_YEAREN + this.tokenStorage.getUser().codeAnnee,
          AppConstants.ERROR_CHANGE_YEAREN, 'error') :
        this.genericsService.confirmModalResponseAPI( AppConstants.CANNOT_CHANGEFR + selectYear.codeAnnee +
          AppConstants.PARTIAL_CURRENT_YEARFR + this.tokenStorage.getUser().codeAnnee, AppConstants.ERROR_CHANGE_YEARFR, 'error');
    } else {
      this.genericsService.postResource(
        `admin/annee/${this.g.nextYear.value}/demarrer`, {}
      ).then((response: any) => {
        this.validated = false;
        this.onResetAnnee();
        const codeAnnee = response.data.codeAnnee;
        const user: UserLoggedModel = this.tokenStorage.getUser();
        user.codeAnnee = codeAnnee;
        this.tokenStorage.saveUser(user);
        this.onLoadListAnnee();
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmModalResponseAPI(
            AppConstants.CHANGE_SUCCESSEN, AppConstants.TERMINATEDEN, 'success') : this.genericsService.confirmModalResponseAPI(
          AppConstants.CHANGE_SUCCESSFR, AppConstants.TERMINATEDFR, 'success');
      }).catch(reason => {
        console.log(reason);
        this.onResetAnnee();
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmModalResponseAPI(
            AppConstants.CHANGE_ERROREN, AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmModalResponseAPI(
          AppConstants.CHANGE_ERRORFR, AppConstants.UNKNOWN_LOADEDFR, 'error');
      });
    }
  }
  ngOnDestroy(): void {
    this.onResetAnnee();
  }

  private initActivateAnnee() {
    this.activateAnneeForm = this.fb.group({
      nextYear: ['', Validators.required]
    });
  }
}
