import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListAnneesComponent} from './list-annees.component';

describe('ListAnneesComponent', () => {
  let component: ListAnneesComponent;
  let fixture: ComponentFixture<ListAnneesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAnneesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAnneesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
