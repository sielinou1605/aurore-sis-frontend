import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PeriodesComponent} from './periodes.component';
import {ListAnneesComponent} from './list-annees/list-annees.component';
import {ListTrimestreComponent} from './list-trimestre/list-trimestre.component';


const routes: Routes = [
  {
    path: '', component: PeriodesComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Periodes',
      // titre: 'userEleveAjaout.Periodes_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Periods | Aurore' : 'Periodes | Aurore',
      icon: 'icofont-ui-calendar bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Liste_des_périodes_Periodes',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'list-annees',
        pathMatch: 'full'
      },
      {
        path: 'list-annees', component: ListAnneesComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Periodes',
         // titre: 'userEleveAjaout.Periodes_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Periods | Aurore' : 'Periodes | Aurore',
            icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_années_Periodes',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'list-trimestres/:id', component: ListTrimestreComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Trimestres',
          // titre: 'userEleveAjaout.Trimestres_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Trimesters | Aurore' : 'Trimestres | Aurore',
          icon: 'icofont-ui-calendar bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_trimetres_Periodes',
          status: true,
          current_role: 'admin'
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeriodesRoutingModule { }
