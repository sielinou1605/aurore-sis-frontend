import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PeriodesRoutingModule} from './periodes-routing.module';
import {PeriodesComponent} from './periodes.component';
import {ListAnneesComponent} from './list-annees/list-annees.component';
import {ListTrimestreComponent} from './list-trimestre/list-trimestre.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [
    PeriodesComponent,
    ListAnneesComponent,
    ListTrimestreComponent
  ],
    imports: [
        CommonModule,
        PeriodesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class PeriodesModule { }
