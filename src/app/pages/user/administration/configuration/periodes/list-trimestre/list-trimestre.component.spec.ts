import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListTrimestreComponent} from './list-trimestre.component';

describe('ListTrimestreComponent', () => {
  let component: ListTrimestreComponent;
  let fixture: ComponentFixture<ListTrimestreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTrimestreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTrimestreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
