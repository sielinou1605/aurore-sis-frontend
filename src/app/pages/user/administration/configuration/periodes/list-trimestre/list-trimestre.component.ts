import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {ConveertDateService} from '../../../../../../shared/_helpers/conveert-date.service';
import {TrimestreRequestModel} from '../../../../../../shared/_models/request-dto/common/trimestre-request-model';
import {SequenceRequestModel} from '../../../../../../shared/_models/request-dto/common/sequence-request-model';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {MustMatch, MustMatchDate} from '../../../../../../shared/_helpers/mustmatch-validator';
import {TranslateService} from '@ngx-translate/core';
import {AppConstants} from "../../../../../../shared/element/app.constants";
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-list-trimestre',
  templateUrl: './list-trimestre.component.html',
  styleUrls: ['./list-trimestre.component.scss']
})
export class ListTrimestreComponent implements OnInit, OnDestroy {
  private idAnnee: string;
  private idAnneeSource: string;
  public trimestres: any;
  public reconduire = false;
  public formTrimestre: FormGroup;
  public submitted: boolean;
  public formSequence: FormGroup;
  private toUpdate: boolean;
  private idTrimestre: number;
  private base_url = 'admin/ajouter/';
  public currentTrimestre: any;
  public currentSequence: any;
  public annees: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private modalService: NgbModal, config: NgbModalConfig,
    private fb: FormBuilder, private genericsService: GenericsService,
    private dateConvert: ConveertDateService, private calendar: NgbCalendar,
    private route: ActivatedRoute, public authService: AuthService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(44);
  }

  ngOnInit() {
    this.initFormTrimestre();
    this.initFormSequence();
    this.route.paramMap.subscribe(params => {
      this.idAnnee = params.get('id');
      this.onLoadListTrimestre(this.idAnnee);
    });
    this.onLoadListAnnees();
  }
  onLoadListAnnees() {
    this.genericsService.getResource(`annees?size=100`).then((result: any) => {
      this.annees = result._embedded.annees.filter(item=>item.id!=this.idAnnee);
      this.idAnneeSource = this.annees[0].id;
      // console.log(this.annees);
    });
  }
  private onLoadListTrimestre(idAnnee: string) {
    this.genericsService.getResource(`annees/${idAnnee}/trimestres`).then((result: any) => {
      this.trimestres = result._embedded.trimestres;
      if (this.trimestres && this.trimestres.length<=0) this.reconduire = true;
        else this.reconduire =false;
      // console.log(this.trimestres);
    });
  }

  openLoadFormAddTrimestre(trimestre: any, contentTimestre: TemplateRef<any>) {
    console.log(trimestre);
    this.toUpdate = false;
    this.submitted = false;
    this.currentTrimestre = trimestre;
    if (trimestre) {
      this.formTrimestre.patchValue({
        nomTrim: trimestre.nom_trimestre,
        nomTrimEn: trimestre.nom_trimestre_2,
        ouverture: trimestre.date_ouverture,
        fermeture: trimestre.date_fermeture,
        numOrdre: trimestre.numero_ordre
      });
    }
    this.modalService.open(contentTimestre,
      {ariaLabelledBy: 'add-trimestre', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => {
    });
  }

  openLoadFormAddSequenceToClasse(trimestre: any, sequence: any, contentSequence: TemplateRef<any>) {
    console.log(sequence);
    this.toUpdate = false;
    this.submitted = false;
    this.idTrimestre = trimestre.id_trimestre;
    this.currentTrimestre = trimestre;
    this.currentSequence = sequence;
    if (trimestre && sequence) {
      this.formSequence.patchValue({
        nomSeq: sequence.nom_sequence,
        nomSeqEn: sequence.nom_sequence_2,
        ouvertureSeq: sequence.date_ouverture,
        fermetureSeq: sequence.date_fermeture,
        numOrdreSeq: sequence.numero_ordre
      });
    }
    this.modalService.open(contentSequence,
      { ariaLabelledBy: 'add-sequence', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => {
    });
  }

  get t() { return this.formTrimestre.controls; }
  get s() { return this.formSequence.controls; }

  onSaveTrimestre() {
    this.submitted = true;
    console.log(this.t);
    if (this.formTrimestre.invalid) {
      return;
    }
    let trimestreCreate;
    trimestreCreate = new TrimestreRequestModel(this.t.fermeture.value, this.t.ouverture.value,
      false, parseInt(this.idAnnee, 0), this.currentTrimestre ? this.currentTrimestre.id_trimestre : 0,
      this.t.nomTrim.value, this.t.nomTrimEn.value, this.t.numOrdre.value
    );
    this.genericsService.postResource(`${this.base_url}trimestre`, trimestreCreate).then((result: any) => {
      this.onLoadListTrimestre(this.idAnnee);
      this.onResetTrimestre();
      this.genericsService.confirmResponseAPI(result.message, 'success');
    }).catch((reason: any) => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONEN,  'error')
        : this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONFR,  'error');
    });
  }

  onSaveSequence() {
    this.submitted = true;
    if (this.formSequence.invalid) {
      return;
    }

    const sequenceCreate = new SequenceRequestModel(this.s.fermetureSeq.value, this.s.ouvertureSeq.value,
      false, this.currentSequence ? this.currentSequence.id_sequence : 0,
      this.s.nomSeq.value, this.s.nomSeqEn.value, this.s.numOrdreSeq.value);
    this.genericsService.postResource(`${this.base_url + this.idTrimestre }/sequence`, sequenceCreate).then((result: any) => {
      this.onLoadListTrimestre(this.idAnnee);
      this.onResetSequence();
      this.genericsService.confirmResponseAPI(result.message, 'success');
    }).catch((reason: any) => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONEN,  'error')
        : this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONFR,  'error');

    });
  }

  onResetSequence() {
    this.currentSequence = null;
    this.currentTrimestre = null;
    this.submitted = false;
    this.initFormSequence();
    this.toUpdate = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onResetTrimestre() {
    this.currentSequence = null;
    this.currentTrimestre = null;
    this.submitted = false;
    this.initFormTrimestre();
    this.toUpdate = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  private initFormTrimestre() {
    this.formTrimestre = this.fb.group({
      nomTrim: ['', Validators.required],
      nomTrimEn: [''],
      ouverture: ['', Validators.required],
      fermeture: ['', Validators.required],
      numOrdre: ['', Validators.required]
    }, {
      validator: MustMatchDate('ouverture', 'fermeture')
    });
  }

  private initFormSequence() {
    this.formSequence = this.fb.group({
      nomSeq: ['', Validators.required],
      nomSeqEn: [''],
      ouvertureSeq: ['', Validators.required],
      fermetureSeq: ['', Validators.required],
      numOrdreSeq: ['', Validators.required]
    }, {
      validator: MustMatchDate('ouvertureSeq', 'fermetureSeq')
    });
  }

  onChangeStateSequence(sequence: any) {
    if (sequence.en_cours === true) {
      this.genericsService.getResource(`admin/cloturer/${sequence.id_sequence}/sequence`).then((response: any) => {
        console.log(response);
        this.onLoadListTrimestre(this.idAnnee);
      }).catch((reason: any) => {
        console.log(reason);
      });
    } else {
      this.genericsService.getResource(`admin/demarrer/${sequence.id_sequence}/sequence`).then((response: any) => {
        this.onLoadListTrimestre(this.idAnnee);
      }).catch((reason: any) => {
        console.log(reason);
      });
    }
  }

  onDeleteTrimestreOrSequence(idRessource: any, type:number) {
    let urlDelete = (type == 1) ? `admin/supprimer/${idRessource}/trimestre`:`admin/supprimer/${idRessource}/sequence`;
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: AppConstants.ARE_U_SUREEN,
        text: AppConstants.IRREVERSIBLEEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITEN
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(urlDelete)
            .then((resultDeleteEtat: any) => {
              console.log(resultDeleteEtat);
              this.onLoadListTrimestre(this.idAnnee);
              this.onResetTrimestre();
              this.genericsService.confirmResponseAPI(AppConstants.DELETE_SUCCESSEN, 'success');
            }).catch((errEtat: any) => {
            console.log(errEtat);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
          });
        }
      }) :
      Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(urlDelete)
            .then((resultDeleteEtat: any) => {
              console.log(resultDeleteEtat);
              this.onLoadListTrimestre(this.idAnnee);
              this.onResetTrimestre();
              this.genericsService.confirmResponseAPI(AppConstants.DELETE_SUCCESSFR, 'success');
            }).catch((errEtat: any) => {
            console.log(errEtat);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }

  ngOnDestroy(): void {
    this.onResetSequence();
    this.onResetTrimestre();
  }

  onClickReconductionTrimestre() {
    console.log(this.idAnneeSource);

    Swal.fire({
      title: localStorage.getItem('activelang') === 'en'? AppConstants.ARE_U_SUREEN : AppConstants.ARE_U_SUREFR,
      text: localStorage.getItem('activelang') === 'en'? AppConstants.IRREVERSIBLEEN : AppConstants.IRREVERSIBLEFR,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: localStorage.getItem('activelang') === 'en'? AppConstants.RECONDUIRE_ITEN:AppConstants.RECONDUIRE_ITFR
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.getResource(`admin/reconduire/${this.idAnneeSource}/trimestre`)
          .then((result: any) => {
            console.log(result);
            this.onLoadListTrimestre(this.idAnnee);
            this.onResetTrimestre();
            this.genericsService.confirmResponseAPI(result.message, 'success');
          }).catch((reason: any) => {
          console.log(reason);
          localStorage.getItem('activelang') === 'en'
            ? this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONEN,  'error')
            : this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONFR,  'error');
        });
      }
    })

  }

  onChangeAnnee(event: any) {
    this.idAnneeSource = event.target.value;
  }
}
