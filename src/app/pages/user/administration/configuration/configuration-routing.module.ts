import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConfigurationComponent} from './configuration.component';
import {MenuConfigComponent} from './menu-config/menu-config.component';
import {GestionRolesComponent} from './gestion-roles/gestion-roles.component';
import {MatieresComponent} from './matieres/matieres.component';
import {GestionEtatsComponent} from './gestion-etats/gestion-etats.component';
import {NumerotationComponent} from './numerotation/numerotation.component';
import {PenaliteComponent} from './penalite/penalite.component';

const routes: Routes = [
  {
    path: '', component: ConfigurationComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Configuration',
      // titre: 'userEleveAjaout.Configuration_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Configuration | Aurore' : 'Configuration | Aurore',
      icon: 'icofont-ui-settings bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Configuration_de_l_institution_et_des_acces_utilisateurs_Configuration',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'menu-config',
        pathMatch: 'full'
      },
      {
        path: 'menu-config', component: MenuConfigComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Configuration',
          // titre: 'userEleveAjaout.Configuration_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Configuration | Aurore' : 'Configuration | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Configuration_de_l_institution_et_des_acces_utilisateurs_Configuration',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'institutions',
        loadChildren: () => import('./institution/institution.module')
          .then(m => m.InstitutionModule)
      },
      {
        path: 'gestion-roles', component: GestionRolesComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Roles',
          //titre: 'userEleveAjaout.Roles_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Roles | Aurore' : 'Roles | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_roles_Roles',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'gestion-etats', component: GestionEtatsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Etats',
         // titre: 'userEleveAjaout.Etats_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'States | Aurore' : 'Etats | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_etats_Etats',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'matieres', component: MatieresComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Matieres',
         // titre: 'userEleveAjaout.Matieres_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Subjects | Aurore' : 'Matieres | Aurore',
            icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_matieres_et_de_groupe_de_matières_Matieres',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'numerotations', component: NumerotationComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Numerotations',
          titre: localStorage.getItem('activelang') === 'en' ? 'Numerotations | Aurore' : 'Numerotations | Aurore',
          // titre: 'userEleveAjaout.Numerotations_Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gestion_de_la_generation_des_numeros_matricules_factures_Numerotations',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'penalite', component: PenaliteComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Pénalité',
         // titre: 'userEleveAjaout.Pénalité_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Penalty | Aurore' : 'Pénalité | Aurore',
            icon: 'icofont-law bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Ajout_modification_et_suppression_de_pénalite_Pénalité',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'periodes',
        loadChildren: () => import('./periodes/periodes.module')
          .then(m => m.PeriodesModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
