import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {
  GroupeMatiereRequestModel,
  MatiereRequestModel
} from '../../../../../shared/_models/request-dto/graduation/gestion-cours-enseignant-model';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-matieres',
  templateUrl: './matieres.component.html',
  styleUrls: ['./matieres.component.scss']
})
export class MatieresComponent implements OnInit, OnDestroy {
  formGroupMatiere: FormGroup;
  submitted: boolean;
  formMatiere: FormGroup;
  public disciplines: any;
  public groupeMatieres: any;
  private currentGroupe: any;
  private currentMatiere: any;
  validated: boolean;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private modalService: NgbModal,
    config: NgbModalConfig,
    private fb: FormBuilder,
    private genericsService: GenericsService,
    public authService: AuthService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(32);
  }

  ngOnInit() {
    this.onloadListDiscipline();
    this.onloadListMatiere();
    this.initFormGroupeMatiere();
    this.initFormMatiere();
  }

  get gm() { return this.formGroupMatiere.controls; }
  get m() { return this.formMatiere.controls; }

  private onloadListDiscipline() {
    this.genericsService.getResource(`admin/disciplines`).then((response: any) => {
      this.disciplines = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadListMatiere() {
    this.genericsService.getResource(`admin/matieres`).then((response: any) => {
      this.groupeMatieres = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  onOpenModalToEditMatiere(groupeMatiere, matiere, contentMatiere: TemplateRef<any>) {
    this.currentGroupe = groupeMatiere;
    this.currentMatiere = matiere;
    if (matiere && groupeMatiere) {
      this.formMatiere.patchValue({
        discipline: matiere.discipline.id_discipline,
        nomFr: matiere.nomMatiere,
        nomEn: matiere.nomMatiere2,
        ordre: matiere.ordreMatiere,
        code: matiere.codeMatiere
      });
    }
    this.modalService.open(contentMatiere,
      {ariaLabelledBy: 'edit-matiere', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => {
    });
  }
  onOpenModalToEditGroupeMatiere(groupeMatiere, contentGroupeMatiere: TemplateRef<any>) {
    this.currentGroupe = groupeMatiere;
    if (groupeMatiere) {
      this.formGroupMatiere.patchValue({
        nomgm: groupeMatiere.nomGroupeMatiere,
        nomgmEn: groupeMatiere.nomGroupeMatiere2,
        specialite: groupeMatiere.specialite,
        ordre: groupeMatiere.numeroOrdre
      });
    }
    this.modalService.open(contentGroupeMatiere,
      {ariaLabelledBy: 'edit-group-matiere', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => {
    });
  }
  private initFormGroupeMatiere() {
    this.formGroupMatiere = this.fb.group({
      nomgm: ['', Validators.required],
      nomgmEn: [''],
      specialite: ['', Validators.required],
      ordre: ['']
    });
  }
  private initFormMatiere() {
    this.formMatiere = this.fb.group({
      discipline: ['', Validators.required],
      nomFr: ['', Validators.required],
      nomEn: [''],
      ordre: [0, Validators.required],
      code: ['', Validators.required]
    });
  }
  onResetMatiere() {
    this.currentGroupe = null;
    this.currentMatiere = null;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormMatiere();
    this.submitted = false;
  }
  onResetGM() {
    this.currentGroupe = null;
    this.currentMatiere = null;
    this.validated = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormGroupeMatiere();
    this.submitted = false;
  }

  onSaveGroupeMatiere() {
    this.submitted = true;
    this.validated = true;
    if (this.formGroupMatiere.invalid) {
      this.validated = false;
      return;
    }
    const dto = new GroupeMatiereRequestModel(this.currentGroupe ? this.currentGroupe.id : 0, this.gm.nomgm.value, this.gm.nomgmEn.value,
      this.gm.ordre.value, this.gm.specialite.value);
    this.genericsService.postResource(`admin/ajouter/groupe-matiere`, dto).then((response: any) => {
      this.onloadListMatiere();
      this.onResetGM();
    }).catch(reason => {
      console.log(reason);
      this.validated = false;
    });
  }

  onSaveMatiere() {
    console.log(this.m);
    this.submitted = true;
    if (this.formMatiere.invalid) {
      return;
    }
    const dto = new MatiereRequestModel(this.m.code.value, this.m.discipline.value, this.currentGroupe.id,
      this.currentMatiere ? this.currentMatiere.id : 0, this.m.nomFr.value, this.m.nomEn.value, this.m.ordre.value);
    this.genericsService.postResource(`admin/ajouter/matiere-groupe`, dto).then((response: any) => {
      console.log(response);
      this.onloadListMatiere();
      this.onResetMatiere();
    }).catch(reason => {
      console.log(reason);
    });

  }

  ngOnDestroy(): void {
    this.onResetMatiere();
    this.onResetGM();
  }

  onDeleteMatiere(matiere: any) {
    localStorage.getItem('activelang') === 'en' ?
    Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService.deleteResource(`matieres/${matiere.id}`).then((resultDelete: any) => {
          this.onloadListMatiere();
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }).catch(reason => {
          console.log(reason);
          this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
        });
      }
    }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`matieres/${matiere.id}`).then((resultDelete: any) => {
            this.onloadListMatiere();
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }
}
