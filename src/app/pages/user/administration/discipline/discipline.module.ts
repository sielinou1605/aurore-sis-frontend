import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DisciplineRoutingModule} from './discipline-routing.module';
import {DisciplineComponent} from './discipline.component';
import {DWelcomeComponent} from './d-welcome/d-welcome.component';
import {SharedModule} from '../../../../shared/shared.module';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AdminAbsencesComponent} from "./admin-absences/admin-absences.component";
import {LogAbscenceComponent} from "./log-abscence/log-abscence.component";
import {ReactiveFormsModule} from "@angular/forms";
import {NgxPaginationModule} from "ngx-pagination";

@NgModule({
  declarations: [ DisciplineComponent, AdminAbsencesComponent, LogAbscenceComponent, DWelcomeComponent ],
  imports: [
    CommonModule,
    DisciplineRoutingModule,
    SharedModule,
    NgMultiSelectDropDownModule,
    TranslateModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ]
})
export class DisciplineModule {
  lang = 'en';
  constructor(
    private translate: TranslateService
  ) {
    this.translate.setDefaultLang(this.lang);
  }
}
