import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DWelcomeComponent} from './d-welcome.component';

describe('DWelcomeComponent', () => {
  let component: DWelcomeComponent;
  let fixture: ComponentFixture<DWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
