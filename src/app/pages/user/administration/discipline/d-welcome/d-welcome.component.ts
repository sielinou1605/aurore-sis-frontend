import {Component, OnInit, TemplateRef} from '@angular/core';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FiltreAbsenceModel} from "../../../../../shared/_models/response-dto/graduation/filtre-note-absence-model";
import {GenericsService} from "../../../../../shared/_services/generics.service";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";

@Component({
  selector: 'app-d-welcome',
  templateUrl: './d-welcome.component.html',
  styleUrls: ['./d-welcome.component.scss']
})
export class DWelcomeComponent implements OnInit {

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  submitted: boolean;
  selectAbsenceClasseForm: FormGroup;
  filtreAbsence: FiltreAbsenceModel;
  public settingsAbsence = {};

  public choix: string;
  public classeAbsenceFiltre: any;

  constructor(private genericsService: GenericsService,private fb: FormBuilder,
              private modalService: NgbModal,public authService: AuthService,
              private router: Router,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    this.choix = localStorage.getItem('activelang') === 'en' ? 'Choose value' : 'Choisir la valeur';

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(69);

  }

  ngOnInit() {
    this.initFormAbsence();
    this.onloadFiltres();

    this.settingsAbsence = localStorage.getItem('activelang') === 'en'
      ? {
        singleSelection: true,
        idField: 'id',
        textField: 'nomClasse',
        enableCheckAll: false,
        selectAllText: 'Check all',
        unSelectAllText: 'Uncheck all',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: 'Search',
        noDataAvailablePlaceholderText: 'No data available',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      }
      : {
        singleSelection: true,
        idField: 'id',
        textField: 'nomClasse',
        enableCheckAll: false,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: 'Rechercher',
        noDataAvailablePlaceholderText: 'Aucune donnée disponible',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };

  }
  private initFormAbsence() {
    this.selectAbsenceClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      classe: ['', Validators.required]
    });
  }

  get g() { return this.selectAbsenceClasseForm.controls; }

  private onloadFiltres() {
    this.genericsService.getResource(`prefet/filtre/saisie/absence`).then((responses: any) => {
      if (responses.success == true) {
        this.filtreAbsence = responses.data;
        this.classeAbsenceFiltre = this.filtreAbsence.classes;
      }
    }).catch(err => {
      console.log(err);
    });
  }
  onDisplayModalToAbsence(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'select-absence', size: 'lg'}).result.then((result) => {
    }, (reason) => {
    });
  }
  onDisplayListeEleve() {
    this.submitted = true;

    const classe = this.g.classe.value[0];
    if (this.selectAbsenceClasseForm.invalid) {
      return;
    }
    this.genericsService.startLoadingPage();
    this.router.navigate([`/administration/discipline/admin-absences/${classe.id}/sequence/${this.g.sequence.value}`])
      .then(() => {
        this.genericsService.stopLoadingPage();
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
  }
  onReset() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }

  onDisplayHistoryLogAbscence() {
    this.router.navigateByUrl('/administration/discipline/log-abscences').then(() => {});
  }
}
