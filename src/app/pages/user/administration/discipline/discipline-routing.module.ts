import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DisciplineComponent} from './discipline.component';
import {DWelcomeComponent} from './d-welcome/d-welcome.component';
import {LogAbscenceComponent} from "./log-abscence/log-abscence.component";
import {AdminAbsencesComponent} from "./admin-absences/admin-absences.component";

const routes: Routes = [
  {
    path: '', component: DisciplineComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Discipline',
      // titre: 'userEleveAjaout.Discipline_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Discipline | Aurore' : 'Discipline | Aurore',
      icon: 'icofont-ui-settings bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Configuration_des_classes_et_enseignants_Discipline',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'd-welcome',
        pathMatch: 'full'
      },
      {
        path: 'd-welcome', component: DWelcomeComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Discipline',
          // titre: 'userEleveAjaout.Discipline_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Discipline | Aurore' : 'Discipline | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Configuration_des_classes_et_enseignants_Discipline',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'enseignants',
        loadChildren: () => import('./enseignants/enseignants.module')
          .then(m => m.EnseignantsModule)
      },
      {
        path: 'classes',
        loadChildren: () => import('./classes/classes.module')
          .then(m => m.ClassesModule)
      },
      {
        path: 'archivages',
        loadChildren: () => import('./archivage/archivage.module')
          .then(m => m.ArchivageModule)
      },
      {
        path: 'log-abscences', component: LogAbscenceComponent,
        data: {
          breadcrumb: 'Historique abscence',
          // titre: 'userAdminFinancesListServices.Aide_Aurore',
          titre: 'Historique des abscences | Aurore',
          icon: 'icofont-history bg-c-blue',
          breadcrumb_caption: 'Gestion des Historiques des abscences - Administration',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'admin-absences/:classe/sequence/:sequence', component: AdminAbsencesComponent,
        data: {
          breadcrumb: 'userAdminFinancesListServices.Absences',
          // titre: 'userAdminFinancesListServices.Absences_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Absences | Aurore' : 'Absences | Aurore',
          icon: 'icofont-refresh bg-youtube',
          breadcrumb_caption: 'userAdminFinancesListServices.Management_complet_des_absences_eleve_d_une_classe_Absences',
          status: true,
          current_role: 'admin'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DisciplineRoutingModule { }
