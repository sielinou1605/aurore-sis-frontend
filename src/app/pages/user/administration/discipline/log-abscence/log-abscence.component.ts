import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {HistoriqueEleveNoteResponse} from '../../../../../shared/_models/response-dto/systeme/data-module-response-model';

@Component({
  selector: 'app-log-abscence',
  templateUrl: './log-abscence.component.html',
  styleUrls: ['./log-abscence.component.scss']
})
export class LogAbscenceComponent implements OnInit {
  public searchForm: FormGroup;
  public pageSize = 40;
  public page = 0;
  public sortBy = '';
  public totalElements: number;
  private dataFilter: string;
  listAbscences: any;
  predicate = '';
  historique = localStorage.getItem('activelang') === 'fr' ? 'Historique des absences' : 'Absence history';

  constructor(
    public genericsService: GenericsService,
    public fb: FormBuilder,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ''
    });
    this.genericsService.startLoadingPage();
    this.onloadListHistorique(this.predicate, this.sortBy, this.page, this.pageSize);
  }

  get f() {
    return this.searchForm.controls;
  }
  onAfficherHistorique() {
    let url = 'admin/get-all/historique/abscence/eleves';
    this.predicate = this.f.search.value;
    console.log(this.predicate);
    this.genericsService.getResource(`${url}?predicat=${this.predicate}&page=${this.page}&size=${this.pageSize}`);

    this.onloadListHistorique(this.predicate, this.sortBy, this.page, this.pageSize);
  }
  onDetailHistorique(note: HistoriqueEleveNoteResponse) {

  }

  setPageChange(event: any) {
    this.page = event - 1;
    this.onloadListHistorique(this.predicate, this.sortBy, this.page, this.pageSize);
  }

  private onloadListHistorique(predicat: string, sortBy: string, page: number, size: number) {
    let url = 'admin/get-all/historique/abscence/eleves';
    this.genericsService.getResource(`${url}?predicat=${predicat}&page=${page}&size=${size}`)
      .then((result: any) => {
        this.genericsService.stopLoadingPage();
        console.log(result);
        this.listAbscences = result.data.content;
        this.totalElements = result.data.totalElements;
      }).catch(err => {
      console.log(err);
    });
  }

}
