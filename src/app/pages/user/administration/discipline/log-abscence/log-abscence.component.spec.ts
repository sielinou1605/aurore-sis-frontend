import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogAbscenceComponent } from './log-abscence.component';

describe('LogAbscenceComponent', () => {
  let component: LogAbscenceComponent;
  let fixture: ComponentFixture<LogAbscenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogAbscenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogAbscenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
