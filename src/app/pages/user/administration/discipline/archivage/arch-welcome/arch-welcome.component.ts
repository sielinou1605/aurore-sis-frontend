/* tslint:disable:triple-equals */
// @ts-ignore
import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {TokenStorageService} from '../../../../../../shared/_services/token-storage.service';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
// @ts-ignore
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
// @ts-ignore
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AddArchiveModel2} from '../../../../../../shared/_models/request-dto/graduation/gestion-archive-model';
// @ts-ignore
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-arch-welcome',
  templateUrl: './arch-welcome.component.html',
  styleUrls: ['./arch-welcome.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ArchWelcomeComponent implements OnInit {
  submitted: boolean;
  creationArchiveForm: FormGroup;
  toUpdate: boolean;
  archives: any;
  public settings: any;
  dataEleve;
  dataEnseignant: any;
  // @ts-ignore
  @ViewChild('selectEleve') selectEleve;
  // @ts-ignore
  @ViewChild('selectEnseignant') selectEnseignant;

  public pageSize = 15;
  public page = 0;
  public totalElements: number;
  public typeArchives: any;
  public listFiltre: any;

  public pageSizeArch = 10;
  public pageArch = 0;
  public totalElementsArch: number;
  public classes: any;
  public currentClasse: any;
  public validated: boolean;
  public canEdit = true;
  public canAdd = true;
  public currentArchive: any;
  public choix_eleve: string;
  public CHOIX_2: string;

public siteLanguage: string;
public supportLanguages = ['en', 'fr'];
public searchForm: FormGroup;
public predicat = '';

  constructor(public authService: AuthService, private tokenStorage: TokenStorageService,
              private modalService: NgbModal, config: NgbModalConfig,
              private fb: FormBuilder, private genericsService: GenericsService,
              private translate: TranslateService) {

    this.CHOIX_2 = localStorage.getItem('activelang') === 'en' ? 'Choose a teacher' : 'Choisir un enseignant';
    this.choix_eleve = localStorage.getItem('activelang') === 'en' ? 'Choose a student' : 'Choisir un eleve';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.currentArchive = '';
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.initFormCreateArchivage();
    this.settings = {
      singleSelection: false,
      idField: 'id',
      textField: localStorage.getItem('activelang') === 'en' ? 'description' : 'description',
      enableCheckAll: true,
      selectAllText: localStorage.getItem('activelang') === 'en' ? 'Check all' : 'Tout cocher',
      unSelectAllText: localStorage.getItem('activelang') === 'en' ? 'Uncheck all' : 'Tout decocher',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 20,
      searchPlaceholderText: localStorage.getItem('activelang') === 'en' ? 'Search' : 'Rechercher',
      noDataAvailablePlaceholderText: localStorage.getItem('activelang') === 'en' ? 'No data available' : 'Aucune donnée disponible',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    };
    this.onloadListEnseignant('');
    this.onloadListEleves('');
    this.onloadListTypeArchives();
    this.onloadListArchive();
    this.onloadListClasse();
  }

  onInitCreateArchivage(content: TemplateRef<any>) {
    this.canEdit = true;
    this.canAdd = true;
    this.modalService.open(content,
      {ariaLabelledBy: 'add-mode-reglement', size: 'lg'}).result.then((result) => {
    }, (reason) => {
    });
  }
  private initFormCreateArchivage() {
    this.creationArchiveForm = this.fb.group({
      id: [0],
      typeEntite: [{value: '0', disabled: false}, Validators.required],
      description: [''],
      enseignant: [[]],
      eleve: [[], [Validators.required, Validators.minLength(1)]],
      classe: ['', Validators.required],
      typeArchivage: ['1', Validators.required]
    });
  }
  onChangeOptions() {
    if (this.f.typeEntite.value == 0) {
      this.creationArchiveForm.controls['eleve'].setValidators([Validators.required, Validators.minLength(1)]);
      this.creationArchiveForm.controls['enseignant'].clearValidators();
      this.creationArchiveForm.controls['classe'].clearValidators();
    } else {
      this.creationArchiveForm.controls['enseignant'].setValidators([Validators.required, Validators.minLength(1)]);
      this.creationArchiveForm.controls['classe'].clearValidators();
      this.creationArchiveForm.controls['eleve'].clearValidators();
    }
    this.creationArchiveForm.get('eleve').updateValueAndValidity();
    this.creationArchiveForm.get('classe').updateValueAndValidity();
    this.creationArchiveForm.get('enseignant').updateValueAndValidity();
  }
  onResetCreateArchivage() {
    this.initFormCreateArchivage();
    this.validated = false;
    this.submitted = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onLoadDetailsArchivage(archive: any, content: TemplateRef<any>, canEdit) {
    console.log(archive);
    this.canEdit = canEdit;
    this.canAdd = false;
    this.currentArchive = archive;
    this.creationArchiveForm.patchValue({
      id: archive.id,
      typeEntite: archive.typeEntite,
      description: archive.description,
      classe: archive.typeEntite == 0 ? archive.classe.id : null,
      eleve: archive.typeEntite == 0 ? [archive.matricule] : null,
      enseignant: archive.typeEntite == 1 ? [archive.utilisateur.displayName] : null,
      typeArchivage: archive.typeArchivage.id
    });
    this.creationArchiveForm.get('typeEntite').disable();
    this.creationArchiveForm.get('classe').disable();
    this.modalService.open(content,
      {ariaLabelledBy: 'add-mode-reglement', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  get f() { return this.creationArchiveForm.controls; }

  onSaveCreateArchive() {
    this.submitted = true;
    this.validated = true;
    if (this.creationArchiveForm.invalid) {
      this.validated = false;
      return;
    }
    const dtos = [];
    console.log(this.f.typeEntite.value);
    console.log(this.f.eleve.value);
    console.log(this.f.typeEntite.value);
    console.log(this.f.typeArchivage.value);
    if (this.f.typeEntite.value == 0 && this.f.eleve.value.length > 0) {
      this.f.eleve.value.forEach(elt => {
        dtos.push(new AddArchiveModel2(
          this.f.description.value,
          this.currentArchive ? this.currentArchive.matricule : elt.id,
          parseInt(this.f.typeEntite.value, 0),
          parseInt(this.f.typeArchivage.value, 0),
          this.currentArchive ? this.currentArchive.id : 0,
          this.f.classe.value,
          null
        ));
      });
    }
    console.log(dtos);
    console.log(this.currentArchive);
    if (this.f.typeEntite.value == 1) {
      this.f.enseignant.value.forEach(elt => {
        dtos.push(new AddArchiveModel2(
          this.f.description.value,
          this.currentArchive ? this.currentArchive.matricule : null,
          this.f.typeEntite.value,
          this.f.typeArchivage.value,
          this.currentArchive ? this.currentArchive.id : 0,
          this.f.classe.value,
          elt.id
        ));
      });
    }
    console.log(dtos);
    console.log('la requete va etre soumise');
    this.genericsService.postResource(`admin/archive/ajouter`, dtos).then((response: any) => {
      this.genericsService.confirmResponseAPI(response.message, 'success', 7000);
      this.onResetCreateArchivage();
      this.onloadListArchive();
      this.currentArchive = '';
    }).catch(reason => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmResponseAPI('Archiving error', 'error', 7000)
      : this.genericsService.confirmResponseAPI('Erreur d\'archivage', 'error', 7000);
    });
  }

  private onloadListEnseignant(predicat: string) {
    const pageSize = 200;
    // this.genericsService.getResource(`admin/management/users?size=${pageSize}&page=${this.page}`).then((result: any) => {
    this.genericsService.getResource(`admin/enseignant/listes?size=${pageSize}&page=${this.page}&predicat=${predicat}`).then((result: any) => {
      this.totalElements = result.data.totalElements;
      this.dataEnseignant = [];
      console.log(result.data);
      result.data.content.forEach(item => {
        this.dataEnseignant.push({ id: item.enseignant.idUtilisateur, description: item.enseignant.displayName });
      });
      console.log(this.dataEnseignant);
    }).catch(reason => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmResponseAPI('Unknown error for user loading', 'error')
      : this.genericsService.confirmResponseAPI('Erreur inconnu pour le chargement des utilisateurs', 'error');
    });
  }

  get search() { return this.searchForm.controls; }
  public onReloadList(event, reInitPage?: boolean) { }

  private onloadListEleves(predicat, reInitPage?: boolean) {
    // console.log(this.search.search.value);
    const params = `predicat=${predicat}&size=${this.pageSize}&page=${this.page}`;
    this.genericsService.getResource(`admin/eleve/liste?${params}`).then((result: any) => {
      this.listFiltre = result.data.content;
      // console.log(this.listFiltre);
      if (result.data.content.length == 0 && !this.currentClasse) {
        // this.onloadListEleves('');
      } else {
        if (this.dataEleve) {
          this.dataEleve = null;
          this.dataEleve = [];
        } else {
          this.dataEleve = [];
        }
        result.data.content.forEach(eleve => {
          this.dataEleve.push({ id: eleve.matricule, description: eleve.matricule + ' ' + eleve.nomEleve + ' ' + eleve.prenomEleve });
        });
        this.settings.data = this.dataEleve;
      }
    }).catch(reason => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmResponseAPI('Unknown error !!! ', 'error')
      : this.genericsService.confirmResponseAPI('Erreur inconnue !!! ', 'error');
    });
  }
  onChangeClasse() {
    this.currentClasse = this.classes.find(classe => classe.id == this.f.classe.value);
    this.onReloadListEleve('');
  }
  onReloadListEleve(event: any) {
    if (this.currentClasse) {
      this.onloadListEleves(this.currentClasse.nomClasse);
    } else {
      this.onloadListEleves(event);
    }
  }
  private onloadListTypeArchives() {
    this.genericsService.getResource(`admin/type-archive/liste`).then((response: any) => {
      this.typeArchives = response.data;
    //  console.log(this.typeArchives);
    }).catch(reason => {
      console.log(reason);
    });
  }
  onloadListArchive() {
    this.predicat = this.search.search.value;
    // tslint:disable-next-line:max-line-length
    this.genericsService.getResource(`admin/archive/liste?predicat=${this.predicat}&page=${this.pageArch}&size=${this.pageSizeArch}`).then((response: any) => {
      console.log(response);
      this.archives = response.data.content;
      // this.totalElementsArch = response.data.numberOfElements;
     // console.log(this.archives);
      this.totalElementsArch = response.data.totalElements;
    //  console.log(this.totalElementsArch);
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadListClasse() {
    this.genericsService.getResource(`admin/classes`).then((response: any) => {
      this.classes = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  setPageChange(event: number) {
    this.pageArch = event - 1;
    this.onloadListArchive();
  }
}
