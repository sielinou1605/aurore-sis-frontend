import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArchWelcomeComponent} from './arch-welcome.component';

describe('ArchWelcomeComponent', () => {
  let component: ArchWelcomeComponent;
  let fixture: ComponentFixture<ArchWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
