import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ArchivageRoutingModule} from './archivage-routing.module';
import {ArchivageComponent} from './archivage.component';
import {ArchWelcomeComponent} from './arch-welcome/arch-welcome.component';
import {SharedModule} from '../../../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {NgxPaginationModule} from 'ngx-pagination';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ArchivageComponent, ArchWelcomeComponent],
    imports: [
        CommonModule,
        ArchivageRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        NgMultiSelectDropDownModule,
        NgxPaginationModule,
        TranslateModule
    ]
})
export class ArchivageModule { }
