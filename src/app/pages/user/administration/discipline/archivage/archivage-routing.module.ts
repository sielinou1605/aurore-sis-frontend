import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ArchWelcomeComponent} from './arch-welcome/arch-welcome.component';

const routes: Routes = [
  {
    path: '', component: ArchWelcomeComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Archivage',
      // titre: 'userEleveAjaout.Archivage_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Archiving | Aurore' : 'Archivage | Aurore',
      icon: 'icofont-archive bg-inverse',
      breadcrumb_caption: 'userEleveAjaout.Liste_des_archives_crées_Archivage',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'arch-welcome',
        pathMatch: 'full'
      },
      {
        path: 'arch-welcome', component: ArchWelcomeComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Archivage',
          // titre: 'userEleveAjaout.Archivage_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Archiving | Aurore' : 'Archivage | Aurore',
          icon: 'icofont-archive bg-inverse',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_archives_crées_Archivage',
          status: true,
          current_role: 'admin'
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchivageRoutingModule { }
