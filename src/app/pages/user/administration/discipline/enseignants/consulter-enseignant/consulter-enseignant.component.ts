/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {ActivatedRoute} from '@angular/router';
import {EditMatiereClasseEnseignant} from '../../../../../../shared/_models/request-dto/graduation/gestion-cours-enseignant-model';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {ItemProgrammationModel} from '../../../../../../shared/_models/response-dto/graduation/programmation-enseignant-response-model';
import {
  AjouterMatiereClasseRequest,
  AjouterProgrammation,
  MatiereClasseRequest,
  ProgrammationModelRequest
} from '../../../../../../shared/_models/request-dto/graduation/gestion-programmation-model';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-consulter-enseignant',
  templateUrl: './consulter-enseignant.component.html',
  styleUrls: ['./consulter-enseignant.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ConsulterEnseignantComponent implements OnInit, OnDestroy {
  enseignantClasseForm: FormGroup;
  submittedEC: boolean;
  annees: any;
  private currentProgrammation: ItemProgrammationModel;
  public currentEnseignant: any;
  public classes: any;
  public matieres: any;
  public enseignants: any;
  public isReattribuer: boolean;
  public isCrenau: boolean;
  private idEnseignant: string;
  public matieresClasse: ItemProgrammationModel[];
  public programmationForm: FormGroup;
  public updateProg: boolean;
  public jours = localStorage.getItem('activelang') === 'en' ? AppConstants.JOURS_SEMAINEEN : AppConstants.JOURS_SEMAINEFR;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private fb: FormBuilder, private genericsService: GenericsService,
              private modalService: NgbModal, config: NgbModalConfig,
              private route: ActivatedRoute, public authService: AuthService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(54);
  }

  ngOnInit() {
    this.onloadListEnseignant();
    this.onloadListClasse();
    this.onLoadListMatiere();
    this.initFormProgrammerEnseignant();
    this.initProgrammationForm();
    this.route.paramMap.subscribe(params => {
      this.idEnseignant = params.get('id');
      this.onloadListAnnees(this.idEnseignant);
    });
  }

  private onConsulterEnseignant(id: any, annee: number) {
    this.genericsService.getResource(`admin/programmation/enseignant/${id}/matiere-classe/liste?idAnnee=${annee}`)
      .then((response: any) => {
        if (response.success === true) {
          this.matieresClasse = response.data.matieres;
          this.currentEnseignant = response.data.enseignant;
          console.log(this.matieresClasse);
        }
      }).catch((reason => {
      console.log(reason);
    }));
  }
  get f() { return this.enseignantClasseForm.controls; }

  private initFormProgrammerEnseignant() {
    localStorage.getItem('activelang') === 'en' ?
      this.enseignantClasseForm = this.fb.group({
        anneeAcc: ['', Validators.required],
        matiere: ['', Validators.required],
        classe: ['', Validators.required],
        coef: [AppConstants.DEFAULT_COEF_MATIEREEN, Validators.required],
        bareme: [AppConstants.DEFAULT_BAREME_NOTEEN, Validators.required],
        enseignant: ['', Validators.required]
      }) : this.enseignantClasseForm = this.fb.group({
        anneeAcc: ['', Validators.required],
        matiere: ['', Validators.required],
        classe: ['', Validators.required],
        coef: [AppConstants.DEFAULT_COEF_MATIEREFR, Validators.required],
        bareme: [AppConstants.DEFAULT_BAREME_NOTEFR, Validators.required],
        enseignant: ['', Validators.required]
      });
  }
  onSupprimerProgrammation(matiere: ItemProgrammationModel) {
    console.log(matiere);
    Swal.fire({
      title: localStorage.getItem('activelang') === 'en' ? AppConstants.ARE_U_SUREEN : AppConstants.ARE_U_SUREFR,
      text: 'La suppression de ' + matiere.matiereClasse.nomMatiere + ' => ' + matiere.matiereClasse.nomClasse + ' est irreversible !',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: localStorage.getItem('activelang') === 'en' ? AppConstants.YES_DELETE_ITEN : AppConstants.YES_DELETE_ITFR
    }).then((result) => {
      if (result.isConfirmed) {
        this.genericsService
          .deleteResource(`admin/matiere-classe/${matiere.matiereClasse.idMatiereClasse}/supprimer`)
          .then((response: any) => {
            if (response.success == false) {
              this.genericsService.confirmResponseAPI(response.message, 'error', 9000);
            } else {
              this.onConsulterEnseignant(this.idEnseignant, this.annees[0].id);
              this.genericsService.confirmResponseAPI(response.message, 'success', 9000);
            }
          }).catch(reason => {
          console.log(reason);
          // tslint:disable-next-line:max-line-length
          localStorage.getItem('activelang') === 'en' ? Swal.fire(AppConstants.FAILEN, AppConstants.DELETE_FAILEN, 'error') : Swal.fire(AppConstants.FAILFR, AppConstants.DELETE_FAILFR, 'error');
        });
      }
    });
  }
  onEditMatiereClasse() {

  }
  onProgrammerEnseignant(programmation: ItemProgrammationModel, isReattribuer: boolean, content: TemplateRef<any>, isCrenau: boolean) {
    this.tp.clear();
    this.currentProgrammation = programmation;
    this.isReattribuer = isReattribuer;
    this.isCrenau = isCrenau;
    this.updateProg = false;
    if (isReattribuer) {
      this.enseignantClasseForm.controls['enseignant'].setValidators([Validators.required]);
    } else {
      this.enseignantClasseForm.controls['enseignant'].clearValidators();
    }
    this.enseignantClasseForm.get('enseignant').updateValueAndValidity();
    if (programmation) {
      this.updateProg = true;
      this.enseignantClasseForm.patchValue({
        anneeAcc: programmation.matiereClasse.idAnnee,
        matiere: programmation.matiereClasse.idMatiere,
        classe: programmation.matiereClasse.idClasse,
        coef: programmation.matiereClasse.coefficient,
        bareme: programmation.matiereClasse.baremeEvaluation,
        enseignant: this.idEnseignant
      });
      programmation.programmations.forEach(item => {
        this.tp.push(
          this.fb.group({
            id: item.id,
            heureDebut: [item.heureDebut, Validators.required],
            heureFin: [item.heureFin, Validators.required],
            jourSemaine: [item.jourSemaine, Validators.required]
          })
        );
      });
    }
    this.modalService.open(content,
      {ariaLabelledBy: 'edit-programmation', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }
  private onloadListAnnees(idEnseignant: any) {
    this.genericsService.getResource(`annees?size=50`).then((result: any) => {
      this.annees = result._embedded.annees.filter(item => item.enCours == true);
      this.enseignantClasseForm.patchValue({anneeAcc: this.annees[0].id });
      this.onConsulterEnseignant(idEnseignant, this.annees[0].id);
    });
  }
  private onloadListClasse() {
    this.genericsService.getResource('admin/classes').then((response: any) => {
      this.classes = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  private onLoadListMatiere() {
    this.genericsService.getResource('matieres?size=200').then((response: any) => {
      this.matieres = response._embedded.matieres;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }
  private onloadListEnseignant() {
    this.genericsService.getResource('admin/enseignants').then((response: any) => {
      this.enseignants = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }
  onResetEnseignantClasse() {
    this.submittedEC = false;
    this.currentProgrammation = null;
    this.initFormProgrammerEnseignant();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onAttributeClasseToEnseignant() {
    console.log(this.tp, this.f );
    this.submittedEC = true;
    if (this.enseignantClasseForm.invalid) {
      return;
    }
    if (this.currentProgrammation) {
      const dto = new EditMatiereClasseEnseignant(this.f.anneeAcc.value, this.f.bareme.value, this.f.classe.value,
        this.f.coef.value, this.isReattribuer ? this.f.enseignant.value : this.idEnseignant,
        this.currentProgrammation ? this.currentProgrammation.matiereClasse.idMatiereClasse : null,
        this.f.matiere.value, null);
      if (this.isReattribuer) {
        this.genericsService.postResource('admin/reattribuer/matiereclasse', dto).then((response: any) => {
          if (response.success === true) {
            this.genericsService.confirmResponseAPI(response.message, 'success');
            this.onResetEnseignantClasse();
            this.onConsulterEnseignant(this.idEnseignant, this.annees[0].id);
          }
        }).catch(reason => {
          console.log(reason);
        });
      } else {
        dto.enseignant_id = parseInt(this.idEnseignant, 0);
        this.genericsService.postResource('admin/update/matiereclasse', dto).then((response: any) => {
          if (response.success === true) {
            this.genericsService.confirmResponseAPI(response.message, 'success');
            this.onResetEnseignantClasse();
            this.onConsulterEnseignant(this.idEnseignant, this.annees[0].id);
          }
        }).catch(reason => {
          localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONEN,  'error') :
          this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONFR,  'error');

          console.log('erreur ici', reason);

        });
      }
    } else {
      if (this.tp.invalid) {
        // tslint:disable-next-line:max-line-length
        localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMFR, 'error');
        return;
      }
      const matiereClasse = new MatiereClasseRequest(this.f.anneeAcc.value, this.f.bareme.value, this.f.classe.value,
        this.f.coef.value, parseInt(this.idEnseignant, 0), null, this.f.matiere.value, null);
      const programmations = [];
      this.tp.controls.forEach(item => {
        programmations.push(new ProgrammationModelRequest(item.value.heureDebut, item.value.heureFin, item.value.jourSemaine));
      });
      const dtoMC = new AjouterMatiereClasseRequest(matiereClasse, programmations);
      console.log(dtoMC);
      this.genericsService.postResource('admin/programmation/matiere-classe/ajouter', dtoMC).then((response: any) => {
        if (response.success === true) {
          this.genericsService.confirmResponseAPI(response.message, 'success');
          this.onResetEnseignantClasse();
          this.onConsulterEnseignant(this.idEnseignant, this.annees[0].id);
        }
      }).catch(reason => {
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONEN,  'error')
          : this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONFR,  'error');

        console.log(reason);
      });
    }
  }

  ngOnDestroy(): void {
    this.onResetEnseignantClasse();
  }

  private initProgrammationForm() {
    this.programmationForm = this.fb.group({
      programmationData: this.fb.array([])
    });
  }

  get fp() { return this.programmationForm.controls; }
  get tp() { return this.fp.programmationData as FormArray; }

  public addNewProgrammation() {
    localStorage.getItem('activelang') === 'en' ?
      this.tp.push(
        this.fb.group({
          id: [null],
          heureDebut: ['', Validators.required],
          heureFin: ['', Validators.required],
          jourSemaine: [AppConstants.DEFAULT_DAY_OF_WEEKEN, Validators.required]
        })
      ) : this.tp.push(
      this.fb.group({
        id: [null],
        heureDebut: ['', Validators.required],
        heureFin: ['', Validators.required],
        jourSemaine: [AppConstants.DEFAULT_DAY_OF_WEEKFR, Validators.required]
      })
      );
  }

  public removeProgrammation(i: number) {
    if (this.tp.at(i).value.id == null) {
      this.tp.removeAt(i);
    } else {
      this.genericsService
        .deleteResource(`admin/programmation/${this.tp.at(i).value.id}/supprimer`).then((response: any) => {
        if (response.success == true) {
          this.tp.removeAt(i);
          localStorage.getItem('activelang') === 'en' ?
            this.genericsService.confirmResponseAPI(AppConstants.DELETE_SUCCESSEN, 'success', 6000)
            : this.genericsService.confirmResponseAPI(AppConstants.DELETE_SUCCESSFR, 'success', 6000);
        }
      }).catch(reason => {
        console.log(reason);
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.DELETE_FAILEN, 'error', 8000)
          : this.genericsService.confirmResponseAPI(AppConstants.DELETE_FAILFR, 'error', 8000);
      });
    }
  }

  onAddCreneauHoraire() {
    console.log('---- crenau horaire ici ----');
    console.log(this.tp, this.f);
    if (this.tp.invalid) {
      localStorage.getItem('activelang') === 'en' ? this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMEN, 'error')
        : this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMFR, 'error');
      return;
    }
    const programmations = [];
    this.tp.controls.forEach(item => {
      programmations.push(new ProgrammationModelRequest(item.value.heureDebut, item.value.heureFin, item.value.jourSemaine));
    });
    const dtoProg = new AjouterProgrammation(this.currentProgrammation.matiereClasse.idMatiereClasse, programmations);
    this.genericsService.postResource('admin/programmation/ajouter', dtoProg).then((response: any) => {
      if (response.success === true) {
        this.onResetEnseignantClasse();
        this.onConsulterEnseignant(this.idEnseignant, this.annees[0].id);
      }
    }).catch(reason => {
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONEN,  'error')
        : this.genericsService.confirmModalResponseAPI(reason.error.message,AppConstants.FAIL_OPERATIONFR,  'error');

      console.log(reason);
    });
  }
}
