/* tslint:disable:triple-equals */
// @ts-ignore
import {AfterViewInit, ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, TemplateRef} from '@angular/core';
// @ts-ignore
import { Router } from '@angular/router';
// @ts-ignore
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
// @ts-ignore
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericsService } from '../../../../../../shared/_services/generics.service';
// @ts-ignore
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthService } from '../../../../../../shared/_services/auth.service';
import {
  AjouterMatiereClasseRequest,
  AjouterProgrammation,
  MatiereClasseRequest,
  ProgrammationModelRequest, ReconduireProgrammationModelRequest
} from '../../../../../../shared/_models/request-dto/graduation/gestion-programmation-model';
import { AddArchiveModel } from '../../../../../../shared/_models/request-dto/graduation/gestion-archive-model';
import { AppConstants } from '../../../../../../shared/element/app.constants';
// @ts-ignore
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list-enseignants',
  templateUrl: './list-enseignants.component.html',
  styleUrls: ['./list-enseignants.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListEnseignantsComponent implements OnInit, OnDestroy {
  public enseignants: any[] = [];
  enseignantClasseForm: FormGroup;
  modalEnseigReprogForm: FormGroup;
  searchEnseignantForm: FormGroup;
  submittedEC: boolean;
  annees: any;
  public anneeEncours: any;
  private currentEnseignant: any;
  public classes: any;
  public matieres: any;
  programmationForm: FormGroup;
  isCrenau: boolean;
  updateProg: Boolean;
  public listEleve: any;
  public searchForm: FormGroup;
  public listMatricule = [];
  public pageSize = 15;
  public page = 0;
  public totalElements: number;
  private dataFilter: any;
  public listEleveFiltre: any;
  private base_url = 'admin/enseignant/';

  public jours = localStorage.getItem('activelang') === 'en' ? AppConstants.JOURS_SEMAINEEN : AppConstants.JOURS_SEMAINEFR;
  private currentMatiereClasse: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  public idCurrentEnseignant: number;
  public enseignantsList: any;
  submittedER: boolean;
  nomCurrentEnseignant: any;
  isRefInput: boolean;
  enseinFinal: any;
  trueFalseTable = false;
  elt: any;
  enseignantsList_tmp: any;
  id_enseignant: any;
  initDate: any;
   anneeChoisie: any;

  constructor(private router: Router, private fb: FormBuilder, private modalService: NgbModal,
              // tslint:disable-next-line:no-shadowed-variable
              config: NgbModalConfig, private genericsService: GenericsService,
              private changeDetectorRef: ChangeDetectorRef,
              public authService: AuthService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(14);
  }

  ngOnInit() {
    this.getAnnee();
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.searchEnseignantForm = this.fb.group({
      searchEnseignant: [''],
    });
    this.onLoadListEleves('');
    // this.onloadListEnseignant();
    this.onloadListAnnees();
    this.initFormProgrammerEnseignant();
    this.initProgrammationForm();
    this.onloadListClasse();
    this.onLoadListMatiere();
    this.initModalEnseigReprogForm('');
    // tslint:disable-next-line:no-unused-expression
    this.getAnneeEncours();
  }

  get search() { return this.searchForm.controls; }
  public onReloadListEnseignants(event, reInitPage?: boolean) { }
  public onLoadListEleves(event, reInitPage?: boolean) {
    console.log('****************************************');
    console.log(event);
    if (reInitPage == true) {
      this.page = 0;
    }
    let predicat: string;
    if (event) {
      predicat = this.genericsService.cleanToken(event);
    } else {
      predicat = event;
    }
    this.dataFilter = predicat;
    this.genericsService.startLoadingPage();
    this.listMatricule = [];
    this.genericsService
      .getResource(`${this.base_url}listes?predicat=${predicat}&size=${this.pageSize}&page=${this.page}`).then((result: any) => {
        if (result.success === true) {
          this.listEleve = result.data.content;
          this.listEleveFiltre = result.data.content;
          this.enseignants = result.data.content;
          console.log(result.data.content);
          this.totalElements = result.data.totalElements;
        }
        this.genericsService.stopLoadingPage();
      }).catch(reason => {
        console.log(reason);
        this.genericsService.stopLoadingPage();
        localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Unknown error !!! ', 'error')
        : this.genericsService.confirmResponseAPI('Erreur inconnue !!! ', 'error');
      });
  }

  setPageChange(event) {
    console.log(event);
    this.page = event - 1;
    this.onLoadListEleves(this.dataFilter);
  }

  private initFormProgrammerEnseignant() {
    localStorage.getItem('activelang') === 'en' ?
    this.enseignantClasseForm = this.fb.group({
      anneeAcc: ['', Validators.required],
      matiere: ['', Validators.required],
      classe: ['', Validators.required],
      coef: [AppConstants.DEFAULT_COEF_MATIEREEN, Validators.required],
      bareme: [AppConstants.DEFAULT_BAREME_NOTEEN, Validators.required]
    }) : this.enseignantClasseForm = this.fb.group({
        anneeAcc: ['', Validators.required],
        matiere: ['', Validators.required],
        classe: ['', Validators.required],
        coef: [AppConstants.DEFAULT_COEF_MATIEREFR, Validators.required],
        bareme: [AppConstants.DEFAULT_BAREME_NOTEFR, Validators.required]
      });
  }

  private onloadListAnnees() {
    this.genericsService.getResource(`admin/annees`).then((response: any) => {
      if (response.success === true) {
        this.annees = response.data.filter(item => item.enCours == true);
      }
    });
  }

  onDetailEnseignant(enseignant: any) {
    if (enseignant) {
      this.router.navigate(
        ['/administration/discipline/enseignants/consulter-enseignant/' + enseignant.id_enseignant]).then(() => {
        });
    } else {
      this.router.navigate(['/administration/discipline/enseignants/consulter-enseignant/1']).then(() => {
      });
    }
  }

  onEditEnseignant(enseignant: any) {
    if (enseignant) {
      this.router.navigate(
        ['/administration/discipline/enseignants/ajouter-enseignant/' + enseignant.id_enseignant]).then(() => {
        });
    } else {
      this.router.navigate(['/administration/discipline/enseignants/ajouter-enseignant']).then(() => {
      });
    }
  }
  get f() { return this.enseignantClasseForm.controls; }

  onProgrammerEnseignant(programmation, content: TemplateRef<any>) {
    this.currentEnseignant = programmation.enseignant;
    this.currentMatiereClasse = programmation.matiereClasses;
    console.log(programmation);
    console.log('programmation de l\'enseignant');
    this.modalService.open(content,
      { ariaLabelledBy: 'edit-programmation', size: 'lg', scrollable: true }).result.then((result) => {
      }, (reason) => {
      });
  }

  onSupprimerEnseignant(enseignant: any) {
    localStorage.getItem('activelang') === 'en'
      ? Swal.fire({
      title: AppConstants.ARE_U_SUREEN,
      text: AppConstants.IRREVERSIBLEEN,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: AppConstants.YES_DELETE_ITEN
    }).then((result) => {
      if (result.isConfirmed) {
        const dto = new AddArchiveModel(
          'act of suppression',
          null,
          1,
          5,
          null,
          enseignant.id_enseignant
        );
        this.genericsService.postResource(`admin/archive/ajouter`, dto).then((response: any) => {
          console.log(response);
          console.log('Supression enseignant');
          this.onloadListEnseignant();
          Swal.fire('Deleted!', response.message, 'success');
        }).catch(reason => {
          console.log(reason);
          Swal.fire('Failure!', 'The teacher was not deleted.', 'error');
        });
      }
    })
      : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          const dto = new AddArchiveModel(
            'Acte de suppression', null, 1, 5, null, enseignant.id_enseignant
          );
          this.genericsService.postResource(`admin/archive/ajouter`, dto).then((response: any) => {
            console.log(response);
            console.log('response a la suppression de l\'enseignat');
            this.onloadListEnseignant();
            Swal.fire('Supprimé!', response.message, 'success');
          }).catch(reason => {
            console.log(reason);
            Swal.fire('Echec!', 'L\'enseignant n\'a pas été supprimé.', 'error');
          });
        }
      });
  }

  onGetEtatRemplissage(enseignant: any) {
    console.log(enseignant);
    this.router.navigate(['/administration/discipline/enseignants/etat-remplissage',
      enseignant.id_enseignant]).then(() => { });
  }

  private onloadListEnseignant() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource('admin/enseignants').then((response: any) => {
      this.enseignants = response.data;
      console.log(response.data);
      this.genericsService.stopLoadingPage();
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  private getAnneeEncours() {
    this.genericsService.getResource('admin/encours').then((response: any) => {
      console.log(response.data);
      this.anneeEncours = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  private reloadAfterUpdateListe() {
    this.genericsService.getResource('admin/enseignants').then((response: any) => {
      this.enseignants = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  onResetEnseignantClasse() {
    this.submittedER = false;
    this.submittedEC = false;
    this.currentEnseignant = undefined;
    this.initFormProgrammerEnseignant();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onAttributeClasseToEnseignant() {
    console.log(this.f, this.tp);
    this.submittedEC = true;
    if (this.enseignantClasseForm.invalid || this.tp.invalid) {
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMFR, 'error');
      return;
    }
    // const dto = new EditMatiereClasseEnseignant(this.f.anneeAcc.value, this.f.bareme.value, this.f.classe.value,
    //   this.f.coef.value, this.currentEnseignant.id_enseignant, null, this.f.matiere.value, null);
    // this.genericsService.postResource('admin/ajouter/matiereclasse/enseignant', dto).then((response: any) => {
    //   if (response.success === true) {
    //     this.genericsService.confirmResponseAPI(response.message, 'success');
    //     this.onResetEnseignantClasse();
    //     this.onloadListEnseignant();
    //   }
    // }).catch((reason: any) => {
    //   console.log(reason);
    // });

    const matiereClasse = new MatiereClasseRequest(this.f.anneeAcc.value, this.f.bareme.value, this.f.classe.value,
      this.f.coef.value, parseInt(this.currentEnseignant.id_enseignant, 0), null, this.f.matiere.value, null);
    const programmations = [];
    this.tp.controls.forEach(item => {
      programmations.push(new ProgrammationModelRequest(item.value.heureDebut, item.value.heureFin, item.value.jourSemaine));
    });
    const dtoMC = new AjouterMatiereClasseRequest(matiereClasse, programmations);
    console.log(dtoMC);
    console.log('programmation ajouter depuis le menu details');
    this.genericsService.postResource('admin/programmation/matiere-classe/ajouter', dtoMC).then((response: any) => {
      if (response.success === true) {
        this.genericsService.confirmResponseAPI(response.message, 'success');
        this.onResetEnseignantClasse();
        this.reloadAfterUpdateListe();
      }
    }).catch(reason => {
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.confirmModalResponseAPI(reason.error.message, 'Failure', 'error')
      : this.genericsService.confirmModalResponseAPI(reason.error.message, 'Echec', 'error');

      // this.genericsService.confirmResponseAPI(reason.error.message, 'error');
      console.log(reason);
    });
  }

  private onloadListClasse() {
    this.genericsService.getResource('admin/classes').then((response: any) => {
      this.classes = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  private onLoadListMatiere() {
    this.genericsService.getResource('matieres?size=200').then((response: any) => {
      this.matieres = response._embedded.matieres;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  ngOnDestroy(): void {
    this.onResetEnseignantClasse();
  }
  private initProgrammationForm() {
    this.programmationForm = this.fb.group({

      programmationData: this.fb.array([])
    });
  }

  get fp() { return this.programmationForm.controls; }
  get tp() { return this.fp.programmationData as FormArray; }

  addNewProgrammation() {
    localStorage.getItem('activelang') === 'en' ?
    this.tp.push(
      this.fb.group({
        id: [null],
        heureDebut: ['', Validators.required],
        heureFin: ['', Validators.required],
        jourSemaine: [AppConstants.DEFAULT_DAY_OF_WEEKEN, Validators.required]
      })
    ) : this.tp.push(
        this.fb.group({
          id: [null],
          heureDebut: ['', Validators.required],
          heureFin: ['', Validators.required],
          jourSemaine: [AppConstants.DEFAULT_DAY_OF_WEEKFR, Validators.required]
        })
      );
  }

  removeProgrammation(i: number) {
    if (this.tp.at(i).value.id == null) {
      this.tp.removeAt(i);
    } else {
      this.genericsService
        .deleteResource(`admin/programmation/${this.tp.at(i).value.id}/supprimer`).then((response: any) => {
          if (response.success == true) {
            this.tp.removeAt(i);
            localStorage.getItem('activelang') === 'en' ?
            this.genericsService.confirmResponseAPI(AppConstants.DELETE_SUCCESSEN, 'success', 6000) : this.genericsService.confirmResponseAPI(AppConstants.DELETE_SUCCESSFR, 'success', 6000);
          }
        }).catch(reason => {
          console.log(reason);
        localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.DELETE_FAILEN, 'error', 8000) : this.genericsService.confirmResponseAPI(AppConstants.DELETE_FAILFR, 'error', 8000);
        });
    }
  }
  onAddCreneauHoraire() {
    console.log(this.f, this.tp);
    if (this.tp.invalid) {
      localStorage.getItem('activelang') === 'en' ?
        // tslint:disable-next-line:max-line-length
      this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMFR, 'error');
      return;
    }
    const programmations = [];
    this.tp.controls.forEach(item => {
      programmations.push(new ProgrammationModelRequest(item.value.heureDebut, item.value.heureFin, item.value.jourSemaine));
    });
    const dtoProg = new AjouterProgrammation(this.currentMatiereClasse.idMatiereClasse, programmations);
    this.genericsService.postResource('admin/programmation/ajouter', dtoProg).then((response: any) => {
      if (response.success === true) {
        this.onResetEnseignantClasse();
        this.reloadAfterUpdateListe();
      }
    }).catch(reason => {
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmModalResponseAPI(reason.error.message, 'Failure', 'error')
      : this.genericsService.confirmModalResponseAPI(reason.error.message, 'Echec', 'error');
      console.log(reason);
    });
  }

  get reconduction() {
    return this.modalEnseigReprogForm.controls;
  }
  initModalEnseigReprogForm(data) {
    // console.log(this.annees.codeAnnee);
    console.log(data);
    console.log(this.anneeChoisie);
    this.modalEnseigReprogForm = this.fb.group({
      anneeChoise: [this.initDate ? this.initDate : null, Validators.required],
      enseigInitial: [this.idCurrentEnseignant],
      id_enseignantFinal: [data ? data.enseignant.id_enseignant : ''],
      enseinFinal: [data ? data.enseignant.nom_enseignant : '']
    });
  }
  getAnnee() {
    this.genericsService.getResource(`admin/annees`).then((response: any) => {
      if (response.success === true) {
        // this.annees = response.data.filter(item => item.enCours == true);
        this.annees = response.data;
        console.log(this.annees);
      }
    });
  }
  onGetReconduireProgrammation(enseignantDialog: TemplateRef<any>, enseignantReprog: any) {
    this.trueFalseTable = false;
    this.currentEnseignant = enseignantReprog.enseignant;
    this.idCurrentEnseignant = enseignantReprog.enseignant.id_enseignant;
    this.nomCurrentEnseignant = enseignantReprog.enseignant.nom_enseignant;
    this.currentMatiereClasse = enseignantReprog.matiereClasses;
    console.log(enseignantReprog);
    console.log(this.idCurrentEnseignant);
    console.log(this.currentEnseignant.date_creation);
    this.initModalEnseigReprogForm(enseignantReprog);

    this.genericsService.getResource('admin/enseignants').then((response: any) => {
      console.log(response);
      this.enseignantsList = response.data;
      this.enseignantsList_tmp = response.data;
      console.log(this.enseignantsList);
    });

    this.initDate = this.reconduction.anneeChoise.value;
    console.log(this.initDate);

    this.getAnnee();
    // console.log(' reconduction des programmations de l\'enseignant');
    this.modalService.open(enseignantDialog,
      { ariaLabelledBy: 'edit-programmation', size: 'lg', scrollable: true }).result.then((result) => {
    }, (reason) => {
    });
  }
  reconduireReprog() {
    this.submittedER = true;
    if (this.modalEnseigReprogForm.invalid) {
      console.log(this.reconduction);
      return;
    }
    // tslint:disable-next-line:no-shadowed-variable
    const url = 'admin/affecter/programmation/enseignant';
    let dto;
    console.log(this.reconduction.anneeChoise.value);
    console.log(this.reconduction.enseigInitial.value);
    dto = new ReconduireProgrammationModelRequest(
      this.reconduction.anneeChoise.value,
      this.reconduction.id_enseignantFinal.value,
      this.reconduction.enseigInitial.value
    );
    console.log(dto);
    this.genericsService.postResource(`${url}`, dto).then(response => {
      console.log(response);
      // @ts-ignore
      if (response.success === true) {
          // @ts-ignore
        this.genericsService.confirmResponseAPI(response.message, 'success');
      } else {
        // @ts-ignore
        this.genericsService.confirmResponseAPI(response.message, 'error');
      }
    });
    this.onLoadListEleves(this.dataFilter, true);
    this.reloadAfterUpdateListe();
    // this.initModalEnseigReprogForm('');
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.reloadAfterUpdateListe();
  }

  getElement(elt) {
    console.log(elt.target.value);
    console.log(this.annees);
    this.anneeChoisie = this.annees.find(x => x.id === parseInt(elt.target.value, 10));
    console.log(this.anneeChoisie.codeAnnee);
  }

  openListEnseignant(openMLE: TemplateRef<any>, elt) {
    // this.getElement(elt);
    this.getAnnee();
    this.initDate = this.reconduction.anneeChoise.value;
    console.log(this.initDate);
    // this.anneeChoisie = this.annees.find(x => x.id === parseInt(this.initDate, 10));
    // console.log(this.anneeChoisie);
    // this.modalEnseigReprogForm.patchValue({
    //   anneeChoise: this.anneeChoisie
    // });

    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    console.log(elt);
    this.modalService.open( openMLE,
      { ariaLabelledBy: 'edit-programmation', size: 'lg', scrollable: true }).result.then((result) => {
        console.log(result);
    }, err => console.log(err));
  }
  // ngAfterViewInit() {
  //   this.modalEnseigReprogForm.get('enseinFinal').markAsTouched();
  //   this.changeDetectorRef.detectChanges();
  // }
  openModalReconduction(modalR: TemplateRef <any>, elt) {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }

    console.log(this.initDate);
    // this.anneeChoisie = this.annees.find(x => x.id === parseInt(this.initDate, 10));
    console.log(elt);
    this.id_enseignant = elt.enseignant.id_enseignant;
    this.initModalEnseigReprogForm(elt);
    this.modalService.open(modalR,
      { ariaLabelledBy: 'edit-programmation', size: 'lg', scrollable: true }).result.then((result) => {

      console.log(result);
    }, err => console.log(err));
  }
  get getSFE() {
    return this.searchEnseignantForm.controls;
  }
  filterEnseignant(e) {
    this.elt = this.getSFE.searchEnseignant.value;
    this.enseignantsList = this.enseignantsList_tmp;

    this.enseignantsList = this.enseignantsList_tmp.filter((item) =>
    item.enseignant.nom_enseignant.toLowerCase().includes(e.target.value.toLowerCase()));

  }

}
