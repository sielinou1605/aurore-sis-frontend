import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconduireProgrammationComponent } from './reconduire-programmation.component';

describe('ReconduireProgrammationComponent', () => {
  let component: ReconduireProgrammationComponent;
  let fixture: ComponentFixture<ReconduireProgrammationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconduireProgrammationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconduireProgrammationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
