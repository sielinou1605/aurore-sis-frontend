import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EnseignantsRoutingModule} from './enseignants-routing.module';
import {EnseignantsComponent} from './enseignants.component';
import {SharedModule} from '../../../../../shared/shared.module';
import {ConsulterEnseignantComponent} from './consulter-enseignant/consulter-enseignant.component';
import {ListEnseignantsComponent} from './list-enseignants/list-enseignants.component';
import {EtatRemplissageComponent} from './etat-remplissage/etat-remplissage.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {AjouterEnseignantComponent} from './ajouter-enseignant/ajouter-enseignant.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import { ReconduireProgrammationComponent } from './reconduire-programmation/reconduire-programmation.component';

@NgModule({
  declarations: [
    EnseignantsComponent,
    ConsulterEnseignantComponent,
    ListEnseignantsComponent,
    EtatRemplissageComponent,
    AjouterEnseignantComponent,
    ReconduireProgrammationComponent,
  ],
    imports: [
        CommonModule,
        EnseignantsRoutingModule,
        SharedModule,
        FormsModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class EnseignantsModule { }
