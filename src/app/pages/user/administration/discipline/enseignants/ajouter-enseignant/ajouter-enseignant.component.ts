/* tslint:disable:triple-equals */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericsService } from '../../../../../../shared/_services/generics.service';
import { HttpEventType } from '@angular/common/http';
import { EditerEnseignantModel } from '../../../../../../shared/_models/request-dto/graduation/gestion-cours-enseignant-model';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthService } from '../../../../../../shared/_services/auth.service';
import { AppConstants } from '../../../../../../shared/element/app.constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ajouter-enseignant',
  templateUrl: './ajouter-enseignant.component.html',
  styleUrls: ['./ajouter-enseignant.component.scss']
})
export class AjouterEnseignantComponent implements OnInit {
  editEnsegnantForm: FormGroup;
  submitted: boolean;
  public selectedFiles: any;
  public progress: number;
  public currentFileUpload: any;
  public institutions: any;
  public departements: any;
  public roles: any;
  public idEnseignant: string;
  public currentEnseignant: any;
  validated: boolean;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private fb: FormBuilder, private genericsService: GenericsService,
    private router: Router, public authService: AuthService,
    private route: ActivatedRoute,
    private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(15);

  }

  ngOnInit() {
    this.initFormEditEnseignant();
    this.route.paramMap.subscribe(params => {
      this.idEnseignant = params.get('id');
      if (this.idEnseignant) {
        this.onConsulterEnseignant(this.idEnseignant);
      }
    });
    this.onloadInstitution();
    this.onloadDepartements();
    this.onloadRoles();
  }

  private initFormEditEnseignant() {
    localStorage.getItem('activelang') === 'en' ?
    this.editEnsegnantForm = this.fb.group({
      nom: ['', Validators.required],
      prenom: [''],
      sexe: ['', Validators.required],
      statutm: [AppConstants.DEFAULT_STATUTEN, Validators.required],
      telephone1: ['', [Validators.required, Validators.pattern('^6[0-9]{8}$')]],
      telephone2: [''],
      email: ['', [Validators.required, Validators.email]],
      matricule: [null],
      photo: [''],
      institution: ['', Validators.required],
      departement: ['', Validators.required],
      fonction: ['', Validators.required],
      typepersonnel: [AppConstants.DEFAULT_TYPE_PERSONNELEN],
      statutprof: [AppConstants.DEFAULT_STATUT_PROFEN],
      anciennete: [''],
      lieuprovenance: ['', Validators.required]
    }) : this.editEnsegnantForm = this.fb.group({
        nom: ['', Validators.required],
        prenom: [''],
        sexe: ['', Validators.required],
        statutm: [AppConstants.DEFAULT_STATUTFR, Validators.required],
        telephone1: ['', [Validators.required, Validators.pattern('^6[0-9]{8}$')]],
        telephone2: [''],
        email: ['', [Validators.required, Validators.email]],
        matricule: [null],
        photo: [''],
        institution: ['', Validators.required],
        departement: ['', Validators.required],
        fonction: ['', Validators.required],
        typepersonnel: [AppConstants.DEFAULT_TYPE_PERSONNELFR],
        statutprof: [AppConstants.DEFAULT_STATUT_PROFFR],
        anciennete: [''],
        lieuprovenance: ['', Validators.required]
      });
  }

  get f() { return this.editEnsegnantForm.controls; }

  onSaveEnseignant() {
    console.log(this.editEnsegnantForm);

    this.submitted = true;
    this.validated = true;
    if (this.editEnsegnantForm.invalid) {
      this.validated = false;
      return;
    }
    const matricule = this.genericsService.strRandom({ includeUpperCase: true, includeNumbers: true, length: 6, startsWithLowerCase: true });
    const dto = new EditerEnseignantModel(this.f.anciennete.value, null, '', '',
      this.f.email.value, this.f.fonction.value, this.f.departement.value, this.f.institution.value,
      this.currentEnseignant ? this.currentEnseignant.id_enseignant : null,
      this.f.matricule.value ? this.f.matricule.value : matricule, 0,
      this.f.nom.value, null, this.f.prenom.value, this.f.lieuprovenance.value, 0, this.f.sexe.value,
      this.f.statutm.value, this.f.telephone1.value, this.f.telephone2.value, this.f.typepersonnel.value);
    console.log(dto);
    this.genericsService.postResource(`admin/enseignant/ajouter`, dto).then((response: any) => {
      this.validated = false;
      console.log("response 1 : ", response);
      if (this.currentEnseignant) {
        this.currentEnseignant = null;
        if (this.selectedFiles) {
          this.uploadPhotoEnseignant(response.data.id_enseignant);
        } else  {
          localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.ENSEIGNANT_UPDATEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.ENSEIGNANT_UPDATEDFR, 'success');
        }
      } else {

          console.log("response 2 : ", response);
          localStorage.getItem('activelang') === 'en' ?
        Swal.fire(
          '<p class="text-danger text-bold mr-3">Important !!!</p> ' +
          ' <p>Please note the teaching parameters !</p>',
          '<span class="text-danger">Login :   </span>' + response.data.login + ' / ' +
          '<span class="text-danger">Password: </span>' +  response.data.password,
          'success'
        ).then((result) => {
          console.log("Result : ", result);
          if (response.success == true) {
            this.router.navigate([`/administration/discipline/enseignants`]).then(() => {
              if (this.selectedFiles) {
                this.uploadPhotoEnseignant(response.data.id_enseignant);
              }
            });
          }
        }) : Swal.fire(
              '<p class="text-danger text-bold mr-3">Important !!!</p> ' +
              ' <p>Relevez les parametres de enseignant svp !</p>',
              '<span class="text-danger">Nom d\'utilisateur :   </span>' + response.data.login + ' / ' +
              '<span class="text-danger">Mot de pass: </span>' +  response.data.password,
              'success'
            ).then((result) => {
              console.log("Result : ", result);
              if (response.success == true) {
                this.router.navigate([`/administration/discipline/enseignants`]).then(() => {
                  if (this.selectedFiles) {
                    this.uploadPhotoEnseignant(response.data.id_enseignant);
                  }
                });
              }
            });
      }
    }).catch(reason => {
      this.validated = false;
      this.genericsService.confirmResponseAPI(reason.error.message, 'error');
      console.log(reason);
    });
  }

  onResetEditEnseignant() {
    this.initFormEditEnseignant();
    this.submitted = false;
    this.selectedFiles = undefined;
  }

  onselectedFile(event: any) {
    this.selectedFiles = event.target.files;
  }

  uploadPhotoEnseignant(id: number) {
    this.progress = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.genericsService.uploadImage(this.currentFileUpload, `admin/enseignant/${id}/photo/upload`).subscribe(event => {

      if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else {
          this.progress = undefined;
          localStorage.getItem('activelang') === 'en' ?
          this.genericsService.confirmResponseAPI(AppConstants.PROFIL_IMAGE_SAVEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.PROFIL_IMAGE_SAVEDFR, 'success');
        }
      },
      errorPhoto => {
        console.log(errorPhoto);
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.PROFILE_IMAGE_FAILEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.PROFILE_IMAGE_FAILEDFR, 'error');
      });
    this.selectedFiles = undefined;
  }

  private onloadInstitution() {
    this.genericsService.getResource(`admin/institutions`).then((response: any) => {
      this.institutions = response.data;
      console.log("this.institutions : ", this.institutions);
      this.editEnsegnantForm.patchValue({
        institution: this.institutions[0].id
      });
    }).catch(reason => {
      console.log(reason);
    });
  }

  private onloadDepartements() {
    this.genericsService.getResource(`admin/departements`).then((response: any) => {
      this.departements = response.data;
      console.log("this.departements : ", this.departements);
      this.editEnsegnantForm.patchValue({
        departement: this.departements[0].id
      });
    }).catch(reason => {
      console.log(reason);
    });
  }

  private onloadRoles() {
    this.genericsService.getResource(`habilitation/role/liste`).then((response: any) => {
      this.roles = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }

  private onConsulterEnseignant(idEnseignant: string) {
    this.genericsService.getResource(`admin/consulter/${idEnseignant}/enseignant`).then((response: any) => {
      this.currentEnseignant = response.data;
      //this.currentEnseignant = response.data.enseignant;
      console.log(" ------ this.currentEnseignant ----- ", this.currentEnseignant);

      this.editEnsegnantForm.patchValue({
        nom: this.currentEnseignant.nom_enseignant,
        prenom: this.currentEnseignant.prenom_enseignant,
        sexe: this.currentEnseignant.sexe,
        statutm: this.currentEnseignant.statut_matrimonial,
        telephone1: this.currentEnseignant.telephone_1,
        telephone2: this.currentEnseignant.telephone_2,
        email: this.currentEnseignant.email,
        matricule: this.currentEnseignant.matricule,
        //photo: this.currentEnseignant.photo,
        institution: this.currentEnseignant.idInstitution,
        departement: this.currentEnseignant.idDepartement,
        fonction: this.currentEnseignant.fonction,
        typepersonnel: this.currentEnseignant.type_personnel,
        statutprof: this.currentEnseignant.statut_professionel ? this.currentEnseignant.statut_professionel : 'enseignant',
        anciennete: this.currentEnseignant.anciennete,
        lieuprovenance: this.currentEnseignant.provenance
      });
    }).catch(reason => {
      console.log(reason);
    });
  }
}
