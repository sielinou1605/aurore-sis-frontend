import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TokenStorageService} from '../../../../../../shared/_services/token-storage.service';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-etat-remplissage',
  templateUrl: './etat-remplissage.component.html',
  styleUrls: ['./etat-remplissage.component.scss']
})
export class EtatRemplissageComponent implements OnInit {
  private idEnseignant: string;
  public etat: any;
  public sequences: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private route: ActivatedRoute, public tokenStorage: TokenStorageService,
              private genericsService: GenericsService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }


  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idEnseignant = params.get('id');
      this.onloadEtatRemplissageEnseignant(this.idEnseignant);
      this.onloadSequencesEncours();
    });
  }

  private onloadEtatRemplissageEnseignant(idEnseignant: string) {
    this.genericsService.getResource(`admin/etat-remplissage/enseignant/${idEnseignant}`).then((response: any) => {
      if (response.success === true) {
        this.etat = response.data;
        console.log(this.etat);
      }
    }).catch(reason => {
      console.log(reason);
    });
  }

  private onloadSequencesEncours() {
    this.genericsService.getResource(`admin/note/enseignant/filtre/saisie`).then((response: any) => {
      console.log(response);
      this.sequences = response.data.sequences;
    }).catch(reason => {
      console.log(reason);
    });
  }
}
