import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EtatRemplissageComponent} from './etat-remplissage.component';

describe('EtatRemplissageComponent', () => {
  let component: EtatRemplissageComponent;
  let fixture: ComponentFixture<EtatRemplissageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtatRemplissageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatRemplissageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
