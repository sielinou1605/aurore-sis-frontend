import {Component, OnInit} from '@angular/core';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-liste-matieres',
  templateUrl: './liste-matieres.component.html',
  styleUrls: ['./liste-matieres.component.scss']
})
export class ListeMatieresComponent implements OnInit {
  private idClasse: string;
  public currentClasse: any;
  public matieresClasse: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private genericsService: GenericsService,
              private route: ActivatedRoute,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idClasse = params.get('idClasse');
      this.onloadMatiereParClasse(this.idClasse);
    });

  }

  private onloadMatiereParClasse(idClasse: string) {
    this.genericsService.getResource(`admin/matiere-classe/${idClasse}/liste`).then((response: any) => {
      this.currentClasse = response.data.classe;
      this.matieresClasse = response.data.matiereClasses;
    }).catch(reason => {
      console.log(reason);
    });
  }
}
