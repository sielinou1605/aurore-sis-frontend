/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {AjouterSanctionModel} from '../../../../../../shared/_models/request-dto/graduation/gestion-eleve-absence-model';
import {TokenStorageService} from '../../../../../../shared/_services/token-storage.service';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-liste-eleves',
  templateUrl: './liste-eleves.component.html',
  styleUrls: ['./liste-eleves.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListeElevesComponent implements OnInit, OnDestroy {
  private idClasse: string;
  public currentEleve: any;
  sanctionForm: FormGroup;
  submitted: boolean;
  public listEleve: any;
  public totalElements = 0;
  public pageSize = 60;
  public page = 0;
  public dataFilter: any;
  public currentClasse: any;
  public sequences: any;
  public penalites: any;
  public currentAnnee: number;
  public nbreSanctionCumule: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
    private genericsService: GenericsService, private fb: FormBuilder,
    private route: ActivatedRoute, private router: Router,
    private modalService: NgbModal, config: NgbModalConfig,
    public authService: AuthService, public tokenStorage: TokenStorageService,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(56);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idClasse = params.get('idClasse');
      if (this.idClasse) {
        this.onloadListEleveByIdClasse(this.idClasse);
      }
    });
    this.initFormSanctionEleve();
    this.onloadListSequence();
    this.onloadListTypeSanction();
    this.currentAnnee = this.tokenStorage.getUser().codeAnnee;
  }
  onloadListEleveByIdClasse(id: any) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`classes/${id}`).then((response: any) => {
      this.currentClasse = response;
    }).catch(reason => {
      console.log(reason);
    });
    this.genericsService.getResource(`admin/classe/${id}/eleve/liste`).then((response: any) => {
      if (response.success === true) {
        console.log(response);
        this.genericsService.stopLoadingPage();
        this.listEleve = response.data;
        // this.dataFilter = response.data.nomClasse;
        // this.onloadListEleve(this.dataFilter);
      }
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  onloadListEleve(event) {
    this.genericsService.startLoadingPage();
    this.totalElements = 0;
    this.genericsService.getResource(`admin/eleve/liste?predicat=${event}&size=${this.pageSize}&page=${this.page}`)
      .then((result: any) => {
        if (result.success === true) {
          this.listEleve = result.data.content;
          this.totalElements = result.data.totalElements;
        }
        this.genericsService.stopLoadingPage();
      }).catch(reason => {
      console.log(reason);
      this.pageSize = 0;
      this.totalElements = 0;
      this.page = 0;
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') :
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }

  setPageChange(event) {
    this.page = event - 1;
    this.onloadListEleve(this.dataFilter);
  }

  get f() { return this.sanctionForm.controls; }
  onAjouterListeEleve() {}
  onAjouterUnEleve() {
    this.router.navigate(['/eleves/ajout-eleve']).then(() => {});
  }
  private onloadListTypeSanction() {
    this.genericsService.getResource(`parametre/type-penalite/liste`).then((response: any) => {
      this.penalites = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
  onSaveSanction() {
    this.submitted = true;
    if (this.sanctionForm.invalid) {
      return;
    }
    const penalite = this.penalites.find(elt => elt.id == this.f.penalite.value);
    const dto = new AjouterSanctionModel(this.f.debut.value, this.f.fin.value,
      this.f.datesanction.value, this.f.sequence.value, this.currentEleve.matricule,
      this.f.motif.value, this.f.realisation.value, this.f.temoin.value, this.f.motif.value,
      this.f.penalite.value, parseInt(this.idClasse, 0), penalite.libelle);
    console.log(dto);
    this.genericsService.postResource(`admin/sanction/ajouter`, dto).then((response: any) => {
      console.log(response);
      this.onResetSanction();
    }).catch(reason => {
      console.log(reason);
    });
  }
  onSanctionnerEleve(eleve, content: TemplateRef<any>) {
    this.currentEleve = eleve;
    this.onloadListSanctions(eleve);
    this.modalService.open(content,
      {ariaLabelledBy: 'sanction-eleve', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }
  private onloadListSanctions(eleve: any) {
    if (eleve) {
      const  suffix = `/${eleve.matricule}/annee/${this.currentAnnee}/liste?page=${this.page}&size=${this.pageSize}`;
      const currentUrl = `admin/sanction/eleve${suffix}`;
      this.genericsService.getResource(currentUrl).then((response: any) => {
        this.nbreSanctionCumule = response.data.totalElements;
        console.log(this.nbreSanctionCumule);
        console.log(response);
      }).catch(reason => {
        console.log(reason);
      });
    }
  }
  onResetSanction() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
    this.initFormSanctionEleve();
  }
  private initFormSanctionEleve() {
    this.sanctionForm = this.fb.group({
      sequence: ['', Validators.required],
      datesanction: ['', Validators.required],
      motif: ['', Validators.required],
      penalite: ['', Validators.required],
      debut: ['', Validators.required],
      fin: [''],
      realisation: ['false'],
      temoin: [''],
    });
  }

  onPrintCertificatScolarite(eleve: any) {

  }
  onImprimerListe() {
    this.genericsService.printReport(``, 'namefile');
  }
  onImprimerSanctions() {}

  private onloadListSequence() {
    this.genericsService.getResource(`admin/bulletin/filtre/sequence`).then((response: any) => {
      this.sequences = response.data.sequences;
      console.log(this.sequences);
    }).catch(reason => {
      console.log(reason);
    });
  }

  ngOnDestroy(): void {
    this.onResetSanction();
  }
}
