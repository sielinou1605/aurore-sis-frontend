import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditerClasseComponent} from './editer-classe.component';

describe('EditerClasseComponent', () => {
  let component: EditerClasseComponent;
  let fixture: ComponentFixture<EditerClasseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditerClasseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditerClasseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
