import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ClassesRoutingModule} from './classes-routing.module';
import {ClassesComponent} from './classes.component';
import {SharedModule} from '../../../../../shared/shared.module';
import {ListeElevesComponent} from './liste-eleves/liste-eleves.component';
import {DetailsClasseComponent} from './details-classe/details-classe.component';
import {EditerClasseComponent} from './editer-classe/editer-classe.component';
import {EtatRemplissageClasseComponent} from './etat-remplissage-classe/etat-remplissage-classe.component';
import {ListeClassesComponent} from './liste-classes/liste-classes.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {ListeMatieresComponent} from './liste-matieres/liste-matieres.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';


@NgModule({
  declarations: [
    ClassesComponent,
    ListeElevesComponent,
    DetailsClasseComponent,
    EditerClasseComponent,
    EtatRemplissageClasseComponent,
    ListeClassesComponent,
    ListeMatieresComponent,
  ],
    imports: [
        CommonModule,
        ClassesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        TranslateModule
    ]
})
export class ClassesModule {
  lang = 'en';
  constructor(
    private translate: TranslateService
  ) {
    translate.setDefaultLang(this.lang);
  }
}
