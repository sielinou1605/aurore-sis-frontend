/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-liste-classes',
  templateUrl: './liste-classes.component.html',
  styleUrls: ['./liste-classes.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListeClassesComponent implements OnInit, OnDestroy {
  nommerTitulaireForm: FormGroup;
  nommerSurveillantForm: FormGroup;
  fgForm: FormGroup;
  submitted: boolean;
  public classes: any;
  public currenClasse: any;
  public enseignants: any;
  public utilisateurs: any;
  public pageSize = 20;
  public page = 0;
  public totalElements = 0;
  public predicat = '';
  currentSwitch = localStorage.getItem('activelang') === 'en' ? AppConstants.SORT_BY_NOM_CLASSEEN : AppConstants.SORT_BY_NOM_CLASSEFR;
  private sortBy: string;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private genericsService: GenericsService, private fb: FormBuilder,
              private modalService: NgbModal, config: NgbModalConfig,
              public authService: AuthService,
              private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(25);
  }

  ngOnInit() {
    this.fgForm = this.fb.group({
      search: [''],
    });
    // tslint:disable-next-line:max-line-length
    this.sortBy = localStorage.getItem('activelang') === 'en' ? AppConstants.SORT_BY_NOM_CLASSE_VALUEEN : AppConstants.SORT_BY_NOM_CLASSE_VALUEFR;
    this.onLoadListClasse(this.sortBy);
    this.onloadListEnseignant();
    this.onloadListUsers();
    this.initFormTitulaire();
    this.initFormSurveillant();
  }
  get searchElt() { return this.fgForm.controls;}
  uploardList(sortBy?: string) {
    // this.page = 0;
    this.predicat = this.searchElt.search.value;
    // tslint:disable-next-line:max-line-length
    this.genericsService.getResource(`admin/classe/liste?predicat=${this.predicat}&size=${this.pageSize}&page=${this.page}&sortBy=${sortBy}`)
      .then((response: any) => {
        this.classes = response.data.content;
        this.totalElements = response.data.totalElements;
      });
  }
  private onLoadListClasse(sortBy?: string) {
    this.predicat = this.searchElt.search.value;
    // this.page = 0;
    localStorage.getItem('activelang') === 'en' ?
    this.genericsService.startLoadingPage(AppConstants.LOADING_CLASSEEN) : this.genericsService.startLoadingPage(AppConstants.LOADING_CLASSEFR);
    this.genericsService.getResource(`admin/classe/liste?predicat=${this.predicat}&size=${this.pageSize}&page=${this.page}&sortBy=${sortBy}`)
      .then((response: any) => {
        this.classes = response.data.content;
        this.totalElements = response.data.totalElements;
        console.log(this.classes);
        this.genericsService.stopLoadingPage();
      }, err => {
        console.log(err);
        this.genericsService.stopLoadingPage();
        localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
      });
  }
  swicthTo(sortBy: string) {
    this.sortBy = sortBy;
    this.page = 0;

    if (localStorage.getItem('activelang') === 'en' ? sortBy == AppConstants.SORT_BY_NIVEAU_VALUEEN : sortBy == AppConstants.SORT_BY_NIVEAU_VALUEFR) {
      this.currentSwitch = localStorage.getItem('activelang') === 'en' ? AppConstants.SORT_BY_NIVEAUEN : AppConstants.SORT_BY_NIVEAUFR;
    } else if (localStorage.getItem('activelang') === 'en' ? sortBy == AppConstants.SORT_BY_NOM_CLASSE_VALUEEN : sortBy == AppConstants.SORT_BY_NOM_CLASSE_VALUEFR) {
      this.currentSwitch = localStorage.getItem('activelang') === 'en' ? AppConstants.SORT_BY_NOM_CLASSEEN : AppConstants.SORT_BY_NOM_CLASSEFR;
    } else {
      this.currentSwitch = localStorage.getItem('activelang') === 'en' ? AppConstants.SORT_BY_NOM_NIVEAUEN : AppConstants.SORT_BY_NOM_NIVEAUFR;
    }
    this.onLoadListClasse(sortBy);
  }
  setPageChange(event) {
    this.page = event - 1;
    this.onLoadListClasse(this.sortBy);
  }
  private onloadListEnseignant() {
    this.genericsService.getResource('admin/enseignants').then((response: any) => {
      this.enseignants = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }
  private onloadListUsers() {
    this.genericsService.getResource('habilitation/utilisateur/liste').then((response: any) => {
      this.utilisateurs = response.data;
    }).catch((reason: any) => {

    });
  }
  onNommerTitulaire(classe: any, content: TemplateRef<any>) {
    this.currenClasse = classe;
    this.submitted = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'nommer-titulaire', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }
  onNommerSurveillant(classe: any, content: TemplateRef<any>) {
    this.currenClasse = classe;
    this.submitted = false;
    this.modalService.open(content,
      {ariaLabelledBy: 'nommer-surveillant',  scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }
  onSaveNommerSurveillant() {
    this.submitted = true;
    if (this.nommerSurveillantForm.invalid) {
      return;
    }
    this.genericsService
      .postResource(`admin/nomination/surveillant/${this.ns.user.value}/classe/${this.currenClasse.id}`,
        {}).then((response: any) => {
      this.submitted = false;
      this.onLoadListClasse(this.sortBy);
      this.onResetNommerSurveillant();
    }).catch((reason: any) => {

    });
  }
  onSaveNommerTitulaire() {
    this.submitted = true;
    if (this.nommerTitulaireForm.invalid) {
      return;
    }
    this.genericsService
      .postResource(`admin/nomination/titulaire/${this.nt.enseignant.value}/classe/${this.currenClasse.id}`,
        {}).then((response: any) => {
      console.log(response);
      this.submitted = false;
      this.onLoadListClasse(this.sortBy);
      this.onResetNommerTitulaire();
    }).catch((reason: any) => {

    });
  }
  onResetNommerSurveillant() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormSurveillant();
  }
  onResetNommerTitulaire() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormTitulaire();
  }
  onSupprimerClasse(classe: any) {

  }
  private initFormTitulaire() {
    this.nommerTitulaireForm = this.fb.group({
      enseignant: ['', Validators.required]
    });
  }
  private initFormSurveillant() {
    this.nommerSurveillantForm = this.fb.group({
      user: ['', Validators.required]
    });
  }
  get nt() { return this.nommerTitulaireForm.controls; }
  get ns() { return this.nommerSurveillantForm.controls; }

  ngOnDestroy(): void {
    this.onResetNommerSurveillant();
    this.onResetNommerTitulaire();
  }
}
