/* tslint:disable:triple-equals */
import {Component, OnInit, TemplateRef} from '@angular/core';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Absence, EleveAbsenceResponse} from '../../../../../shared/_models/response-dto/graduation/filtre-note-absence-model';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {
  AbsencesARemplir,
  AjouterSanctionModel,
  JustifierAbsenceModel,
  RemplirAbsenceModel
} from '../../../../../shared/_models/request-dto/graduation/gestion-eleve-absence-model';
import {AppConstants} from '../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-admin-absences',
  templateUrl: './admin-absences.component.html',
  styleUrls: ['./admin-absences.component.scss']
})
export class AdminAbsencesComponent implements OnInit {
  private idclasse: string;
  private idsequence: string;
  currentSequence: any;
  currentClasse: any;
  public eleves: EleveAbsenceResponse[];
  public editAbsenceForm: FormGroup;
  public searchForm: FormGroup;
  submitted = false;
  private currentEleve: EleveAbsenceResponse;
  private currentAbsence: Absence;
  sanctionForm: FormGroup;
  public sequences: any;
  public penalites: any;
  public nbreSanctionCumule: any;
  currentAnnee: any;
  public totalElements = 0;
  public pageSize = 60;
  public page = 0;

  public Heures: string;
  public eric: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  public predicat = '';
  public eleves_temp: any[] = [];
   nbrHeurCourant: number;
   nbrHeurJustifier: number;
   justifierAbsence = false;
   showMessage: boolean;

  constructor(
    public tokenStorage: TokenStorageService,
    private genericsService: GenericsService,
    private fb: FormBuilder, private modalService: NgbModal,
    config: NgbModalConfig, private route: ActivatedRoute,
    private translate: TranslateService) {

    this.Heures = localStorage.getItem('activelang') === 'en' ? 'Hours' : 'Heures';
    this.eric = localStorage.getItem('activelang') === 'en'
      ? 'Filling of absences'
      : 'Remplissage des absences';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.route.paramMap.subscribe(params => {
      this.idclasse = params.get('classe');
      this.idsequence = params.get('sequence');
      this.initHeader(this.idclasse, this.idsequence);
    });
    this.initFormSanctionEleve();
    this.onloadListSequence();
    this.onloadListTypeSanction();
    this.currentAnnee = this.tokenStorage.getUser().codeAnnee;
  }

  private onloadListTypeSanction() {
    this.genericsService.getResource(`parametre/type-penalite/liste`).then((response: any) => {
      this.penalites = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }

  onResetSanction() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
    this.initFormSanctionEleve();
  }

  private initFormSanctionEleve() {
    this.sanctionForm = this.fb.group({
      sequence: ['', Validators.required],
      datesanction: ['', Validators.required],
      motif: ['', Validators.required],
      penalite: ['', Validators.required],
      debut: ['', Validators.required],
      fin: [''],
      realisation: ['false'],
      temoin: [''],
    });
  }

  get f() { return this.sanctionForm.controls; }

  private initHeader(classe: any, sequence: any) {
    this.genericsService
      .getResource(`admin/consulter/${classe}/classe`).then((response: any) => {
      this.currentClasse = response.data;
    }).catch(err => {
      console.log(err);
    });

    this.genericsService
      .getResource(`admin/consulter/${sequence}/sequence`).then((response: any) => {
      this.currentSequence = response.data;
      console.log(this.currentSequence);
    }).catch(err => {
      console.log(err);
    });

    this.initBody(classe, sequence);
  }

  initBody(classe: any, sequence: any) {
    this.predicat = this.fSearch.search.value;
    this.genericsService
      .getResource(`prefet/absence/classe/${classe}/sequence/${sequence}/saisie`).then((response: any) => {
      this.eleves = response.data;
      this.eleves_temp = response.data;
      console.log(this.eleves);
    }).catch(err => {
      console.log(err);
    });
  }

  get g() { return this.editAbsenceForm.controls; }

  get fSearch() { return this.searchForm.controls}
  filterElt(elt) {
    this.eleves = this.eleves_temp;
    console.log(this.eleves_temp);
    this.eleves = this.eleves_temp.filter(item =>
      item.eleve.nomEleve.toLowerCase().includes(elt.target.value.toLowerCase())
    );
    console.log(this.eleves);
  }
  uploardTable() {
    this.predicat = this.fSearch.search.value;
    console.log(this.predicat);
    this.genericsService
      .getResource(`prefet/absence/classe/${this.idclasse}/sequence/${this.idsequence}/saisie`).then((response: any) => {
      this.eleves = response.data;
      this.eleves_temp = response.data;
      console.log(this.eleves_temp);
      // this.eleves.forEach((item) => {
      //   console.log(item.absences);
      // });
    }).catch(err => {
      console.log(err);
    });
  }

  onReset() {
    this.justifierAbsence = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }

  initFormAbsence(eleve: EleveAbsenceResponse, absence: Absence) {
    this.editAbsenceForm = this.fb.group({
      matricule: [eleve.eleve.matricule],
      noms: [eleve.eleve.nomEleve + ' ' + eleve.eleve.prenomEleve],
      id_absence: [absence != null ? absence.id_absence : null],
      nbre_heures: [absence != null ? absence.nbre_heures_nj : 0, [Validators.min(1)]],
    });
  }

  onEditAbsence(noteGlobale: TemplateRef<any>, eleve: EleveAbsenceResponse, absence: Absence) {

    console.log(absence);
    if (absence != null) {
      this.justifierAbsence = true;
      this.showMessage = false;
    } else {
      this.justifierAbsence = false;
    }
    this.currentEleve = eleve;
    this.currentAbsence = absence;
    this.initFormAbsence(this.currentEleve, this.currentAbsence);
    this.modalService.open(noteGlobale,
      {ariaLabelledBy: 'edit-absence', size: 'sm', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  testValue(elt: any) {
    if (this.justifierAbsence === true) {
      this.nbrHeurJustifier = elt.target.value;
      this.nbrHeurCourant = this.currentAbsence.nbre_heures_nj;
      if ((this.nbrHeurJustifier > this.nbrHeurCourant) || (this.nbrHeurJustifier <= 0)) {
        this.showMessage = true;
      } else {
        this.showMessage = false;
      }
      // console.log(this.nbrHeurJustifier);
      // console.log(this.nbrHeurCourant);
      // console.log(this.currentAbsence);
    }
  }

  onSaveAbsence() {
    this.submitted = true;
    this.justifierAbsence = false;
    if (this.editAbsenceForm.invalid) {
      return;
    }
    this.genericsService.startLoadingPage();
    if (this.currentAbsence == null) {
      const dto = new RemplirAbsenceModel(parseInt(this.idclasse, 0), parseInt(this.idsequence, 0), []);
      dto.absencesARemplir.push(new AbsencesARemplir(null, this.g.matricule.value, this.g.nbre_heures.value));
      console.log(dto);
      this.onReset();
       this.genericsService.postResource(`admin/absence/remplir`, dto).then((response: any) => {
         this.initBody(this.idclasse, this.idsequence);
         this.genericsService.confirmResponseAPI(response.message, 'success');
         this.genericsService.stopLoadingPage();
       }).catch(reason => {
         this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
       });
    } else {
      const dto = new JustifierAbsenceModel(parseInt(this.idsequence, 0), this.currentEleve.eleve.matricule, []);
      dto.absencesAJustifier.push(new AbsencesARemplir(this.g.id_absence.value, this.g.matricule.value, this.g.nbre_heures.value));
      console.log(dto);
      this.onReset();
       this.genericsService.postResource(`admin/absence/justifier`, dto).then((response: any) => {
         this.initBody(this.idclasse, this.idsequence);
         this.genericsService.confirmResponseAPI(response.message, 'success');
         this.genericsService.stopLoadingPage();
       }).catch(reason => {
         console.log(reason);
         this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
       });
    }
  }

  onDeleteAbsence(item: Absence) {
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: AppConstants.ARE_U_SUREEN,
        text: AppConstants.IRREVERSIBLEEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITEN
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`admin/absence/${item.id_absence}/supprimer`).then((resultDelete: any) => {
            console.log(resultDelete);
            this.initBody(this.idclasse, this.idsequence);
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
          });
        }
      }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`admin/absence/${item.id_absence}/supprimer`).then((resultDelete: any) => {
            console.log(resultDelete);
            this.initBody(this.idclasse, this.idsequence);
            this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }

  private onloadListSequence() {
    this.genericsService.getResource(`admin/bulletin/filtre/sequence`).then((response: any) => {
      this.sequences = response.data.sequences;
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadListSanctions(eleve: any) {
    if (eleve) {
      const  suffix = `/${eleve.eleve.matricule}/annee/${this.currentAnnee}/liste?page=${this.page}&size=${this.pageSize}`;
      const currentUrl = `admin/sanction/eleve${suffix}`;
      this.genericsService.getResource(currentUrl).then((response: any) => {
        this.nbreSanctionCumule = response.data.totalElements;
        console.log(this.nbreSanctionCumule);
        console.log(response);
      }).catch(reason => {
        console.log(reason);
      });
    }
  }

  onSaveSanction() {
    this.submitted = true;
    if (this.sanctionForm.invalid) {
      return;
    }
    const penalite = this.penalites.find(elt => elt.id == this.f.penalite.value);
    const dto = new AjouterSanctionModel(this.f.debut.value, this.f.fin.value,
      this.f.datesanction.value, this.f.sequence.value, this.currentEleve.eleve.matricule,
      this.f.motif.value, this.f.realisation.value, this.f.temoin.value, this.f.motif.value,
      this.f.penalite.value, parseInt(this.idclasse, 0), penalite.libelle);
    console.log(dto);
    this.genericsService.postResource(`admin/sanction/ajouter`, dto).then((response: any) => {
      console.log(response);
      this.onResetSanction();
    }).catch(reason => {
      console.log(reason);
    });
  }
  onSanctionnerEleve(eleve, content: TemplateRef<any>) {
    this.currentEleve = eleve;
    this.onloadListSanctions(eleve);
    this.modalService.open(content,
      {ariaLabelledBy: 'sanction-eleve', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }
}
