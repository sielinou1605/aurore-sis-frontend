import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../../shared/_services/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(public authService: AuthService) {
    authService.returnListAction(95);
  }

  ngOnInit() {
  }

}
