import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { PortailComponent } from './portail.component';
import { SynchronisationComponent } from './synchronisation/synchronisation.component';


const routes: Routes = [
  {
    path: '', component: PortailComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Portail',
      // titre: 'userEleveAjaout.Portail_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Portal | Aurore' : 'Portail | Aurore',
      icon: 'icofont-ui-settings bg-c-blue',
      breadcrumb_caption: 'Gestion des acces au portail - Portail',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome', component: WelcomeComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Portail',
          // titre: 'userEleveAjaout.Portail_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Portal | Aurore' : 'Portail | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_acces_au_portail_Portail',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'gestion-comptes',
        loadChildren: () => import('./gestion-comptes/gestion-comptes.module')
          .then(m => m.GestionComptesModule)
      },
      {
        path: 'synchronisation', component: SynchronisationComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Synchronisation',
          // titre: 'userEleveAjaout.Synchronisation_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Synchronization | Aurore' : 'Synchronisation | Aurore',
            icon: 'icofont-refresh bg-youtube',
          breadcrumb_caption: 'userEleveAjaout.Mettre_a_jour_les_données_en_ligne_et_en_local_Synchronisation',
          status: true,
          current_role: 'admin'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortailRoutingModule { }
