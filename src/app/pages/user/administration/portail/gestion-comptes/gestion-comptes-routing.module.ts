import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { EditerUtilisateurPortailComponent } from './editer-utilisateur-portail/editer-utilisateur-portail.component';
import { ListUtilisateurPortailComponent } from './list-utilisateur-portail/list-utilisateur-portail.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'list-utilisateur-portail',
    pathMatch: 'full'
  },
  {
    path: 'list-utilisateur-portail', component: ListUtilisateurPortailComponent,
    data: {
      breadcrumb: 'autAct.Comptes_d_accès_au_portail',
      // titre: 'autAct.Comptes_d_accès_au_portail_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Portal access accounts | Aurore' : 'Comptes d\'accès au portail | Aurore',
      icon: 'icofont-ui-calendar bg-c-blue',
      breadcrumb_caption: 'autAct.Les_Parents_ayant_demande_accès_a_l_application_en_ligne_Comptes',
      status: true,
      current_role: 'admin'
    }
  },
  // {
  //   path: 'editer-utilisateur-portail', component: EditerUtilisateurPortailComponent,
  //   data: {
  //     breadcrumb: 'Profil',
  //     icon: 'icofont-ui-calendar bg-c-blue',
  //     breadcrumb_caption: 'Ajouter, Modifer un utilisateur du portail - Profils',
  //     status: true,
  //     current_role: 'admin'
  //   }
  // },
  // {
  //   path: 'editer-utilisateur-portail/:id', component: EditerUtilisateurPortailComponent,
  //   data: {
  //     breadcrumb: 'Profil',
  //     icon: 'icofont-ui-calendar bg-c-blue',
  //     breadcrumb_caption: 'Ajouter, Modifer et supprimer un utilisateur du portail - Profils',
  //     status: true,
  //     current_role: 'admin'
  //   }
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionComptesRoutingModule { }
