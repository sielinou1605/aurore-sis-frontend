import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { GestionComptesRoutingModule } from './gestion-comptes-routing.module';
import { ListUtilisateurPortailComponent } from './list-utilisateur-portail/list-utilisateur-portail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'ngx-avatar';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [ListUtilisateurPortailComponent],
  imports: [
    CommonModule,
    GestionComptesRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    /** /https://www.npmjs.com/package/ngx-avatar **/
    AvatarModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxPaginationModule
  ]
})
export class GestionComptesModule { }
