import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUtilisateurPortailComponent } from './list-utilisateur-portail.component';

describe('ListUtilisateurPortailComponent', () => {
  let component: ListUtilisateurPortailComponent;
  let fixture: ComponentFixture<ListUtilisateurPortailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUtilisateurPortailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUtilisateurPortailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
