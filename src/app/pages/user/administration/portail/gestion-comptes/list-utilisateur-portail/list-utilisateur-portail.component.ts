/* tslint:disable:triple-equals */
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { GenericsService } from '../../../../../../shared/_services/generics.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../../../../../../shared/_helpers/mustmatch-validator';
import { AddUserRequest, Caisse, RoleModel, UpdateUserRequestModel } from '../../../../../../shared/_models/auth/add-user-request';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AjouterCaisseToUser, AjouterRoleToUser } from '../../../../../../shared/_models/auth/request-role-management-model';
import { AuthService } from '../../../../../../shared/_services/auth.service';
import { AppConstants } from '../../../../../../shared/element/app.constants';
import { UtilisateurPortailRequestDto } from '../../../../../../shared/_models/request-dto/common/utilisateur-portail-model';

@Component({
  selector: 'app-list-utilisateur-portail',
  templateUrl: './list-utilisateur-portail.component.html',
  styleUrls: ['./list-utilisateur-portail.component.scss']
})
export class ListUtilisateurPortailComponent implements OnInit {

  public utilisateurs: any;
  public edit: boolean;
  currentUser = null;

  UtilisateurForm: FormGroup;
  public roles: any;
  submitted: boolean;
  public idUser: number;
  public caisses: any;
  public error_add_user: any;
  public servicesForm: FormGroup;
  public applicationBonusForm: FormGroup;
  private url_caisse = 'admin/caisse/';
  // @ts-ignore
  @ViewChild('selectRole') selectRole;
  // @ts-ignore
  @ViewChild('selectCaisse') selectCaisse;
  dataEleve;
  // @ts-ignore
  @ViewChild('selectEleve') selectEleve;
  dataParent;
  // @ts-ignore
  @ViewChild('selectParent') selectParent;
  public loadContent = false;
  public name = 'Cricketers';
  public utilisateursPortail = [];
  public dataListeEleves = [];
  public dataRole = [];
  public dataCaisse = [];
  public eleveUserSelect = [];
  public searchForm: FormGroup;
  public settings: any;
  public settingsParent: any;
  public selectedRoles = [];
  public selectedCaisse = [];
  public isNotifieAdd = false;
  public isNotifieEdit = false;
  public isNotifieDel = false;
  public totalElements = 0;
  public pageSize = 40;
  public page = 0;
  public modeleEcheanciers: any;
  public echeanciersEleve: any;
  private EcheancierDelete = [];
  private canAddService = false;
  public canSave = false;
  private baseEcheancier: number;
  public listParentEleves: any;
  public listeEleves: any;
  private base_url = 'admin/utilisateurs-portail';
  listInscriptionFiltre: any;
  private dataFilter = '';
  public placeholder: string;

  constructor(public genericsService: GenericsService, private fb: FormBuilder,
              private modalService: NgbModal, config: NgbModalConfig,
              public authService: AuthService) {
    localStorage.getItem('activelang') === 'en' ? this.placeholder = '----- Select a student -----' : this.placeholder = '----- Choisir un eleve -----';
    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(94);
  }

  ngOnInit() {
    // this.onLoadListUtilisateur();
    localStorage.getItem('activelang') === 'en' ?
      this.settings = {
        singleSelection: true,
        idField: 'id',
        textField: 'description',
        enableCheckAll: false,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: AppConstants.SEARCHEN,
        noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEEN,
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      } : this.settings = {
        singleSelection: true,
        idField: 'id',
        textField: 'description',
        enableCheckAll: false,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: AppConstants.SEARCHFR,
        noDataAvailablePlaceholderText: AppConstants.UNAIVALAIBLEFR,
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };
    this.searchForm = this.fb.group({
      search: [''],
      searchParent: ['']
    });
    // this.onLoadListRoles();
    // this.onLoadListCaisses();
    this.initFormUser();
    this.initServiceForm();
    // this.onFilterInscription('');
    // this.onLoadListUtilisateurPortail();
    this.onLoadListParent('', true);
    this.onLoadListParentPortail('', true);
    // this.onLoadListEleves();
    this.onloadListEleves('');
    this.initFormSearchEleve();
  }

  public onLoadListParent(event, reInitPage?: boolean) {
    console.log('****************************************');
    console.log(event);
    if (reInitPage == true) {
      this.page = 0;
    }
    let predicat: string;
    if (event) {
      predicat = this.genericsService.cleanToken(event);
    } else {
      predicat = event;
    }
    this.dataFilter = predicat;
    this.genericsService.startLoadingPage();
    // this.listMatricule = [];
    this.genericsService
      .getResource(`${this.base_url}?predicat=${predicat}&size=${this.pageSize}&page=${this.page}`).then((result: any) => {
      if (result.success === true) {
        // this.listEleve = result.data.content;
        this.utilisateursPortail = result.data.content;
        // this.enseignants = result.data.content;
        console.log(result.data.content);
        this.totalElements = result.data.totalElements;
      }
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Unknown error !!! ', 'error')
        : this.genericsService.confirmResponseAPI('Erreur inconnue !!! ', 'error');
    });
  }

  public onLoadListElevesParent(event) {
    // this.UtilisateurForm.patchValue({
    //   id: utilisateur.id,
    //   nomParent: utilisateur.nomParent,
    //   identifiantPortail: utilisateur.identifiantPortail,
    //   email: utilisateur.email
    // });

    console.log('****************************************');

    this.dataFilter = event;
    this.genericsService.startLoadingPage();
    // this.listMatricule = [];
    this.genericsService
      .getResource(`admin/parents-eleves/${event}`).then((result: any) => {
      if (result.success === true) {

        this.eleveUserSelect = [];
        this.t.clear();
        result.data.forEach(el => {
          this.t.push(
            this.fb.group({
              matricule: [el.matricule],
              nomEleve: [el.nomEleve],
              classe: [el.classe],
            })
          );
          this.eleveUserSelect.push(el.matricule);
        });
        //this.currentUser = utilisateur;
      }
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmResponseAPI('Unknown error !!! ', 'error')
        : this.genericsService.confirmResponseAPI('Erreur inconnue !!! ', 'error');
    });
  }

  private onLoadListUtilisateurPortail() {
    this.genericsService.startLoadingPage();

    localStorage.getItem('activelang') === 'en' ?
      this.genericsService.getResource(`admin/utilisateurs-portail`).then((result: any) => {
        this.utilisateursPortail = result.data.content;
        this.genericsService.stopLoadingPage();
        if (this.isNotifieAdd === true) {
          this.isNotifieAdd = false;
          this.genericsService.confirmResponseAPI(AppConstants.COMPTE_ACCES_CREATEDEN, 'success');
        }
        if (this.isNotifieEdit === true) {
          this.isNotifieEdit = false;
          this.genericsService.confirmResponseAPI(AppConstants.USER_UPDATEDEN, 'success');
        }
        if (this.isNotifieDel === true) {
          this.isNotifieDel = false;
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDEN, AppConstants.DELETE_SUCCESSEN, 'success');
        }
      }, err => {
        console.log(err);
        this.genericsService.stopLoadingPage();
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error');
      }) : this.genericsService.getResource(`admin/utilisateurs-portail`).then((result: any) => {
        this.utilisateursPortail = result.data.content;
        this.genericsService.stopLoadingPage();
        if (this.isNotifieAdd === true) {
          this.isNotifieAdd = false;
          this.genericsService.confirmResponseAPI(AppConstants.COMPTE_ACCES_CREATEDFR, 'success');
        }
        if (this.isNotifieEdit === true) {
          this.isNotifieEdit = false;
          this.genericsService.confirmResponseAPI(AppConstants.USER_UPDATEDFR, 'success');
        }
        if (this.isNotifieDel === true) {
          this.isNotifieDel = false;
          this.genericsService.confirmModalResponseAPI(AppConstants.DELETEDFR, AppConstants.DELETE_SUCCESSFR, 'success');
        }
      }, err => {
        console.log(err);
        this.genericsService.stopLoadingPage();
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
      });
  }

  private onLoadListParentPortail(event, reInitPage?: boolean) {
    console.log(event);
    if (reInitPage == true) {
      this.page = 0;
    }
    let predicat: string;
    if (event) {
      predicat = this.genericsService.cleanToken(event);
    } else {
      predicat = event;
    }
    this.dataFilter = predicat;

    localStorage.getItem('activelang') === 'en' ?
      this.genericsService.getResource(`admin/parents-eleves?predicat=${predicat}&size=${this.pageSize}&page=${this.page}`).then((result: any) => {
        this.listParentEleves = result.data.content;

      }, err => {
        console.log(err);
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error');
      }) : this.genericsService.getResource(`admin/parents-eleves?predicat=${predicat}&size=${this.pageSize}&page=${this.page}`).then((result: any) => {
        this.listParentEleves = result.data.content;
      }, err => {
        console.log(err);
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
      });
  }


  onDeleteUtilisateurPortail(id: any) {
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: AppConstants.ARE_U_SUREEN,
        text: AppConstants.IRREVERSIBLEEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITEN
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}/${id}`).then((resultDelete: any) => {
            this.isNotifieDel = true;
            // this.onLoadListUtilisateurPortail();
            this.onLoadListParent('', true);
          }).catch(reason => {
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDEN, AppConstants.DELETE_FAILEN, 'error');
          });
        }
      }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_DELETE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.deleteResource(`${this.base_url}/${id}`).then((resultDelete: any) => {
            this.isNotifieDel = true;
            // this.onLoadListUtilisateurPortail();
            this.onLoadListParent('', true);
          }).catch(reason => {
            this.genericsService.confirmModalResponseAPI(AppConstants.NO_DELETEDFR, AppConstants.DELETE_FAILFR, 'error');
          });
        }
      });
  }

  d() {
    // const params = `predicat=${predicat}&size=${this.pageSize}&page=${this.page}`;
    this.genericsService.getResource(`admin/generer/compte-portail`).then((result: any) => {
      console.log(result);
    }).catch(reason => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }
  onGenereCompteParent() {
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: AppConstants.ARE_U_SUREEN,
        text: AppConstants.IRREVERSIBLEEN,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: AppConstants.CANCELEN,
        confirmButtonText: AppConstants.GENERATE_ITEN
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.startLoadingPage();
          this.genericsService.getResource(`admin/generer/compte-portail`).then((result: any) => {
            console.log(result);
            this.genericsService.stopLoadingPage();
            this.onLoadListParent('', true);
          }).catch(reason => {
            console.log(reason);
            this.genericsService.stopLoadingPage();
            localStorage.getItem('activelang') === 'en' ?
              this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
          });
        }
      }) : Swal.fire({
        title: AppConstants.ARE_U_SUREFR,
        text: AppConstants.IRREVERSIBLEFR,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: AppConstants.CANCELFR,
        confirmButtonText: AppConstants.GENERATE_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.startLoadingPage();
          this.genericsService.getResource(`admin/generer/compte-portail`).then((result: any) => {
            console.log(result);
            this.genericsService.stopLoadingPage();
            this.onLoadListParent('', true);
          }).catch(reason => {
            console.log(reason);
            this.genericsService.stopLoadingPage();
            localStorage.getItem('activelang') === 'en' ?
              this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
          });
        }
      });
  }
  get g() { return this.applicationBonusForm.controls; }
  initFormSearchEleve() {
    this.applicationBonusForm = this.fb.group({
      eleve: [],
    });
  }
  public onChangeEleve(model: any, i: number) {
    const isExist = this.eleveUserSelect.filter(o => String(o) === String(model.matricule));
    console.log(i, ' Eleve : ', this.eleveUserSelect, '\n isExist : ', isExist, '\n Amodel', model);

    if (isExist.length === 0) {
      this.genericsService.getResource(`admin/eleves/matricule/${model.matricule}`).then((result: any) => {
        console.log('Result', result);
        // const IsExist = this.eleveUserSelect.filter(o => String(o) === String(result.data.matricule));
        // console.log(this.eleveUserSelect, 'pppppppp : ', IsExist);
        // (IsExist.length === 0) ? this.eleveUserSelect.push(result.data.matricule) : { this.genericsService.confirmResponseAPI(AppConstants.ELEVE_EXISTE, 'error'); return; };
        if (i === this.eleveUserSelect.length) {
          this.eleveUserSelect.push(result.data.matricule);
        } else if (i <= this.eleveUserSelect.length) {
          this.eleveUserSelect[i] = result.data.matricule;
        }
        this.t.at(i).patchValue({
          nomEleve: result.data.nomEleve,
          classe: result.data.classe.nomClasse
        });
        console.log(this.t.at(i), ' ****GET init : ', this.t.value, '  ***GET ME : ', this.echeanciersEleve);
        this.canAddService = true;
      });
    } else {
      // localStorage.getItem('activelang') === 'en' ?
      //   this.genericsService.confirmResponseAPI(AppConstants.ELEVE_EXISTEEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.ELEVE_EXISTEFR, 'error');

    }

    console.log(this.eleveUserSelect, model.matricule, i);
  }

  onloadListEleves(predicat: string) {
    const params = `predicat=${predicat}&size=${this.pageSize}&page=${this.page}`;
    this.genericsService.getResource(`admin/inscription/liste?${params}`).then((result: any) => {
      this.listeEleves = result.data.content;
      if (result.data.content.length == 0) {
        this.onloadListEleves('');
      } else {
        if (this.dataEleve) {
          this.dataEleve = null;
          this.dataEleve = [];
        } else {
          this.dataEleve = [];
        }
        result.data.content.forEach(eleve => {//+ ' ' + eleve.noms
          this.dataEleve.push({ id: eleve.matricule, description: eleve.matricule});
        });
        this.settings.data = this.dataEleve;
      }
    }).catch(reason => {
      console.log(reason);
      localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }
  onChangeListParent(event: any) {
    // console.log(" E_________onChange Event : ", event.target.value);

    const find = this.listParentEleves.find(x => x.nomParent === event.target.value);

    if (find) {
      this.UtilisateurForm.patchValue({
        id: find.id,
        nomParent: find.nomParent,
        identifiantPortail: find.telephoneParent,
        email: find.email
      });
      // this.currentUser = utilisateur;
      console.log(find);
      if (find.telephoneParent) {
        this.onLoadListElevesParent(find.telephoneParent);
      }
      // this.onLoadListElevesParent(find.telephoneParent?find.telephoneParent:find.nomParent);

    }

    this.onLoadListParentPortail(event.target.value, true);
  }
  onChange(event: any, i: number) {
    console.log(this.listeEleves, ' E_________onChange Event : ', event.target.value);
    const find = this.listeEleves.find(x => x.matricule === event.target.value);

    if (find) {
      //this.currentUser = utilisateur;
      console.log(find);
      this.onChangeEleve(find, i);
    }
    this.onloadListEleves(event.target.value);
  }
  onReloadListEleve(event: any) {
    // this.predicat += event.target.value;
    // let elevesSelectionne = event;
    // if (elevesSelectionne.length > this.g.eleve.value.length) {
    //   elevesSelectionne.forEach(e => {
    //     const isExist = this.g.eleve.value.filter(o => o.id === e.id);
    //     if (isExist.length === 0) this.g.eleve.value.push(e);
    //   });
    // } else {
    //   console.log(" Pensez a retire les elts ");

    //   //this.g.eleve.value.forEach
    // }
    // console.log(" E_________ onReloadListEleve Event : ", event);

  }


  public onFilterInscription(event, reInitPage?: boolean) {
    if (reInitPage == true) {
      this.page = 0;
    }
    console.log(event);
    let predicat: string;
    if (event) {
      predicat = this.genericsService.cleanToken(event);
    } else {
      predicat = event;
    }

    console.log(predicat);
    this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(`admin/inscription/liste?predicat=${predicat}&size=${this.pageSize}&page=${this.page}`)
      .then((response: any) => {
        // this.inscriptions = response.data.content;
        this.listInscriptionFiltre = response.data.content;
        this.totalElements = response.data.totalElements;

        this.genericsService.stopLoadingPage();
      }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      this.genericsService.confirmResponseAPI(reason.message, 'error');
    });

    console.log('--------------------------------------------------------------- T : ', this.t);
  }
  onEditUser(utilisateur: any, modalEditUser: TemplateRef<any>) {
    console.log('utilisateur C ____ : ', utilisateur);
    this.modalService.open(modalEditUser,
      { ariaLabelledBy: 'edit-user', scrollable: true, size: 'lg' }).result.then((result) => {
    }, (reason) => {
    });
    if (utilisateur != null) {
      // this.UtilisateurForm.controls['password'].clearValidators();
      // this.UtilisateurForm.controls['confirmation'].clearValidators();
      // this.UtilisateurForm.get('password').updateValueAndValidity();
      // this.UtilisateurForm.get('confirmation').updateValueAndValidity();


      this.UtilisateurForm.patchValue({
        id: utilisateur.id,
        nomParent: utilisateur.nomParent,
        identifiantPortail: utilisateur.identifiantPortail,
        email: utilisateur.email
      });
      this.eleveUserSelect = [];
      this.t.clear();
      utilisateur.eleves.forEach(el => {
        this.t.push(
          this.fb.group({
            matricule: [el.matricule],
            nomEleve: [el.nomEleve],
            classe: [el.classe],
          })
        );
        this.eleveUserSelect.push(el.matricule);
      });
      this.currentUser = utilisateur;
    } else {
      this.eleveUserSelect = [];
      this.t.clear();
      this.t.push(
        this.fb.group({
          matricule: [''],
          nomEleve: [],
          classe: [],
        })
      );

      this.UtilisateurForm.patchValue({
        id: 0,
        nomParent: '',
        identifiantPortail: '',
        email: ''
      });
    }
    this.currentUser = utilisateur;
    this.edit = true;
    console.log('Liste Eleve : ', this.eleveUserSelect, ' List From Eleve : ', this.t);

  }

  updateView(event: any) {
    this.edit = event;
  }


  private initFormUser() {
    this.UtilisateurForm = this.fb.group({
      id: [0],
      nomParent: ['', Validators.required],
      identifiantPortail: ['', [Validators.required, Validators.pattern('^6[0-9]{8}$')]],
      email: [''],
    }, {
      //validator: MustMatch('password', 'confirmation')
    });
  }
  // initialisation du formulaire array sur les echeanciers
  private initServiceForm() {
    this.servicesForm = this.fb.group({
      servicesData: new FormArray([])
    });
  }
  // les elements du formulaire globale
  get f() { return this.servicesForm.controls; }

  // les elements du formulaire local
  get t() { return this.f.servicesData as FormArray; }

  // Ajouter un element dans le array des echeanciers selectionné
  onAddLineToServiceSelect() {
    console.log('init : ', this.t, ' ME : ', this.modeleEcheanciers);
    // this.canSave = true;
    this.t.push(
      this.fb.group({
        matricule: [''],
        nomEleve: [],
        classe: [],
      })
    );
  }



  // supprimer une ligne de formulaire locale
  onDeleleLineToServiceSelect(position) {
    const elt = this.t.value[position];
    console.log('element : ', elt);
    const indexOf = (elt.nomEleve !== null) ? this.eleveUserSelect.findIndex(o => (String(o) === String(elt.matricule))) : -1;
    console.log('AVANT : ' + indexOf + ' Position : ' + position, 'Object : ', this.eleveUserSelect, ' Eleve echeance : ', this.echeanciersEleve);
    //if (indexOf === -1) {
    if (elt.nomEleve == null || indexOf === -1) {
      this.onDeleleLineToServiceSelectApi(position);
    } else {
      //const indexOf = this.eleveUserSelect.findIndex(o => (String(o) === String(elt.matricule[0].id)));
      this.onDeleleLineToServiceSelectApi(position);
      (elt.nomEleve !== null) ? this.eleveUserSelect.splice(indexOf, 1) : '';
    }
    console.log('Apres : ', this.eleveUserSelect);
  }
  onDeleleLineToServiceSelectApi(position) {
    this.t.removeAt(position);
    if (this.t.length == this.baseEcheancier) {
      this.canSave = false;
    }
  }

  get search() { return this.searchForm.controls; }
  get searchParent() { return this.searchForm.controls; }

  get fuser() { return this.UtilisateurForm.controls; }
  onSaveUtilisateur() {
    console.log('---------------', this.eleveUserSelect);
    this.submitted = true;
    if (this.UtilisateurForm.invalid) {
      localStorage.getItem('activelang') === 'en' ?
        this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.ERROR_FORMFR, 'error');
      return;
    }
    this.genericsService.startLoadingPage();
    if (this.currentUser) {
      const updateUserRequest = new UtilisateurPortailRequestDto(
        this.fuser.id.value,
        this.fuser.identifiantPortail.value,
        this.fuser.email.value,
        this.fuser.nomParent.value,
        this.eleveUserSelect
      );
      console.log(updateUserRequest);
      this.genericsService.putResource(`admin/utilisateurs-portail/modifier`, updateUserRequest).then((result: any) => {
        this.isNotifieEdit = true;
        this.genericsService.stopLoadingPage();
        this.onResetUtilisateur();
      }, err => {
        console.log(err);
        this.genericsService.stopLoadingPage();
        this.genericsService.confirmModalResponseAPI(err.error.message, 'Echec', 'error');
      });
    } else {
      const userRequest = new UtilisateurPortailRequestDto(
        this.fuser.id.value,
        this.fuser.identifiantPortail.value,
        this.fuser.email.value,
        this.fuser.nomParent.value,
        this.eleveUserSelect
      );
      console.log(userRequest);
      this.genericsService.postResource(`admin/utilisateurs-portail/ajouter`, userRequest).then((result: any) => {
          this.isNotifieAdd = true;
          this.genericsService.stopLoadingPage();
          this.onResetUtilisateur();
          // this.genericsService.confirmResponseAPI(AppConstants.COMPTE_ACCES_CREATED, 'succes');
        },
        err => {
          console.log(err);
          //this.error_add_user = err.error.message;
          this.genericsService.stopLoadingPage();
          this.genericsService.confirmModalResponseAPI(err.error.message, 'Echec', 'error');
        });
    }
  }


  onCloseModalUtilisateur() {
    this.applicationBonusForm.setValue({ eleve: [] });
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormUser();
    this.edit = false;
    this.currentUser = null;
    this.submitted = false;
  }
  onResetUtilisateur() {
    this.applicationBonusForm.setValue({ eleve: [] });
    // this.onLoadListUtilisateurPortail();
    this.onLoadListParent('', true);
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormUser();
    this.edit = false;
    this.currentUser = null;
    this.submitted = false;
  }


  ngOnDestroy(): void {
    // this.onResetUtilisateur();
    this.onCloseModalUtilisateur();
  }
  setPageChange(event) {
    this.page = event - 1;
    // this.onLoadListUtilisateurPortail();
    this.onLoadListParent(this.dataFilter);
  }

  onReloadListUserPortail(value: any, b: boolean) {

  }

}
