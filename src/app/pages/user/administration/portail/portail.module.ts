import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortailRoutingModule } from './portail-routing.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { PortailComponent } from './portail.component';
import { GestionComptesComponent } from './gestion-comptes/gestion-comptes.component';
import { GestionComptesModule } from './gestion-comptes/gestion-comptes.module';
import { SharedModule } from '../../../../shared/shared.module';
import { SynchronisationComponent } from './synchronisation/synchronisation.component';

@NgModule({
  declarations: [PortailComponent,
    SynchronisationComponent, WelcomeComponent, GestionComptesComponent],
  imports: [
    CommonModule,
    PortailRoutingModule,
    GestionComptesModule,
    SharedModule
  ]
})
export class PortailModule { }
