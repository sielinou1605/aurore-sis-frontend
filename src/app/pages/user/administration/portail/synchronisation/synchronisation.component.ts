/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit} from '@angular/core';
import { AuthService } from '../../../../../shared/_services/auth.service';
import { GenericsService } from '../../../../../shared/_services/generics.service';
import { AppConstants } from '../../../../../shared/element/app.constants';
import { TranslateService } from '@ngx-translate/core';
import {Subscription} from 'rxjs/Subscription';
import {interval} from 'rxjs';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';

@Component({
  selector: 'app-synchronisation',
  templateUrl: './synchronisation.component.html',
  styleUrls: ['./synchronisation.component.scss']
})
export class SynchronisationComponent implements OnInit, OnDestroy {

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  private checkActiveSynchroSubscription: Subscription;
  public checkActiveSynchroPortail = true;
  public checkActiveSynchroCode = false;
  public groupe = 'seca';
  public compteRestantASynchroniser=0;
  public totalCompte=0;
  public codeInstitution = 'COSAJO';
  public host = 'http://127.0.0.1:8090/';
  public lastStatusSynchro = 'PENDING';
  constructor(
    private translate: TranslateService,
    public authService: AuthService,
    public tokenStorage: TokenStorageService,
    private genericsService: GenericsService) {
    this.siteLanguage = localStorage.getItem('activelang');
    authService.returnListAction(97);
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
    // recuperer le nom du groupe
    this.genericsService.getResource(`parametre/variable-globale/CODE_GRP_ETAB/consulter`)
      .then((resp: any) => {
        this.groupe = resp.data.valeur;
        // recuperer l'hote du portail pour la synchronisation
        this.genericsService.getResource(`parametre/variable-globale/AURORE_BACKEND_HOST/consulter`)
          .then((resp1: any) => {
            this.host = resp1.data.valeur;
            // recuperer l'institution qu'on manipule
            this.genericsService.getResource(`institutions`).then((resp2: any) => {
              this.codeInstitution =  resp2._embedded.institutions[0].code_institution;
              // verifier et checker la derniere synchronisation et la valider
              if (this.tokenStorage.getGenericElement('ID_SYNCHRO')) {
                this.getLastSynchro(this.tokenStorage.getGenericElement('ID_SYNCHRO').data);
                this.onCheckStatusSynchronisation(this.tokenStorage.getGenericElement('ID_SYNCHRO').data);
              } else {
                this.lastStatusSynchro = 'CANCEL';
              }
            }).catch(err => console.log(err));
          })
          .catch(errVG => { console.log(errVG); });
      })
      .catch(errVG => { console.log(errVG); });
    this.onGetStatSynchro()
  }
  onGetStatSynchro(){
    this.genericsService.getResource(`admin/statistique/synchro`).then((response: any) => {
      console.log("Stat synchro",response)
      this.totalCompte= response.data.totalCompte;
      this.compteRestantASynchroniser=response.data.totalCompte - response.data.compteDejaSynchroniser;
    }).catch(err => console.log(err));
  }
  onPublierAuPortail() {
    this.genericsService.startLoadingPage();
    this.genericsService.postResource(`admin/synchronisation/recevoir`, {})
      .then((response1: any) => {
        this.genericsService.stopLoadingPage();
        this.publier();
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmModalResponseAPI(response1.success == true ? AppConstants.SYNC_ENCOURSEN : AppConstants.SYNC_FAILEDEN,
          response1.success == true ? AppConstants.SYNC_INFOEN : AppConstants.SYNC_FAILEDEN,
          response1.success == true ? 'success' : 'error')
          : this.genericsService.confirmModalResponseAPI(response1.success == true ? AppConstants.SYNC_ENCOURSFR : AppConstants.SYNC_FAILEDFR,
          response1.success == true ? AppConstants.SYNC_INFOFR : AppConstants.SYNC_FAILEDFR,
          response1.success == true ? 'success' : 'error');
      }).catch(err => {
      this.publier();
      // this.genericsService.stopLoadingPage();
      // this.genericsService.confirmModalResponseAPI('Error sur le serveur, contacter l\'administrateur',
      //   'Echec de synchronisation', 'error');
    });

  }

  onGetCompteUtilisateurPortail() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/synchronisation/code-acces/liste`)
      .then((response1: any) => {
        this.genericsService.stopLoadingPage();
        this.totalCompte= response1.data.totalCompte;
        this.compteRestantASynchroniser=response1.data.totalCompte - response1.data.compteDejaSynchroniser;
        // this.publier();
        localStorage.getItem('activelang') === 'en'
          ? this.genericsService.confirmModalResponseAPI(response1.message,
          response1.success == true ? response1.message : AppConstants.SYNC_FAILEDEN,
          response1.success == true ? 'success' : 'error')
          : this.genericsService.confirmModalResponseAPI(response1.message,
          response1.success == true ? response1.message : AppConstants.SYNC_FAILEDFR,
          response1.success == true ? 'success' : 'error');
      }).catch(err => {
      // this.publier();
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en'
        ? this.genericsService.confirmModalResponseAPI('Error on the server, contact the administrator',
        'Synchronization failure', 'error')
        : this.genericsService.confirmModalResponseAPI('Error sur le serveur, contacter l\'administrateur',
        'Echec de synchronisation', 'error');
    });
  }

  private publier() {
    this.genericsService.postResource('admin/synchronisation/new', {}).then((resp: any) => {
      this.tokenStorage.saveGenericElement('ID_SYNCHRO', resp);
      if (this.checkActiveSynchroSubscription) {
        this.checkActiveSynchroSubscription.unsubscribe();
      }

      this.lastStatusSynchro='PENDING';
      //this.checkActiveSynchroCode = false;
      this.onCheckStatusSynchronisation(resp.data);
      this.genericsService.postResource(`admin/synchronisation/publier/${resp.data}`, {})
        .then((response2: any) => {
          console.log(response2);
          this.lastStatusSynchro='STOP';
          if (this.checkActiveSynchroSubscription) {
            this.checkActiveSynchroSubscription.unsubscribe();
          }
          this.checkActiveSynchroCode = true;
        }).catch(err => {
        console.log(err);
        this.genericsService.stopLoadingPage();
        // this.genericsService.confirmModalResponseAPI(AppConstants.INTERNAL_SERVER_ERROR,
        //   AppConstants.SYNC_FAILED, 'error');
      });
    }).catch(errNew => {
      console.log(errNew);
    });
  }

  private getLastSynchro(id: any) {
    this.genericsService
      .getRawResource(`${this.host}api/admin/synchronisation/prefix/${this.groupe}/etablissement/${this.codeInstitution}/statut/${id}`)
      .then((resp: any) => {
        if (resp == 'SUCCESS' || resp == 'CANCEL'|| resp == 'FAIL') {
          this.lastStatusSynchro = resp;
          if (this.checkActiveSynchroSubscription) {
            this.checkActiveSynchroSubscription.unsubscribe();
          }
          this.checkActiveSynchroCode = true;
          console.log(resp);
        }
      }).catch(errStatus => {
      this.lastStatusSynchro = 'STOP';
      console.log(errStatus);
    });
  }

  private onCheckStatusSynchronisation(id: any) {
    this.checkActiveSynchroSubscription = interval(30000).subscribe((val) => {
      this.getLastSynchro(id);
    });
  }

  private onloadListeSynchronisation() {
    this.genericsService.getResource(`admin/synchronisation/groupe/liste`)
      .then((response: any) => {
        console.log(response);
      }).catch(err => {
      console.log(err);
    });
  }

  onValiderTelephone() {
    this.genericsService.startLoadingPage();
    this.genericsService.postResource(`admin/synchronisation/formater/telephone`, {})
      .then((response: any) => {
        this.genericsService.stopLoadingPage();
        console.log(response);
      }).catch(err => {
      console.log(err);
      this.genericsService.stopLoadingPage();
    });
  }

  onSyncTemponLocal() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/valid/note/from/tempon`)
      .then((response: any) => {
        this.genericsService.stopLoadingPage();
        console.log(response);
      }).catch(err => {
      console.log(err);
      this.genericsService.stopLoadingPage();
    });
  }

  ngOnDestroy(): void {
    if (this.checkActiveSynchroSubscription) {
      this.checkActiveSynchroSubscription.unsubscribe();
    }
  }
}
