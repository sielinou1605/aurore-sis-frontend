/* tslint:disable:triple-equals */
import {Component, ElementRef, HostListener, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {TokenStorageService} from '../../../../../shared/_services/token-storage.service';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EleveNoteResponse} from '../../../../../shared/_models/response-dto/graduation/filtre-note-absence-model';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {
  attribuerNoteRequestDto,
  EleveNoteModel,
  ModifierNoteModel,
  RemplirNoteModel
} from '../../../../../shared/_models/request-dto/graduation/gestion-notes-enseignant_model';
import {AppConstants} from '../../../../../shared/element/app.constants';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {TranslateService} from '@ngx-translate/core';
import {HttpEventType} from '@angular/common/http';

@Component({
  selector: 'app-admin-notes',
  templateUrl: './admin-notes.component.html',
  styleUrls: ['./admin-notes.component.scss']
})
export class AdminNotesComponent implements OnInit {

  // @ts-ignore
  @ViewChild('fileInput') fileInput: ElementRef;
  file1: File = null;

  private idmatiereclasse: string;
  private idsequence: string;
  currentMatiereClasse: any;
  currentSequence: any;
  elevesNote: EleveNoteResponse[];
  noteGlobaleForm: FormGroup;
  form: FormGroup;
  private notesForm: FormGroup;
  submitted = false;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  filtreNote: any;
  matierDestination: any;
  selectMatiere: any;
  choix: string;
  settingsNote = {};
  line: any;
  matiereSource: any;
  istrue: boolean;
  selectedFiles: any;
  file: any;
  progress: number;
  importFiles: boolean;
  fileToUpload: File | null = null;

  constructor(
    public tokenStorage: TokenStorageService,
    private genericsService: GenericsService,
    private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    private route: ActivatedRoute, private router: Router,
    private translate: TranslateService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.choix = localStorage.getItem('activelang') === 'en' ? 'Choose value' : 'Choisir la valeur';

    this.settingsNote = localStorage.getItem('activelang') === 'en'
      ? {
        singleSelection: true,
        idField: 'matiereClasseId',
        textField: 'matiereClasseLabel',
        enableCheckAll: false,
        selectAllText: 'Check all',
        unSelectAllText: 'Uncheck all',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: 'Search',
        noDataAvailablePlaceholderText: 'No data available',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      }
      : {
        singleSelection: true,
        idField: 'matiereClasseId',
        textField: 'matiereClasseLabel',
        enableCheckAll: false,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: 'Rechercher',
        noDataAvailablePlaceholderText: 'Aucune donnée disponible',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };

    this.initForm();
    this.initNotesForm();
    this.route.paramMap.subscribe(params => {
      this.idmatiereclasse = params.get('matiereclasse');
      this.idsequence = params.get('sequence');
      console.log(this.idmatiereclasse);
      this.initHeader(this.idmatiereclasse, this.idsequence);
    });
  }

  private initNotesForm() {
    this.notesForm = this.fb.group({
      notesData: new FormArray([])
    });
  }

  get fNotes() {
    return this.notesForm.controls;
  }

  get tNotes() {
    return this.fNotes.notesData as FormArray;
  }

  get g() {
    return this.noteGlobaleForm.controls;
  }

  private initHeader(matiereclasse: string, sequence: string) {
    this.genericsService.getResource(`admin/consulter/${matiereclasse}/matiereclasse`).then((response: any) => {
      this.currentMatiereClasse = response.data;
      console.log(this.currentMatiereClasse);
    }).catch(err => {
      console.log(err);
    });

    this.genericsService.getResource(`admin/consulter/${sequence}/sequence`).then((response: any) => {
      this.currentSequence = response.data;
    }).catch(err => {
      console.log(err);
    });

    this.genericsService
      .getResource(`prefet/note/matiereclasse/${matiereclasse}/sequence/${sequence}/saisie`).then((response: any) => {
      this.elevesNote = response.data;
      const bareme = this.currentMatiereClasse ? parseInt(this.currentMatiereClasse.baremeEvaluation, 0) : 20;
      this.elevesNote.forEach(item => {
        // console.log(item)
        if (item.noteAtt > 0) {
          this.istrue = true;
        }
        this.tNotes.push(
          this.fb.group({
            idNote: [item.idNote],
            matricule: [item.matricule],
            noms: [item.nomEleve],
            note: [item.noteAtt, [Validators.min(0),
              Validators.max(bareme)]],
            observation: [item.observation],
            update: false,
            canUpdate: item.canUpdate
          })
        );
      });
    }).catch(err => {
      console.log(err);
    });
  }

  openImportExport(modalImportExport: TemplateRef<any>, currentMatiereClasse: any) {
    console.log(currentMatiereClasse);
    this.onRefreshNotes();
    this.modalService.open(modalImportExport,
      {ariaLabelledBy: 'edit-note', size: 'lg', scrollable: true}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onEditNoteGlobale(noteGlobale: TemplateRef<any>, classe: any) {
    this.getClassDestination();
    const bareme = classe ? classe.baremeEvaluation : 20;
    this.noteGlobaleForm = this.fb.group({
      matricule: [''],
      noms: [''],
      idNote: [null],
      note: [0, [Validators.min(0), Validators.max(bareme)]],
      observation: ['-'],
    });
    this.modalService.open(noteGlobale,
      {ariaLabelledBy: 'edit-note', size: 'lg', scrollable: true}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  onSaveNoteGlobale() {
    this.submitted = true;
    console.log(this.elevesNote.length);
    if (this.noteGlobaleForm.invalid) {
      return;
    }
    let j;
    j = 1;
    this.elevesNote.forEach((elt, i) => {
      if (!elt.canUpdate) {
        const dto = new RemplirNoteModel(this.currentMatiereClasse.id_matiereclasse, this.currentSequence.id_sequence, []);
        dto.eleveNotes.push(new EleveNoteModel(elt.matricule, this.g.note.value, this.g.observation.value));
        this.genericsService.postResource(`admin/note/saisie`, dto).then((response: any) => {
          // console.log(dto.eleveNotes.length);
          if (j === this.elevesNote.length) {
            this.genericsService.confirmResponseAPI(response.message + ' ' + elt.matricule, 'success');
          }
          j = j + 1;
        }).catch(reason => {
          if (j === this.elevesNote.length) {
            this.genericsService.confirmResponseAPI('Enregistrement échoué ' + elt.matricule, 'error');
          }
          j = j + 1;
        });
      }
    });
    this.initNotesForm();
    this.onReset();
    this.initHeader(this.idmatiereclasse, this.idsequence);
  }

  onReset() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }

  onObservation(i: number, obs: string) {
    this.tNotes.at(i).patchValue({observation: obs, update: true});
  }

  // gestion du clavier sur les input
  @HostListener('window:keyup', ['$event']) keyUp(e: KeyboardEvent) {
    if ((e.code == 'Insert' || e.code == 'Enter')) {
      this.onSubmitNotes();
    }
  }

  onSubmitNotes() {
    const listToDelete = [];

    const dtoCreate = new RemplirNoteModel(this.currentMatiereClasse.id_matiereclasse, this.currentSequence.id_sequence, []);

    this.tNotes.controls.forEach((fg, i) => {
      if ((fg.value.note > 0 || fg.value.observation == 'malade' || fg.value.observation == 'absent') && fg.value.update == true) {
        if (!this.tNotes.at(i).invalid) {
          if (fg.value.canUpdate) {
            const dtoUpdate = new ModifierNoteModel(this.currentMatiereClasse.id_matiereclasse, fg.value.idNote,
              this.currentSequence.id_sequence, fg.value.matricule, fg.value.note, fg.value.note > 0 ? '-' : fg.value.observation);
            console.log(dtoUpdate);
            this.genericsService.postResource(`admin/note/modifier`, dtoUpdate).then((response: any) => {
              console.log('Mise a jour d\'une note existante');
              this.genericsService.confirmResponseAPI(response.message, 'success');
              this.tNotes.removeAt(i);
            }).catch(reason => {
              console.log(reason);
              this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
            });
          } else if (this.tNotes.at(i).value.update) {
            this.tNotes.at(i).patchValue({update: false});
            dtoCreate.eleveNotes.push(new EleveNoteModel(fg.value.matricule, fg.value.note, fg.value.observation));
            listToDelete.push(i);
          }
        }
      }
    });
    console.log(dtoCreate);
    if (dtoCreate.eleveNotes.length > 0) {
      this.genericsService.postResource(`admin/note/saisie`, dtoCreate).then((response: any) => {
        listToDelete.forEach(i => this.tNotes.removeAt(i));
        this.genericsService.confirmResponseAPI(response.message, 'success');
      }).catch(reason => {
        this.genericsService.confirmResponseAPI('Enregistrement échoué', 'error');
      });
    }
    // this.onRefreshNotes();
  }

  onChanStateUpdate(i: number) {
    console.log(i);
    this.tNotes.at(i).patchValue({update: true});
  }

  onAnnulerSequence(matricule: string, noms: string) {
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: AppConstants.CANCELLING_SEQUENCEEN,
        html: '<i>' + AppConstants.AJEN + '</i> pour <b>' + noms + '</b>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_CANCEL_ITEN
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.getResource(`admin/eleves/${matricule}/sequence/${this.idsequence}/annuler`).then((resultAJ: any) => {
            this.genericsService.confirmModalResponseAPI(AppConstants.TERMINATEDEN, AppConstants.TERMINATEDEN, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.CANCELEN, AppConstants.CANCELEN, 'error');
          });
        }
      }) : Swal.fire({
        title: AppConstants.CANCELLING_SEQUENCEFR,
        html: '<i>' + AppConstants.AJFR + '</i> pour <b>' + noms + '</b>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: AppConstants.YES_CANCEL_ITFR
      }).then((result) => {
        if (result.isConfirmed) {
          this.genericsService.getResource(`admin/eleves/${matricule}/sequence/${this.idsequence}/annuler`).then((resultAJ: any) => {
            this.genericsService.confirmModalResponseAPI(AppConstants.TERMINATEDFR, AppConstants.TERMINATEDFR, 'success');
          }).catch(reason => {
            console.log(reason);
            this.genericsService.confirmModalResponseAPI(AppConstants.CANCELFR, AppConstants.CANCELFR, 'error');
          });
        }
      });
  }

  get f() {
    return this.form.controls;
  }

  initForm() {
    this.form = this.fb.group({
      matiereClassAffect: ['', Validators.required],
    });
  }

  getClassDestination() {
    console.log(this.currentMatiereClasse);
    console.log(this.matierDestination);
    this.genericsService.getResource(`admin/classe/${this.currentMatiereClasse.classe.id}/matieres`).then((responses: any) => {
      if (responses.success == true) {
        this.filtreNote = responses.data;
        this.matierDestination = this.filtreNote;
        console.log(responses);
      }
    }).catch(err => {
      console.log(err);
    });
  }

  onChange(elt) {
    this.selectMatiere = elt;
    console.log(elt);
  }

  getMatierClassDestination(matiereSelect) {
    this.line = matiereSelect;
    console.log(matiereSelect);
    this.matiereSource = this.currentMatiereClasse.matiere.nom_matiere;
    console.log(this.currentMatiereClasse);
    console.log(this.matiereSource);
  }

  onSave() {
    console.log(this.f.matiereClassAffect.value);

    let dto;
    dto = new attribuerNoteRequestDto(
      this.currentMatiereClasse.classe.id,
      parseInt(this.idmatiereclasse, 10),
      parseInt(this.line.id, 10),
      parseInt(this.currentMatiereClasse.matiere.id_matiere, 10),
      parseInt(this.idsequence, 10)
    );
    console.log(dto);

    this.genericsService.postResource(`admin/attribuer-note/classe/matiere`, dto).then((response) => {
      console.log(response);
      this.genericsService.confirmResponseAPI('Affecté avec success', 'success');
      this.line = null;
      this.onReset();
    }).catch(reason => {
      console.log(reason);
      this.genericsService.confirmResponseAPI(reason.error.message, 'error');
      this.line = null;
      this.onReset();
    });
    this.initHeader(this.idmatiereclasse, this.idsequence);
  }

  onSelectedFile(event) {
    console.log(event);
    this.fileToUpload = event.target.files[0];

    console.log(this.fileToUpload);
    this.onImportExport();
  }

  onImportExport() {

    console.log(this.fileToUpload);
    const renameFile = this.fileToUpload.name.substring(0, this.fileToUpload.name.indexOf('3') + 1) + '.xls';
    console.log(renameFile);
    const formData = new FormData();
    if (this.fileToUpload != null) {
      formData.append('file', this.fileToUpload, this.fileToUpload.name);
      this.genericsService.uploadFile(`admin/import/notes/eleves`, formData).subscribe((response: any) => {
        console.log(response);
        this.genericsService.confirmResponseAPI(response.message, 'success');
      }, error => {
        console.error(error);
        this.genericsService.confirmResponseAPI(error.error.message, 'error');
      });
    }
    console.log(this.fileToUpload);

  }

  export() {
    this.genericsService.reportPostResource(`admin/export/releve-note/matiere-classe/${this.idmatiereclasse}/sequence/${this.idsequence}`)
      .then((result: any) => {
        console.log(result);
        // this.genericsService.startLoadingPage();
        const date = new Date().getDate();
        // const renameFile = this.fileToUpload.name.substring(0, this.fileToUpload.name.indexOf('3') + 1) + '.xls';
        const fileName = 'Note_exporte_' + this.idmatiereclasse + '_' + this.idsequence;
        this.genericsService.getByteArrayAndSaveReportPDFNote(result, fileName, true);
        // this.genericsService.stopLoadingPage();
      }).catch((err) => {
      console.log(err);
      this.genericsService.confirmResponseAPI(err.message, 'error');
    });
  }

  onRefreshNotes() {
    this.tNotes.clear();
    this.initHeader(this.idmatiereclasse, this.idsequence);
  }
}
