import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NWelcomeComponent} from './n-welcome.component';

describe('NWelcomeComponent', () => {
  let component: NWelcomeComponent;
  let fixture: ComponentFixture<NWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
