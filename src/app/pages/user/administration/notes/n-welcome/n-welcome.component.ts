import {Component, OnInit, TemplateRef} from '@angular/core';
import {AuthService} from '../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GenericsService} from "../../../../../shared/_services/generics.service";
import {Router} from "@angular/router";
import {FiltreNoteModel} from "../../../../../shared/_models/response-dto/graduation/filtre-note-absence-model";

@Component({
  selector: 'app-n-welcome',
  templateUrl: './n-welcome.component.html',
  styleUrls: ['./n-welcome.component.scss']
})
export class NWelcomeComponent implements OnInit {


  submitted: boolean;
  selectNoteClasseForm: FormGroup;

  filtreNote: FiltreNoteModel;
  public settingsNote = {};
  public classeNoteFiltre: any;

  public choix: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(private genericsService: GenericsService,private router: Router,
              private translate: TranslateService, private fb: FormBuilder,
              private modalService: NgbModal,
              public authService: AuthService) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    this.choix = localStorage.getItem('activelang') === 'en' ? 'Choose value' : 'Choisir la valeur';

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    authService.returnListAction(70);
  }

  ngOnInit() {
    this.initFormNote();
    this.onloadFiltres();
    this.settingsNote = localStorage.getItem('activelang') === 'en'
      ? {
        singleSelection: true,
        idField: 'matiereClasseId',
        textField: 'matiereClasseLabel',
        enableCheckAll: false,
        selectAllText: 'Check all',
        unSelectAllText: 'Uncheck all',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: 'Search',
        noDataAvailablePlaceholderText: 'No data available',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      }
      :  {
        singleSelection: true,
        idField: 'matiereClasseId',
        textField: 'matiereClasseLabel',
        enableCheckAll: false,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: 'Rechercher',
        noDataAvailablePlaceholderText: 'Aucune donnée disponible',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };
  }

  private initFormNote() {
    this.selectNoteClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      matiereclasse: ['', Validators.required]
    });
  }

  onDisplayModalToNote(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'select-note', size: 'lg'}).result.then((result) => {
    }, (reason) => {
    });
  }

  get f() { return this.selectNoteClasseForm.controls; }

  onDisplayListeEleve() {
    this.submitted = true;
    const matiereclasse = this.f.matiereclasse.value[0];
    if (this.selectNoteClasseForm.invalid) {
      return;
    }
    this.genericsService.startLoadingPage();
    this.router.navigate([`/administration/notes/admin-notes/${matiereclasse.matiereClasseId}/sequence/${this.f.sequence.value}`])
      .then(() => {
        this.genericsService.stopLoadingPage();
        if (this.modalService.hasOpenModals()) {
          this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
        }
      });
  }

  private onloadFiltres() {
    this.genericsService.getResource(`prefet/filtre/saisie/note`).then((responses: any) => {
      if (responses.success == true) {
        this.filtreNote = responses.data;
        this.classeNoteFiltre = this.filtreNote.matiereClasses;
      }
    }).catch(err => {
      console.log(err);
    });
  }
  onReset() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }

  onDisplayHistoryLogNotes() {
    this.router.navigateByUrl('/administration/notes/log-notes').then(() => {});
  }
}
