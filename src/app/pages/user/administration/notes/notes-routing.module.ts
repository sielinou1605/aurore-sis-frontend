import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotesComponent} from './notes.component';
import {NWelcomeComponent} from './n-welcome/n-welcome.component';
import {AdminNotesComponent} from './admin-notes/admin-notes.component';
import {LogNotesComponent} from "./log-notes/log-notes.component";

const routes: Routes = [
  {
    path: '', component: NotesComponent,
    data: {
      breadcrumb: 'userAdminFinancesListServices.Notes',
      // titre: 'userAdminFinancesListServices.Notes_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Notes | Aurore' : 'Notes | Aurore',
      icon: 'icofont-ui-settings bg-c-blue',
      breadcrumb_caption: 'userAdminFinancesListServices.Configuration_des_classes_et_enseignants_Notes',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'n-welcome',
        pathMatch: 'full'
      },
      {
        path: 'n-welcome', component: NWelcomeComponent,
        data: {
          breadcrumb: 'userAdminFinancesListServices.Notes',
          // titre: 'userAdminFinancesListServices.Notes_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Notes | Aurore' : 'Notes | Aurore',
          icon: 'icofont-ui-settings bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesListServices.Configuration_des_classes_et_enseignants_Notes',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'enseignants',
        loadChildren: () => import('./enseignants/enseignants.module')
          .then(m => m.EnseignantsModule)
      },
      {
        path: 'classes',
        loadChildren: () => import('./classes/classes.module')
          .then(m => m.ClassesModule)
      },
      {
        path: 'admin-notes/:matiereclasse/sequence/:sequence',
        component: AdminNotesComponent,
        data: {
          breadcrumb: 'Admin notes',
          // titre: 'userAdminFinancesListServices.Notes_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Note | Aurore' : 'Notes | Aurore',
          icon: 'icofont-refresh bg-youtube',
          breadcrumb_caption: 'userAdminFinancesListServices.Management_complet_des_notes_eleves_Notes',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'log-notes', component: LogNotesComponent,
        data: {
          breadcrumb: 'Historique notes',
          // titre: 'userAdminFinancesListServices.Aide_Aurore',
          titre: 'Historique de notes | Aurore',
          icon: 'icofont-history bg-c-blue',
          breadcrumb_caption: 'Gestion des Historiques de notes - Administration',
          status: true,
          current_role: 'admin'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotesRoutingModule { }
