import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogNotesComponent } from './log-notes.component';

describe('LogNotesComponent', () => {
  let component: LogNotesComponent;
  let fixture: ComponentFixture<LogNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
