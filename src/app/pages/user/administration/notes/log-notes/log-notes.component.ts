import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HistoriqueEleveNoteResponse} from '../../../../../shared/_models/response-dto/systeme/data-module-response-model';
import {GenericsService} from '../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../shared/_services/auth.service';

@Component({
  selector: 'app-log-notes',
  templateUrl: './log-notes.component.html',
  styleUrls: ['./log-notes.component.scss']
})
export class LogNotesComponent implements OnInit {
  searchForm: FormGroup;
  listNotes: HistoriqueEleveNoteResponse[];
  public pageSize = 40;
  public page = 0;
  public sens = 'ASC';
  public totalElements: number;
  private dataFilter: string;

  constructor(private fb: FormBuilder, private genericsService: GenericsService,
              public authService: AuthService) {
    authService.returnListAction(98);
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.genericsService.startLoadingPage();
    this.onloadListHistorique('', this.sens, this.page, this.pageSize);
  }

  private onloadListHistorique(token: string, sens: string, page: number, size: number) {
    this.genericsService.getResource(`admin/note/historiques?token=${token}&sens=${sens}&page=${page}&size=${size}`)
      .then((result: any) => {
        this.genericsService.stopLoadingPage();
        console.log(result);
        this.listNotes = result.data.content;
        this.totalElements = result.data.totalElements;
      }).catch(err => {
      console.log(err);
    });
  }

  setPageChange(event: any) {
    this.page = event - 1;
    this.onloadListHistorique(this.dataFilter, this.sens, this.page, this.pageSize);
  }

  onDetailHistorique(note: HistoriqueEleveNoteResponse) {

  }

  onAfficherHistorique(event, reInitPage: boolean) {
    if (reInitPage === true) {
      this.page = 0;
    }
    let predicat: string;
    if (event) {
      predicat = this.genericsService.cleanToken(event);
    } else {
      predicat = event;
    }
    this.dataFilter = predicat;
    this.genericsService.startLoadingPage();
    this.onloadListHistorique(this.dataFilter, this.sens, this.page, this.pageSize);
  }

  get search() { return this.searchForm.controls; }
}
