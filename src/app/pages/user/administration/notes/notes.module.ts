import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NotesRoutingModule} from './notes-routing.module';
import {NotesComponent} from './notes.component';
import {NWelcomeComponent} from './n-welcome/n-welcome.component';
import {SharedModule} from '../../../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import { AdminNotesComponent } from './admin-notes/admin-notes.component';
import {LogNotesComponent} from './log-notes/log-notes.component';
import {NgxPaginationModule} from "ngx-pagination";


@NgModule({
  declarations: [NotesComponent, AdminNotesComponent, LogNotesComponent, NWelcomeComponent],
    imports: [
        CommonModule,
        NotesRoutingModule,
        SharedModule,
        TranslateModule,
        ReactiveFormsModule,
        NgMultiSelectDropDownModule,
        NgxPaginationModule,
        FormsModule
    ]
})
export class NotesModule { }
