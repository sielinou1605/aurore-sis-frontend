import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ClassesRoutingModule} from './classes-routing.module';
import {ClassesComponent} from './classes.component';
import {ListeElevesComponent} from './liste-eleves/liste-eleves.component';
import {DetailsClasseComponent} from './details-classe/details-classe.component';
import {EditerClasseComponent} from './editer-classe/editer-classe.component';
import {EtatRemplissageClasseComponent} from './etat-remplissage-classe/etat-remplissage-classe.component';
import {ListeClassesComponent} from './liste-classes/liste-classes.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [
    ClassesComponent,
    ListeElevesComponent,
    DetailsClasseComponent,
    EditerClasseComponent,
    EtatRemplissageClasseComponent,
    ListeClassesComponent,
  ],
    imports: [
        CommonModule,
        ClassesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ]
})
export class ClassesModule { }
