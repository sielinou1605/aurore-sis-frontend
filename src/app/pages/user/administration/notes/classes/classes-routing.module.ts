import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClassesComponent} from './classes.component';
import {ListeClassesComponent} from './liste-classes/liste-classes.component';
import {DetailsClasseComponent} from './details-classe/details-classe.component';
import {EditerClasseComponent} from './editer-classe/editer-classe.component';
import {EtatRemplissageClasseComponent} from './etat-remplissage-classe/etat-remplissage-classe.component';
import {ListeElevesComponent} from './liste-eleves/liste-eleves.component';

const routes: Routes = [
  {
    path: '', component: ClassesComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Classes',
      titre: localStorage.getItem('activelang') === 'en' ? 'Classes | Aurore' : 'classes | Aurore',
      icon: 'icofont-group-students bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Liste_des_classe_enregistrés_Classes',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'list-classes',
        pathMatch: 'full'
      },
      {
        path: 'list-classes', component: ListeClassesComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Classes',
          titre: localStorage.getItem('activelang') === 'en' ? 'Classes | Aurore' : 'classes | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Gestion_de_la_liste_des_classes_Classes',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'details-classe', component: DetailsClasseComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Detail_Classe',
          titre: localStorage.getItem('activelang') === 'en' ? 'Detail classes | Aurore' : 'Detail classes | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Detail_de_la_classe_Classes',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'editer-classe', component: EditerClasseComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Edition_Classe',
          titre: localStorage.getItem('activelang') === 'en' ? 'Classroom edition | Aurore' : 'Edition classes | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Modifier_les_parametres_d_une_classe_Classes',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'etat-remplissage-classe/:idClasse', component: EtatRemplissageClasseComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Etat_remplissage_Classe',
          titre: localStorage.getItem('activelang') === 'en' ? 'Class filling status | Aurore' : 'Etat remplissage Class | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Verifier_l_état_de_remplissage_de_la_classe_Classes',
          status: true,
          current_role: 'admin'
        }
      },
      {
        path: 'liste-eleves/:idClasse', component: ListeElevesComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Liste_eleve_de_la_classe',
          // tslint:disable-next-line:max-line-length
          titre: localStorage.getItem('activelang') === 'en' ? 'List of students in the class | Aurore' : 'Liste eleve de la classe | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Afficher_la_liste_des_élèves_d_une_classe_Classes',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'etat-remplissage-classe', component: EtatRemplissageClasseComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Etat_de_remplissage_de_classe',
          titre: localStorage.getItem('activelang') === 'en' ? 'Class filling status  | Aurore' : 'Etat de remplissage de classe | Aurore',
          icon: 'icofont-group-students bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Controler_l_etat_de_remplissage_pour_une_classe_Classes',
          status: true,
          current_role: 'admin'
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassesRoutingModule { }
