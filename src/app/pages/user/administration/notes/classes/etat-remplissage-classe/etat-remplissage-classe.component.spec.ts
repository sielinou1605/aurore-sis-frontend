import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EtatRemplissageClasseComponent} from './etat-remplissage-classe.component';

describe('EtatRemplissageClasseComponent', () => {
  let component: EtatRemplissageClasseComponent;
  let fixture: ComponentFixture<EtatRemplissageClasseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtatRemplissageClasseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatRemplissageClasseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
