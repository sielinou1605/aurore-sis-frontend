import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TokenStorageService} from '../../../../../../shared/_services/token-storage.service';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-etat-remplissage-classe',
  templateUrl: './etat-remplissage-classe.component.html',
  styleUrls: ['./etat-remplissage-classe.component.scss']
})
export class EtatRemplissageClasseComponent implements OnInit {
  public idClasse: string;
  public sequences: any;
  public etat: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private route: ActivatedRoute, public tokenStorage: TokenStorageService,
    private genericsService: GenericsService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idClasse = params.get('idClasse');
      this.onloadEtatRemplissageClasse(this.idClasse);
      this.onloadListSequences();
    });
  }

  private onloadEtatRemplissageClasse(idClasse: string) {
    console.log(this.idClasse);
    this.genericsService.getResource(`admin/etat-remplissage/classe/${idClasse}`).then((response: any) => {
      this.etat = response.data;
      console.log(this.etat);
    }).catch(reason => {
      console.log(reason);
    });
  }
  private onloadListSequences() {
    this.genericsService.getResource(`admin/bulletin/filtre/sequence`).then((response: any) => {
      this.sequences = response.data.sequences;
      console.log(this.sequences);
    }).catch(reason => {
      console.log(reason);
    });
  }
}
