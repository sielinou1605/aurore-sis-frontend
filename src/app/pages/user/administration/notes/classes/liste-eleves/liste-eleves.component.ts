import {Component, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-liste-eleves',
  templateUrl: './liste-eleves.component.html',
  styleUrls: ['./liste-eleves.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListeElevesComponent implements OnInit {
  private idClasse: string;
  sanctionForm: FormGroup;
  submitted: boolean;
  public listEleve: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private genericsService: GenericsService, private fb: FormBuilder,
    private route: ActivatedRoute, private router: Router,
    private modalService: NgbModal, config: NgbModalConfig,
    public authService: AuthService
  ) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(73);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idClasse = params.get('idClasse');
      if (this.idClasse) {
        this.onloadListEleve(this.idClasse);
      }
    });
    this.initFormSanctionEleve();
  }

  onloadListEleve(id: any) {
    this.genericsService.getResource(`admin/consulter/${id}/classe`).then((response: any) => {
      if (response.success === true) {
        this.genericsService.getResource(`admin/eleve/filtrer?predicat=${response.data.nomClasse}`).then((result: any) => {
          if (result.success === true) {
            this.listEleve = result.data;
            console.log(this.listEleve);
          }
        }, err => {
          console.log(err);
        });
      }
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  get f() { return this.sanctionForm.controls; }
  onAjouterListeEleve() {

  }
  onImprimerListe() {

  }
  onImprimerSanctions() {

  }
  onAjouterUnEleve() {
    this.router.navigate(['/eleves/ajout-eleve']).then(() => {

    });
  }
  onSanctionnerEleve(eleve, content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'sanction-eleve', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }
  onResetSanction() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormSanctionEleve();
  }
  onSaveSanction() {

  }
  private initFormSanctionEleve() {
    this.sanctionForm = this.fb.group({
      sequence: [''],
      datesanction: [''],
      motif: [''],
      penalite: [''],
      debut: [''],
      fin: [''],
    });
  }
}
