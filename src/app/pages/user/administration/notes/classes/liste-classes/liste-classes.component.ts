import {Component, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-liste-classes',
  templateUrl: './liste-classes.component.html',
  styleUrls: ['./liste-classes.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListeClassesComponent implements OnInit {
  nommerTitulaireForm: FormGroup;
  nommerSurveillantForm: FormGroup;
  submitted: boolean;
  public classes: any;
  public currenClasse: any;
  public enseignants: any;
  public utilisateurs: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private genericsService: GenericsService, private fb: FormBuilder,
              private modalService: NgbModal, config: NgbModalConfig,
              public authService: AuthService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(72);
  }

  ngOnInit() {
    this.onLoadListClasse();
    this.onloadListEnseignant();
    this.onloadListUsers();
    this.initFormTitulaire();
    this.initFormSurveillant();
  }
  private onLoadListClasse() {
    this.genericsService.getResource(`admin/classes`).then((response: any) => {
      this.classes = response.data;
      console.log(this.classes);
    }, err => {
      console.log(err);
      localStorage.getItem('activelang') === 'en' ?
        // tslint:disable-next-line:max-line-length
      this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }

  private onloadListEnseignant() {
    this.genericsService.getResource('admin/enseignants').then((response: any) => {
      this.enseignants = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }
  private onloadListUsers() {
    this.genericsService.getResource('habilitation/utilisateur/liste').then((response: any) => {
      this.utilisateurs = response.data;
    }).catch((reason: any) => {

    });
  }

  onNommerTitulaire(classe: any, content: TemplateRef<any>) {
    this.currenClasse = classe;
    this.modalService.open(content,
      {ariaLabelledBy: 'nommer-titulaire', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onNommerSurveillant(classe: any, content: TemplateRef<any>) {
    this.currenClasse = classe;
    this.modalService.open(content,
      {ariaLabelledBy: 'nommer-surveillant',  scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onResetNommerTitulaire() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormTitulaire();
  }

  onSaveNommerTitulaire() {

  }

  onResetNommerSurveillant() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
    this.initFormSurveillant();
  }

  onSaveNommerSurveillant() {

  }
  onSupprimerClasse() {

  }

  private initFormTitulaire() {
    this.nommerTitulaireForm = this.fb.group({
      enseignantT: ['']
    });
  }

  private initFormSurveillant() {
    this.nommerSurveillantForm = this.fb.group({
      enseignantS: ['']
    });
  }
  get nt() { return this.nommerTitulaireForm.controls; }
  get ns() { return this.nommerSurveillantForm.controls; }
}
