import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {EditMatiereClasseEnseignant} from '../../../../../../shared/_models/request-dto/graduation/gestion-cours-enseignant-model';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-list-enseignants',
  templateUrl: './list-enseignants.component.html',
  styleUrls: ['./list-enseignants.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListEnseignantsComponent implements OnInit, OnDestroy {
  public enseignants: any;
  enseignantClasseForm: FormGroup;
  submittedEC: boolean;
  annees: any;
  private currentEnseignant: any;
  public classes: any;
  public matieres: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private router: Router, private fb: FormBuilder, private modalService: NgbModal,
    config: NgbModalConfig, private genericsService: GenericsService,
    public authService: AuthService) {


    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(77);
  }

  ngOnInit() {
    this.onLoadListEnseignants();
    this.onloadListAnnees();
    this.initFormProgrammerEnseignant();
    this.onloadListClasse();
    this.onLoadListMatiere();
  }

  private initFormProgrammerEnseignant() {
    this.enseignantClasseForm = this.fb.group({
      anneeAcc: ['', Validators.required],
      matiere: ['', Validators.required],
      classe: ['', Validators.required],
      coef: ['1', Validators.required],
      bareme: ['20', Validators.required]
    });
  }

  private onloadListAnnees() {
    this.genericsService.getResource(`admin/annees`).then((result: any) => {
      this.annees = result.data;
    });
  }

  onDetailEnseignant(enseignant: any) {
    if (enseignant) {
      this.router.navigate(
        ['/administration/discipline/enseignants/consulter-enseignant/' + enseignant.id_enseignant]).then(() => {
      });
    } else {
      this.router.navigate(['/administration/discipline/enseignants/consulter-enseignant/1']).then(() => {
      });
    }
  }

  onEditEnseignant(enseignant: any) {
    if (enseignant) {
      this.router.navigate(
        ['/administration/discipline/enseignants/ajouter-enseignant/' + enseignant.id_enseignant]).then(() => {
      });
    } else {
      this.router.navigate(['/administration/discipline/enseignants/ajouter-enseignant']).then(() => {
      });
    }
  }
  get f() { return this.enseignantClasseForm.controls; }

  onProgrammerEnseignant(enseignant, content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'edit-programmation', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }

  onSupprimerEnseignant(enseignant) {

  }

  onGetEtatRemplissage(enseignant: any) {
    this.router.navigate(['/administration/discipline/enseignants/etat-remplissage',
      enseignant.id_enseignant]).then(() => {});
  }

  private onLoadListEnseignants() {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/enseignants`).then((result: any) => {
      this.enseignants = result.data;
      this.genericsService.stopLoadingPage();
    }).catch(reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
      localStorage.getItem('activelang') === 'en' ?
      this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.UNKNOWN_LOADEDFR, 'error');
    });
  }

  onResetEnseignantClasse() {
    this.submittedEC = false;
    this.currentEnseignant = undefined;
    this.initFormProgrammerEnseignant();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  onAttributeClasseToEnseignant() {
    this.submittedEC = true;
    if (this.enseignantClasseForm.invalid) {
      return;
    }
    const dto = new EditMatiereClasseEnseignant(this.f.anneeAcc.value, this.f.bareme.value, this.f.classe.value,
      this.f.coef.value, this.currentEnseignant.id_enseignant, null, this.f.matiere.value, null);
    this.genericsService.postResource('admin/ajouter/matiereclasse/enseignant', dto).then((response: any) => {
      if (response.success === true) {
        this.genericsService.confirmResponseAPI(response.message, 'success');
        this.onResetEnseignantClasse();
        this.onLoadListEnseignants();
      }
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  private onloadListClasse() {
    this.genericsService.getResource('admin/classes').then((response: any) => {
      this.classes = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  private onLoadListMatiere() {
    this.genericsService.getResource('matieres').then((response: any) => {
      this.matieres = response._embedded.matieres;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  ngOnDestroy(): void {
    this.onResetEnseignantClasse();
  }

}
