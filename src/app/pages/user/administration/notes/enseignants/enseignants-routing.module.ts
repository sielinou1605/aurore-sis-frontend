import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EnseignantsComponent} from './enseignants.component';
import {ListEnseignantsComponent} from './list-enseignants/list-enseignants.component';
import {ConsulterEnseignantComponent} from './consulter-enseignant/consulter-enseignant.component';
import {EtatRemplissageComponent} from './etat-remplissage/etat-remplissage.component';
import {AjouterEnseignantComponent} from './ajouter-enseignant/ajouter-enseignant.component';


const routes: Routes = [
  {
    path: '', component: EnseignantsComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Enseignants',
      // titre: 'userEleveAjaout.Enseignants_Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Teachers | Aurore' : 'Enseignants | Aurore',
      icon: 'icofont-teacher bg-c-blue',
      breadcrumb_caption: 'userEleveAjaout.Liste_des_enseignants_Enseignants',
      status: true,
      current_role: 'admin'
    },
    children: [
      {
        path: '',
        redirectTo: 'list-enseignants',
        pathMatch: 'full'
      },
      {
        path: 'list-enseignants', component: ListEnseignantsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Enseignants',
          // titre: 'userEleveAjaout.Enseignants_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Teachers | Aurore' : 'Enseignants | Aurore',
          icon: 'icofont-school-bag bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_enseignants_Enseignants',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'consulter-enseignant/:id', component: ConsulterEnseignantComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Détail_Enseignant',
         // titre: 'userEleveAjaout.Détail_Enseignant_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Detail Teachers | Aurore' : 'Détail Enseignants | Aurore',
          icon: 'icofont-school-bag bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Afficher_la_fiche_enseignants_Enseignants',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'etat-remplissage/:id', component: EtatRemplissageComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Etat_remplissage',
          // titre: 'userEleveAjaout.Etat_remplissage_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Filling state | Aurore' : 'Etat remplissage | Aurore',
          icon: 'icofont-school-bag bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Liste_des_états_de_remplissage_enseignants_Enseignants',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'ajouter-enseignant', component: AjouterEnseignantComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Ajout_enseignant',
          // titre: 'userEleveAjaout.Ajout_enseignant_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Add teacher | Aurore' : 'Ajout enseignant | Aurore',
          icon: 'icofont-school-bag bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Ajouter_un_enseignant_Enseignants',
          status: true,
          current_role: 'admin'
        },
      },
      {
        path: 'ajouter-enseignant/:id', component: AjouterEnseignantComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Edition_enseignant',
          // titre: 'userEleveAjaout.Edition_enseignant_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Teacher\'s edition | Aurore' : 'Edition enseignant | Aurore',
          icon: 'icofont-school-bag bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Ajouter_ou_modifier_les_attributs_d_un_enseignant_Enseignants',
          status: true,
          current_role: 'admin'
        },
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnseignantsRoutingModule { }
