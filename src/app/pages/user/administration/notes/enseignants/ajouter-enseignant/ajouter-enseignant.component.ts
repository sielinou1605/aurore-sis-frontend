/* tslint:disable:triple-equals */
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {EditerEnseignantModel} from '../../../../../../shared/_models/request-dto/graduation/gestion-cours-enseignant-model';
import {HttpEventType} from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AppConstants} from '../../../../../../shared/element/app.constants';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-ajouter-enseignant',
  templateUrl: './ajouter-enseignant.component.html',
  styleUrls: ['./ajouter-enseignant.component.scss']
})
export class AjouterEnseignantComponent implements OnInit {
  editEnsegnantForm: FormGroup;
  submitted: boolean;
  public selectedFiles: any;
  public progress: number;
  public currentFileUpload: any;
  public institutions: any;
  public departements: any;
  public roles: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder, private genericsService: GenericsService,
              private router: Router, public authService: AuthService) {


    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(75);

  }

  ngOnInit() {
    this.initFormEditEnseignant();
    this.onloadInstitution();
    this.onloadDepartements();
    this.onloadRoles();
  }

  private initFormEditEnseignant() {
    localStorage.getItem('activelang') === 'en' ?
    this.editEnsegnantForm = this.fb.group({
      nom: ['', Validators.required],
      prenom: [''],
      sexe: ['', Validators.required],
      statutm: [AppConstants.DEFAULT_STATUTEN, Validators.required],
      telephone1: ['', Validators.required],
      telephone2: [''],
      email: ['', Validators.required],
      photo: [''],
      institution: ['', Validators.required],
      departement: ['', Validators.required],
      fonction: ['', Validators.required],
      typepersonnel: [AppConstants.DEFAULT_TYPE_PERSONNELEN, Validators.required],
      statutprof: [AppConstants.DEFAULT_STATUT_PROFEN, Validators.required],
      anciennete: [''],
      lieuprovenance: ['', Validators.required]
    }) : this.editEnsegnantForm = this.fb.group({
        nom: ['', Validators.required],
        prenom: [''],
        sexe: ['', Validators.required],
        statutm: [AppConstants.DEFAULT_STATUTFR, Validators.required],
        telephone1: ['', Validators.required],
        telephone2: [''],
        email: ['', Validators.required],
        photo: [''],
        institution: ['', Validators.required],
        departement: ['', Validators.required],
        fonction: ['', Validators.required],
        typepersonnel: [AppConstants.DEFAULT_TYPE_PERSONNELFR, Validators.required],
        statutprof: [AppConstants.DEFAULT_STATUT_PROFFR, Validators.required],
        anciennete: [''],
        lieuprovenance: ['', Validators.required]
      });
  }

  get f() { return this.editEnsegnantForm.controls; }

  onSaveEnseignant() {
    this.submitted = true;
    if (this.editEnsegnantForm.invalid) {
      return;
    }
    const matricule = this.genericsService.strRandom({includeUpperCase: true, includeNumbers: true, length: 6, startsWithLowerCase: true});
    const dto = new EditerEnseignantModel(this.f.anciennete.value, null, '', '',
      this.f.email.value, this.f.fonction.value, this.f.departement.value, this.f.institution.value, null, matricule, 0,
      this.f.nom.value, null, this.f.prenom.value, this.f.lieuprovenance.value, 0, this.f.sexe.value,
      this.f.statutm.value, this.f.telephone1.value, this.f.telephone2.value, this.f.typepersonnel.value);
    this.genericsService.postResource(`admin/enseignant/ajouter`, dto).then((response: any) => {
      console.log(response);
      localStorage.getItem('activelang') === 'en' ?
      Swal.fire(
        '<p class="text-danger text-bold mr-3">Important !!!</p> ' +
        ' <p>Please note the teaching parameters !</p>',
        '<span class="text-danger">Login :   </span>' + response.data.login + ' / ' +
        '<span class="text-danger">Password: </span>' +  response.data.password,
        'success'
      ) : Swal.fire(
          '<p class="text-danger text-bold mr-3">Important !!!</p> ' +
          ' <p>Relevez les parametres de enseignant svp !</p>',
          '<span class="text-danger">Login :   </span>' + response.data.login + ' / ' +
          '<span class="text-danger">Mot de passe : </span>' +  response.data.password,
          'success'
        );
      if (response.success == true) {
        this.router.navigate([`/administration/discipline/enseignants`]).then(() => {
          if (this.selectedFiles) {
            this.uploadPhotoEnseignant(response.data.id_enseignant);
          }
        });
      }
    }).catch(reason => {
      console.log(reason);
    });
  }

  onResetEditEnseignant() {
    this.initFormEditEnseignant();
    this.submitted = false;
    this.selectedFiles = undefined;
  }

  onselectedFile(event: any) {
    this.selectedFiles = event.target.files;
  }

  uploadPhotoEnseignant(id: number) {
    this.progress = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.genericsService.uploadImage(this.currentFileUpload, `upload/photo/${id}/enseignant`).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else {
          this.progress = undefined;
          localStorage.getItem('activelang') === 'en' ?
            // tslint:disable-next-line:max-line-length
          this.genericsService.confirmResponseAPI(AppConstants.PROFIL_IMAGE_SAVEDEN, 'success') : this.genericsService.confirmResponseAPI(AppConstants.PROFIL_IMAGE_SAVEDFR, 'success');
        }
      },
      errorPhoto => {
        console.log(errorPhoto);
        localStorage.getItem('activelang') === 'en' ?
          // tslint:disable-next-line:max-line-length
        this.genericsService.confirmResponseAPI(AppConstants.PROFILE_IMAGE_FAILEDEN, 'error') : this.genericsService.confirmResponseAPI(AppConstants.PROFILE_IMAGE_FAILEDFR, 'error');
      });
    this.selectedFiles = undefined;
  }

  private onloadInstitution() {
    this.genericsService.getResource(`admin/institutions`).then((response: any) => {
      this.institutions = response.data;
      this.editEnsegnantForm.patchValue({
        institution: this.institutions[0].id
      });
    }).catch(reason => {
      console.log(reason);
    });
  }

  private onloadDepartements() {
    this.genericsService.getResource(`admin/departements`).then((response: any) => {
      this.departements = response.data;
      this.editEnsegnantForm.patchValue({
        departement: this.departements[0].id
      });
    }).catch(reason => {
      console.log(reason);
    });
  }

  private onloadRoles() {
    this.genericsService.getResource(`habilitation/role/liste`).then((response: any) => {
      this.roles = response.data;
    }).catch(reason => {
      console.log(reason);
    });
  }
}
