import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../../../../../../shared/_services/generics.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../../../../shared/_services/auth.service';
import {EditMatiereClasseEnseignant} from '../../../../../../shared/_models/request-dto/graduation/gestion-cours-enseignant-model';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-consulter-enseignant',
  templateUrl: './consulter-enseignant.component.html',
  styleUrls: ['./consulter-enseignant.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ConsulterEnseignantComponent implements OnInit, OnDestroy {
  enseignantClasseForm: FormGroup;
  submittedEC: boolean;
  annees: any;
  private currentProgrammation: any;
  public currentEnseignant: any;
  public classes: any;
  public matieres: any;
  public enseignants: any;
  public isReattribuer: boolean;
  private idEnseignant: string;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder, private genericsService: GenericsService,
              private modalService: NgbModal, config: NgbModalConfig,
              private route: ActivatedRoute, public authService: AuthService) {


    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(76);
  }

  ngOnInit() {
    this.onloadListAnnees();
    this.onloadListEnseignant();
    this.onloadListClasse();
    this.onLoadListMatiere();
    this.initFormProgrammerEnseignant();
    this.route.paramMap.subscribe(params => {
      this.idEnseignant = params.get('id');
      this.onConsulterEnseignant(this.idEnseignant);
    });
  }
  private onConsulterEnseignant(id: any) {
    this.genericsService.startLoadingPage();
    this.genericsService.getResource(`admin/consulter/${id}/enseignant`)
      .then((response: any) => {
        if (response.success === true) {
          this.currentEnseignant = response.data;
        }
        this.genericsService.stopLoadingPage();
      }).catch((reason => {
      console.log(reason);
      this.genericsService.stopLoadingPage();
    }));
  }
  get f() { return this.enseignantClasseForm.controls; }

  private initFormProgrammerEnseignant() {
    this.enseignantClasseForm = this.fb.group({
      anneeAcc: ['', Validators.required],
      matiere: ['', Validators.required],
      classe: ['', Validators.required],
      coef: ['1', Validators.required],
      bareme: ['20', Validators.required],
      enseignant: ['', Validators.required]
    });
  }
  onSupprimerProgrammation() {
  }
  onEditMatiereClasse() {

  }
  onProgrammerEnseignant(programmation, isReattribuer: boolean, content: TemplateRef<any>) {
    this.currentProgrammation = programmation;
    console.log(programmation);
    this.isReattribuer = isReattribuer;
    if (isReattribuer) {
      this.enseignantClasseForm.controls['enseignant'].setValidators([Validators.required]);
    } else {
      this.enseignantClasseForm.controls['enseignant'].clearValidators();
    }
    this.enseignantClasseForm.get('enseignant').updateValueAndValidity();
    if (programmation) {
      this.enseignantClasseForm.patchValue({
        anneeAcc: programmation.idAnnee,
        matiere: programmation.idMatiere,
        classe: programmation.idClasse,
        coef: programmation.coefficient,
        bareme: programmation.baremeEvaluation,
        enseignant: this.idEnseignant
      });
    }
    this.modalService.open(content,
      {ariaLabelledBy: 'edit-programmation', size: 'lg', scrollable: true}).result.then((result) => {
    }, (reason) => {
    });
  }
  private onloadListAnnees() {
    this.genericsService.getResource(`annees?size=50`).then((result: any) => {
      this.annees = result._embedded.annees;
    });
  }
  private onloadListClasse() {
    this.genericsService.getResource('admin/classes').then((response: any) => {
      this.classes = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }

  private onLoadListMatiere() {
    this.genericsService.getResource('matieres').then((response: any) => {
      this.matieres = response._embedded.matieres;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }
  private onloadListEnseignant() {
    this.genericsService.getResource('admin/enseignants').then((response: any) => {
      this.enseignants = response.data;
    }).catch((reason: any) => {
      console.log(reason);
    });
  }
  onResetEnseignantClasse() {
    this.submittedEC = false;
    this.currentProgrammation = null;
    this.initFormProgrammerEnseignant();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onAttributeClasseToEnseignant() {
    this.submittedEC = true;
    if (this.enseignantClasseForm.invalid) {
      return;
    }
    const dto = new EditMatiereClasseEnseignant(this.f.anneeAcc.value, this.f.bareme.value, this.f.classe.value,
      this.f.coef.value, this.isReattribuer ? this.f.enseignant.value : this.idEnseignant,
      this.currentProgrammation ? this.currentProgrammation.idMatiereClasse : null,
      this.f.matiere.value, null);
    if (this.currentProgrammation) {
      if (this.isReattribuer) {
        this.genericsService.postResource('admin/reattribuer/matiereclasse', dto).then((response: any) => {
          if (response.success === true) {
            this.genericsService.confirmResponseAPI(response.message, 'success');
            this.onResetEnseignantClasse();
            this.onConsulterEnseignant(this.idEnseignant);
          }
        }).catch(reason => {
          console.log(reason);
        });
      } else {
        dto.enseignant_id = parseInt(this.idEnseignant, 0);
        this.genericsService.postResource('admin/update/matiereclasse', dto).then((response: any) => {
          if (response.success === true) {
            this.genericsService.confirmResponseAPI(response.message, 'success');
            this.onResetEnseignantClasse();
            this.onConsulterEnseignant(this.idEnseignant);
          }
        }).catch(reason => {
          console.log('erreur ici', reason);
        });
      }
    } else {
      this.genericsService.postResource('admin/ajouter/matiereclasse/enseignant', dto).then((response: any) => {
        if (response.success === true) {
          this.genericsService.confirmResponseAPI(response.message, 'success');
          this.onResetEnseignantClasse();
          this.onConsulterEnseignant(this.idEnseignant);
        }
      }).catch(reason => {
        console.log(reason);
      });
    }

  }

  ngOnDestroy(): void {
    this.onResetEnseignantClasse();
  }
}
