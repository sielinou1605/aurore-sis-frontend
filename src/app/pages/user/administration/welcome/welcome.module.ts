import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {WelcomeRoutingModule} from './welcome-routing.module';
import {WelcomeComponent} from './welcome.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '../../../../shared/shared.module';
import {HttpLoaderFactory} from '../../../../app.module';
import {HttpClient} from '@angular/common/http';


@NgModule({
  declarations: [WelcomeComponent],
    imports: [
        CommonModule,
        WelcomeRoutingModule,
        ReactiveFormsModule,
        NgMultiSelectDropDownModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
      SharedModule
    ]
})
export class WelcomeModule { }
