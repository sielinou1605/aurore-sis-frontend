import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome.component';


const routes: Routes = [
  {
    path: '', component: WelcomeComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Administration',
      titre: localStorage.getItem('activelang') === 'en' ? 'Administration | Aurore' : 'Administration | Aurore',
      icon: 'icofont-gears bg-c-blue',
      breadcrumb_caption: ' ',
      status: true,
      current_role: 'admin'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
