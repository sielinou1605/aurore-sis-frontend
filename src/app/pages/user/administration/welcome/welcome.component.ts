/* tslint:disable:triple-equals */
import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../shared/_services/auth.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericsService} from '../../../../shared/_services/generics.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FiltreAbsenceModel, FiltreNoteModel} from '../../../../shared/_models/response-dto/graduation/filtre-note-absence-model';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit, OnDestroy {
  @ViewChild('modalSelectNote', { static: true }) modalSelectNote: ElementRef;
  @ViewChild('modalSelectAbsence', { static: true }) modalSelectAbsence: ElementRef;

  submitted: boolean;
  selectNoteClasseForm: FormGroup;
  selectAbsenceClasseForm: FormGroup;

  filtreNote: FiltreNoteModel;
  filtreAbsence: FiltreAbsenceModel;
  public settingsNote = {};
  public settingsAbsence = {};
  public classeNoteFiltre: any;
  public classeAbsenceFiltre: any;


  public choix: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(
    private genericsService: GenericsService,  private fb: FormBuilder,
    private modalService: NgbModal, config: NgbModalConfig,
    private tokenStorage: TokenStorageService,
    public authService: AuthService,
    private router: Router,
    private translate: TranslateService
  ) {

    this.choix = localStorage.getItem('activelang') === 'en' ? 'Choose value' : 'Choisir la valeur';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.initFormAbsence();
    this.initFormNote();
    this.authService.returnListAction(3);
    this.onloadFiltres();
    this.settingsNote = localStorage.getItem('activelang') === 'en'
    ? {
      singleSelection: true,
      idField: 'matiereClasseId',
      textField: 'matiereClasseLabel',
      enableCheckAll: false,
      selectAllText: 'Check all',
      unSelectAllText: 'Uncheck all',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 10,
      searchPlaceholderText: 'Search',
      noDataAvailablePlaceholderText: 'No data available',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    }
    :  {
        singleSelection: true,
        idField: 'matiereClasseId',
        textField: 'matiereClasseLabel',
        enableCheckAll: false,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: 'Rechercher',
        noDataAvailablePlaceholderText: 'Aucune donnée disponible',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };

    this.settingsAbsence = localStorage.getItem('activelang') === 'en'
    ? {
      singleSelection: true,
      idField: 'id',
      textField: 'nomClasse',
      enableCheckAll: false,
      selectAllText: 'Check all',
      unSelectAllText: 'Uncheck all',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 10,
      searchPlaceholderText: 'Search',
      noDataAvailablePlaceholderText: 'No data available',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    }
    : {
        singleSelection: true,
        idField: 'id',
        textField: 'nomClasse',
        enableCheckAll: false,
        selectAllText: 'Tout cocher',
        unSelectAllText: 'Tout decocher',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 10,
        searchPlaceholderText: 'Rechercher',
        noDataAvailablePlaceholderText: 'Aucune donnée disponible',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: false,
        defaultOpen: false
      };
  }

  private initFormNote() {
    this.selectNoteClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      matiereclasse: ['', Validators.required]
    });
  }

  private initFormAbsence() {
    this.selectAbsenceClasseForm = this.fb.group({
      sequence: ['', Validators.required],
      classe: ['', Validators.required]
    });
  }

  private onloadFiltres() {
    this.genericsService.getResource(`prefet/filtre/saisie/absence`).then((responses: any) => {
      if (responses.success == true) {
        this.filtreAbsence = responses.data;
        this.classeAbsenceFiltre = this.filtreAbsence.classes;
      }
    }).catch(err => {
      console.log(err);
    });
    this.genericsService.getResource(`prefet/filtre/saisie/note`).then((responses: any) => {
      if (responses.success == true) {
        this.filtreNote = responses.data;
        this.classeNoteFiltre = this.filtreNote.matiereClasses;
      }
    }).catch(err => {
      console.log(err);
    });
  }

  get f() { return this.selectNoteClasseForm.controls; }

  get g() { return this.selectAbsenceClasseForm.controls; }

  isActif(url: string, id: string) {
    console.log(url);
    console.log(id);
    if (this.authService.isActif(id)) {
      this.router.navigate([url]).then(() => {});
    }
  }

  onDisplayListeEleve(composant: number) {
    this.submitted = true;
    if (composant == 0) {
      const matiereclasse = this.f.matiereclasse.value[0];
      if (this.selectNoteClasseForm.invalid) {
        return;
      }
      this.genericsService.startLoadingPage();
      this.router.navigate([`/administration/admin-notes/${matiereclasse.matiereClasseId}/sequence/${this.f.sequence.value}`])
        .then(() => {
          this.genericsService.stopLoadingPage();
          if (this.modalService.hasOpenModals()) {
            this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
          }
        });
    } else {
      const classe = this.g.classe.value[0];
      if (this.selectAbsenceClasseForm.invalid) {
        return;
      }
      this.genericsService.startLoadingPage();
      this.router.navigate([`/administration/admin-absences/${classe.id}/sequence/${this.g.sequence.value}`])
        .then(() => {
          this.genericsService.stopLoadingPage();
          if (this.modalService.hasOpenModals()) {
            this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
          }
        });
    }
  }

  onDisplayModalToNote(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'select-note', size: 'lg'}).result.then((result) => {
    }, (reason) => {
    });
  }

/*  onDisplayModalToAbsence(content: TemplateRef<any>) {
    this.modalService.open(content,
      {ariaLabelledBy: 'select-absence', size: 'lg'}).result.then((result) => {
    }, (reason) => {
    });
  }*/

    onReset() {
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll();
      }
    }

  ngOnDestroy(): void {
    this.onReset();
  }

/*  onDisplayHistoryLogNotes() {
    this.router.navigateByUrl('/administration/log-notes').then(() => {});
  }*/
  onDisplayHistoryLogAbscence() {
    this.router.navigateByUrl('/administration/log-abscences').then(() => {});
  }
}
