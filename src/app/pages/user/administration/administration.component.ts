// @ts-ignore
import {Component, OnInit} from '@angular/core';
// @ts-ignore
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnInit {

  public siteLanguage: string;
 public supportLanguages = ['en', 'fr'];

  constructor(private translate: TranslateService) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
  }

}
