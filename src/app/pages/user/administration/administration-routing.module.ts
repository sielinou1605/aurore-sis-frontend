import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdministrationComponent } from './administration.component';
import { AideComponent } from './aide/aide.component';
import { StatistiquesComponent } from './statistiques/statistiques.component';
import { SynchronisationComponent } from './portail/synchronisation/synchronisation.component';
//import { AdminNotesComponent } from './notes/admin-notes/admin-notes.component';
import { AdminAbsencesComponent } from './discipline/admin-absences/admin-absences.component';
import { PortailComponent } from './portail/portail.component';
// import {LogNotesComponent} from './notes/log-notes/log-notes.component';
import {LogAbscenceComponent} from './discipline/log-abscence/log-abscence.component';

const routes: Routes = [
  {
    path: '', component: AdministrationComponent,
    data: {
      breadcrumb: 'userAdminFinancesListServices.Administration',
      // titre: 'userAdminFinancesListServices.Administration_Aurore',
      titre: 'Administration | Aurore',
      icon: 'icofont-gears bg-c-blue',
      breadcrumb_caption: 'userAdminFinancesListServices.Administration_et_gestion_des_taches_critiques_Administration',
      status: true,
      current_role: 'admin',
      action: 'bouton-administration'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome',
        loadChildren: () => import('./welcome/welcome.module')
          .then(m => m.WelcomeModule)
      },
      {
        path: 'utilisateurs',
        loadChildren: () => import('./utilisateurs/utilisateurs.module')
          .then(m => m.UtilisateursModule)
      },
      {
        path: 'configuration',
        loadChildren: () => import('./configuration/configuration.module')
          .then(m => m.ConfigurationModule)
      },
      {
        path: 'discipline',
        loadChildren: () => import('./discipline/discipline.module')
          .then(m => m.DisciplineModule)
      },
      {
        path: 'notes',
        loadChildren: () => import('./notes/notes.module')
          .then(m => m.NotesModule)
      },
      {
        path: 'finances',
        loadChildren: () => import('./finances/finances.module')
          .then(m => m.FinancesModule)

      },
      {
        path: 'portail',
        loadChildren: () => import('./portail/portail.module')
          .then(m => m.PortailModule)

      },
      // {
      //   path: 'portail', component: PortailComponent,
      //   data: {
      //     breadcrumb: 'userEleveAjaout.Portail',
      //     icon: 'icofont-question-circle bg-c-blue',
      //     breadcrumb_caption: 'userEleveAjaout.Administration_et_gestion_des_acces_au_portail_Portail',
      //     status: true,
      //     current_role: 'admin'
      //   }
      // },
      {
        path: 'statistiques', component: StatistiquesComponent,
        data: {
          breadcrumb: 'userAdminFinancesListServices.Stats',
          // titre: 'userAdminFinancesListServices.Stats_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Stats | Aurore' : 'Stats | Aurore',
          icon: 'icofont-question-circle bg-c-blue',
          breadcrumb_caption: 'userAdminFinancesListServices.Détails_financiers_par_classe_Statistiques',
          status: true,
          current_role: 'admin'
        }
      },
      // {
      //   path: 'admin-notes/:matiereclasse/sequence/:sequence', component: AdminNotesComponent,
      //   data: {
      //     breadcrumb: 'userAdminFinancesListServices.Notes',
      //     // titre: 'userAdminFinancesListServices.Notes_Aurore',
      //     titre: localStorage.getItem('activelang') === 'en' ? 'Note | Aurore' : 'Notes | Aurore',
      //     icon: 'icofont-refresh bg-youtube',
      //     breadcrumb_caption: 'userAdminFinancesListServices.Management_complet_des_notes_eleves_Notes',
      //     status: true,
      //     current_role: 'admin'
      //   }
      // },
      /*{
        path: 'admin-absences/:classe/sequence/:sequence', component: AdminAbsencesComponent,
        data: {
          breadcrumb: 'userAdminFinancesListServices.Absences',
          // titre: 'userAdminFinancesListServices.Absences_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Absences | Aurore' : 'Absences | Aurore',
          icon: 'icofont-refresh bg-youtube',
          breadcrumb_caption: 'userAdminFinancesListServices.Management_complet_des_absences_eleve_d_une_classe_Absences',
          status: true,
          current_role: 'admin'
        }
      },*/
      {
        path: 'synchronisation', component: SynchronisationComponent,
        data: {
          breadcrumb: 'userAdminFinancesListServices.Synchronisation',
          // titre: 'userAdminFinancesListServices.Absences_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Absences | Aurore' : 'Absences | Aurore',
          icon: 'icofont-refresh bg-youtube',
          breadcrumb_caption: 'userAdminFinancesListServices.Mettre_a_jour_les_données_en_ligne_et_en_local_Synchronisation',
          status: true,
          current_role: 'admin'
        }
      },
      /*{
        path: 'log-notes', component: LogNotesComponent,
        data: {
          breadcrumb: 'Historique notes',
          // titre: 'userAdminFinancesListServices.Aide_Aurore',
          titre: 'Historique de notes | Aurore',
          icon: 'icofont-history bg-c-blue',
          breadcrumb_caption: 'Gestion des Historiques de notes - Administration',
          status: true,
          current_role: 'admin'
        }
      },*/
      /*{
        path: 'log-abscences', component: LogAbscenceComponent,
        data: {
          breadcrumb: 'Historique abscence',
          // titre: 'userAdminFinancesListServices.Aide_Aurore',
          titre: 'Historique des abscences | Aurore',
          icon: 'icofont-history bg-c-blue',
          breadcrumb_caption: 'Gestion des Historiques des abscences - Administration',
          status: true,
          current_role: 'admin'
        }
      },*/
      {
        path: 'help', component: AideComponent,
        data: {
          breadcrumb: 'Aide',
          // titre: 'userAdminFinancesListServices.Aide_Aurore',
          titre: localStorage.getItem('activelang') === 'en' ? 'Help | Aurore' : 'Aide | Aurore',
          icon: 'icofont-question-circle bg-c-blue',
          breadcrumb_caption: 'Gestion des aides - Administration',
          status: true,
          current_role: 'admin'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
