import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome.component';


const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Aurore',
      titre: localStorage.getItem('activelang') === 'en' ? 'Aurore | Aurore' : 'Aurore | Aurore',
      icon: 'icofont icofont-ui-home bg-c-pink',
      breadcrumb_caption: 'userAdminFinancesListRemise.La_page_d_accueil_Welcome_Page',
      status: true,
      current_role: 'welcome'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
