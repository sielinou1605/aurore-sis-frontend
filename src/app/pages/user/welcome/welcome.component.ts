/* tslint:disable:triple-equals */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {TokenStorageService} from '../../../shared/_services/token-storage.service';
import {AuthService} from '../../../shared/_services/auth.service';
import {ActionResponse} from '../../../shared/_models/response-dto/systeme/data-module-response-model';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {GenericsService} from '../../../shared/_services/generics.service';
import {TranslateService} from '@ngx-translate/core';
import {AdminComponent} from '../../../layout/admin/admin.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
  providers: [DatePipe]
})
export class WelcomeComponent implements OnInit, OnDestroy {
  myDate = new Date();
  public dateJour: string;
  public actions: ActionResponse[];
  private i = 0;
  private subscription: Subscription;
  private classeStats: any;
  public joursFin = new Date('2022-06-15T04:18:31');
  private annee: any;

  public siteLanguage: string;
 public supportLanguages = ['en', 'fr'];

  constructor(private datePipe: DatePipe, private tokenStorage: TokenStorageService,
              public authService: AuthService, private router: Router,
              private genericsService: GenericsService,
              private translate: TranslateService
              ) {
    //this.translate.setDefaultLang('en');

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    this.dateJour = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
    authService.returnListAction(2);
    this.subscription = authService.actions.subscribe((response) => {
      this.actions = response;
    });
  }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
    this.genericsService.getResource(`admin/annees`).then((response: any) => {
      this.annee = response.data.find(item => item.codeAnnee == this.tokenStorage.getUser().codeAnnee);
      this.joursFin = new Date(this.annee.dateFermeture);
    }).catch(reason => console.log(reason));
  }

  isActif(url: string, id: string) {
    if (this.authService.isActif(id)) {
      this.router.navigate([url]).then(() => {});
    }
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
