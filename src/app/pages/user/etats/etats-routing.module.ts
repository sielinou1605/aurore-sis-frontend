import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EtatsComponent} from './etats.component';
import {EtWelcomeComponent} from './et-welcome/et-welcome.component';
import {ListEtatsComponent} from './list-etats/list-etats.component';

const routes: Routes = [
  {
    path: '', component: EtatsComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Etats',
      titre: localStorage.getItem('activelang') === 'en' ? 'State | Aurore' : 'Etats | Aurore',
      icon: 'icofont-print bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Gestion_des_etats_Etats',
      status: true,
      current_role: 'etat',
      action: 'bouton-etat'
    },
    children: [
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      },
      {
        path: 'welcome', component: EtWelcomeComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Etats',
          titre: localStorage.getItem('activelang') === 'en' ? 'State | Aurore' : 'Etats | Aurore',
          icon: 'icofont-print bg-c-pink',
          breadcrumb_caption: 'userEleveAjaout.Gestion_des_etats_Etats',
          status: true,
          current_role: 'etat'
        }
      },
      {
        path: 'list-etats', component: ListEtatsComponent,
        data: {
          breadcrumb: 'userEleveAjaout.Etats',
          titre: localStorage.getItem('activelang') === 'en' ? 'State | Aurore' : 'Etats | Aurore',
          icon: 'icofont-boy bg-c-blue',
          breadcrumb_caption: 'userEleveAjaout.Consulter_et_imprimer_tout_les_etats_de_l_application_Etats',
          status: true,
          current_role: 'etat'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtatsRoutingModule { }
