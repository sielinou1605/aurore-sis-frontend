/* tslint:disable:triple-equals */
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericsService } from '../../../../shared/_services/generics.service';
import { ModalDismissReasons, NgbCalendar, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ParamEtat, ParamImpressionModel } from '../../../../shared/_models/request-dto/common/etats-model';
import { TokenStorageService } from '../../../../shared/_services/token-storage.service';
import { SituationFinanceEleveModel } from '../../../../shared/_models/request-dto/finance/situation-finance-eleve-model';
import { AuthService } from '../../../../shared/_services/auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list-etats',
  templateUrl: './list-etats.component.html',
  styleUrls: ['./list-etats.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ListEtatsComponent implements OnInit, OnDestroy {
  etats: any;
  etatsData: any;
  etatForm: FormGroup;
  currentEtat: any;
  // @ts-ignore
  @ViewChild('selectFiltre') selectFiltre;
  public settings = {};
  printEtatForm: FormGroup;
  printEleveEtatForm: FormGroup;
  printBulletinForm: FormGroup;
  submittedPrint: boolean;
  public availableFiltre: any;
  public fenetres: any;
  public modeleImpressions: any;
  public ownerFiltre: any;
  public annees: any[];
  public classes: any;
  private searchvalue: string = '';
  searchForm: FormGroup;

  public totalElements = 0;
  public pageSize = 100;
  public page = 0;
  public impressionReleveForm: FormGroup;
  public submitted: boolean;
  filtreEtatForm: FormGroup;
  validated: boolean;
  public listOldAnnees: any;

  public eric: string;
  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(private genericsService: GenericsService, private modalService: NgbModal,
    config: NgbModalConfig, private fb: FormBuilder, private calendar: NgbCalendar,
    private tokenStorage: TokenStorageService, public authService: AuthService,
    private router: Router, private translate: TranslateService) {


    this.eric = localStorage.getItem('activelang') === 'en' ? 'Choose the value' : 'Choisir la valeur';
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    config.backdrop = 'static';
    config.keyboard = false;
    authService.returnListAction(82);
  }

  ngOnInit() {
    this.settings = {
      singleSelection: true,
      idField: 'valeur',
      textField: 'texte',
      enableCheckAll: true,
      selectAllText: localStorage.getItem('activelang') === 'en' ? 'Check all' : 'Tout cocher',
      unSelectAllText: localStorage.getItem('activelang') === 'en' ? 'Uncheck all' : 'Tout decocher',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 5,
      searchPlaceholderText: localStorage.getItem('activelang') === 'en' ? 'Search' : 'Rechercher',
      noDataAvailablePlaceholderText: localStorage.getItem('activelang') === 'en' ? 'No data available' : 'Aucune donnée disponible',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    };
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.onloadListEtat();
    this.onloadListFenetre();
    this.onloadListeClasse();
    this.onloadAnneeFermees();
    this.initFormEtat();
    this.initFormPrintEtat();
    this.initFormPrintEleveEtat();
    this.initImpressionReleveForm();
    this.initFormFiltreEtat();
    this.initFormPrintBulletin();
  }
  get f() { return this.impressionReleveForm.controls; }

  get search() { return this.searchForm.controls; }

  onOpenPrintModal(etat, modalPrintEtat: TemplateRef<any>) {
    if (etat) {
      this.currentEtat = etat;
      this.onloadOwnerFiltre(etat);
      this.onloadModelesImpressions();
      this.modalService.open(modalPrintEtat,
        { ariaLabelledBy: 'print-etat', size: 'lg' }).result.then((result) => {
        }, (reason) => { });
    }
  }

  private onloadListEtat() {
    this.genericsService.startLoadingPage();
    this.genericsService
      .getResource(`parametre/etat/liste/paged?size=${this.pageSize}&page=${this.page}`)
      .then((resultEtat: any) => {
        if (resultEtat.success === true) {
          this.etats = resultEtat.data.content;
          this.etatsData = resultEtat.data.content;
          this.totalElements = resultEtat.data.totalElements;
        }
        this.genericsService.stopLoadingPage();
      }).catch((errEtat: any) => {
        this.genericsService.stopLoadingPage();
        console.log(errEtat);
      });
  }

  setPageChange(event) {
    this.page = event - 1;
    this.onloadListEtat();
  }

  private onloadListFenetre() {
    this.genericsService.getResource(`systeme/fenetre/liste`).then((resultFenetre: any) => {
      if (resultFenetre.success === true) {
        this.fenetres = resultFenetre.data;
      }
    }).catch((errFenetre: any) => {
      console.log(errFenetre);
    });
  }

  private onloadOwnerFiltre(etat) {
    this.genericsService.getResource(`parametre/etat/${etat.id}/traiter/1/filtre/liste`)
      .then((resultFiltre: any) => {
        this.ownerFiltre = resultFiltre.data.filtres;
        this.availableFiltre = [];
        this.ownerFiltre.forEach(elt => {
          this.availableFiltre[elt.filtre.id] = elt.sourceDonnees;
          if (elt.filtre.typeSelection == 0 || elt.filtre.typeSelection == 1) {
            this.tp.push(
              this.fb.group({
                libelle: [elt.filtre.libelle],
                libelle_En: [elt.filtre.libelle_En],
                nomParamEtat: [elt.filtre.nomParametreEtat],
                nom: [elt.filtre.id],
                valeur: [''],
                type: [elt.filtre.typeSelection]
              })
            );
          } else if (elt.filtre.typeSelection == 4) {
            this.tp.push(
              this.fb.group({
                libelle: [elt.filtre.libelle],
                libelle_En: [elt.filtre.libelle_En],
                nomParamEtat: [elt.filtre.nomParametreEtat],
                nom: [elt.filtre.id],
                valeur: [''],
                type: [elt.filtre.typeSelection]
              })
            );
          } else {
            console.log(this.availableFiltre);
            this.tp.push(
              this.fb.group({
                libelle: [elt.filtre.libelle],
                libelle_En: [elt.filtre.libelle_En],
                nomParamEtat: [elt.filtre.nomParametreEtat],
                nom: [elt.filtre.id],
                valeur: [[]],
                type: [elt.filtre.typeSelection]
              })
            );
          }
        });
      }).catch((errFiltre: any) => {
        console.log(errFiltre);
      });
  }
  private onloadModelesImpressions() {
    this.genericsService.getResource(`parametre/etat/${this.currentEtat.id}/modele/liste`)
      .then((resultModeleI: any) => {
        this.modeleImpressions = resultModeleI.data;
      }).catch((errModeleI: any) => {
        console.log(errModeleI);
      });
  }
  private onloadListeClasse() {
    this.genericsService.getResource(`admin/classes`).then((response: any) => {
      this.classes = response.data;
    }).catch(reason => console.log(reason));
  }
  private initFormEtat() {
    this.etatForm = this.fb.group({
      libelle: ['', Validators.required],
      description: [''],
      groupe: ['', Validators.required],
      vue: ['', Validators.required],
      chemin: ['', Validators.required],
      filtres: [[]]
    });
  }
  private initFormPrintEtat() {
    this.printEtatForm = this.fb.group({
      choisirEtat: [''],
      cat: [''],
      datePrint: ['']
    });
  }

  private initFormPrintEleveEtat() {
    this.printEleveEtatForm = this.fb.group({
      codeAnnee: [this.tokenStorage.getUser().codeAnnee],
      dateAdmissionDebut: [null],
      dateAdmissionFin: [null],
      idEntite: ['', Validators.required],
      nomEntite: ['CLASSE', Validators.required],
      predicat: [null],
      type: ['INSOLVABLE', Validators.required]
    });
  }
  get ef() { return this.etatForm.controls; }
  get ep() { return this.printEtatForm.controls; }
  get epe() { return this.printEleveEtatForm.controls; }
  onChangeOptions() {

  }
  onPrintEtatType(type: boolean) {
    this.submittedPrint = true;
    if (this.printEtatForm.invalid || this.filtreEtatForm.invalid) {
      localStorage.getItem('activelang') === 'en'
     ? this.genericsService.confirmResponseAPI('Error in the form', 'error', 7000)
      :  this.genericsService.confirmResponseAPI('Erreur dans le formulaire', 'error', 7000);
      return;
    }
    const paramsEtats = [];
    this.tp.controls.forEach(item => {
      if (paramsEtats.find(param => param.nom == item.value.nomParamEtat) == undefined) {
        paramsEtats.push(new ParamEtat(
          item.value.nomParamEtat,
          (item.value.type == 0 || item.value.type == 1 || item.value.type == 4)
            ? (item.value.valeur.length > 0 ? item.value.valeur : null)
            : (item.value.valeur.length > 0 ? item.value.valeur[0].valeur : null)
        ));
      }
    });
    const dto = new ParamImpressionModel(type, this.currentEtat.id, paramsEtats);
    this.genericsService.reportPostPostResource(`parametre/etat/imprimer`, dto).then((result: any) => {
      this.genericsService.getByteArrayAndSaveReportPDF(result, `Report_File_`, type);
      this.tp.clear();
      this.initFormPrintEtat();
      this.initFormFiltreEtat();
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
    });
  }

  onPrintEleveEtatType() {
    this.submittedPrint = true;
    if (this.printEleveEtatForm.invalid) {
      return;
    }
    const dto = new SituationFinanceEleveModel(
      this.epe.codeAnnee.value,
      this.epe.dateAdmissionDebut.value,
      this.epe.dateAdmissionFin.value,
      this.epe.idEntite.value,
      this.epe.nomEntite.value,
      this.epe.predicat.value,
      this.epe.type.value
    );
    this.genericsService.reportPostPostResource(`admin/impression/eleve/situation-financiere`, dto).then((result: any) => {
      if (this.modalService.hasOpenModals()) {
        this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
      }
      localStorage.getItem('activelang') === 'en'
      ? this.genericsService.getByteArrayAndSaveReportPDF(result, 'Students_')
      : this.genericsService.getByteArrayAndSaveReportPDF(result, 'Eleves_');
    });
  }
  onResetPrintEtat() {
    this.submittedPrint = false;
    this.tp.clear();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onResetPrintEleveEtat() {
    this.submittedPrint = false;
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }
  onChangeValueSearch(event: any) {
    console.log(event.target.value);
    this.searchvalue = event.target.value;
  }
  onSearchEtat() {
    console.log(this.etatsData);
    if (this.authService.isActif('bouton-recherche-etat')) {
      let predicat = new RegExp(this.searchvalue, 'i'); // prepare a regex object
      this.etats = this.etatsData.filter(
        item => (
          predicat.test((this.siteLanguage == 'en' ) ? item.description_en : item.description)
          || predicat.test((this.siteLanguage == 'en' ) ? item.libelle_en : item.libelle)
          || predicat.test(item.groupe))
)}
 }
  onConfigImpressionReleve(impressionReleve: TemplateRef<any>) {
    this.modalService.open(impressionReleve,
      { ariaLabelledBy: 'print-releve', size: 'lg', scrollable: true }).result.then((result) => {
      }, (reason) => { });
  }

  onReset() {
    this.initImpressionReleveForm();
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  private initImpressionReleveForm() {
    this.impressionReleveForm = this.fb.group({
      dateDebut: [this.calendar.getToday(), Validators.required],
      dateFin: ['', Validators.required]
    });
  }

  onPrintReleveCaisse() {
    this.submitted = true;
    this.validated = true;
    if (this.impressionReleveForm.invalid) {
      this.validated = false;
      return;
    }
    const filter = { dateDebut: this.f.dateDebut.value, dateFin: this.f.dateFin.value };
    this.genericsService.reportPostPostResource(`admin/impression/releve-caisse`, filter).then((result: any) => {
      this.validated = false;
      localStorage.getItem('activelang') === 'en'
     ? this.genericsService.getByteArrayAndSaveReportPDF(result, 'Cash_Release_')
      : this.genericsService.getByteArrayAndSaveReportPDF(result, 'Releve_caisse_');
      this.onReset();
    }).catch((reason => {
      this.validated = false;
    }));
  }

  ngOnDestroy(): void {
    this.onReset();
    this.onResetPrintEleveEtat();
    this.onResetPrintEtat();
  }

  get fp() { return this.filtreEtatForm.controls; }
  get tp() { return this.fp.filtreData as FormArray; }
  private initFormFiltreEtat() {
    this.filtreEtatForm = this.fb.group({
      filtreData: this.fb.array([])
    });
  }

  private onloadAnneeFermees() {
    this.genericsService.getResource(`admin/annees/fermes/listes`)
      .then((result: any) => {
        this.listOldAnnees = result.data;
        console.log(this.listOldAnnees);
      }).catch((err) => {
        console.log(err);
      });
  }

  private initFormPrintBulletin() {
    this.printBulletinForm = this.fb.group({
      idAnnee: [0, Validators.required]
    });
  }

  public get annee() {
    return this.printBulletinForm.controls;
  }

  onSelectAnnuel(content: TemplateRef<any>) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-annuel', scrollable: true }).result.then((result) => {
    }, (reason) => {
    });
  }

  onDisplayListBulletin() {
    this.submitted = true;
    this.validated = true;
    if (this.printBulletinForm.invalid || this.annee.idAnnee.value == 0) {
      this.validated = false;
      return;
    }
    this.router.navigate([`/gestion-notes/bulletins/list-bulletins/${this.annee.idAnnee.value}`])
      .then((elt) => console.log(elt));
  }
}
