import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EtWelcomeComponent} from './et-welcome.component';

describe('EtWelcomeComponent', () => {
  let component: EtWelcomeComponent;
  let fixture: ComponentFixture<EtWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
