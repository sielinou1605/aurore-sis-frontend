import {Component, OnDestroy, OnInit} from '@angular/core';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {AuthService} from '../../../../shared/_services/auth.service';
import {ActionResponse} from '../../../../shared/_models/response-dto/systeme/data-module-response-model';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-et-welcome',
  templateUrl: './et-welcome.component.html',
  styleUrls: ['./et-welcome.component.scss']
})
export class EtWelcomeComponent implements OnInit, OnDestroy {
  public actions: ActionResponse[];
  private subscription: Subscription;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  constructor(private tokenStorage: TokenStorageService,
              public authService: AuthService,
              private router: Router,
              private translate: TranslateService) {
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

    authService.returnListAction(93);
  }

  ngOnInit() {
    this.subscription = this.authService.actions.subscribe((response) => console.log(response));
  }

  isActif(url: string, id: string) {
    if (this.authService.isActif(id)) {
      this.router.navigate([url]).then(() => {});
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
