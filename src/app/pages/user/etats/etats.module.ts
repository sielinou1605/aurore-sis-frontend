import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EtatsRoutingModule} from './etats-routing.module';
import {EtatsComponent} from './etats.component';
import {SharedModule} from '../../../shared/shared.module';
import {EtWelcomeComponent} from './et-welcome/et-welcome.component';
import {ListEtatsComponent} from './list-etats/list-etats.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {NgxPaginationModule} from 'ngx-pagination';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [EtatsComponent, EtWelcomeComponent, ListEtatsComponent],
    imports: [
        CommonModule,
        EtatsRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgMultiSelectDropDownModule,
        NgxPaginationModule,
        TranslateModule
    ]
})
export class EtatsModule { }
