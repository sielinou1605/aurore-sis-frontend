import {Component, OnInit} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {GenericsService} from '../../../shared/_services/generics.service';
import {UserLoggedModel} from '../../../shared/_models/auth/user-logged-model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AppConstants} from '../../../shared/element/app.constants';
import {HttpEventType} from '@angular/common/http';
import {EditerEnseignantModel} from '../../../shared/_models/request-dto/graduation/gestion-cours-enseignant-model';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
})
export class ProfileComponent implements OnInit {
  editProfile = true;
  editProfileIcon = 'icofont-edit';
  editAbout = true;
  editAboutIcon = 'icofont-edit';
  public basicContent: string;
  public rowsOnPage = 10;
  public sortBy = '';
  public sortOrder = 'desc';
  public currentUser: UserLoggedModel;
  public user: any;
  public enseignant: any;
  profileForm: FormGroup;
  submitted: boolean;
  private timestamp: any;
  public urlImage  = AppConstants.API_URL + 'admin/recuperation/image/';

  private progress: number;
  private currentFileUpload: any;
  private selectedFiles: any;
  private currentEnseignant: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];

  constructor(
            public genericsService: GenericsService,
            private fb: FormBuilder,
            private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);

    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
    this.loadProfileUser();
    this.initProfileForm();
    this.timestamp = Date.now();
  }
  toggleEditProfile() {
    this.editProfileIcon = (this.editProfileIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
    this.editProfile = !this.editProfile;
  }
  toggleEditAbout() {
    this.editAboutIcon = (this.editAboutIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
    this.editAbout = !this.editAbout;
  }
  private loadProfileUser() {
    this.genericsService.getResource(`admin/user/profile`).then((response: any) => {
      this.user = response.data.user;
      this.enseignant = response.data.enseignant;
      console.log(this.enseignant);
      if (this.enseignant) {
        this.genericsService.getResource(`admin/consulter/${this.enseignant.id_enseignant}/enseignant`).then((response1: any) => {
          console.log(response1);
          this.currentEnseignant = response1.data.enseignant;
          this.profileForm.patchValue({
            nom: this.currentEnseignant.nom_enseignant,
            prenom: this.currentEnseignant.prenom_enseignant,
            sexe: this.currentEnseignant.sexe,
            statutm: this.currentEnseignant.statut_matrimonial,
            telephone1: this.currentEnseignant.telephone_1,
            telephone2: this.currentEnseignant.telephone_2,
            email: this.currentEnseignant.email,
            matricule: this.currentEnseignant.matricule,
            photo: this.currentEnseignant.photo,
            institution: null,
            departement: null,
            fonction: this.currentEnseignant.fonction,
            typepersonnel: this.currentEnseignant.type_personnel,
            statutprof: this.currentEnseignant.statut_professionnel,
            anciennete: this.currentEnseignant.anciennete,
            lieuprovenance: this.currentEnseignant.provenance
          });
        });
      }
    }).catch(reason => {

    });
  }
  private initProfileForm() {
    this.profileForm = this.fb.group({
      nom: [''],
      prenom:  [''],
      sexe:  [''],
      statutm:  [''],
      telephone1:  [''],
      telephone2:  [''],
      email:  [''],
      matricule:  [''],
      photo:  [''],
      institution:  [''],
      departement:  [''],
      fonction:  [''],
      typepersonnel: [],
      statutprof: [''],
      anciennete: [''],
      lieuprovenance: ['']
    });
  }
  get f() { return this.profileForm.controls; }
  onUpdateProfileLoggedUser() {
    console.log(this.f);
    this.submitted = true;
    if (this.profileForm.invalid) {
      return;
    }
    const dto = new EditerEnseignantModel(this.f.anciennete.value, null, '', '',
      this.f.email.value, this.f.fonction.value, this.f.departement.value, this.f.institution.value,
      this.currentEnseignant.id_enseignant, this.f.matricule.value, 0,
      this.f.nom.value,  this.f.photo.value, this.f.prenom.value, this.f.lieuprovenance.value, 0, this.f.sexe.value,
      this.f.statutm.value, this.f.telephone1.value, this.f.telephone2.value, this.f.typepersonnel.value);
    this.genericsService.postResource(`admin/enseignant/ajouter`, dto).then((response: any) => {
      if (this.currentEnseignant) {
        this.currentEnseignant = null;
        this.toggleEditProfile();
        if (this.selectedFiles) {
          this.uploadPhotoEnseignant(response.data.id_enseignant);
        }
      }
    }).catch(reason => {
      console.log(reason);
    });
  }
  getTs() { return this.timestamp; }

  uploadPhotoEnseignant(id: number) {
    this.progress = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.genericsService.uploadImage(this.currentFileUpload, `admin/enseignant/${id}/photo/upload`).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else {
          this.progress = undefined;
          this.genericsService.confirmResponseAPI('Photo de profil mise a jour avec succès', 'success');
        }
      },
      errorPhoto => {
        console.log(errorPhoto);
        this.genericsService.confirmResponseAPI('Echec de mise à jour photo de profil', 'error');
      });
    this.selectedFiles = undefined;
  }

  onselectedFile(event: any) {
    this.selectedFiles = event.target.files;
  }
}
