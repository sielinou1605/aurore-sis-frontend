import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from './profile.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Profil_utilisateur',
      titre: localStorage.getItem('activelang') === 'en' ? 'User profile | Aurore' : 'Profil utilisateur | Aurore',
      icon: 'icofont-justify-all bg-c-green',
      breadcrumb_caption: 'userEleveAjaout.Les_détails_sur_l_utilisateur_connecté_Profil_Utilisateur',
      status: true,
      current_role: 'welcome'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
