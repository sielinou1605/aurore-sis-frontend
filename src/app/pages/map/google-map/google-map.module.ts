import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {GoogleMapRoutingModule} from './google-map-routing.module';
import {GoogleMapComponent} from './google-map.component';
import {SharedModule} from '../../../shared/shared.module';
import {AgmCoreModule} from '@agm/core';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        GoogleMapRoutingModule,
        SharedModule,
        AgmCoreModule.forRoot({apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk'}),
        TranslateModule
    ],
  declarations: [GoogleMapComponent]
})
export class GoogleMapModule { }
