import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GoogleMapComponent} from './google-map.component';

const routes: Routes = [
  {
    path: '',
    component: GoogleMapComponent,
    data: {
      breadcrumb: 'userEleveAjaout.Google_Map',
      icon: 'icofont-map bg-c-pink',
      breadcrumb_caption: 'userEleveAjaout.Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elit - Google Map',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoogleMapRoutingModule { }
