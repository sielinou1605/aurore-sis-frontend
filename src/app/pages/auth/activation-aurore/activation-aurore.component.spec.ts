import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActivationAuroreComponent} from './activation-aurore.component';

describe('ActivationAuroreComponent', () => {
  let component: ActivationAuroreComponent;
  let fixture: ComponentFixture<ActivationAuroreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivationAuroreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivationAuroreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
