import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ActivationAuroreComponent} from './activation-aurore.component';


const routes: Routes = [
  {
    path: '',
    component: ActivationAuroreComponent,
    data: {
      title: localStorage.getItem('activelang') === 'en' ? 'Activation of the aurora product' : 'Activation du produit aurore'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivationAuroreRoutingModule { }
