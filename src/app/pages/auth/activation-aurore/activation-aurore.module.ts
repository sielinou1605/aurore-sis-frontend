import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ActivationAuroreRoutingModule} from './activation-aurore-routing.module';
import {ActivationAuroreComponent} from './activation-aurore.component';
import {SharedModule} from '../../../shared/shared.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ActivationAuroreComponent],
  imports: [
    CommonModule,
    ActivationAuroreRoutingModule,
    SharedModule,
    NgxPaginationModule,
    TranslateModule
  ]
})
export class ActivationAuroreModule { }
