/* tslint:disable:triple-equals */
import {Component, OnInit} from '@angular/core';
import {AppConstants} from '../../../../shared/element/app.constants';
import {AuthService} from '../../../../shared/_services/auth.service';
import {TokenStorageService} from '../../../../shared/_services/token-storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../../shared/_services/user.service';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  currentUser: any;
  public responseStatus: any;

  public siteLanguage: string;
  public supportLanguages = ['en', 'fr'];
  logo: string;

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private translate: TranslateService
  ) {
    this.logo = AppConstants.LOGO_LOGIN;
    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1) {
      this.translate.setDefaultLang(this.siteLanguage);
    } else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }

  }

  ngOnInit() {
    console.log('Le numero de port', location.port);
    console.log('Hote courant', AppConstants.BASE_URL);
    this.translate.setDefaultLang(localStorage.getItem('activelang'));
    const token: string = this.route.snapshot.queryParamMap.get('token');
    const error: string = this.route.snapshot.queryParamMap.get('error');
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
    if (this.tokenStorage.getUser()) {
      this.isLoggedIn = true;
      this.currentUser = this.tokenStorage.getUser();
    } else if (token) {
      this.tokenStorage.saveToken(token);
      this.userService.getCurrentUser().subscribe(
        data => {
          this.login(data);
        },
        err => {
          this.errorMessage = localStorage.getItem('activelang') == 'en' ? AppConstants.FAIL_LOGIN_PASSEN : AppConstants.FAIL_LOGIN_PASSFR;
          // this.errorMessage = err.error.message;
          this.isLoginFailed = true;
        }
      );
    } else if (error) {
      this.errorMessage = error;
      this.isLoginFailed = true;
    }
  }


  onSubmit(): void {
    this.authService.login(this.form).subscribe(
      (data: any) => {
        this.tokenStorage.saveToken(data.data.accessToken);
        this.tokenStorage.saveCaisse(data.data.caisse);
        if (data.data.authenticated) {
          this.login(data.data.user);
        } else {
          this.router.navigate(['/authentication/totp']).then(() => {});
        }
      },
      err => {
        console.log(err.status);
        this.responseStatus = err.status;
        if (err.error.success == false) {
          this.errorMessage = err.error.message; // + ' ' + err.error.data;
        }
        this.isLoginFailed = true;
      }
    );
  }

  private login(user: any) {
    this.tokenStorage.saveUser(user);
    this.isLoginFailed = false;
    this.isLoggedIn = true;
    this.currentUser = this.tokenStorage.getUser();
    this.router.navigate(['/welcome']).then(() => {
      // window.location.reload();
    });
  }
}
