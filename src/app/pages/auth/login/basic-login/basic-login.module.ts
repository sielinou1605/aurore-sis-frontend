import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BasicLoginComponent} from './basic-login.component';
import {BasicLoginRoutingModule} from './basic-login-routing.module';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        BasicLoginRoutingModule,
        SharedModule,
        FormsModule,
        TranslateModule
    ],
  declarations: [BasicLoginComponent]
})
export class BasicLoginModule { }
