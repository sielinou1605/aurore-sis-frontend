import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TotpRoutingModule} from './totp-routing.module';
import {TotpComponent} from './totp.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [TotpComponent],
    imports: [
        CommonModule,
        TotpRoutingModule,
        FormsModule,
        SharedModule,
        TranslateModule
    ]
})
export class TotpModule { }
