import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TotpComponent} from './totp.component';

const routes: Routes = [
  {
    path: '',
    component: TotpComponent,
    data: {
      title: localStorage.getItem('activelang') === 'en' ? 'Totp Login' : 'Connexion Totp'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TotpRoutingModule { }
