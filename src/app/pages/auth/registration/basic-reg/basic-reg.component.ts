import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../shared/_services/auth.service';
import {ServiceTranslateService} from '../../../../service-translate.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-basic-reg',
  templateUrl: './basic-reg.component.html',
  styleUrls: ['./basic-reg.component.scss']
})
export class BasicRegComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  isUsing2FA = false;
  errorMessage = '';
  qrCodeImage = '';

 public siteLanguage: string;
 public supportLanguages = ['en', 'fr'];

  constructor(private authService: AuthService, private translate: TranslateService) {

    this.siteLanguage = localStorage.getItem('activelang');
    this.translate.addLangs(this.supportLanguages);
    if (this.supportLanguages.indexOf(this.siteLanguage) > -1){
      this.translate.setDefaultLang(this.siteLanguage);
    }else {
      this.translate.setDefaultLang(this.supportLanguages[0]);
    }
  }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');

  }

  onSubmit(): void {
    this.authService.register(this.form).subscribe((data: any) => {
        console.log(data);
        if (data.using2FA) {
          this.isUsing2FA = true;
          this.qrCodeImage = data.qrCodeImage;
        }
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.message;
        this.isSignUpFailed = true;
      });
  }

}
