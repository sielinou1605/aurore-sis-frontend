import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BasicRegComponent} from './basic-reg.component';
import {BasicRegRoutingModule} from './basic-reg-routing.module';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        BasicRegRoutingModule,
        SharedModule,
        FormsModule,
        TranslateModule
    ],
  declarations: [BasicRegComponent]
})
export class BasicRegModule { }
