import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthRoutingModule} from './auth-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {TotpModule} from './totp/totp.module';
import {ActivationAuroreModule} from './activation-aurore/activation-aurore.module';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    TotpModule,
    ActivationAuroreModule
  ],
  declarations: []
})
export class AuthModule { }
