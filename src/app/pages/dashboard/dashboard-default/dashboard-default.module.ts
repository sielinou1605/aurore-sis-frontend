import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardDefaultRoutingModule} from './dashboard-default-routing.module';
import {DashboardDefaultComponent} from './dashboard-default.component';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        DashboardDefaultRoutingModule,
        SharedModule,
        ChartModule,
        TranslateModule
    ],
  declarations: [DashboardDefaultComponent]
})
export class DashboardDefaultModule { }
