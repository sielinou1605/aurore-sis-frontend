import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {AdminComponent} from './layout/admin/admin.component';
import {TitleComponent} from './layout/admin/title/title.component';
import {AuthComponent} from './layout/auth/auth.component';
import {SharedModule} from './shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {authInterceptorProviders} from './shared/_helpers/auth.interceptor';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HelpComponent} from './pages/user/help/help.component';
import {HashLocationStrategy, LocationStrategy, registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {AvatarModule} from 'ngx-avatar';
import {PaiementAbonnementComponent} from './pages/user/paiement-abonnement/paiement-abonnement.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

registerLocaleData(localeFr, 'fr');
@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    TitleComponent,
    AuthComponent,
    HelpComponent,
    PaiementAbonnementComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
        AvatarModule,
        NgxPaginationModule
    ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [
    authInterceptorProviders,
    // en-US
    {provide: LOCALE_ID, useValue: 'en'},
    {provide: LocationStrategy, useClass: HashLocationStrategy} // fixer l'actualisation de la page en mode prod
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  // return new TranslateHttpLoader(http);
}
