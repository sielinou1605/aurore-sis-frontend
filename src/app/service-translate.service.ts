import {Injectable, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceTranslateService implements OnInit {

  change = 'fr';

  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang(this.change);
    translate.setDefaultLang('en');
    translate.use('en');
  }
  ngOnInit() {
    this.changeLang('en');
  }
  public changeLang(elt: string): void {
    this.change = elt;
    this.translate.getTranslation(this.change);
  }
}
