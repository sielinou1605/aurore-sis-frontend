import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SaisieCasComponent} from './saisie-cas.component';

describe('SaisieCasComponent', () => {
  let component: SaisieCasComponent;
  let fixture: ComponentFixture<SaisieCasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieCasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieCasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
