import {Component, OnInit} from '@angular/core';
import {GenericsService} from '../_services/generics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-saisie-cas',
  templateUrl: './saisie-cas.component.html',
  styleUrls: ['./saisie-cas.component.scss']
})
export class SaisieCasComponent implements OnInit {
  saisieCASForm: FormGroup;
  public submitted = false;

  constructor(private genericsService: GenericsService,
              private fb: FormBuilder, private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.initFormSaisieCAS();
  }

  onSaisieCAS() {
    this.submitted = true;
    if (this.saisieCASForm.invalid) {
      return;
    }
    this.genericsService.postResource(`serial/saisir/cas`,  this.f.textCAS.value).then((response: any) => {
      console.log(response);
      this.router.navigate(['/welcome']).then(() => {
        this.genericsService.confirmResponseAPI('Aurore activé avec success', 'success', 3000);
      });
    }).catch(err => console.log(err));
  }

  private initFormSaisieCAS() {
    this.saisieCASForm = this.fb.group({
      textCAS: ['', Validators.required]
    });
  }
  get f() { return this.saisieCASForm.controls; }

}
