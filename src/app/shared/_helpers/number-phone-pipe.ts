/* tslint:disable:triple-equals */
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'phoneFormat'
})
export class NumberPhonePipe implements PipeTransform {
  transform(number): any {
    if (number && number.length > 0) {
      let newStr = '';
      if (number.charAt(0) == '+') {
        newStr = number.slice(0, 4) + ' ';
        number = number.slice(4);
      } else if (number.slice(0, 2) == '00') {
        newStr = number.slice(0, 5) + ' ';
        number = number.slice(5);
      } else {
        number = '' + number;
      }

      let i = 0;
      for (; i < Math.floor(number.length / 3) - 1; i++) {
        newStr = newStr + number.substr(i * 3, 3) + ' ';
      }
      return newStr + number.substr(i * 3);
    } else {
      return '';
    }
  }
}
