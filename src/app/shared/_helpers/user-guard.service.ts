import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {TokenStorageService} from '../_services/token-storage.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AuthService} from '../_services/auth.service';
import {AppConstants} from '../element/app.constants';

@Injectable({
  providedIn: 'root'
})
export class UserGuardService implements CanActivate {

  constructor(
    private router: Router,
    private tokenStorage: TokenStorageService,
    private authService: AuthService
  ) {
    if (tokenStorage.getUser()) {
      authService.returnListAction(2);
    }
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const action = route.data.action;
    console.log(action);
    if (this.tokenStorage.getToken()) {
      const helper = new JwtHelperService();
      if (action && this.tokenStorage.getUser()) {
        if (!helper.isTokenExpired(this.tokenStorage.getToken()) && this.authService.isActif(action)) {
          return true;
        } else {
          localStorage.getItem('activelang') === 'en' ?
          Swal.fire({
            icon: 'error',
            title: AppConstants.TITLE_ACCESS_PAGE_FAILEN,
            text: AppConstants.CONTENT_ACCESS_PAGE_FAILEN,
            footer: AppConstants.RETURN_MESSAGEEN
          }).then(() => {
            this.router.navigate(['/']);
          }) : Swal.fire({
              icon: 'error',
              title: AppConstants.TITLE_ACCESS_PAGE_FAILFR,
              text: AppConstants.CONTENT_ACCESS_PAGE_FAILFR,
              footer: AppConstants.RETURN_MESSAGEFR
            }).then(() => {
              this.router.navigate(['/']);
            });
        }
      } else {
        if (!helper.isTokenExpired(this.tokenStorage.getToken())) {
          return true;
        } else {
          localStorage.getItem('activelang') === 'en' ?
          Swal.fire({
            icon: 'error',
            title: AppConstants.TITLE_ACCESS_PAGE_FAILEN,
            text: AppConstants.CONTENT_ACCESS_PAGE_FAILEN,
            footer: AppConstants.RETURN_MESSAGEEN
          }).then(() => {
            this.router.navigate(['/']);
          }) : Swal.fire({
              icon: 'error',
              title: AppConstants.TITLE_ACCESS_PAGE_FAILFR,
              text: AppConstants.CONTENT_ACCESS_PAGE_FAILFR,
              footer: AppConstants.RETURN_MESSAGEFR
            }).then(() => {
              this.router.navigate(['/']);
            });
        }
      }
    }
    this.router.navigate(['/authentication/login'], { queryParams: { returnUrl: state.url }});
    return false;
  }
}
