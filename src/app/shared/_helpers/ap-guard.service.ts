import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {TokenStorageService} from '../_services/token-storage.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AppConstants} from '../element/app.constants';

@Injectable({
  providedIn: 'root'
})
export class ApGuardService implements CanActivate {

  constructor(private router: Router, private tokenStorage: TokenStorageService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.tokenStorage.getToken()) {
      const helper = new JwtHelperService();
      if (!helper.isTokenExpired(this.tokenStorage.getToken()) &&
        // tslint:disable-next-line:triple-equals
        this.tokenStorage.getUser().roles.find(
          // tslint:disable-next-line:max-line-length
          localStorage.getItem('activelang') === 'en' ? elt => (elt.name === AppConstants.ROLE_COMPTABLEEN) : elt => (elt.name === AppConstants.ROLE_COMPTABLEFR))) {
        return true;
      } else {
        localStorage.getItem('activelang') === 'en' ?
        Swal.fire({
          icon: 'error',
          title: AppConstants.TITLE_ACCESS_PAGE_FAILEN,
          text: AppConstants.CONTENT_ACCESS_PAGE_FAILEN,
          footer: AppConstants.RETURN_MESSAGEEN
        }).then(() => {
          this.router.navigate(['/']);
        }) :  Swal.fire({
            icon: 'error',
            title: AppConstants.TITLE_ACCESS_PAGE_FAILFR,
            text: AppConstants.CONTENT_ACCESS_PAGE_FAILFR,
            footer: AppConstants.RETURN_MESSAGEFR
          }).then(() => {
            this.router.navigate(['/']);
          });
      }
    }
    this.router.navigate(['/authentication/login'], { queryParams: { returnUrl: state.url }});
    return false;
  }
}
