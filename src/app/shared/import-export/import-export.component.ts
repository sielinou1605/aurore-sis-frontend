import {Component, Input, OnInit} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {GenericsService} from '../_services/generics.service';
import {TokenStorageService} from '../_services/token-storage.service';

@Component({
  selector: 'app-import-export',
  templateUrl: './import-export.component.html',
  styleUrls: ['./import-export.component.scss']
})
export class ImportExportComponent implements OnInit {

  @Input() idmatiereclasse: any;
  @Input() idsequence: any;

  fileToUpload: File | null = null;
   currentMatiereClasse: any;
   currentSequence: any;

  constructor(
    private modalService: NgbModal,
    public tokenStorage: TokenStorageService,
    private genericsService: GenericsService
    ) { }

  ngOnInit() {
    console.log(this.tokenStorage.getUser().displayName);
    console.log(this.tokenStorage.getUser());
    this.initHeader(this.idmatiereclasse, this.idsequence);
  }

  private initHeader(matiereclasse: string, sequence: string) {
    this.genericsService.getResource(`admin/consulter/${matiereclasse}/matiereclasse`).then((response: any) => {
      this.currentMatiereClasse = response.data;
      console.log(this.currentMatiereClasse);
    }).catch(err => {
      console.log(err);
    });

    this.genericsService.getResource(`admin/consulter/${sequence}/sequence`).then((response: any) => {
      this.currentSequence = response.data;
      console.log(this.currentSequence);
    }).catch(err => {
      console.log(err);
    });
  }

  onSelectedFile(event) {
    console.log(event);
    this.fileToUpload = event.target.files[0];

    console.log(this.fileToUpload);
    this.onImportExport();
  }

  onImportExport() {

    console.log(this.fileToUpload);
    const formData = new FormData();
    if (this.fileToUpload != null) {
      formData.append('file', this.fileToUpload, this.fileToUpload.name);
      this.genericsService.uploadFile(`admin/import/notes/eleves`, formData).subscribe((response: any) => {
        console.log(response);
        this.genericsService.confirmResponseAPI(response.message, 'success');
      }, error => {
        console.error(error);
        this.genericsService.confirmResponseAPI(error.error.message, 'error');
      });
    }
    console.log(this.fileToUpload);

  }

  export() {
    this.genericsService.reportPostResource(`admin/export/releve-note/matiere-classe/${this.idmatiereclasse}/sequence/${this.idsequence}`)
      .then((result: any) => {
        console.log(result);

        let chaine1 = this.tokenStorage.getUser().displayName;
        // Supprimer les espaces et les caractères non alphabétiques
        chaine1 = chaine1.replace(/[^a-zA-Z]/g, '');
          // Convertir la première lettre en majuscule
        chaine1 = chaine1.charAt(0).toUpperCase() + chaine1.slice(1);
        let fileName = 'Releve_de_note_' + this.currentMatiereClasse.matiere.nom_matiere + '_' + this.currentMatiereClasse.classe.nomClasse + '_' + this.currentSequence.trimestre.nom_trimestre + '_' + this.currentSequence.nom_sequence + '_' + chaine1;
        // Suppression des espaces
        fileName = fileName.replace(/\s/g, '');
        this.genericsService.getByteArrayAndSaveReportPDFNote(result, fileName, true);
      }).catch((err) => {
      console.log(err);
      this.genericsService.confirmResponseAPI(err.message, 'error');
    });
  }

  onReset() {
    if (this.modalService.hasOpenModals()) {
      this.modalService.dismissAll();
    }
  }
}
