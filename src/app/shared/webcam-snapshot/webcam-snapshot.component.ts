import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';
import {HttpEventType} from '@angular/common/http';
import {GenericsService} from '../_services/generics.service';

@Component({
  selector: 'app-webcam-snapshot',
  templateUrl: './webcam-snapshot.component.html',
  styleUrls: ['./webcam-snapshot.component.scss']
})
export class WebcamSnapshotComponent implements AfterViewInit, OnDestroy {

  WIDTH = 640 / 2;
  HEIGHT = 480 / 2;

  // @ts-ignore
  @ViewChild('video')
  public video: ElementRef;

  // @ts-ignore
  @ViewChild('canvas')
  public canvas: ElementRef;

  captures: string[] = [];
  error: any;
  isCaptured: boolean;
  public progress: number;
  public selectedFile: any;
  public currentFileUpload: any;
  public timestamp: number;

  @Input() matricule: string;
  @Output() imageSaved = new EventEmitter<boolean>();
  constructor(private genericsService: GenericsService) {
  }

  async ngAfterViewInit() {
    await this.setupDevices();
  }

  async setupDevices() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      try {
        const stream = await navigator.mediaDevices.getUserMedia({
          video: true
        });
        if (stream) {
          this.video.nativeElement.srcObject = stream;
          this.video.nativeElement.play();
          this.error = null;
        } else {
          // tslint:disable-next-line:max-line-length
          this.error = localStorage.getItem('activelang') === 'en' ? 'You have no output video device' : 'Vous n\'avez pas de périphérique de sortie vidéo';
        }
      } catch (e) {
        this.error = e;
      }
    }
  }

  async capture() {
    this.drawImageToCanvas(this.video.nativeElement);
    this.captures.push(this.canvas.nativeElement.toDataURL('image/png'));
    this.isCaptured = true;
  }

  async removeCurrent() {
    this.isCaptured = false;
  }

  async setPhoto(idx: number) {
    this.isCaptured = true;
    const image = new Image();
    image.src = this.captures[idx];
    const base64 = this.captures[idx];
    const base64Response = await fetch(base64);
    const blob = await  base64Response.blob();
    this.selectedFile = new File([blob], this.matricule + '.png', {type: 'image/png'});
    this.drawImageToCanvas(image);
  }

  drawImageToCanvas(image: any) {
    this.canvas.nativeElement
      .getContext('2d')
      .drawImage(image, 0, 0, this.WIDTH, this.HEIGHT);
  }

  async ngOnDestroy(): Promise<void> {
    await this.stopWebcamCamera();
  }

  async stopWebcamCamera() {
    this.video.nativeElement.srcObject = null;
  }

  uploadPhotoEleve(matricule: string) {
    this.progress = 0;
    this.currentFileUpload = this.selectedFile;
    this.genericsService.uploadImage(this.currentFileUpload,
      `admin/upload/image/${matricule}/eleve`).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else {
          this.progress = undefined;
          localStorage.getItem('activelang') === 'en'
           ? this.genericsService.confirmResponseAPI('Saved profile photo', 'success')
           : this.genericsService.confirmResponseAPI('Photo de profil enregistré', 'success');
          this.timestamp = Date.now();
          this.imageSaved.emit(true);
        }
      },
      errorPhoto => {
        localStorage.getItem('activelang') === 'en'
         ? this.genericsService.confirmResponseAPI('Profile photo not taken into account', 'error')
         : this.genericsService.confirmResponseAPI('Photo de profil pas prise en compte', 'error');
        this.imageSaved.emit(false);
      });
    this.selectedFile = undefined;
  }
}
