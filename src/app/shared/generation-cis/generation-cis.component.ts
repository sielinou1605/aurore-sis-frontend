import {Component, OnInit} from '@angular/core';
import {GenericsService} from '../_services/generics.service';

@Component({
  selector: 'app-generation-cis',
  templateUrl: './generation-cis.component.html',
  styleUrls: ['./generation-cis.component.scss']
})
export class GenerationCisComponent implements OnInit {
  todaysDate: Date = new Date();
  textMessage: any;
  msgHideAndShow = false;
  tsCisData = '';
  constructor(private genericsService: GenericsService) {
    setInterval(() => {
      this.todaysDate = new Date();
    }, 1);
  }

  ngOnInit() {}

  onGenererCIS() {
    this.genericsService.getResource(`serial/generate/cis`).then((response: any) => {
      console.log(response);
      this.tsCisData = response.data;
    }).catch(err => {
      console.log(err);
    });
  }

  textMessageFunc(msgText) {
    this.textMessage = msgText + ' est copié dans le presse papier !';
    this.msgHideAndShow = true;
    setTimeout(() => {
      this.textMessage = '';
      this.msgHideAndShow = false;
    }, 3000);
  }

  copyInputMessage(inputElement) {
    console.log(inputElement);
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.textMessageFunc(this.tsCisData);
  }

  copyTodaysDate(date) {
    date.select();
    document.execCommand('copy');
    date.setSelectionRange(0, 0);
    this.textMessageFunc('Date');
  }

  copyCurrentTime(time) {
    time.select();
    document.execCommand('copy');
    time.setSelectionRange(0, 0);
    this.textMessageFunc('Time');
  }
}
