import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GenerationCisComponent} from './generation-cis.component';

describe('GenerationCisComponent', () => {
  let component: GenerationCisComponent;
  let fixture: ComponentFixture<GenerationCisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerationCisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerationCisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
