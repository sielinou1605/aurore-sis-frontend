import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {GenericsService} from '../../_services/generics.service';
import {TokenStorageService} from '../../_services/token-storage.service';
import moment from 'moment';
import {ColumnFilter, DataSelect, FilterEntity, FindEssentialEntity} from '../../_models/request-dto/common/filter-request-response-model';

@Component({
  selector: 'app-dynamic-filter',
  templateUrl: './dynamic-filter.component.html',
  styleUrls: ['./dynamic-filter.component.scss']
})
export class DynamicFilterComponent implements OnInit {
  @Output() eventFilter = new EventEmitter();
  /**
   *  attributs permettants d'effectuer le filtre sur les elements de la liste en utilisant le findEntity
   */
  filterDocForm: FormGroup;
  submitted = false;
  selectOperator = [
    new DataSelect( 'LIKE', 'contient'),
    new DataSelect( 'BETWEEN', 'est compris entre'),
    new DataSelect( 'LIKE BEGIN', 'commence par'),
    new DataSelect( 'LIKE END', 'se termine par'),
    new DataSelect( '=', 'est egal a'),
    new DataSelect( '!=', 'est different de'),
    new DataSelect( '>=', 'est superieur ou egal a'),
    new DataSelect( '>', 'est superieur a'),
    new DataSelect( '<=', 'est inferieur ou egal a'),
    new DataSelect( '<', 'est inferieur a'),
  ];
  selectOperatorEnumOrDate = [
    new DataSelect( 'BETWEEN', 'est compris entre' ),
    new DataSelect( '=', 'est egal a' ),
    new DataSelect(  '!=', 'est different de' ),
    new DataSelect( '>=', 'est superieur ou egal a' ),
    new DataSelect ( '>', 'est superieur a' ),
    new DataSelect( '<=', 'est inferieur ou egal a' ),
    new DataSelect( '<', 'est inferieur a' )
  ];

  selectOperatorBoolean = [
    new DataSelect( '=', 'est egal a' ),
    new DataSelect( '!=', 'est different de' )
  ];
  selectColumns: ColumnFilter[];
  columns: ColumnFilter[] = [];
  @Input() public trie: string;
  @Input() public windowId: number;

  currendate = moment();
// les elements du select qui vont s'afficher dynamiquement dans la section  de donnees a choisir a l'extreme droite
  elementFiltreSelect = [];

  constructor(
    private genericsService: GenericsService,
    private fb: FormBuilder,
    private tokenStorage: TokenStorageService
  ) { }

  ngOnInit() {
    this.onInitFilter();
  }

// recuperer la liste des filtre a partir d'une entity
  onGetFilterEntity(trie, windowId) {
    let url: string;
    if (trie === 'vente' || trie === 'achat' || trie === 'stock' || trie === 'interne') {
      url = `/system/filter/find/window/${windowId}/${trie}`;
    } else {
      url = `/system/filter/find/window/${windowId}`;
    }
    this.genericsService.getResource(url).then((resp: any) => {
      if (resp.code === 200) {
        this.selectColumns = resp.data;
        this.columns = resp.data;
        this.selectColumns.forEach(item => {
          if (item.ref === true) { this.arrayOfSelectTypeElement(item); }
        });
        if (this.columns.length > 0) { this.onAddLineToFilters(); }
      }
    });
  }

// Initialiser le formulaire dynamique avec formGroup et FormArray
  onInitFilter() {
    this.filterDocForm = this.fb.group({
      filterData: new FormArray([])
    });
    this.onGetFilterEntity(this.trie, this.windowId);
  }

// recuperer l'element courant de recherche selectionner sur la vue
  onGetCC(value: string): ColumnFilter {
    const resp = this.columns.filter(elt => elt.nameColumn === value);
    return resp[0];
  }

// Preparer les elements de la rubrique droite c'est a dire les valeurs a passer dans le filtre
  arrayOfSelectTypeElement(column: ColumnFilter) {
    let filter: FilterEntity;
    if (column.dataType) {
      filter = new FilterEntity('data_type', 'CHAINE', null, '=', column.dataType);
    }
    const findEntity: FindEssentialEntity =  new  FindEssentialEntity(column.refTable,  filter ? [filter] : null,
      this.tokenStorage.getUser().id, column.refKey, column.refValue);
    this.genericsService.postResource(``, findEntity).then((result: any) => {
      const elementSelect = [];
      if (result.code === 200) {
        result.data.forEach(item => elementSelect[item.id] = item.libelle);
      }
      this.elementFiltreSelect[column.nameColumn] = elementSelect;
    });
  }

// les elements du formulaire globale
  get f() { return this.filterDocForm.controls; }
// les elements du formulaire local
  get t() { return this.f.filterData as FormArray; }

  onAddLineToFilters() {
    this.onGetCC(this.columns[0].nameColumn);
    this.t.push(
      this.fb.group({
        column: [this.columns[0].nameColumn],
        operator: [this.columns[0].typeColumn === 'DATE' || this.columns[0].ref === true ? '=' : 'LIKE'],
        valueBegin: [''],
        valueEnd: ['']
      })
    );
  }

// supprimer une ligne de formulaire locale
  onDeleleLineToFilters(position) {
    this.t.removeAt(position);
  }

// reinitialiser le formulaire
  onReset() {
    this.t.clear();
    this.onInitFilter();
    this.eventFilter.emit([]);
  }

  onSubmitFilter() {
    const listFilter = [];
    this.t.controls.forEach((formGroup, index) => {
      const column = this.onGetCC(formGroup.value.column);
      const filter = new FilterEntity(formGroup.value.column,
        (column.typeColumn === 'BOOLEAN' || column.typeColumn === 'BOOLEAN_2' || column.typeColumn === 'ENTIER_2') ?
          'ENTIER' : column.typeColumn,
        (formGroup.value.operator === 'BETWEEN') ? 'AND' : null,  formGroup.value.operator,
        column.typeColumn === 'DATE' ? formGroup.value.valueBegin.format('yyyy-MM-DD') : formGroup.value.valueBegin,
        (formGroup.value.valueEnd ? (column.typeColumn === 'DATE' ? formGroup.value.valueEnd.format('yyyy-MM-DD')
          : formGroup.value.valueEnd ) : null)
      );
      listFilter.push(filter);
    });
    this.eventFilter.emit(listFilter);
    // this.onReset();
  }
}
