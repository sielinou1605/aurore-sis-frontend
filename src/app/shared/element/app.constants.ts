import { environment } from '../../../environments/environment';

export class AppConstants {

  public static BASE_URL = environment.host + location.port + '/';
  private static API_BASE_URL = environment.dev ? environment.api_base_url : AppConstants.BASE_URL;
  private static OAUTH2_URL = AppConstants.API_BASE_URL + 'oauth2/authorization/';
  private static REDIRECT_URL = '?redirect_uri=' + AppConstants.API_BASE_URL + 'login';
  public static API_URL = AppConstants.API_BASE_URL + 'api/';
  public static AUTH_API = AppConstants.API_URL + 'auth/';
  public static GOOGLE_AUTH_URL = AppConstants.OAUTH2_URL + 'google' + AppConstants.REDIRECT_URL;
  public static FACEBOOK_AUTH_URL = AppConstants.OAUTH2_URL + 'facebook' + AppConstants.REDIRECT_URL;
  public static GITHUB_AUTH_URL = AppConstants.OAUTH2_URL + 'github' + AppConstants.REDIRECT_URL;
  public static LINKEDIN_AUTH_URL = AppConstants.OAUTH2_URL + 'linkedin' + AppConstants.REDIRECT_URL;
  public static LOGO_LOGIN = environment.logo === 1 ? './assets/img/logo-blanc.png' : './assets/img/logo-blanc-debrandine.png';
  public static LOGO_SIDE = environment.logo === 1 ? './assets/img/logo-blanc.svg' : './assets/img/logo-blanc-debrandine.svg';

  /**
   * Constantes pour les messages affichables de l'application francais
   */
  public static FAIL_MODIFICATIONFR = 'Echec de Modification';
  public static KNOW_ERRORFR = 'Erreur inconnue !!!';
  public static FAIL_OPERATIONFR = 'Echec lors de l\'opération';
  public static FAIL_LOGIN_PASSFR = 'Le couple Login/Password ne correspond pas, Contacter l\'administrateur !!! ';
  public static SEARCHFR = 'Rechercher';
  public static UNAIVALAIBLEFR = 'Aucune donnée disponible';
  public static SELECT_ALLFR = 'Tout cocher';
  public static UNSELECT_ALLFR = 'Tout decocher';
  public static DESCRIPTIONFR = 'description';
  public static ALL_SERVICESFR = 'Tous les services';
  public static ALL_MODELESFR = 'Tout les modeles';
  public static UNKNOWN_LOADEDFR = 'Erreur inconnu pour le chargement des donnees';
  public static SERVICE_LOADEDFR = 'Service modifié avec succes';
  public static SERVICE_CREATEDFR = 'Service ajouté avec succes';
  public static SERVICE_DELETEFR = 'Service supprimer avec succes';
  public static SERVICE_OPTIONNEL_ERRORFR = 'Aucun nouveau service n\'as été ajouter ';
  public static SERVICE_OPTIONNEL_EXISTEFR = 'Le service sélectionné existe déjà parmi les services de cet élevé ';
  public static ELEVE_EXISTEFR = 'L\'eleve sélectionné existe déjà \n parmi les eleves de ce parent ';
  public static COMPTE_ACCES_CREATEDFR = 'Compte d\'accès ajouté avec succes ';
  public static COMPTE_ACCES_UPDATEFR = 'Compte d\'accès modifié avec succes ';
  public static COMPTE_ACCES_DELETEFR = 'Compte d\'accès supprimer avec succes ';
  public static JOURNAL_CREATEDFR = 'Journal ajouté avec succes';
  public static CAISSE_CREATEDFR = 'Caisse ajouté avec succes';
  public static REMISE_NIVEAU_UPDATEDFR = 'Remise niveau modifiée avec succes';
  public static REMISE_NIVEAU_CREATEDFR = 'Remise niveau ajoutee avec succes';
  public static REMISE_UPDATEDFR = 'Remise  modifiée avec succes';
  public static JOURNAL_UPDATEDFR = 'Journal modifiée avec succes';
  public static CAISSE_UPDATEDFR = 'Caisse modifiée avec succes';
  public static BOURSE_UPDATEDFR = 'Bourse modifiée avec succes';
  public static USER_UPDATEDFR = 'Utilisateur modifiée avec succes';
  public static ANNEE_UPDATEDFR = 'Année modifié avec succes';
  public static ENSEIGNANT_UPDATEDFR = 'Enseignant mise ajour avec succès !';
  public static SECTION_CREATEDFR = 'Section modifié avec succes';
  public static SECTION_UPDATEDFR = 'Section ajouté avec succes';
  public static NIVEAU_UPDATEDFR = 'Niveau modifié avec succes';
  public static CLASSE_UPDATEDFR = 'Classe modifié avec succes';
  public static ETABLISSEMENT_UPDATEDFR = 'Etablissement modifié avec succes';
  public static DEPARTEMENT_UPDATEDFR = 'Département Ajouté avec succes';
  public static MODELE_ECHEANCE_UPDATEDFR = 'Modele écheance modifié avec succes';
  public static CYCLE_UPDATEDFR = 'Cycle modifié avec succes';
  public static INSTITUTION_UPDATEDFR = 'L\'institution à été mise a jour avec succes';
  public static LOGO_UPLOAD_FAILEDFR = 'Le logo de l\'institution n\'a pas été envoyé reéssayer a nouveau';
  public static REMISE_CREATEDFR = 'Remise ajoutée avec succes';
  public static BOURSE_CREATEDFR = 'Bourse ajoutée avec succes';
  public static ANNEE_CREATEDFR = 'Annee ajouté avec succes';
  public static MODE_REGLEMENT_CREATEDFR = 'Mode reglement ajoutee avec succes';
  public static NIVEAU_CREATEDFR = 'Niveau ajouté avec succes';
  public static CLASSE_CREATEDFR = 'Classe ajouté avec succes';
  public static ETABLISSEMENT_CREATEDFR = 'Etablissement ajouté avec succes';
  public static DICIPLINE_CREATED = 'Discipline ajouté avec succès';
  public static DICIPLINE_CREATEDEN = ' Discipline successfully added';
  public static DICIPLINE_UPDATE = 'Dicipline modifié avec succes';
  public static CYCLE_CREATEDFR = 'Cycle ajouté avec succes';
  public static CYCLE_WITH_NIVEAUXFR = 'Ce cycle comporte des niveaux !!!';
  public static UNKNOWN_ERRORFR = 'Erreur inconnu pour le chargement des utilisateurs';
  public static INTERNAL_SERVER_ERRORFR = 'Error sur le serveur, contacter l\'administrateur';
  public static USER_SAVEDFR = 'Utilisateur enregistré avec succes';
  public static SYNC_SUCCESSFR = 'Synchronisation terminée';
  public static SYNC_INFOFR = 'Le serveur central traite les données, vous serez notifié quand l\'opération sera terminée';
  public static SYNC_FAILEDFR = 'Echec de synchronisation';
  public static SYNC_ENCOURSFR = 'Synchronisation en cours ...';
  public static UPDATE_PENALITE_FAILEDFR = 'Echec de mise a jour du type pénalité';
  public static CREATE_PENALITE_FAILEDFR = 'Echec d\'ajout du type pénalité';
  public static UPDATE_NUMEROTATION_FAILFR = 'Echec de mise a jour de la numerotation';
  public static UPDATE_VAR_GLOBALE_FAILFR = 'Echec de mise a jour de la variable globale';
  public static ARE_U_SUREFR = 'Etes-vous sure?';
  public static CANCELLING_SEQUENCEFR = 'Annulation de sequence?';
  public static IRREVERSIBLEFR = 'Cette opération est irreversible !';
  public static AJFR = 'Toutes les notes de la sequence auront pour motif Absence justifiee';
  public static YES_DELETE_ITFR = 'Oui, Supprimer!';
  public static YES_LOCK_FR = 'Oui, Bloquer !';
  public static YES_UN_LOCK_FR = 'Oui, Débloquer !';
  public static GENERATE_ITFR = 'Oui, génerer!';
  public static RECONDUIRE_ITFR = 'Oui, Reconduire!';
  public static YES_CANCEL_ITFR = 'Oui, Annuler!';
  public static YES_CHANGE_ITFR = 'Oui, Changer!';
  public static NO_DONT_CHANGE_ITFR = 'Non, Ne changer pas !';
  public static BON_A_SAVOIRFR = 'Bon a savoir ...';
  public static DELETEDFR = 'Deleted!';
  public static DELETE_SUCCESSFR = 'Suppression reussie';
  public static NO_DELETEDFR = 'No Deleted!';
  public static DELETE_FAILFR = 'Suppression échouée !!!';
  public static CAISSIERFR = 'Caissier';
  public static ADMINISTRATEURFR = 'Administrateur';
  public static ROLE_COMPTABLEFR = 'ROLE_COMPTABLE';
  public static WAITINGFR = 'Veuillez patienter';
  public static ARCHIVED_ERRORFR = 'Erreur d\'archivage';
  public static DEFAULT_STATUTFR = 'célibatiare';
  public static DEFAULT_TYPE_PERSONNELFR = 'enseignant';
  public static DEFAULT_STATUT_PROFFR = 'permanent';
  public static DEFAULT_BAREME_NOTEFR = '20';
  public static DEFAULT_COEF_MATIEREFR = '1';
  public static DEFAULT_DAY_OF_WEEKFR = 'LUNDI';
  public static PROFIL_IMAGE_SAVEDFR = 'Photo de profil enregistré';
  public static PROFILE_IMAGE_FAILEDFR = 'Photo de profil pas prise en compte';

  public static PROPAGER_MODIFFR = 'Propager la modification ?';
  public static PROPAGER_MODIF_DETAILFR = 'Voulez vous propager la modification sur les paiements déjà éffectué ?';
  public static PROPAGER_MODIF_CONFIRMFR = 'Oui, Propager!';
  public static PROPAGER_MODIF_OKFR = 'Modele echeance ajouté avec succes';
  public static ANOTHER_CAISSE_EXISTFR = 'Il existe déjà une caisse portant ce nom ';

  public static CANNOT_CHANGEFR = 'Vous ne pouvez pas basculer de ';
  public static PARTIAL_CURRENT_YEARFR = ' alors que l\'année courante est ';
  public static ERROR_FORMFR = 'Erreur dans le formulaire';
  public static ERROR_FORM_EMAIL_EXISTFR = 'Erreur dans le formulaire';
  public static ERROR_FORM_TELEPHONEFR = 'Le numéro de téléphone doit comporter exactement 09 chiffres, commençant par 6 et sans espace.';
  public static ERROR_LOADED_PRIVILEGEFR = 'Erreur inconnu pour le chargement des privilège';
  public static ERROR_CHANGE_YEARFR = 'Erreur de changement d\'année';
  public static AUCUNE_MODIF_APPORTEFR = 'Aucune modification apporté a votre systeme !';
  public static CHANGE_SUCCESSFR = 'Le changement de l\'année  éffectué avec succès.';
  public static CHANGE_ERRORFR = 'Une erreur s\'est produite lors du changement de l\'année scolaire.';
  public static FAILFR = 'Echec !';
  public static CANCELFR = 'Annuler !';
  public static TERMINATEDFR = 'Terminée !';
  public static LOADING_CLASSEFR = 'Chargement des classes !';
  public static ACTIONS_TRANSFEREFR = 'Actions transférer avec succès';

  public static SORT_BY_NOM_CLASSEFR = 'Par nom de classe';
  public static SORT_BY_NOM_NIVEAUFR = 'Par nom de niveau';
  public static SORT_BY_NIVEAUFR = 'Par niveau';
  public static SORT_BY_NOM_CLASSE_VALUEFR = 'nomClasse';
  public static SORT_BY_NIVEAU_VALUEFR = 'niveau';
  public static SORT_BY_NOM_NIVEAU_VALUEFR = 'nomNiveau';

  public static TITLE_ACCESS_PAGE_FAILFR = 'Oops...';
  public static CONTENT_ACCESS_PAGE_FAILFR = 'Vous n\'etes pas autorisé a acceder a cette page !';
  public static RETURN_MESSAGEFR = '<a routerLink="/welcome">Revenier a l\'accueil?</a>';
  public static JOURS_SEMAINEFR = [
    { id: 'LUNDI', valeur: 'Lundi' },
    { id: 'MARDI', valeur: 'Mardi' },
    { id: 'MERCREDI', valeur: 'Mercredi' },
    { id: 'JEUDI', valeur: 'Jeudi' },
    { id: 'VENDREDI', valeur: 'Vendredi' },
    { id: 'SAMEDI', valeur: 'Samedi' },
    { id: 'DIMANCHE', valeur: 'Dimanche' }
  ];
  public static HTML_NOTIFICATION_TRANSFERTFR = '<ul>\n' +
    '  <li>Toutes les données de l\'année fermer seront juste accessible en lecture simple !</li>\n' +
    '  <li>De nouvelle configuration seront faites pour la nouvelle année crée ! </li>\n' +
    '  <li>Vous allez programmer a nouveau les enseignants dans de nouvelles classes</li>\n' +
    '</ul>';
  // non translate
  public static CONTENT_FAIL_CONFIG_INSCIPTIONFR
    = 'la configuration actuel des services de cette classe ne vous permet pas de faire des inscriptionns !!! ';
  public static TITLE_CONFIG_INSCIPTIONFR = 'configuration incomplete pour cette classe !!!';
  public static CONTENT_FAIL_CONFIG_MIN_INSCIPTIONFR = 'Aucun montant minimal n\'as ete definit pour cette classe !!! ';
  public static TITLE_FAIL_INSCIPTIONFR = 'Echec d\'inscription';

  /**
   * Constantes pour les messages affichables de l'application anglais
   */
  public static FAIL_MODIFICATIONEN = 'Modification failure';
  public static KNOW_ERROREN = 'Unknown error !!!';
  public static FAIL_OPERATIONEN = 'Failure during the operation';
  public static FAIL_LOGIN_PASSEN = 'The couple Login/Password does not match, Contact the administrator !!! ';
  public static SEARCHEN = 'Search';
  public static UNAIVALAIBLEEN = 'No data available';
  public static SELECT_ALLEN = 'Check all';
  public static UNSELECT_ALLEN = 'Uncheck all';
  public static DESCRIPTIONEN = 'description';
  public static ALL_SERVICESEN = 'All services';
  public static ALL_MODELESEN = 'All models';
  public static UNKNOWN_LOADEDEN = 'Unknown error for data loading';
  public static SERVICE_LOADEDEN = 'Successfully modified service';
  public static SERVICE_CREATEDEN = 'Successfully added service';
  public static SERVICE_DELETEEN = 'Successfully removed service';
  public static SERVICE_OPTIONNEL_ERROREN = 'No new services have been added ';
  public static SERVICE_OPTIONNEL_EXISTEEN = 'The selected service already exists among the services of this high ';
  public static ELEVE_EXISTEEN = 'The selected student already exists among the students of this parent ';
  public static COMPTE_ACCES_CREATEDEN = 'Access account added with success ';
  public static COMPTE_ACCES_UPDATEEN = 'Successfully modified access account ';
  public static COMPTE_ACCES_DELETEEN = 'Access account delete with success ';
  public static JOURNAL_CREATEDEN = 'Successfully added journal';
  public static CAISSE_CREATEDEN = 'Case added with success';
  public static REMISE_NIVEAU_UPDATEDEN = 'Modified upgrade with success';
  public static REMISE_NIVEAU_CREATEDEN = 'Upgrade successfully added';
  public static REMISE_UPDATEDEN = 'Modified discount with success';
  public static JOURNAL_UPDATEDEN = 'Successfully modified journal';
  public static CAISSE_UPDATEDEN = 'Successfully modified case';
  public static BOURSE_UPDATEDEN = 'Successfully modified stock exchange';
  public static USER_UPDATEDEN = 'User modified with success';
  public static ANNEE_UPDATEDEN = 'Year modified with success';
  public static ENSEIGNANT_UPDATEDEN = 'Teacher successfully adjourned !';
  public static SECTION_UPDATEDEN = 'Section successfully modified';
  public static NIVEAU_UPDATEDEN = 'Successfully modified level';
  public static CLASSE_UPDATEDEN = 'Class successfully modified';
  public static ETABLISSEMENT_UPDATEDEN = 'Successfully modified establishment';
  public static DEPARTEMENT_UPDATEDEN = 'Department successfully modified';
  public static MODELE_ECHEANCE_UPDATEDEN = 'Successfully modified scale model';
  public static CYCLE_UPDATEDEN = 'Successfully modified cycle';
  public static INSTITUTION_UPDATEDEN = 'The institution has been successfully updated';
  public static LOGO_UPLOAD_FAILEDEN = 'The logo of the institution has not been sent again';
  public static REMISE_CREATEDEN = 'Discount added with success';
  public static BOURSE_CREATEDEN = 'Successfully added stock exchange';
  public static ANNEE_CREATEDEN = 'Year added with success';
  public static MODE_REGLEMENT_CREATEDEN = 'Settlement mode successfully added';
  public static NIVEAU_CREATEDEN = 'Level added with success';
  public static CLASSE_CREATEDEN = 'Class added with success';
  public static ETABLISSEMENT_CREATEDEN = 'Establishment added with success';
  public static CYCLE_CREATEDEN = 'Cycle added with success';
  public static CYCLE_WITH_NIVEAUXEN = 'This cycle has levels !!!';
  public static UNKNOWN_ERROREN = 'Unknown error for user loading';
  public static INTERNAL_SERVER_ERROREN = 'Error on the server, contact the administrator';
  public static USER_SAVEDEN = 'Successful registered user';
  public static SYNC_SUCCESSEN = 'Synchronization completed';
  public static SYNC_INFOEN = 'The central server processes the data, you will be notified when the operation is completed';
  public static SYNC_FAILEDEN = 'Synchronization failure';
  public static SYNC_ENCOURSEN = 'Synchronization in progress';
  public static UPDATE_PENALITE_FAILEDEN = 'Penalty type update failure';
  public static CREATE_PENALITE_FAILEDEN = 'Failed to add penalty type';
  public static UPDATE_NUMEROTATION_FAILEN = 'Numbering update failure';
  public static UPDATE_VAR_GLOBALE_FAILEN = 'Failure to update the global variable';
  public static ARE_U_SUREEN = 'Are you sure?';
  public static CANCELLING_SEQUENCEEN = 'Sequence cancellation?';
  public static IRREVERSIBLEEN = 'This operation is irreversible !';
  public static AJEN = 'All the notes of the sequence will have the reason Absence justified';
  public static YES_DELETE_ITEN = 'Yes, Delete !';
  public static YES_LOCK_EN = 'Yes, Lock !';
  public static YES_UN_LOCK_EN = 'Yes, Unlock !';
  public static GENERATE_ITEN = 'Renew';
  public static RECONDUIRE_ITEN = 'generate';
  public static YES_CANCEL_ITEN = 'Yes, Cancel !';
  public static YES_CHANGE_ITEN = 'Yes, Change !';
  public static NO_DONT_CHANGE_ITEN = 'No, don\'t change !';
  public static BON_A_SAVOIREN = 'Good to know ...';
  public static DELETEDEN = 'Deleted !';
  public static DELETE_SUCCESSEN = 'Successful deletion';
  public static NO_DELETEDEN = 'No Deleted !';
  public static DELETE_FAILEN = 'Deletion failed !!!';
  public static CAISSIEREN = 'Cashier';
  public static ADMINISTRATEUREN = 'Administrator';
  public static ROLE_COMPTABLEEN = 'ACCOUNTABLE_ROLE';
  public static WAITINGEN = 'Please wait';
  public static ARCHIVED_ERROREN = 'Archiving error';
  public static DEFAULT_STATUTEN = 'single';
  public static DEFAULT_TYPE_PERSONNELEN = 'teacher';
  public static DEFAULT_STATUT_PROFEN = 'permanent';
  public static DEFAULT_BAREME_NOTEEN = '20';
  public static DEFAULT_COEF_MATIEREEN = '1';
  public static DEFAULT_DAY_OF_WEEKEN = 'MONDAY';
  public static PROFIL_IMAGE_SAVEDEN = 'Registered profile picture';
  public static PROFILE_IMAGE_FAILEDEN = 'Profile picture not taken into account';

  public static PROPAGER_MODIFEN = 'Spreading the change ?';
  public static PROPAGER_MODIF_DETAILEN = 'Do you want to propagate the change on the payments already made ?';
  public static PROPAGER_MODIF_CONFIRMEN = 'Yes, propagate !';
  public static PROPAGER_MODIF_OKEN = 'Model maturity added with success';
  public static ANOTHER_CAISSE_EXISTEN = 'There is already a fund with this name ';

  public static CANNOT_CHANGEEN = 'You cannot switch from ';
  public static PARTIAL_CURRENT_YEAREN = 'while the current year is';
  public static ERROR_FORMEN = 'Error in the form';
  public static ERROR_FORM_EMAIL_EXISTEN = 'Error in the form';
  public static ERROR_FORM_TELEPHONEEN = 'The telephone number must have exactly 09 digits, starting with 6 and without spaces.';
  public static ERROR_LOADED_PRIVILEGEEN = 'Unknown error for privilege loading';
  public static ERROR_CHANGE_YEAREN = 'Year change error';
  public static AUCUNE_MODIF_APPORTEEN = 'No changes to your system !';
  public static CHANGE_SUCCESSEN = 'The change of the year successfully completed.';
  public static CHANGE_ERROREN = 'An error occurred when the school year was changed.';
  public static FAILEN = 'Failure !';
  public static CANCELEN = 'Cancel !';
  public static TERMINATEDEN = 'Completed !';
  public static LOADING_CLASSEEN = 'Loading classes !';
  public static ACTIONS_TRANSFEREEN = 'Actions successfully transferred';

  public static SORT_BY_NOM_CLASSEEN = 'By class name';
  public static SORT_BY_NOM_NIVEAUEN = 'By level name';
  public static SORT_BY_NIVEAUEN = 'By level';
  public static SORT_BY_NOM_CLASSE_VALUEEN = 'classname';
  public static SORT_BY_NIVEAU_VALUEEN = 'level';
  public static SORT_BY_NOM_NIVEAU_VALUEEN = 'levelName';

  public static TITLE_ACCESS_PAGE_FAILEN = 'Oops...';
  public static CONTENT_ACCESS_PAGE_FAILEN = 'You are not authorized to access this page !';
  public static RETURN_MESSAGEEN = '<a routerLink="/welcome">Return to home page?</a>';
  public static JOURS_SEMAINEEN = [
    { id: 'LUNDI', valeur: 'Monday' },
    { id: 'MARDI', valeur: 'Tuesday' },
    { id: 'MERCREDI', valeur: 'Wednesday' },
    { id: 'JEUDI', valeur: 'Thursday' },
    { id: 'VENDREDI', valeur: 'Friday' },
    { id: 'SAMEDI', valeur: 'Saturday' },
    { id: 'DIMANCHE', valeur: 'Sunday' }
  ];
  public static HTML_NOTIFICATION_TRANSFERTEN = '<ul>\n' +
    '  <li>All the data of the closed year will just be accessible in simple reading !</li>\n' +
    '  <li>New configurations will be made for the new year ! </li>\n' +
    '  <li>You will be re-programming teachers into new classrooms</li>\n' +
    '</ul>';

  // non translate
  public static CONTENT_FAIL_CONFIG_INSCIPTIONEN =
    'la configuration actuel des services de cette classe ne vous permet pas de faire des inscriptionns !!! ';
  public static TITLE_CONFIG_INSCIPTIONEN = 'configuration incomplete pour cette classe !!!';
  public static CONTENT_FAIL_CONFIG_MIN_INSCIPTIONEN = 'Aucun montant minimal n\'as ete definit pour cette classe !!! ';
  public static TITLE_FAIL_INSCIPTIONEN = 'Echec d\'inscription';

}
