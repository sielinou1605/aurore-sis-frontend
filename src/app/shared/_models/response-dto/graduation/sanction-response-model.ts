export class SanctionResponseModel {
  constructor(
    public id: number
  ) {
  }
}

export class GenericsObjectSanctionFilter {
  constructor(
    public cle: string,
    public valeur: string
  ) {
  }
}
