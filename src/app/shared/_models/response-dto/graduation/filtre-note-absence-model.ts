export class Sequence {
  constructor(
    public id: number,
    public nomSequence: string,
    public nomSequence_2: string,
    public numeroOrdre: number,
    public dateOuverture: string,
    public dateFermeture: string,
    public enCours: boolean,
    public idTrimestre: number,
    public nomTrimestre: string,
    public idAnnee: number
  ) {
  }
}

export class Classe {
  constructor(
    public id: number,
    public nomClasse: string,
    public serie: string,
    public optionClasse: string,
    public specialite: string,
    public idTitulaire: number
  ) {}
}

export class MatiereClasse {
  constructor(
    public matiereClasseId: number,
    public matiereClasseLabel: string
  ) {}
}

export class Absence {
  constructor(
    public id_absence: number,
    public date_absence: string,
    public nbre_heures_j: number,
    public nbre_heures_nj: number
  ) {
  }
}
export class Eleve {
  constructor(
    public active: boolean,
    public date_admission: string,
    public date_naissance: string,
    public emailParent: string,
    public exclu: boolean,
    public handicape: boolean,
    public inscrit: boolean,
    public lieu_naissance: string,
    public matricule: string,
    public nationalite: string,
    public nomEleve: string,
    public nomMere: string,
    public nomParent: string,
    public numero_eleve: string,
    public photo: string,
    public prenomEleve: string,
    public religion: string,
    public residence_eleve: string,
    public residence_parent: string,
    public sexe: string,
    public statut: string,
    public telephone_Mere: string,
    public telephonePere: string
  ) {
  }
}

export class FiltreNoteModel {
  constructor(
    public sequences: Sequence[],
    public matiereClasses: MatiereClasse[]
  ) {}
}

export class FiltreAbsenceModel {
  constructor(
    public sequences: Sequence[],
    public classes: Classe[]
  ) {}
}

export class EleveAbsenceResponse {
  constructor(
    public sequence: Sequence,
    public eleve: Eleve,
    public classe: Classe,
    public absences: Absence[]
  ) {
  }
}

export class EleveNoteResponse {
  constructor(
    public matricule: string,
    public nomEleve: string,
    public noteAtt: number,
    public observation: string,
    public idNote: number,
    public canUpdate: boolean
  ) {}
}
