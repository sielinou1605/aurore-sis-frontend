export class MatiereClasseModel {
  constructor(
    public baremeEvaluation: number,
    public coefficient: number,
    public idAnnee: number,
    public idClasse: number,
    public idMatiere: number,
    public idMatiereClasse: number,
    public nomClasse: string,
    public nomMatiere: string,
    public nomMatiere2: string,
    public quotaHoraire: number
  ) {
  }
}

export class ProgrammationModel {
  constructor(
    public heureDebut: string,
    public heureFin: string,
    public jourSemaine: string,
    public codeAnnee: string,
    public id: number,
    public idAnnee: number,
    public idMatiereclasse: number
  ) {
  }
}

export class ItemProgrammationModel {
  constructor(
    public matiereClasse: MatiereClasseModel,
    public programmations: ProgrammationModel[]
  ) {
  }
}
