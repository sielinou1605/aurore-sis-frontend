export class ModuleResponse {
  constructor(
    public id: number,
    public nomModule: string,
    public description: string
  ) {
  }
}

export class ActionResponse {
  constructor(
    public  description: string,
    public id: number,
    public  idObjet: string,
    public libelleAction: string,
    public typeObjet: number,
    public conditionActif: string,
    public  conditionEditable: string,
    public conditionVisible: string,
    public  idActionSysteme: number,
    public  idFenetre: number,
    public idRole: number,
    public idRoleAction: number,
    public libelleFenetre: string,
    public nomRole: string
  ) {
  }
}

export class FenetreResponse {
  constructor(
    public description: string,
    public fenetreEditable: number,
    public fenetreFiltre: number,
    public fenetreImpression: number,
    public id: number,
    public idModule: number,
    public idObjet: number,
    public libelleFenetre: string,
    public url: string
  ) {
  }
}
export class DataFenetreResponse {
  constructor(
    public fenetre: FenetreResponse,
    public actions: ActionResponse[]
  ) {
  }
}
export class DataModuleResponseModel {
  constructor(
    public module: ModuleResponse,
    public fenetres: DataFenetreResponse[]
  ) {
  }
}

export interface HistoriqueEleveNoteResponse {
   id: number;
  matriculeEleve: string;
  nomsEleve: string;
  idClasseEleve: number;
  nomClasseEleve: string;
  idMatiereClasse: number;
  idMatiere: number;
  nomMatiere: string;
  notePrecedent: number;
  noteActuel: number;
  idUtilisateurImplique: number;
  nomUtilisateurImplique: string;
  idSequence: number;
  nomSequence: string;
  idAnnee: number;
  codeAnnee: string;
  dateOperation: string;
}
