export class ServiceModel {
  constructor(
    public id: number,
    public nomService: string,
    public description: string,
    public obligatoire: string,
    public suspendu: number,
    public priorite: number,
    // section audit
    public date_creation: string,
    public date_modif: string,
    public forwarded: string,
    public util_creation: string,
    public util_modif: string,
    public version: 0
  ) {
  }
}
