import {NiveauModel} from '../common/niveau-model';

export class RemiseNiveauModel {
  constructor(
    public anneeAcademique: number,
    public date_creation: string,
    public date_modif: string,
    public echeanceApplication: string,
    public forwarded: string,
    public globale: string,
    public id: number,
    public idSource: number,
    public matricule: string,
    public modaliteApplication: number,
    public montant_rn: number,
    public nom_rn: string,
    public sourceRemise: number,
    public type_rn: number,
    public util_creation: string,
    public util_modif: string,
    public version: number,
    public niveau?: NiveauModel,
    public service?: any,
    public remise_globale?: any
  ) {
  }
}
