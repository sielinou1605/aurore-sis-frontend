export class JournalModel {
  constructor(
    public id: number,
    public code_jrnl: string,
    public designation_jrnl: string,
    public type_jrnl: string,
    // section audit
    public date_creation: string,
    public date_modif: string,
    public forwarded: string,
    public util_creation: string,
    public util_modif: string,
    public version: 0
  ) {
  }
}
export class JournalFinanceModel {
  constructor(
    public id: number,
    public code_jrnl: string,
    public designation_jrnl: string,
    public type_jrnl: string,
    // section audit
    public date_creation: string,
    public date_modif: string,
    public forwarded: string,
    public util_creation: string,
    public util_modif: string,
    public version: 0
  ) {
  }
}

