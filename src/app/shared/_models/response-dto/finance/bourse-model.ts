export class BourseModel {
  constructor(
    public id: number,
    public anneeAcademique: number,
    public modalite_application: number,
    public montant_bourse: number,
    public intitule_bourse: string,
    public motif_bourse: string,
    public date_creation: Date,
    public date_modif: Date,
    public forwarded: string,
    public util_creation: string,
    public util_modif: string,
    public version: number,
    public service: any
  ) {
  }
}
