export class ModeleEcheancierModel {
  // a completer plutard
  constructor(
    public id: number,
    public intitule: string,
    public anneeAcademique: number,
    public montant_a_ech: number,
    public date_ech: string,
    public nom_ech: string,
    public niveau: any,
    public service: any,
    public autoriserRemiseAutres: number,
    public autoriserRemiseBourse: number,
    public autoriserRemiseDispense: number,
    // section audit
    public date_creation: string,
    public date_modif: string,
    public forwarded: string,
    public util_creation: string,
    public util_modif: string,
    public version: 0
  ) {
  }
}
