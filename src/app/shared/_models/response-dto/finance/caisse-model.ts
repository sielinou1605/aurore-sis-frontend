export class CaisseModel {
  constructor(
    public id: number,
    public nomCaisse: string,
    public description_caisse: string,
    // section audit
    public date_creation: string,
    public date_modif: string,
    public forwarded: string,
    public util_creation: string,
    public util_modif: string,
    public version: 0
  ) {
  }
}
