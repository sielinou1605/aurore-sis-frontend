export class RemiseModel {
  constructor(
    public id: number,
    public type_rg: number,
    public nom_modele: string,
    public montant_rg: number,
    public nom_rg: string,
    // section audit
    public date_creation: string,
    public date_modif: string,
    public forwarded: string,
    public util_creation: string,
    public util_modif: string,
    public version: 0
  ) {
  }
}
