export class ModuleModel {
  constructor(
    public nomModule: string,
    public description: string,
    public version?: number,
    public  date_creation?: string,
    public date_modif?: string,
    public util_creation?: string,
    public  util_modif?: string,
    public forwarded?: string,
    public id?: number,
    public fenetres?: FenetreModel[]
  ) {
  }
}

export class FenetreModel {
  constructor(
    public description: string,
    public fenetreEditable: number,
    public  fenetreFiltre: number,
    public fenetreImpression: number,
    public  idObject: string,
    public idModule: number,
    public id: number,
    public libelleFenetre: string,
    public url: string,
    public actions: ActionModel[]
  ) {}
}

export class ActionModel {
  constructor(
    public description: string,
    public id: number,
    public idObjet: string,
    public libelleAction: string,
    public typeObjet: number
  ) {
  }
}
