export class ClasseStatsResponseModel {
  constructor(
    public detteTotale: number,
    public idClasse: number,
    public montantAttendu: number,
    public montantRecu: number,
    public nbTotalEleves: number,
    public nbTotalElevesFilles: number,
    public  nbTotalElevesGarcons: number,
    public nomClasse: string,
    public bonusOffert: number
  ) {
  }
}
