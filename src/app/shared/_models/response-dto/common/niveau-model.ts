export class NiveauModel {
  constructor(
    public date_creation: string,
    public date_modif: string,
    public forwarded: string,
    public id: number,
    public nomNiveau: string,
    public numeroOrdre: number,
    public util_creation: string,
    public util_modif: string
  ) {}
}
