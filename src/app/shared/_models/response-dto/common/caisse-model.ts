export class CaisseModelDto {
  constructor(
    public descriptionCaisse: string,
    public id: number,
    public nomCaisse: string
  ) {
  }
}
