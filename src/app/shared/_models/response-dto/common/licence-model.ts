export class LicenceModel {
  constructor(
    public cas: string,
    public dateDebut: string,
    public dateFin: string,
    public dateEnreg: string,
    public statut: string
  ) {
  }
}
