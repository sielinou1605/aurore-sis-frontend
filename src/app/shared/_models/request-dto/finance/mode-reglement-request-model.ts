export class ModeReglementRequestModel {
  constructor(
    public id: number,
    public libelle: string,
    public description: string
  ) {
  }
}
