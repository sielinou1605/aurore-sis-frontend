export class InscriptionRequestModel {
  constructor(
    public dateNaissance: string,
    public emailParent: string,
    public handicape: boolean,
    public idAnneeAcademique: number,
    public idClasse: number,
    public lieuNaissance: string,
    public matricule: string,
    public nationalite: string,
    public nomEleve: string,
    public nomParent: string,
    public numeroEleve: number,
    public photo: string,
    public prenomEleve: string,
    public religion: string,
    public residenceEleve: string,
    public residenceParent: string,
    public sexe: string,
    public statut: string,
    public telephoneParent: string,
    public transfert: boolean,
    public services?: ServiceInscriptionRequestModel[],
    public detailsTransfert?: DetailTransfertInscriptionRequestModel,
    public  telephoneEleve?: string,
    public emailEleve?: string,
    public nomMere?: string,
    public telephoneMere?: string
  ) {}
}

export class UpdateInscriptionModel {
  constructor(
    public dateNaissance: string,
    public emailParent: string,
    public handicape: boolean,
    public idClasseDestination: number,
    public lieuNaissance: string,
    public matricule: string,
    public motifTransfert: string,
    public nationalite: string,
    public nomEleve: string,
    public nomParent: string,
    public prenomEleve: string,
    public religion: string,
    public residenceEleve: string,
    public residenceParent: string,
    public services: ServiceInscriptionRequestModel[],
    public sexe: string,
    public telephoneParent: string,
    public nomMere: string,
    public telephoneMere: string
  ) {
  }
}

export class ServiceInscriptionRequestModel {
  constructor(
    public dateEcheance: string,
    public idModeleEcheancier: number,
    public montant: number
  ) {}
}

export class DetailTransfertInscriptionRequestModel {
  constructor(
    public codeEtabProvenance: string,
    public idNiveau: number,
    public montantRemise: number,
    public motifTransfert: string,
    public nomEtabProvenance: string,
    public villeEtabProvenance: string
  ) {}
}


