// tslint:disable-next-line:class-name
export class acompteRequestModel {

  constructor(
    public dateAdmission: string,
    public dateNaissance: string,
    public eleveTransferer: boolean,
    public emailEleve: string,
    public emailParent: string,
    public handicape: number,
    public id: number,
    public idClasse: number,
    public lieuNaissance: string,
    public matricule: string,
    public montantVerse: number,
    public nationalite: string,
    public nomEleve: string,
    public nomMere: string,
    public nomParent: string,
    public numeroEleve: number,
    public photo: string,
    public prenomEleve: string,
    public religion: string,
    public residenceEleve: string,
    public residenceParent: string,
    public sexe: string,
    public statut: string,
    public telephoneEleve: string,
    public telephoneMere: string,
    public telephoneParent: string,
    public telephonePere: string,
    public transfert: boolean
  ) {

  }
}
// tslint:disable-next-line:class-name
export class acompteListeRequestModel {

  constructor(
    public dateAdmission: string,
    public dateNaissance: string,
    public eleveTransferer: boolean,
    public emailEleve: string,
    public emailParent: string,
    public handicape: boolean,
    public id: number,
    public idClasse: number,
    public lieuNaissance: string,
    public matricule: string,
    public montantVerse: number,
    public nationalite: string,
    public nomEleve: string,
    public nomMere: string,
    public nomParent: string,
    public numeroEleve: number,
    public photo: string,
    public prenomEleve: string,
    public religion: string,
    public residenceEleve: string,
    public residenceParent: string,
    public sexe: string,
    public statut: string,
    public statutAccompte: boolean,
    public telephoneEleve: string,
    public telephoneMere: string,
    public telephoneParent: string,
    public telephonePere: string,
    public transfert: boolean
  ) {

  }
}
// tslint:disable-next-line:class-name
export class AddAcompteRequestModel {

  constructor(
    public dateAdmission: string,
    public dateNaissance: string,
    public eleveTransferer: boolean,
    public emailEleve: string,
    public emailParent: string,
    public handicape: boolean,
    public id: number,
    public idClasse: number,
    public lieuNaissance: string,
    public matricule: string,
    public montantVerse: number,
    public nationalite: string,
    public nomEleve: string,
    public nomMere: string,
    public nomParent: string,
    public numeroEleve: number,
    public photo: string,
    public prenomEleve: string,
    public religion: string,
    public residenceEleve: string,
    public residenceParent: string,
    public sexe: string,
    public statut: string,
    public telephoneEleve: string,
    public telephoneMere: string,
    public telephoneParent: string,
    public telephonePere: string,
    public transfert: boolean
  ) {

  }
}
// tslint:disable-next-line:class-name
export class acompteResponseModel {

  constructor(
    public code: number,
    public data: any,
    public message: string,
    public success: boolean,
    public timestamp: Date
  ) {

  }
}
