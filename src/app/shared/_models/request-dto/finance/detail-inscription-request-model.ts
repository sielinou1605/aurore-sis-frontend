export class DetailInscriptionRequestModel {
  constructor(
    public dateSouscription: string,
    public id: number,
    public idInscription: number,
    public idService: number,
    public montantIns: number,
    public montantRemise: number,
    public nomService: string,
    public quantite: number,
    public typeRemise: number
  ) {}
}
