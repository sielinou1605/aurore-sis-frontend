export class CaisseRequestModel {
  constructor(
    public id: number,
    public nomCaisse: string,
    public descriptionCaisse: string,
  ) {}
}
