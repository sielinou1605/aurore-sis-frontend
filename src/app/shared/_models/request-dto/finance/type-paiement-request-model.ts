export class TypePaiementRequestModel {
  constructor(
    public code: string,
    public description: string,
    public id: number,
    public nom_type: string,
  ) {
  }
}
