export class ServiceRequestModel {
  constructor(
    public description: string,
    public id: number,
    public nomService: string,
    public obligatoire: string,
    public priorite: number,
    public suspendu: number,
  ) {
  }
}
