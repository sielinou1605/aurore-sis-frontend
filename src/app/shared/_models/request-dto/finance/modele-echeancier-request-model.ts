export class ModeleEcheancierRequestModel {
  constructor(
    public anneeAcademique: number,
    public dateEch: string,
    public id: number,
    public idNiveau: number,
    public idService: number,
    public intitule: string,
    public montant: number,
    public nomEch: string,
    public autoriserRemiseAutres: number,
    public autoriserRemiseBourse: number,
    public autoriserRemiseDispense: number,
  ) {}
}
