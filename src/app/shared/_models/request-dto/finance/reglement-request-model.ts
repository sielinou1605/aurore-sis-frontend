export class ReglementRequestModel {
  constructor(
    public codeAnnee: number,
    public idCaisse: number,
    public idModeReglement: number,
    public matricule: string,
    public montant: number,
    public ventilationAuto: boolean,
    public idEchancier?: number
  ) {}
}
