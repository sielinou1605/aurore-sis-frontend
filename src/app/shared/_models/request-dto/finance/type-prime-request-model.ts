export class TypePrimeRequestModel {
  constructor(
    public description: string,
    public id: number,
    public montant_maximal: number,
    public nom_type: string,
  ) {
  }
}
