export class JournalRequestModel {
  constructor(
    public code_jrnl: string,
    public designation_jrnl: string,
    public id: number,
    public type_jrnl: string,
  ) {
  }
}
