export class EcheancierRequestModel {
  constructor(
    public anneAcademique: number,
    public date_ech: string,
    public detail_inscription: string,
    public id: number,
    public idClasse: number,
    public idCycle: number,
    public idModeleEcheancier: number,
    public idNiveau: number,
    public idSection: number,
    public langue: string,
    public matricule: string,
    public montant_ech: number,
    public nom_classe: string,
    public nom_cycle: string,
    public nom_niveau: string,
    public nom_section: string,
    public numero_ordre_classe: number,
    public numero_ordre_cycle: number,
    public numero_ordre_niveau: number,
    public solde_ech: number,
    public statut_ech: string

  ) {}
}
