export class SituationFinanceEleveModel {
  constructor(
    public codeAnnee: number,
    public dateAdmissionDebut: string,
    public dateAdmissionFin: string,
    public idEntite: number,
    public nomEntite: string,
    public predicat: string,
    public type: string
  ) {
  }
}
