export class RemiseRequestModel {
  constructor(
    public id: number,
    public montantRg: number,
    public nomModele: string,
    public nomRg: string,
    public typeRg: number
  ) {}
}
