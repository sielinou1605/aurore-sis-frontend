export class RemiseNiveauRequestModel {
  constructor(
    public anneeAcademique: number,
    public echeanceApplication: string,
    public id: number,
    public idNiveau: number,
    public idRemiseGlobale: number,
    public idService: number,
    public idSource: number,
    public matricule: string,
    public modaliteApplication: number,
    public montant: number,
    public nomRemise: string,
    public sourceRemise: number,
    public typeRemise: number
  ) {
  }
}
