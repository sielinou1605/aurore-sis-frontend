export class PrimeRequestModel {
  constructor(
    public dateTime: string,
    public etatPaiement: boolean,
    public id: number,
    public idTypePrime: number,
    public montant: number,
    public nomPrime: string
  ) {}
}
