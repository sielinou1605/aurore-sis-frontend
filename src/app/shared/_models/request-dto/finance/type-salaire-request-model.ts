export class TypeSalaireRequestModel {
  constructor(
    public code: string,
    public description: string,
    public id: number,
    public nom_type: string
  ) {}
}
