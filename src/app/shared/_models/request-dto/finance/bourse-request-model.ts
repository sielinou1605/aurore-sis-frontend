export class BourseRequestModel {
  constructor(
    public id: number,
    public anneeAcademique: number,
    public intituleBourse: string,
    public modaliteApplication: number,
    public montantBourse: number,
    public motifBourse: string,
    public idService: number
  ) {}
}
