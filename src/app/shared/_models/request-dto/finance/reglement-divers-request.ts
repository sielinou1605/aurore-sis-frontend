export class ReglementDiversRequest {
  constructor(
  public codeAnnee: number,
  public idCaisse: number,
  public idModeReglement: number,
  public montant: number,
  public motifReglement: string,
  public nomPartieVersante: string,
  public numeroCni: string
  ) {
  }
}
