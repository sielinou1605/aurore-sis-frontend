export class AttributionBourseRequestModel {
  constructor(
    public anneeAcademique: number,
    public dateAllocation: string,
    public id: number,
    public idBourse: number,
    public intituleBourse: string,
    public matricule: string,
    public montantBourse: number
  ) {}
}
