export class AddFicheProgressionModel {
  constructor(
    public autorisation_remplissage: boolean,
    public id_ficheprogression: number,
    public matiereclasse_id: number,
    public nbre_heures_faites: number,
    public nbre_heures_prevues: number,
    public nbre_lecons_faites: number,
    public nbre_lecons_prevues: number,
    public nom_ficheprogression: string,
    public sequence_id: number,
    public enseignantSelect: any,
  ) {
  }
}
