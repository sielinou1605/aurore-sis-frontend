export class EleveNoteModel {
  constructor(
    public matricule: string,
    public note: number,
    public observation: string
  ) {
  }
}

export class RemplirNoteModel {
  constructor(
    public id_matiereclasse: number,
    public id_sequence: number,
    public  eleveNotes: EleveNoteModel[]
  ) {}
}

export class ModifierNoteModel {
  constructor(
    public id_matiereclasse: number,
    public id_note: number,
    public id_sequence: number,
    public matricule: string,
    public newNote: number,
    public observation: string
  ) {}
}

export class FicheProgressionModel {
  constructor(
    public date_fermeture: string,
    public date_ouverture: string,
    public en_cours: boolean,
    public id_annee: number,
    public id_sequence: number,
    public id_trimestre: number,
    public nom_sequence: string,
    public nom_sequence_2: string,
    public numero_ordre: number
  ) {
  }
}

// tslint:disable-next-line:class-name
export class attribuerNoteRequestDto {
  constructor(
    public idClasse: number,
    public idMatiereClasse: number,
    public idMatiereDest: number,
    public idMatiereSrc: number,
    public idSequence: number,
  ) {
  }
}
