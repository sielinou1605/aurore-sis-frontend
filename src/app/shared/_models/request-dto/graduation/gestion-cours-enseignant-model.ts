export class EditMatiereClasseEnseignant {
  constructor(
    public annee_id: number,
    public bareme_evaluation: number,
    public classe_id: number,
    public coefficient: number,
    public enseignant_id: number,
    public id_matiereclasse: number,
    public matiere_id: number,
    public quota_horaire: number
  ) {}
}

export class DisciplineRequestModel {
  constructor(
    public departement_id: number,
    public id_discipline: number,
    public nom_discipline: string
  ) {}
}

export class GroupeMatiereRequestModel {
  constructor(
    public id_groupematiere: number,
    public nom_groupematiere: string,
    public nom_groupematiere_2: string,
    public numero_ordre: number,
    public specialite: string
  ) {}
}

export class MatiereRequestModel {
  constructor(
    public code_matiere: string,
    public discipline_id: number,
    public groupematiere_id: number,
    public id_matiere: number,
    public nom_matiere: string,
    public nom_matiere_2: string,
    public ordre_matiere: number
  ) {}
}

export class EditerEnseignantModel {
  constructor(
 public anciennete: number,
 public credit: number,
 public date_affectation: string,
 public decision_affectation: string,
 public email: string,
 public fonction: string,
 public idDepartement: number,
 public idInstitution: number,
 public id_enseignant: number,
 public matricule: string,
 public montant_heure: number,
 public nom_enseignant: string,
 public photo: string,
 public prenom_enseignant: string,
 public provenance: string,
 public salaire_base: number,
 public sexe: string,
 public statut_matrimonial: string,
 public telephone_1: string,
 public telephone_2: string,
 public type_personnel: string
  ) {}
}
