export class AjoutTypePenaliteModel {
  constructor(
    public description: string,
    public libelle: string,
    public gravite: number
  ) {
  }
}

export class ModifierTypePenaliteModel {
  constructor(
    public description: string,
    public libelle: string,
    public gravite: number,
    public id: number
  ) {
  }
}
