export class AbsencesARemplir {
  constructor(
    public id_absence: number,
    public matricule: string,
    public nbreHeure: number
  ) {}
}

export class RemplirAbsenceModel {
  constructor(
    public id_classe: number,
    public id_sequence: number,
    public absencesARemplir: AbsencesARemplir[]
  ) {
  }
}

export class JustifierAbsenceModel {
  constructor(
    public id_sequence: number,
    public matricule: string,
    public absencesAJustifier: AbsencesARemplir[]
  ) {
  }
}

export class AjouterSanctionModel {
  constructor(
    public date_debut_penalite: string,
    public date_fin_penalite: string,
    public date_penalite: string,
    public id_sequence: number,
    public matricule: string,
    public motif_sanction: string,
    public realisation_sanction: boolean,
    public temoin_faute: string,
    public libelle_faute: string,
    public id_type_penalite: string,
    public id_classe: number,
    public penalite: string,
    public id_sanction?: number
  ) {
  }
}
