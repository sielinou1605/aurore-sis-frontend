
export class MatiereClasseRequest {
  constructor(
    public annee_id: number,
    public bareme_evaluation: number,
    public classe_id: number,
    public coefficient: number,
    public enseignant_id: number,
    public id_matiereclasse: number,
    public matiere_id: number,
    public quota_horaire: number
  ) {}
}

export class ProgrammationModelRequest {
  constructor(
    public heureDebut: string,
    public heureFin: string,
    public jourSemaine: string
  ) {
  }
}

export class AjouterMatiereClasseRequest {
  constructor(
    public matiereClasse: MatiereClasseRequest,
    public programmations: ProgrammationModelRequest[]
  ) {
  }
}

export class AjouterProgrammation {
  constructor(
    public idMatiereClasse: number,
    public programmations: ProgrammationModelRequest[]
  ) {
  }
}

export class ReconduireProgrammationModelRequest {
constructor(
    public idAnneeSource: number,
    public idEnseignantDestinataire: number,
    public idEnseignantSource: number
  ) {
  }
}
