export class AddArchiveModel  {
  constructor(
    public description: string,
    public matricule: string,
    public typeEntite: number,
    public idTypeArchivage: number,
    public idClasse: number,
    public idUtilisateur: number
  ) {
  }
}
export class AddArchiveModel2  {
  constructor(
    public description: string,
    public matricule: string,
    public typeEntite: number,
    public idTypeArchivage: number,
    public idArchivage: number,
    public idClasse: number,
    public idUtilisateur: number
  ) {
  }
}
