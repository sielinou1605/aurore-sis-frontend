export class SequenceRequestModel {
  constructor(
    public dateFermeture: string,
    public dateOuverture: string,
    public enCours: boolean,
    public id: number,
    public nomSequence: string,
    public nomSequence_2: string,
    public numeroOrdre: number
  ) {
  }
}
