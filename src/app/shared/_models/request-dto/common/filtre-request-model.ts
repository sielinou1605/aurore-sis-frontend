export class FiltreRequestModel {
  constructor(
    public codeAnnee: number,
    public dateAdmissionDebut: string,
    public dateAdmissionFin: string,
    public predicat: string,
    public nomEntite: string,
    public idEntite: number,
    public type: string
  ) {}
}
