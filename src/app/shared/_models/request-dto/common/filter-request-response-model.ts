export class ColumnFilter {
  constructor(
    public dataType?: string,
    public entity?: string,
    public id?: number,
    public  label?: string,
    public nameColumn?: string,
    public numOrder?: string,
    public ref?: boolean,
    public refKey?: string,
    public refTable?: string,
    public refValue?: string,
    public typeColumn?: string,
    public version?: number,
    public windowId?: number
  ) {
  }
}
export class FilterEntity {
  constructor(
    public column?: string,
    public columnType?: string,
    public internalOperator: string = 'AND',
    public operator: string = 'LIKE',
    public valueBegin?: string,
    public valueEnd?: string
  ) {}
}
export class FindEssentialEntity {
  constructor(
    public entity?: string,
    public filters?: FilterEntity[],
    public idSite: number = 0,
    public key?: string,
    public label?: string,
    public secondLabel?: string,
    public keyAlias?: string,
    public labelAlias?: string,
  ) {}
}

export class DataSelect {
  constructor(
    public cle: string,
    public valeur: string
  ) {
  }
}
