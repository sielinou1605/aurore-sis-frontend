export class ParamModel {
  constructor(
    public texte: string,
    public valeur: string
  ) {}
}

export class MessageModelRequest {
  constructor(
    public idSysMessageDataSource: number,
    public listeNumTelDestinataire: string,
    public modeleMessage: string,
    public titre: string,
    public params?: ParamModel[]
  ) {}
}

export class ModelEnvoieMessageRequest {
  constructor(
    public idsMessage:any[],
    public idCommunication? : number,
    public listeNumTelDestinataire? : String,
    public message? : String
  ) {}

}

export class ModelCampagneResponse {
  constructor(
    public id: number,
    public label: string,
    public modeleMessage: string,
    public sqlQuery
  ) {}
}

export class CommunicationModelResponse {
  constructor(
    public dateEnvoi: string,
    public id: number,
    public titre: string,
    public modeleMessage: string,
    public labelModele: string,
    public listeNumTelDestinataire: string,
    public urlApi: string,
    public sqlQuery: string,
    public auteur: string,
    public nbreMsgTransmis: number
  ) {}
}
