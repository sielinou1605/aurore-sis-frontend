export class NiveauRequestModel {
  constructor(
  public cycle_id: number,
  public id_niveau: number,
  public nom_niveau: string,
  public numero_ordre: number
  ) {
  }
}
