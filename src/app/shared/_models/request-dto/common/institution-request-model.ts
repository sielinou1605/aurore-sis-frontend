export class InstitutionRequestModel {
  constructor(
  public arrondissement: string,
  public boitePostale: string,
  public code_institution: string,
  public delegation: string,
  public delegationDepEn: string,
  public delegationDepFr: string,
  public delegationEn: string,
  public departement: string,
  public deviseInstitution: string,
  public deviseInstitutionEn: string,
  public email: string,
  public id: number,
  public logoInstitution: string,
  public nomInstitution: string,
  public nomInstitutionEn: string,
  public raisonSociale: string,
  public region: string,
  public siteWeb: string,
  public telephone1: string,
  public telephone2: string,
  public ville: string
  ) {
  }
}
