export class ClasseRequestModel {
  constructor(
  public id: number,
  public idTitulaire: number,
  public nomClasse: string,
  public optionClasse: string,
  public serie: string,
  public specialite: string
  ) {
  }
}
