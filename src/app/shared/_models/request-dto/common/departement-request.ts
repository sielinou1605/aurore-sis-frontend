export class DepartementRequest {
  constructor(
    public etablissement_id: number,
    public id_departement: number,
    public nomDepartement: string,
    public specialite: string,
    public ap: number
  ) {}
}
