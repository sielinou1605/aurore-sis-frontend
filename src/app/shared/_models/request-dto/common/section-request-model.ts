export class SectionRequestModel {
  constructor(
    public etablissement_id: number,
    public id_section: number,
    public langue: string,
    public nom_section: string
  ) {
  }
}
