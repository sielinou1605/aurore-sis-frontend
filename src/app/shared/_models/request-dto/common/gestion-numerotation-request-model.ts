export class EditerNumerotation {
  constructor(
    public id: 0,
    public numeroIndexMatricule: 0,
    public numeroIndexReglementDivers: 0,
    public numeroIndexReglementEleve: 0,
    public soucheNumeroMatricule: string,
    public soucheNumeroReglementDivers: string,
    public soucheNumeroReglementEleve: string
  ) {
  }
}

export class NumerotationResponseModel {
  constructor(
    public version: number,
    public date_creation: string,
    public date_modif: string,
    public util_creation: string,
    public util_modif: string,
    public forwarded: string,
    public id: number,
    public soucheNumeroMatricule: string,
    public soucheNumeroReglementEleve: string,
    public soucheNumeroReglementDivers: string,
    public numeroIndexMatricule: number,
    public numeroIndexReglementEleve: number,
    public numeroIndexReglementDivers: number
  ) {}
}
