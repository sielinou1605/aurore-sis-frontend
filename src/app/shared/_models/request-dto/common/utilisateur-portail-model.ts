export class UtilisateurPortailRequestDto {
    constructor(
        public id: number,
        public identifiantPortail: string,
        public email: string,
        public nomParent: string,
        public matricules: any[]
    ) {
    }
}