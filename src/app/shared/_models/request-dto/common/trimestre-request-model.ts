export class TrimestreRequestModel {
  constructor(
    public date_fermeture: string,
    public date_ouverture: string,
    public en_cours: boolean,
    public id_annee: number,
    public id_trimestre: number,
    public nom_trimestre: string,
    public nom_trimestre_2: string,
    public numero_ordre: number
  ) {
  }
}
