export class EtatImprimableModel {
  constructor(
    public chemin: string,
    public description: string,
    public exportable: number,
    public groupe: string,
    public libelle: string,
    public type: number
  ) {}
}

export class ParamEtat {
  constructor(
    public texte: string,
    public valeur: string
  ) {}
}

export class Filtre {
  constructor(
    public id: number,
    public description?: string,
    public label?: string,
    public labelFinPlage?: string,
    public libelle?: string,
    public nomParametreEtat?: string,
    public nomParametreEtatFinPlage?: string,
    public selectionEnPlage?: number,
    public sourceDeDonnees?: string,
    public type?: number,
    public typeSelection?: number
  ) {
  }
}

export class AjouterEtat {
  constructor(
    public idFenetre: number,
    public etatImprimables: EtatImprimableModel[]
  ) {}
}
export class ModifierEtatModel {
  constructor(
    public chemin: string,
    public description: string,
    public exportable: number,
    public groupe: string,
    public id: number,
    public idFenetre: number,
    public libelle: string,
    public type: number
  ) {}
}
export class ParamImpressionModel {
  constructor(
    public exporter: boolean,
    public idEtat: number,
    public paramEtats: ParamEtat[]
  ) {
  }
}

export class AjouterFiltreEtatModel {
  constructor(
    public idEtat: number,
    public filtres: Filtre[]
  ) {
  }
}

export const FACTUREACCOMPTE = 53;
export const FACTURELISTEACCOMPTE = 52;
