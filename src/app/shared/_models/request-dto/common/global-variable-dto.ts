export class GlobalVariableDto {
  constructor(
    public id: number,
    public valeur: string,
    public description: string
  ) {}
}
