export class EtablissementRequestModel {
  constructor(
    public code_etablissement: string,
    public decret_creation: string,
    public email: string,
    public id_etablissement: number,
    public institution_id: string,
    public nom_etablissement: string,
    public telephone_1: string,
    public telephone_2: string
  ) {
  }
}
