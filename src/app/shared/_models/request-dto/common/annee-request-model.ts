export class AnneeRequestModel {
  constructor(
  public codeAnnee: string,
  public dateFermeture: string,
  public dateOuverture: string,
  public enCours: boolean,
  public id: number
  ) {
  }
}
