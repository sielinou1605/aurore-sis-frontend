export class CycleRequestModel {
  constructor(
    public id_cycle: number,
    public nom_cycle: string,
    public numero_ordre: number,
    public section_id: number
  ) {
  }
}
