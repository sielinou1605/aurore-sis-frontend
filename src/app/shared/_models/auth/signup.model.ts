export class SignupModel {
  constructor(
    public displayName: string,
    public email: string,
    public password: string,
    public  matchingPassword: string,
    public socialProvider: 'LOCAL',
    public using2FA: string,
  ) {
  }
}
