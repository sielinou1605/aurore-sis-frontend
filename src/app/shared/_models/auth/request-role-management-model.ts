import {Caisse, RoleModel} from './add-user-request';

export class AjouterCaisseModel {
  constructor(
    public caisses: Caisse[],
    public idUtilisateur: number
  ) {
  }
}

export class AjoutActionToRole {
  constructor(
    public idRole: number,
    public roleActions: RoleAction[]
  ) {
  }
}

export class AjouterCaisseToUser {
  constructor(
    public idUtilisateur: number,
    public caisses: Caisse[]
  ) {
  }
}

export class AjouterRoleToUser {
  constructor(
    public roles: RoleModel[],
    public idUtilisateur: number
  ) {
  }
}

export class RoleAction {
  constructor(
    public conditionActif: string,
    public conditionEditable: string,
    public conditionVisible: string,
    public idRoleAction: number
  ) {
  }
}
