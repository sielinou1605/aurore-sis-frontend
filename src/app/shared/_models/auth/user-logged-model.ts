export class RoleDto {
  constructor(
    public id: number,
    public name: string
  ) {
  }
}

export class UserLoggedModel {
  constructor(
    public id: number,
    public displayName: string,
    public email: string,
    public codeAnnee: number,
    public roles: RoleDto[]
  ) {
  }
}
