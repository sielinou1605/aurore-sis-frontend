export class AddUserRequest {
  constructor(
    public  displayName: string,
    public  email: string,
    public  password: string,
    public  username: string,
    public  telephone: boolean,
    public  roles?: Role[],
    public caisses?: Caisse[]
  ) {
  }
}

export class Role {
  constructor(
    public  id: number,
    public  description?: string,
    public  name?: string,
  ) {
  }
}

export class RoleModel {
  constructor(
    public  roleId: number,
    public  name?: string,
    public  description?: string,
  ) {
  }
}

export class Caisse {
  constructor(
    public  id: string,
    public  nomCaisse?: string,
    public  descriptionCaisse?: string,
  ) {
  }
}

export class UpdateUserRequestModel {
  constructor(
    public displayName: string,
    public email: string,
    public id: number,
    public password: string,
    public telephone: string,
    public username: string
  ) {}
}
