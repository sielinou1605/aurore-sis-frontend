import {MainMenuItems} from './main.menu.items';

export interface Menu {
  label: string;
  main: MainMenuItems[];
}
