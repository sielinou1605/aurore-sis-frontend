import {Injectable} from '@angular/core';
import {UserLoggedModel} from '../_models/auth/user-logged-model';
import {CaisseModelDto} from '../_models/response-dto/common/caisse-model';
import {HttpClient} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const CAISSE_KEY = 'auth-caisse';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor(private http: HttpClient, private translate: TranslateService) { }

  public signOut(): void {
    window.sessionStorage.clear();
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser(): UserLoggedModel {
    return JSON.parse(window.sessionStorage.getItem(USER_KEY));
  }

  public saveCaisse(caisse): void {
    window.sessionStorage.removeItem(CAISSE_KEY);
    window.sessionStorage.setItem(CAISSE_KEY, JSON.stringify(caisse));
  }

  public getCaisse(): CaisseModelDto {
    return JSON.parse(window.sessionStorage.getItem(CAISSE_KEY));
  }

  public getActiveLang() {
    let lang = localStorage.getItem('activelang');
    if (!lang) {
      lang = this.translate.getBrowserLang();
      window.localStorage.setItem('activelang', lang);
    }
    return lang;
  }

  public saveGenericElement(key: string, value: any) {
    window.localStorage.removeItem(key);
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  public getGenericElement(key): any {
    return JSON.parse(window.localStorage.getItem(key));
  }

}
