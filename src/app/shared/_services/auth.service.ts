/* tslint:disable:triple-equals */
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, Observer} from 'rxjs';
import {AppConstants} from '../element/app.constants';
import {SigninModel} from '../_models/auth/signin.model';
import {SignupModel} from '../_models/auth/signup.model';
import {ActionResponse} from '../_models/response-dto/systeme/data-module-response-model';
import {TokenStorageService} from './token-storage.service';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public actions: Observable<ActionResponse[]>;
  private localActions: ActionResponse[] = [];
  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }

  public login(credentials): Observable<any> {
    return this.http.post(`${AppConstants.AUTH_API}signin`,
      new SigninModel(credentials.username, credentials.password), httpOptions);
  }

  public register(user) {
    return this.http.post(`${AppConstants.AUTH_API}signup`,
      new SignupModel(user.displayName, user.email, user.password, user.matchingPassword, 'LOCAL', user.using2FA),
      httpOptions);
  }

  verify(credentials): Observable<any> {
    return this.http.post(`${AppConstants.AUTH_API}verify`, credentials.code,
      {headers: new HttpHeaders({'Content-Type': 'text/plain'})});
  }

  public listMyActions(roleId: number, fenetreId) {
    return this.http.get(`${AppConstants.API_URL}systeme/role/${roleId}/fenetre/${fenetreId}/action/liste`).toPromise();
  }

  public returnListAction(idFenetre: number) {
    console.log(idFenetre);
    const acts = [];
    const rolesToCheck = this.tokenStorage.getUser().roles.filter(item => item.id != 1 && item.id != 2);
    if (rolesToCheck.length > 0) {
      rolesToCheck.forEach(item => {
        this.listMyActions(item.id, idFenetre).then((response: any) => {
          if (response.success == true) {
            response.data.forEach(action => {
              acts.push(action);
            });
          }
        }).catch(reason => console.log(reason));
      });
      this.localActions = acts;
    }
    this.actions = new Observable((observer: Observer<ActionResponse[]>) => {
      observer.next(acts);
    });
  }

  isActif(id: string): boolean {
    if (this.tokenStorage.getUser()) {
      const btns = this.localActions.length > 0 ? this.localActions.filter(item => item.idObjet == id) : [];
      const validation = btns.some(btnItem => btnItem.conditionActif == 'true');
      return (this.tokenStorage.getUser().roles.some(item => item.id == 1) || validation);
    }
  }
  isVisible(id: string): boolean {
    if (this.tokenStorage.getUser()) {
      const btns = this.localActions.length > 0 ? this.localActions.filter(item => item.idObjet == id) : [];
      const validation = btns.some(btnItem => btnItem.conditionVisible == 'true');
      return (this.tokenStorage.getUser().roles.some(item => item.id == 1) || validation);
    }
  }

  isEditable(id: string): boolean {
    if (this.tokenStorage.getUser()) {
      const btns = this.localActions.length > 0 ? this.localActions.filter(item => item.idObjet == id) : [];
      const validation = btns.some(btnItem => btnItem.conditionEditable == 'true');
      // const btn = this.localActions.find(item => item.idObjet == id);
      return (this.tokenStorage.getUser().roles.some(item => item.id == 1) || validation);
    }
  }
  // permet de savoir si la donnée a modifier est une donnée de l'année cloturée ou pas
  canEditData(selectYear: any) {
    return this.tokenStorage.getUser().codeAnnee == selectYear;
  }

  isAdmin() {
    return this.tokenStorage.getUser().roles.some(item => item.id == 1);
  }
}
