/* tslint:disable:triple-equals */
import {Injectable, SecurityContext} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Location} from '@angular/common';
import {AppConstants} from '../element/app.constants';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import * as FileSaver from 'file-saver';
import {Observable} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';
import html2canvas from 'html2canvas';
import JsPDF from 'jspdf';

const EXCEL_EXTENSION = '.xlsx';
const OLD_EXCEL_EXTENSION = '.xls';
const PDF_EXTENSION = '.pdf';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const PDF_TYPE = 'application/pdf';
const OTHER_TYPE = 'application/octet-stream';

@Injectable({
  providedIn: 'root'
})
export class GenericsService {

  constructor(private http: HttpClient, private location: Location,
              protected sanitizer: DomSanitizer) { }
  public getResource(url) {
    console.log(AppConstants.BASE_URL);
    return this.http.get(`${AppConstants.API_URL + url}`).toPromise();
  }

  public getRawResource(url) {
    return this.http.get(`${url}`).toPromise();
  }

  public getTextResource(url) {
    return this.http.get(`${AppConstants.API_URL + url}` , {responseType: 'text'}).toPromise();
  }
  public postTextResource(url, body) {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.http.post(`${AppConstants.API_URL + url}` , body, {headers: headers}).toPromise();
  }
  public postResource(url, dto) {
    return this.http.post(`${AppConstants.API_URL + url}`, dto).toPromise();
  }
  public putResource(url, dto) {
    return this.http.put(`${AppConstants.API_URL + url}`, dto).toPromise();
  }
  public patchResource(url, dto) {
    return this.http.patch(`${AppConstants.API_URL + url}`, dto).toPromise();
  }
  public deleteResource(url: string) {
    return this.http.delete(`${AppConstants.API_URL + url}`).toPromise();
  }
  onBackClicked() {
    this.location.back();
  }
  onForwardClicked() {
    this.location.forward();
  }
  public incrementCodeAnnee(code: string) {
    return parseInt(code, 0) + 1;
  }

  public confirmResponseAPI(title: string, icon: string, customTimer?: number) {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: customTimer ? customTimer : 5000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      }
    });
    Toast.fire({
      icon: icon,
      title: title,
    });
  }

  public confirmModalResponseAPI(message: string, title: string, icon: string) {
    Swal.fire(title, message, icon);
  }
  public reportPostResource(path: string) {
    return this.http.get(AppConstants.API_URL + path, {
      observe: 'response',
      responseType: 'arraybuffer' as 'json'
    }).toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  public reportPostPostResource(path: string, filter: any) {
    return this.http.post(AppConstants.API_URL + path,  filter, {
      observe: 'response',
      responseType: 'arraybuffer' as 'json'
    }).toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  uploadFile( path: string, formData: FormData) {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'multipart/form-data');

    return this.http.post(AppConstants.API_URL + path, formData, { headers });
  }

  public getByteArrayAndSaveReportPDF(value: any, filename: string, isExport?: boolean) {
    console.log(isExport);
    const file = new Blob([value], {type: (isExport && isExport == true) ? 'application/xls' : PDF_TYPE});
    // la ligne suivante telecharge et enregistre directement en pdf dans le repertoire telechargement
    if (isExport && isExport == true) {
      FileSaver.saveAs(file, filename + new Date().getTime() + OLD_EXCEL_EXTENSION);
    } else {
      // @ts-ignore
      const isFirefox = typeof InstallTrigger !== 'undefined';
      if (isFirefox) {
        FileSaver.saveAs(file, filename + new Date().getTime() + PDF_EXTENSION);
      } else {
        const blobUrl = URL.createObjectURL(file);
        const iframe = document.createElement('iframe');
        iframe.style.display = 'none';
        // Sans DOMSanitizer cela peut bien marcher mais le navigateur ne reconnait pas ce binaire comme une ressource sûre.
        // iframe.src = blobUrl;
        // DOMSanitizer permet d'en faire une ressource sûre.
        iframe.src = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl(blobUrl));
        document.body.appendChild(iframe);
        iframe.contentWindow.print();
      }
    }
  }

  public getByteArrayAndSaveReportPDFNote(value: any, filename: string, isExport?: boolean) {
    console.log(isExport);
    const file = new Blob([value], {type: (isExport && isExport == true) ? 'application/xls' : PDF_TYPE});
    // la ligne suivante telecharge et enregistre directement en pdf dans le repertoire telechargement
    if (isExport && isExport == true) {
      FileSaver.saveAs(file, filename + OLD_EXCEL_EXTENSION);
    } else {
      // @ts-ignore
      const isFirefox = typeof InstallTrigger !== 'undefined';
      if (isFirefox) {
        FileSaver.saveAs(file, filename + new Date().getTime() + PDF_EXTENSION);
      } else {
        const blobUrl = URL.createObjectURL(file);
        const iframe = document.createElement('iframe');
        iframe.style.display = 'none';
        // Sans DOMSanitizer cela peut bien marcher mais le navigateur ne reconnait pas ce binaire comme une ressource sûre.
        // iframe.src = blobUrl;
        // DOMSanitizer permet d'en faire une ressource sûre.
        iframe.src = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl(blobUrl));
        document.body.appendChild(iframe);
        iframe.contentWindow.print();
      }
    }
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Promise.reject(errMsg);
  }

  private extractData(res: HttpResponse<any>) {
    let body;
    if (res) {
      body = res.body;
    }
    return body;
  }

  public printReport(url: string, nameFile: string) {
    this.startLoadingPage();
    this.reportPostResource(url).then((result: any) => {
      this.getByteArrayAndSaveReportPDF(result, nameFile);
      this.stopLoadingPage();
    }).catch(reason => {
      this.stopLoadingPage();
    });
  }

  public printPostReport(url, data, nameFile) {
    this.reportPostPostResource(url, data).then((result: any) => {
      this.getByteArrayAndSaveReportPDF(result, nameFile);
    }).catch(reason => {
      this.stopLoadingPage();
    });
  }

  public uploadImage(file: File, url: string): Observable<HttpEvent<{}>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    const req = new HttpRequest('POST', `${AppConstants.API_URL + url}`, formData,
      { reportProgress: true, responseType: 'text' });
    return this.http.request(req);
  }

  public startLoadingPage(texte?: string ) {
    localStorage.getItem('activelang') === 'en' ?
      Swal.fire({
        title: texte ? texte : AppConstants.WAITINGEN,
        allowEscapeKey: false,
        allowOutsideClick: false,
        background: '#eeede8',
        showConfirmButton: false,
        onOpen: () => {
          Swal.showLoading();
        }
      }) : Swal.fire({
        title: texte ? texte : AppConstants.WAITINGFR,
        allowEscapeKey: false,
        allowOutsideClick: false,
        background: '#eeede8',
        showConfirmButton: false,
        onOpen: () => {
          Swal.showLoading();
        }
      });
  }

  public stopLoadingPage() {
    Swal.close();
    // Swal.hideLoading();
  }

  public strRandom(o) {
    const b = 'abcdefghijklmnopqrstuvwxyz';
    let a = 10, c = '', d = 0, e = '' + b;
    if (o) {
      if (o.startsWithLowerCase) {
        c = b[Math.floor(Math.random() * b.length)];
        d = 1;
      }
      if (o.length) {
        a = o.length;
      }
      if (o.includeUpperCase) {
        e += b.toUpperCase();
      }
      if (o.includeNumbers) {
        e += '1234567890';
      }
    }
    for (; d < a; d++) {
      c += e[Math.floor(Math.random() * e.length)];
    }
    return c;
  }

  public traiterDisplayName(displayName: string) {
    const idx = displayName.indexOf(' ');
    if (idx > 0) {
      return displayName.slice(0, idx + 2);
    } else {
      return displayName;
    }
  }

  public generatePDF(idElement: string, filename: string) {
    this.startLoadingPage();
    const data = document.getElementById(idElement);
    html2canvas(data, { scale: 3}).then(canvas => {
      const contentDataURL = canvas.toDataURL('image/pdf', 1.0);
      const pdf = new JsPDF('l', 'cm', 'a4');
      pdf.addImage(contentDataURL, 'PNG', 0, 0,  29.7, 21.0);
      pdf.save(filename);
      this.stopLoadingPage();
    });
  }

  public cleanToken(token: string): string {
    return token.split('{').join('').split('}').join('').split('[').join('')
      .split(']').join('').split('^').join('').split('\\').join('').split('<').join('')
      .split('>').join('').split('\/').join('').split('|').join('').split('%').join('')
      .split('$').join('').split('#').join('').split('&').join('').split('(').join('')
      .split(')').join('').split('>').join('-').split('_').join('');
  }
}
