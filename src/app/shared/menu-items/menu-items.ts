/* tslint:disable:triple-equals */
import {Injectable} from '@angular/core';
import {Menu} from '../_models/menu/menu';

const WELCOMEITEMS = [
  {
    label: 'userAdminFinancesListRemiseNiveau.Menu_principal',
    main: [
      {
        state: 'welcome',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Accueil',
        type: 'link',
        icon: 'ti-home',
      },
      {
        state: 'user',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Profile',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'administration',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Administration',
        type: 'link',
        icon: 'ti-settings',
      },
      {
        state: 'finances',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Finances',
        type: 'link',
        icon: 'ti-bookmark-alt',
      },
      {
        state: 'eleves',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Eleves',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'gestion-notes',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Gestion_des_notes',
        type: 'link',
        icon: 'ti-marker-alt',
      },
      {
        state: 'discipline',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Discipline',
        type: 'link',
        icon: 'ti-write',
      },
      {
        state: 'etats',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Etats',
        type: 'link',
        icon: 'ti-file',
      },
      {
        state: 'help',
        short_label: 'D',
        name: 'userAdminFinancesListRemiseNiveau.Aide',
        type: 'link',
        icon: 'ti-help-alt'
      }
    ]
  }
];

const ADMINITEMS = [
  {
    label: 'userAdminFinancesListRemiseNiveau.Menu_principal',
    main: [
      {
        state: 'welcome',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Accueil',
        type: 'link',
        icon: 'ti-home',
      },
      {
        state: 'user',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Profile',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'administration',
        short_label: 'E',
        name: 'userAdminFinancesListRemiseNiveau.Administration',
        type: 'sub',
        icon: 'ti-settings',
        children: [
          {
            state: 'finances',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Finances',
            type: 'sub',
            icon: 'ti-settings',
            children: [
              {
                state: 'list-services',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Services',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'list-remise',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Remises',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'list-bourse',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Bourse',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'list-caisse',
                short_label: 'D',
                name: 'Caisses',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'list-mode-reglement',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Modes_reglement',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'list-modele-echeance',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Modeles_écheance',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'list-journal',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Journaux',
                type: 'link',
                icon: 'ti-home'
              },
            ]
          },
          {
            state: 'configuration',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Configuration',
            type: 'sub',
            icon: 'ti-settings',
            children: [
              {
                state: 'institutions',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Institutions',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'gestion-roles',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Gestion_des_roles',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'gestion-etats',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Gestion_des_etats',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'matieres',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Matieres',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'periodes',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Periodes',
                type: 'link',
                icon: 'ti-home'
              },
            ]
          },
          {
            state: 'utilisateurs',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Utilisateurs',
            type: 'link',
            icon: 'ti-home'
          },
          {
            state: 'discipline',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Discipline',
            type: 'sub',
            icon: 'ti-home',
            children: [
              {
                state: 'classes',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Classes',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'enseignants',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Enseignants',
                type: 'link',
                icon: 'ti-home'
              },
            ]
          },
          {
            state: 'notes',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Notes',
            type: 'sub',
            icon: 'ti-home',
            children: [
              {
                state: 'classes',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Classes',
                type: 'link',
                icon: 'ti-home'
              },
              {
                state: 'enseignants',
                short_label: 'D',
                name: 'userAdminFinancesListRemiseNiveau.Enseignants',
                type: 'link',
                icon: 'ti-home'
              },
            ]
          }
        ]
      }
    ]
  }
];
const FINANCESITEMS = [
  {
    label: 'userAdminFinancesListRemiseNiveau.Menu_principal',
    main: [
      {
        state: 'welcome',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Accueil',
        type: 'link',
        icon: 'ti-home',
      },
      {
        state: 'user',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Profile',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'finances',
        short_label: 'E',
        name: 'userAdminFinancesListRemiseNiveau.Finances',
        type: 'sub',
        icon: 'ti-home',
        children: [
          {
            state: 'liste-inscription',
            short_label: 'A',
            name: 'Reglement Eleve',
            type: 'link',
            icon: 'ti-user',
          },
          {
            state: 'reglements-divers',
            short_label: 'A',
            name: 'userAdminFinancesListRemiseNiveau.Reglement_divers',
            type: 'link',
            icon: 'ti-user',
          },
          {
            state: 'reglements-divers',
            short_label: 'A',
            name: 'userAdminFinancesListRemiseNiveau.Configuration',
            type: 'link',
            icon: 'ti-user',
          }
        ]
      }

    ]
  }
];
const ELEVEITEMS = [
  {
    label: 'userAdminFinancesListRemiseNiveau.Menu_principal',
    main: [
      {
        state: 'welcome',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Accueil',
        type: 'link',
        icon: 'ti-home',
      },
      {
        state: 'user',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Profile',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'eleves',
        short_label: 'E',
        name: 'userAdminFinancesListRemiseNiveau.Eleves',
        type: 'sub',
        icon: 'ti-money',
        children: [
          {
            state: 'ajout-eleve',
            short_label: 'A',
            name: 'userAdminFinancesListRemiseNiveau.Nouvel_élève',
            type: 'link',
            icon: 'ti-user',
          },
          {
            state: 'liste-eleve',
            short_label: 'E',
            name: 'userAdminFinancesListRemiseNiveau.Liste_eleves',
            type: 'link',
            icon: 'ti-clip',
            color: 'bg-dribble'
          },
          {
            state: 'liste-inscription',
            short_label: 'E',
            name: 'userAdminFinancesListRemiseNiveau.Liste_inscription',
            type: 'link',
            icon: 'ti-money',
            color: 'bg-c-blue'
          },
          {
            state: 'liste-eleve',
            short_label: 'E',
            name: 'userAdminFinancesListRemiseNiveau.Configuration',
            type: 'link',
            icon: 'ti-money',
            color: 'bg-c-blue'
          }
        ]
      }
    ]
  }
];
const ENSEIGNANTITEMS = [
  {
    label: 'userAdminFinancesListRemiseNiveau.Menu_principal',
    main: [
      {
        state: 'welcome',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Accueil',
        type: 'link',
        icon: 'ti-home',
      },
      {
        state: 'user',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Profile',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'enseignant',
        short_label: 'E',
        name: 'userAdminFinancesListRemiseNiveau.Enseignant',
        type: 'sub',
        icon: 'ti-medall-alt',
        children: [
          {
            state: 'consulter-classes',
            name: 'userAdminFinancesListRemiseNiveau.Consulter_mes_classes',
            short_label: 'EL',
            type: 'link',
            icon: 'ti-user'
          },
          {
            state: 'remplir-notes',
            name: 'userAdminFinancesListRemiseNiveau.Saisie_de_notes',
            short_label: 'ES',
            type: 'link',
            icon: 'ti-user'
          },
          {
            state: 'modifier-notes',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Modification_de_note',
            type: 'link',
            icon: 'ti-home'
          },
          {
            state: 'consulter-notes',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Consultation_de_notes',
            type: 'link',
            icon: 'ti-home'
          },
          {
            state: 'fiches-progression',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Fiches_de_progression',
            type: 'link',
            icon: 'ti-home'
          },
          {
            state: 'bulletins',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Mes_bulletins',
            type: 'link',
            icon: 'ti-home'
          },
          {
            state: 'help',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Aide',
            type: 'link',
            icon: 'ti-home'
          }
        ]
      }
    ]
  }
];
const CENSEURITEMS = [
  {
    label: 'userAdminFinancesListRemiseNiveau.Menu_principal',
    main: [
      {
        state: 'welcome',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Accueil',
        type: 'link',
        icon: 'ti-home',
      },
      {
        state: 'user',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Profile',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'discipline',
        short_label: 'E',
        name: 'userAdminFinancesListRemiseNiveau.Discipline',
        type: 'sub',
        icon: 'ti-calendar',
        children: [
          {
            state: 'remplir-absences',
            name: 'userAdminFinancesListRemiseNiveau.Saisie_des_absences',
            short_label: 'EL',
            type: 'link',
            icon: 'ti-user'
          },
          {
            state: 'enregistrer-sanctions',
            name: 'userAdminFinancesListRemiseNiveau.Saisie_des_sanctions',
            short_label: 'ES',
            type: 'link',
            icon: 'ti-user'
          },
          {
            state: 'remplir-absences',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Justification_absence',
            type: 'link',
            icon: 'ti-home'
          },
          {
            state: 'proces-verbaux',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Procès_verbaux',
            type: 'link',
            icon: 'ti-home'
          },
          {
            state: 'help',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Configuration',
            type: 'link',
            icon: 'ti-home'
          }
        ]
      }
    ]
  }
];
const ETATSITEMS = [
  {
    label: 'userAdminFinancesListRemiseNiveau.Menu_principal',
    main: [
      {
        state: 'welcome',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Accueil',
        type: 'link',
        icon: 'ti-home',
      },
      {
        state: 'user',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Profile',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'etats',
        short_label: 'E',
        name: 'userAdminFinancesListRemiseNiveau.Etats',
        type: 'sub',
        icon: 'ti-printer',
        children: [
          {
            state: 'list-etats',
            name: 'userAdminFinancesListRemiseNiveau.Etats',
            short_label: 'EL',
            type: 'link',
            icon: 'ti-user'
          },
          {
            state: 'help',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Configuration',
            type: 'link',
            icon: 'ti-home'
          }
        ]
      }
    ]
  }
];

const MESSAGESITEMS =  [
  {
    label: 'userAdminFinancesListRemiseNiveau.Menu_principal',
    main: [
      {
        state: 'welcome',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Accueil',
        type: 'link',
        icon: 'ti-home',
      },
      {
        state: 'user',
        short_label: 'A',
        name: 'userAdminFinancesListRemiseNiveau.Profile',
        type: 'link',
        icon: 'ti-user',
      },
      {
        state: 'messages',
        short_label: 'E',
        name: 'userAdminFinancesListRemiseNiveau.Communication',
        type: 'sub',
        icon: 'ti-comments',
        children: [
          {
            state: 'list-messages',
            name: 'userAdminFinancesListRemiseNiveau.Communication',
            short_label: 'EL',
            type: 'link',
            icon: 'ti-user'
          },
          {
            state: 'help',
            short_label: 'D',
            name: 'userAdminFinancesListRemiseNiveau.Configuration',
            type: 'link',
            icon: 'ti-home'
          }
        ]
      }
    ]
  }
];

@Injectable()
export class MenuItems {
  getAll(currentRole: string): Menu[] {
    if (currentRole == 'finances') {
      return FINANCESITEMS;
    }
    if (currentRole == 'enseignant') {
      return ENSEIGNANTITEMS;
    }
    if (currentRole == 'censeur') {
      return CENSEURITEMS;
    }
    if (currentRole == 'admin') {
      return ADMINITEMS;
    }
    if (currentRole == 'eleve') {
      return ELEVEITEMS;
    }
    if (currentRole == 'etat') {
      return ETATSITEMS;
    }
    if (currentRole == 'messages') {
      return MESSAGESITEMS;
    }
    return WELCOMEITEMS;
  }
}
