import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AccordionAnchorDirective, AccordionDirective, AccordionLinkDirective} from './accordion';
import {ToggleFullScreenDirective} from './fullscreen/toggle-fullscreen.directive';
import {CardRefreshDirective} from './card/card-refresh.directive';
import {CardToggleDirective} from './card/card-toggle.directive';
import {SpinnerComponent} from './spinner/spinner.component';
import {CardComponent} from './card/card.component';
import {ModalAnimationComponent} from './modal-animation/modal-animation.component';
import {ModalBasicComponent} from './modal-basic/modal-basic.component';
import {DataFilterPipe} from './element/data-filter.pipe';
import {MenuItems} from './menu-items/menu-items';
import {ParentRemoveDirective} from './element/parent-remove.directive';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ClickOutsideModule} from 'ng-click-outside';
import {BreadcrumbsComponent} from '../layout/admin/breadcrumbs/breadcrumbs.component';
import {RouterModule} from '@angular/router';
import {DateDirective} from './_helpers/date.directive';
import {DynamicFilterComponent} from './partials/dynamic-filter/dynamic-filter.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NumberPhonePipe} from './_helpers/number-phone-pipe';
import {CountDownComponent} from './count-down/count-down.component';
import {WebcamSnapshotComponent} from './webcam-snapshot/webcam-snapshot.component';
import {GenerationCisComponent} from './generation-cis/generation-cis.component';
import {SaisieCasComponent} from './saisie-cas/saisie-cas.component';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {AvatarModule} from 'ngx-avatar';

import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ImportExportComponent} from './import-export/import-export.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    ClickOutsideModule,
    NgbModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    AvatarModule
  ],
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ToggleFullScreenDirective,
    CardRefreshDirective,
    CardToggleDirective,
    SpinnerComponent,
    CardComponent,
    ModalAnimationComponent,
    ModalBasicComponent,
    DataFilterPipe,
    ParentRemoveDirective,
    BreadcrumbsComponent,
    DateDirective,
    DynamicFilterComponent,
    NumberPhonePipe,
    CountDownComponent,
    WebcamSnapshotComponent,
    GenerationCisComponent,
    SaisieCasComponent,
    ImportExportComponent
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ToggleFullScreenDirective,
    CardRefreshDirective,
    CardToggleDirective,
    SpinnerComponent,
    CardComponent,
    ModalAnimationComponent,
    ModalBasicComponent,
    DataFilterPipe,
    ParentRemoveDirective,
    NgbModule,
    PerfectScrollbarModule,
    ClickOutsideModule,
    BreadcrumbsComponent,
    DateDirective,
    DynamicFilterComponent,
    NumberPhonePipe,
    CountDownComponent,
    WebcamSnapshotComponent,
    GenerationCisComponent,
    SaisieCasComponent,
    TranslateModule,
    ImportExportComponent
  ],
  providers: [
    MenuItems,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class SharedModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
