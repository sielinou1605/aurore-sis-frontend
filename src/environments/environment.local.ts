export const environment = {
  production: false,
  local: true,
  live: false,
  union: false,
  dev: false,
  logo: 1,
  api_base_url: 'http://localhost:8096/',
  host: 'http://localhost:'
};
